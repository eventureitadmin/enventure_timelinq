<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	$checkedn ="checked";
	$checkedy ="";
	$mdgcheckedn ="checked";
	$mdgcheckedy ="";
	if(isset($_GET['id'])){
		$pirmasterres = $dbase->executeQuery("SELECT p.*,(SELECT is_rework FROM pirlist WHERE id= p.pirno ) as is_rework,(SELECT is_internalpir FROM pirlist WHERE id= p.pirno ) as is_internalpir,(SELECT `clientname` FROM `clientlist` WHERE id=p.clientname)as cname FROM `pirmaster`as p WHERE p.id=".$_GET['id'],"single");
		$pirlist = $dbase->executeQuery("SELECT `contracttype` FROM `pirlist` WHERE id=".$pirmasterres['pirno'],"single");
	  
		if($pirmasterres != ""){
			$department_id = $pirmasterres['department_id'];
			$subdepartment_id = $pirmasterres['subdepartment_id'];
			$clientname = $pirmasterres['clientname'];
			$cname = $pirmasterres['cname'];
			$selpirno = $pirmasterres['pirno'];
			$selptrojadmin = $pirmasterres['projadminid'];
			$selpirnores = explode("/",$selpirno);
			$clientmappingres = $dbase->executeQuery("SELECT `is_partlinq_mapping`,`mdg_mapping` FROM `clientlist` WHERE `isActive` =1 AND `id`='".$clientname."'","single");
			$is_partlinq_mapping = $clientmappingres['is_partlinq_mapping'];
			if($is_partlinq_mapping ==1){
				$checkedy ="checked";
				$checkedn ="";
			}
			$mdg_mapping = $clientmappingres['mdg_mapping'];
			if($mdg_mapping ==1){
				$mdgcheckedy ="checked";
				$mdgcheckedn ="";
			}
			if($pirmasterres['pir_mappingid'] >0){
				$existingpirchecked ="checked";
				$renewalpirchecked ="";
			}else if($pirmasterres['pir_mappingid'] == "" || $pirmasterres['pir_mappingid'] == 0){
				$existingpirchecked ="checked";
				$renewalpirchecked ="";
			}
			
			$radiodisabled ="disabled";
			$is_rework = $pirmasterres['is_rework'];
			$is_internalpir = $pirmasterres['is_internalpir'];
			
		}
	}else{
		    $pirlist = "";
			$selpirno ="";
			$reworkchk="";
			$cname ="";
			$internalpirchk="";
			$existingpirchecked ="";
			$renewalpirchecked ="";
			$department_id ="";
			$subdepartment_id="";
			$clientname ="";
			//$pirchecked ="";
			$selpirno ="";
			$radiodisabled ="";
			$selptrojadmin ="";
			$is_rework="";
			$is_internalpir="";
	} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	  <link rel="stylesheet" href="css/chosen.css">
	  <link href="css/custom.css" rel="stylesheet">
	  <!---wizard---->
	  <link type="text/css" href="css/wizard/jquery.steps.css" rel="stylesheet" />
	  <link type="text/css" href="css/wizard/main.css" rel="stylesheet" />
	  <link type="text/css" href="css/wizard/normalize.css" rel="stylesheet" />
	   
   	  <!---wizard---->
      <link href="css/custom.css" rel="stylesheet">
	  <script type="text/javascript" src="js/jquery_validate.js"></script>
	  <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <script src="js/jquery.mask.min.js" type="text/javascript"></script>
	   <!---wizard---->
      <script type="text/javascript" src="js/wizard/jquery.steps.min.js"></script>
	    <!---wizard---->
	  <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	<!--   <link type="text/css" href="css/bootstrap.css" rel="stylesheet" />
	   	<script type="text/javascript" src="js/bootstrap.js"></script>-->
	 <style>
		table#clienttable,table#pirtable,table#pirmaptable,table#previewtblinfo,table.projactivity {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#clienttable td, table#clienttable th,table#pirtable td, table#pirtable th,table#pirmaptable td, table#pirmaptable th 
		 ,table#previewtblinfo td, table#previewtblinfo th,table.projactivity th,table.projactivity td{
			border: 1px solid black;
		}	
		 .wizard > .content{
			 height:30em;/*overflow-x:scroll;width:100% max-height:auto;*/
			 overflow:scroll;
		 }
		.wizard>.steps>ul>li:nth-child(1), .wizard>.steps>ul>li:nth-child(2) ,.wizard>.steps>ul>li:nth-child(3),.wizard>.steps>ul>li:nth-child(4){
   		 	width: 15%;
		}
		 .wizard > .steps a, .wizard > .steps a:hover, .wizard > .steps a:active{
			 padding:4px
		 }
		.chosen-container {
			width: 20% !important;
		}
		 .previewtbl{
		 	padding-left:10px
		 }
		 .projectname{
			 padding:5px
		 }
		
	  </style>
   </head>
   <body>
	<?php include("menu.php");?>
	<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
	   <form id="activityform" name="activityform" style="display:none">
		   <script>
		 
		   </script>
		   <table id="activitytable" >
			   <input type="hidden" name="activityeditid" id="activityeditid" value="0">
				<tr>
					<td height="40px" align="left" style="padding-left:10px" width="50%"><b>Department : </b></td>
					<input type="hidden" name="dept_id" id="dept_id" value="">
					<td id="dept_name"></td>
				</tr>
			   	<tr>
					<td height="40px" align="left" style="padding-left:10px" width="50%"><b>Sub Department : </b></td>
					<input type="hidden" name="subdept_id" id="subdept_id" value="">
					<td id="subdept_name"></td>
				</tr>
			   	<tr>
					<td height="40px" align="left" style="padding-left:10px" width="50%"><b>Activity : </b></td>
					<td><input type="text" name="sactivity" id="sactivity" value="" class="required alphanumeric"></td>
				</tr>
			   <tr>
					<td height="40px" align="left" style="padding-left:10px" width="50%"><b>Partlinq Mapping : </b></td>
					<td>
						<input type="radio" name="partlinqmapping" id="partlinqmapping" value="1"  >Yes
						<input type="radio" name="partlinqmapping" id="partlinqmapping" value="0"   checked>No
				   </td>
				</tr>
			    <tr>
					<td height="40px" align="left" class="entrytypediv" style="padding-left:10px;display:none" width="50%"><b>Entry Type : </b></td>
					<td class="entrytypediv" style="display:none">
						<select id="entrytypesel" name="entrytype"  style="width:200px">
							<option value="">-Select-</option>
							<?php
							$usertypes = $master->getusertypes();
							if(count($usertypes)>0){
								for($k=0;$k<count($usertypes);$k++){
								
									echo '<option value="'.$usertypes[$k]['type'].'" >'.$usertypes[$k]['name'].'</option>';
								}
							}
						?>
						</select>
				   </td>
				</tr>
			   
			   <tr>
					<td height="40px" align="left" style="padding-left:10px" width="50%"><b>MDG Mapping : </b></td>
					<td>
						<input type="radio" name="mdgmapping" id="mdgmapping" value="1"  >Yes
						<input type="radio" name="mdgmapping" id="mdgmapping" value="0"   checked>No
				   </td>
				</tr>
			    <tr>
					<td height="40px" align="left" class="mentrytypediv" style="padding-left:10px;display:none" width="50%"><b>MDG Entry Type : </b></td>
					<td class="mentrytypediv" style="display:none">
						<select id="mentrytypesel" name="mentrytype"  style="width:200px">
							<option value="">-Select-</option>
							<?php
							$mdgusertypes = $master->getmldemousertypes();
							if(count($mdgusertypes)>0){
								for($k=0;$k<count($mdgusertypes);$k++){
								
									echo '<option value="'.$mdgusertypes[$k]['type'].'" >'.$mdgusertypes[$k]['name'].'</option>';
								}
							}
						?>
						</select>
				   </td>
				</tr>
			</table>
	  </form>
	<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
		
		<tr>
			<td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
			<?php include("userrolemenu.php"); ?>
			</td>
			<td align="center" width="80%" valign="top">
				<form id="frm_projsetup" action="projsetup.php" method="POST">
					<input type="hidden" name="editid" id="editid" value="<?php echo $_GET['id'];?>">
					
				<div id="maindiv">
				<h4>Client</h4>
				<section class="secproj" >
					<div class="secdiv">
					<table id="clienttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
						
						<tr>
							<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department
								<span style="color:red"> *</span></b></td>
							<td height="40px" align="left" style="padding-left:10px">
							<select id="department_id" name="department_id" class="required chosen-select" onchange="getsubdepartment('<?php echo $subdepartment_id;?>');" 	
									style="width:170px">
							<option value="">-Select-</option>
							<?php
								$depart_cond = "";
							    if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
									$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
								}																								   
								$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
								$deptResult = $dbase->executeQuery($deptQuery,"multiple");
								for($i=0;$i<count($deptResult);$i++){
									if($deptResult[$i]['id']==$pirmasterres['department_id']){
										$select = "selected";
									}
									else{
										$select = "";
									}
									echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
								}
							?>
							</select>
							<span class="error chosenerr" id="depterror" style="display:none;color:red" >This field is required.</span>	
							</td>
						</tr>
						<tr>
							<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department
								<span style="color:red"> *</span></b></td>
							<td height="40px" align="left" style="padding-left:10px">
							<select id="subdepartment_id" name="subdepartment_id" class="required chosen-select"  onchange="getclientlist(<?php echo $clientname;?>);"  style="width:170px" >
							<option value="">-Select-</option>
							</select>
							<span class="error chosenerr" id="subdepterror" style="display:none;color:red" >This field is required.</span>	
							</td>
						</tr>
						<tr>
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client Type</b></td>
								<td height="40px" align="left" style="padding-left:10px">
									<input type="radio" name="clienttype" value="1" class="clienttype" style="display:inline-block" <?php echo $radiodisabled;?>>  New 
									<input type="radio" name="clienttype" class="clienttype " value="2" style="display:inline-block" checked > 
									Existing</td>
							</tr>
					   <tr id="newclient" style="display:none">
							<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client Name
								<span style="color:red">*</span></b></td>
							<td height="40px" align="left" style="padding-left:10px">
								<input type="text" name="client_name" id="client_name"  value="" style="width:180px;padding:6px">
							</td>
						</tr>
						<tr id="existclient">
							<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Client
								<span style="color:red"> *</span></b></td>
							<td height="40px" align="left" style="padding-left:10px" >
								<select id="client_id" name="client_id" class="required chosen-select" onchange="getPIRListbyClient();"
										style="width:180px;" >
								<option value="">-Select-</option>
								</select>
								<span class="error chosenerr" id="clienterror" style="display:none;color:red" >This field is required.</span>	
							</td>
						</tr>
						<tr>
							<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client Partlinq Mapping
								</b></td>
							<td height="40px" align="left" style="padding-left:10px" >
								<input type="radio" name="clientpmapping"   value="1" style="display:inline-block" <?php echo $checkedy;?>>Yes
								<input type="radio" name="clientpmapping"    value="0" style="display:inline-block" <?php echo $checkedn;?>>No
							</td>
						</tr>
						<tr>
							<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client MDG Mapping
								</b></td>
							<td height="40px" align="left" style="padding-left:10px" >
								<input type="radio" name="clientmdgmapping"   value="1" style="display:inline-block" <?php echo $mdgcheckedy;?>>Yes
								<input type="radio" name="clientmdgmapping"    value="0" style="display:inline-block" <?php echo $mdgcheckedn;?>>No
							</td>
						</tr>
					

								</table>	<!-------1-->
						</div>
						</section>
						<h4>PIR</h4>
						<section class="secproj" >
							<div class="secdiv">
							<input type="hidden" name="totrow" id="totrow" value="0">
						<table id="pirtable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
							<tr>
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>PIR Type</b></td>
								<td height="40px" align="left" style="padding-left:10px">
									
									<input type="radio" name="pirtype" value="1" class="pirtype" style="display:inline-block"  <?php echo $radiodisabled;?> <?php if($existingpirchecked =="" && $renewalpirchecked == ""){?>checked<?php } ?> >New 
									<input type="radio" name="pirtype" class="pirtype " value="2" style="display:inline-block"  <?php if($existingpirchecked !=""){?>checked<?php } ?>> 
									Existing
								<input type="radio" name="pirtype" class="pirtype " value="3" style="display:inline-block"  <?php if($renewalpirchecked !=""){?>checked<?php } ?>> 
									Renewal
								</td>
							</tr>
							<!----add PIR and Project-->
							<tr>
								<?php if($renewalpirchecked !=""){$renewaldiv = "display:inline-block";}else{$renewaldiv = "display:none";}?>
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>PIR No</b></td>
								<td height="40px" align="left" style="padding-left:10px"  style="display:none">
									
									<span class="renewal" style="<?php echo $renewaldiv;?>" ><b>New PIR</b></span><input type="text" style="width:170px;padding:5px;display:inline-block" name="pirno" 
									value="<?php echo $pir_result['pirno']; ?>"  class="newpir" onkeyup="getpirmask()"/>
									<span class="renewal" style="<?php echo $renewaldiv;?>"><b>Old PIR</b></span><select id="pirno1" name="pirno1" class="required chosen-select existpir" 
										style="width:180px;display:none;" onchange="loadtable();checkpirdata();loadprojadmin(<?php echo $selptrojadmin;?>);validatechosen('#pirtable')">
									<option value="">-Select-</option>
								
								</select>
									<span class="error chosenerr" id="pirerror" style="display:none;color:red" >This field is required.</span>	
									
								</td>
							</tr>
							<tr>
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Is rework</b></td>
								<td height="40px" align="left" style="padding-left:10px">
									<input type="radio" name="is_rework" value="1" class="is_rework" style="display:inline-block"  <?php if($is_rework =="1"){?>checked<?php } ?> >Yes 
									<input type="radio" name="is_rework" class="is_rework " value="0" style="display:inline-block"  <?php if($is_rework !="1"){?>checked<?php } ?>>No
									
								</td>
							
							</tr>
<tr>
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Is Internal PIR</b></td>
								<td height="40px" align="left" style="padding-left:10px">
									<input type="radio" name="is_internalpir" value="1" class="is_internalpir" style="display:inline-block"  <?php if($is_internalpir =="1"){?>checked<?php } ?> >Yes 
									<input type="radio" name="is_internalpir" class="is_internalpir " value="0" style="display:inline-block"  <?php if($is_internalpir !="1"){?>checked<?php } ?>>No
									
								</td>
							
							</tr>							
							<tr>
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Project Admin</b></td>
								<td height="40px" align="left" style="padding-left:10px">
								  <select id="padmin" name="padmin" class="required chosen-select" onchange="validatechosen('#pirtable')">
									<option value="">-Select-</option>
									  	
								</select>
									<span class="error chosenerr" id="padminerror" style="display:none;color:red" >This field is required.</span>	
								</td>
							</tr>
							<tr class="newpirdiv">
								<input type="hidden" name="is_change" id="is_change" value="">
								<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Contract Type</b></td>
								<td height="40px" align="left" style="padding-left:10px" > 
									<input class="radioBtn"  style="display:inline-block" type="radio" name="contracttype" value="0" <?php if($pirlist['contracttype']=='0'){ ?> checked <?php } ?> 
										   <?php if($id==''){ ?> checked <?php } ?>> T&M
									<input class="radioBtn" style="display:inline-block" type="radio" name="contracttype" value="1" <?php if($pirlist['contracttype']=='1'){ ?> checked <?php } ?>> 
									Fixed Price
									<input class="radioBtn" style="display:inline-block" type="radio" name="contracttype" value="2" <?php if($pirlist['contracttype']=='2'){ ?> checked <?php } ?>> 
									Dedicated Team
								</td>
							</tr>
						<tr class="newpirdiv">
							<td id="activitytr" style="" height="40px" align="left" 
								colspan="2" width="20%"></td>
						</tr>	
						<!-------------------->
					
						</table>
						</div>
						</section>
						<h4>PIR Mapping</h4>
						<section>
							<div  class="secdiv">
								<form name="projectform" id="projectform"  method="POST">
								<table id="pirmaptable" class="pirtbl" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
								</table>
								</form>
							</div>	
						</section>
						<h4>Preview Info</h4>
						<section>
							<div id="previewprojdiv" class="secdiv"></div>
						</section>
					
					</div>
					
				</form>
				
			</td>
		
		</tr>
	  </table>
<?php } ?>
</body>
 <script type="text/javascript" src="js/notify.js"></script>
<script type="text/javascript">
	var maximum_admin_entry = <?php echo MAXIMUM_ADMIN_ENTRY; ?>;
	var clientexists = true;
	var pirexists = true;
	var projexists = true;

		$(document).ready(function(){
			
			jQuery.validator.addMethod("alphanumericpir", function(value, element) {
				var selpirval = value.split("/");
				var selpir = "";
				if(selpirval.length>0){
					var selpir = selpirval[1];
				}
				return this.optional( element ) || /^[0-9]{4}[A-Z]{0,1}$/i.test(selpir);
			 //return true;
			}, 'Please enter only 4 numbers and 1 alphabet letter');	
			jQuery.validator.addMethod("alphanumeric", function(value, element) {
			  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
			}, 'Please enter only alphabets and numbers.');	
			jQuery.validator.addMethod("alphanumericspace", function(value, element) {
			  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
			}, 'Please enter only alphabets and numbers.');	
			jQuery.validator.addMethod("timeformat", function(value, element) {
			 // return this.optional( element ) || /^([1][0-2]|[0]?[0-9])([.:][0-5][0-9])?$/.test( value );
			  return this.optional( element ) || /^([0-9][0-9]|[0]?[0-9])([.:][0-5][0-9])?$/.test( value );
			}, 'Please enter only HH:MM');	
			jQuery.validator.addMethod("btimesec", function(value, element) {
			  return this.optional( element ) || /^[0-9\s]+$/i.test( value );
			}, 'Please enter budgettime in seconds');	
    // Setup - add a text input t
		var form = $("#frm_projsetup");
			//project setup wizard
			form.children("div").steps({
						enablePagination: true,
						headerTag: "h4",
						bodyTag: "section",
						transitionEffect: "slideLeft",
						labels: {
						current: "current step:",
						pagination: "Pagination",
						finish: "Submit",
						next: "Next",
						previous: "Previous"
					},
					onStepChanging: function (event, currentIndex, newIndex)
					{
						
						var isvalidpir = true;
							if(currentIndex == 1 && newIndex == 2){
								enablepartlinqmapping();
								if($("input[name='pirtype']:checked").val() == 1 || $("input[name='pirtype']:checked").val() == 3){
									$(".newpir").addClass("required");
									$(".newpir").addClass("alphanumericpir");
								}else{
									$(".newpir").removeClass("required");
									$(".newpir").removeClass("alphanumericpir");
								}
							}
						form.validate().settings.ignore = ":disabled,:hidden";
						
							//previous btn no validation
						if((currentIndex == 1 && newIndex == 0) || (currentIndex == 3 && newIndex == 2) ||(currentIndex == 2 && newIndex == 1)){
							var isvalid = true;
						}else{
							var isvalid = form.valid();
						}
						if(isvalid){
							//next btn
							if(currentIndex == 0 && newIndex == 1){
						
									var isvalidclient = true;var errorindex = "";
									$("#clienttable").find(".chosen-select").each(function(index){
										
										if($(this).val() == ""){
											if(index == 2 && $("input[name=clienttype]:checked").val() == 2){
												$(".chosenerr").eq(index).show();
												isvalidclient = false;
											}else if(index == 2 && $("input[name=clienttype]:checked").val() == 1){
												$(".chosenerr").eq(index).hide();
												
											}else{
												$(".chosenerr").eq(index).show();
												isvalidclient = false;
											}
											
										
										}else{
											$(".chosenerr").eq(index).hide();
										}
									});
									if(!isvalidclient){
										return false;
									}
									var pirtypeobj = $("input[name='pirtype']")[1];
									if($("#client_name").val() != ""){
										var clientexists = checkclient();
										if(!clientexists){
											$("#client_name").after("<label class='error' id='client'>Client already exists</label>");
											return false;
										}else{
											$("#client").css("display","none");
										}
										
										pirtypeobj.checked =false;
										$("input[name='pirtype']")[0].checked = true;
										$(".existpir").hide();
										$(".newpir").show();
									
										pirtypeobj.disabled = true;
									}else{
										pirtypeobj.disabled = false;
									}
							
									
									loadprojadmin("<?php echo $selptrojadmin;?>");
									loadtable();
								     //contracttypechange();
								}
						
								
								var pirtype = $("input[name='pirtype']:checked").val();
							
									if(pirtype == 1 ){
											getpirmask();
											$("#pirerror").css("display","none");
											$("#pirno1_chosen").hide();
											$("#pirno1").hide();
									}else if(pirtype == 2){
										$(".newpir").hide();
										if($("#editid").val() > 0 && currentIndex == 0 && newIndex == 1){
											getpirlist("<?php echo $selpirno;?>");
										}
										$("#pirno1_chosen").show();
									}else if(pirtype == 3){
										$(".newpir").show();
										if($("#editid").val() > 0 && currentIndex == 0 && newIndex == 1){
											getpirlist("<?php echo $selpirno;?>");
										}
										$("#pirno1_chosen").show();
									}	//contracttypechange();
							if(currentIndex == 1 && newIndex == 2){
								 isvalidpir = validatechosen("#pirtable");
										if(!isvalidpir){
											return false;
										}
							}
							//2nd tab
							if(currentIndex == 1 && newIndex == 2){
							
								if((pirtype == 2 && $("#pirno1").val() != "") || (pirtype == 1 && $("#pirno").val() != "") || (pirtype == 3 && $("#pirno").val() != "")){
									var pirexists = checkpirdata();
									if(!pirexists){
										$(".newpir").after("<label class='error' id='newpir'>PIR already exists</label>");
										return false;
									}else{
										$("#newpir").css("display","none");
									}
								}
								getPIRMappinginfo("<?php echo MAXIMUM_ADMIN_ENTRY;?>");
								var projexists = checkproject();
								if(!projexists){
									showalertmessage("Projectlist Already Exists","");
									return false;
								}
									contracttypechange();
							}
								//3rd tab
						
							if(currentIndex == 2 && newIndex == 3){
								var previewhtml = showprojpreviewdata();
								$("#previewprojdiv").html('');
								$("#previewprojdiv").html(previewhtml);
							}
							return true;
						}else{
							return form.valid();
						}
					},
					onFinishing: function (event, currentIndex)
					{
						form.validate().settings.ignore = ":disabled";
						
						if(form.valid){
							beforesubmitinfo();
							
						}else{
							return form.valid();
						}
					},
					onFinished: function (event, currentIndex)
					{
						//alert("Submitted!");
					}
				});	
			  
				$(".chosen-select").chosen({search_contains: true});
			if($("#editid").val() > 0){
				getsubdepartment("<?php echo $subdepartment_id;?>");
				//$('.chosen-select option:not(:selected').attr('disabled', true).trigger('chosen:updated'); 
				$('.chosen-select').attr('disabled', true).trigger('chosen:updated'); 
			
			}
			   
			//radio click
				$(".radioBtn").click(function() {
					$("#is_change").val(1);
					//$("#noofresource").attr("disabled", true);
					if ($("input[name=contracttype]:checked").val() == "2") {
					//	$("#noofresource").attr("disabled", false);
					//	$("#noofresource").select().focus();
					
					}else{
						//$("#noofresource").val("");
					}
					contracttypechange();
				});
				$(".clienttype").click(function() {
					var clienttype = $("input[name=clienttype]:checked").val();
				
					if($("input[name=clienttype]:checked").val() == 1){
						$("#newclient").show();
						$("#client_name").addClass("required alphanumeric");
						$("#client_id").val("").trigger("chosen:updated");
						$("#existclient").hide();
						$("input[name='pirtype']")[1].disabled=false;
					}else{
						$("#client_name").val("");
						$("input[name='pirtype']")[1].disabled=true;
						$("#client_name").removeClass("required alphanumeric");
						$("#newclient").hide();
						$("#existclient").show();
					}
				});
			//radio click
			
				$(".pirtype").click(function() {
			 $(".renewal").css("display","none");
					loadprojadmin("<?php echo $selptrojadmin;?>");
					$("label.error").hide();
					if($("input[name=pirtype]:checked").val() ==2){
						getpirlist("<?php echo $selpirno;?>");
					
					 }else if($("input[name=pirtype]:checked").val() ==1 || $("input[name=pirtype]:checked").val() ==3){
					 	getpirmask();
						$(".newpir").addClass("required");
						$(".newpir").addClass("alphanumericpir");
						$(".newpir").show();
						 if($("input[name=pirtype]:checked").val() == 1){
							 $("#pirno1_chosen").hide();
							 $("#pirno1").val('').trigger("chosen:updated");
							  if($(".radioBtn").val() == 0){
								$("#noofresource").val('');
							  $('.radioBtn')[0].checked = true;
							 }
							$("#pirno1").val('');
							  loadtable();
						 }else if($("input[name=pirtype]:checked").val() == 3){
							 $(".renewal").css("display","inline-block");
							 $("#pirno1_chosen").show();
						 }
						
					 }
						
				});
			
				$(document).delegate('.add-record', 'click', function(e) {
					 e.preventDefault(); 
					 var content = $('#activitydetailstable tbody').children('tr.projtr:first'),
					 size = $('#activitydetailstable >tbody >tr.projtr').length + 1,
						element = null,    
						element = content.clone();
						if(size<=<?php echo MAXIMUM_ADMIN_ENTRY;?>){
							element.attr('id', 'row'+size);
							element.find('.projectname').attr('id', 'projectname'+size);
							element.find('.projectname').attr('name', 'projectname'+size);
						    element.find('#projectname'+size).val('');
							element.find('.projectid').attr('id', 'projectid'+size);
							element.find('.projectid').attr('name', 'projectid'+size);
							element.find('#projectid'+size).removeClass('required');
							element.find('#projectid'+size).val('');
					
							element.find('.noofresource').attr('id', 'noofresource'+size);
							element.find('.noofresource').attr('name', 'noofresource'+size);
						    element.find('#noofresource'+size).val('');
					
							element.find('.is_partlinq_mapping').attr('name', 'is_partlinq_mapping'+size);
							element.find('.is_partlinq_mapping').attr('class', 'is_partlinq_mapping'+size);
							element.find('.is_partlinq_mapping').attr('checked', 'checked');
					
							element.find('.is_mdg_mapping').attr('name', 'is_mdg_mapping'+size);
							element.find('.is_mdg_mapping').attr('class', 'is_mdg_mapping'+size);
							element.find('.is_mdg_mapping').attr('checked', 'checked');
							if($("input[name='pirtype']:checked").val() == 2){
							element.find(".imgtd").append('<img src="images/remove.png" height="16" width="16" alt="RemoveRow"' 
															 +' class="delete-record"'+ 
															  'rowid='+size+' />');
							}else{
								element.find('.delete-record').attr('rowid', size);
							}
			
							element.appendTo('#activitydetailstable');					
							$("#totrow").val(size);
							<?php if($id!=''){ ?>
							 dropdownchange(size);
							<?php } ?>							
						}
					  else{
						$('<div>Only '+maximum_admin_entry+' can enter</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Ok": function() {
									$( "div" ).remove( ".ui-widget-overlay" );	
									$(this).dialog("close");
									return false;
								},
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
					  }				  
			   });
	
   
			$(document).delegate('.delete-record', 'click', function(e) {
				e.preventDefault();  
			  var id = $(this).attr('rowid');

			  if(id > 1){
			  var targetDiv = $(this).attr('targetDiv');
				$('<div>Are you sure  to delete ?</div>').dialog({
					resizable: false,
					open: function(){
					 $(".ui-dialog-title").html("Alert");
					},				
						buttons: {
						"Yes": function() {
							  $('#row' + id).remove();
							//regnerate index number on table
							$('#activitydetailstable tbody tr.projtr').each(function(index) {
							  var rowid = index+1;
								$(this).attr('id', 'row'+rowid);
								$(this).find('.budgettime').attr('id', 'budgettime'+rowid);
								$(this).find('.budgettime').attr('name', 'budgettime'+rowid);
								$(this).find('.delete-record').attr('rowid', rowid);
								$("#totrow").val(rowid);
							});
							$( "div" ).remove( ".ui-widget-overlay" );	
							$(this).dialog("close");
							return true;


						},
						"No": function() {
							$( "div" ).remove( ".ui-widget-overlay" );	
							$(this).dialog("close");
							return false;
						}
					}
				});	
				$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
			  }
			  else{
				$('<div>You cant delete this row</div>').dialog({
					resizable: false,
					open: function(){
					 $(".ui-dialog-title").html("Alert");
					},				
						buttons: {
						"Ok": function() {
							$( "div" ).remove( ".ui-widget-overlay" );	
							$(this).dialog("close");
							return false;
						},
					}
				});
				$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
			  }
			});	
	

			 loadtable();
			$("#partlinqmapping").click(function(){
				if($("input[name='partlinqmapping']:checked").val() == 1){
					$(".entrytypediv").css("display","table-cell");
				}else{
					$(".entrytypediv").css("display","none");
					$("#entrytype").val("");
				}

			});
			$("#mdgmapping").click(function(){
				if($("input[name='mdgmapping']:checked").val() == 1){
					$(".mentrytypediv").css("display","table-cell");
				}else{
					$(".mentrytypediv").css("display","none");
					$("#mentrytype").val("");
				}

			});
	});
	function getsubdepartment(subdept_id){
		var id=$("#department_id").val();
		
		if(subdept_id != ""){
				var subdept = subdept_id;
		}else{
			var subdept = "";
	     }
		$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
			$("#subdepartment_id").html("");
			$("#subdepartment_id").html(data);
			$("#subdepartment_id").chosen().trigger("chosen:updated");
			$("#subdepartment_id").chosen().trigger("change");
			
		});
	}
	function getclientlist(sel_id){
		var dept_id=$("#department_id").val();
		var subdept_id=$("#subdepartment_id").val();
		var client_id=$("#client_id option:selected").val();
		if(client_id > 0){
			var client_id1= client_id;
		}else{
			if(sel_id != ""){
				var client_id1= sel_id;
			}else{
				var client_id1= 0;
			}
			
		}
		
		$.post("getclientlist.php",{dept_id:dept_id,subdept_id:subdept_id,client_id:client_id1},function(data){
			$("#client_id").html("");
			$("#client_id").html(data);
			$("#client_id").chosen().trigger("chosen:updated");
		});
	}
	function getPIRListbyClient(selpirno){
		var dept_id=$("#department_id option:selected").val();
		var subdept_id=$("#subdepartment_id option:selected").val();
		var client_id = $("#client_id option:selected").val();
		var pirno = $("#pirno option:selected").val();
		if(selpirno != ""){
			var pirno = selpirno;
		}else{
			var pirno = $("#pirno option:selected").val();
		}
		$.post("getpirlistbyclient.php",{dept_id:dept_id,subdept_id:subdept_id,client_id:client_id,pirno:pirno},function(data){
			$("#pirno1").html("");
			$("#pirno1").html(data);
			$("#pirno1").chosen().val(pirno).trigger("chosen:updated");
			$("#pirno1").chosen().trigger("chosen:updated");
			console.log($("input[name='clientmdgmapping']").val());
			loadtable();
		});
	}

	function changeclient(obj){
		var selclientname = $("#client_id option:selected" ).text();
		if(selclientname == "Other"){
			$("#existclient").hide();
			$("#newclient").show();
		}else{
			$("#existclient").show();
			$("#newclient").hide();
		}
	}
	
	function loadtable(){console.log("loadtable");
		var pirtype = $("input[name='pirtype']:checked").val();
		
		var id=$("#pirno1").val();
		var deptid=$("#department_id").val();
		var cpm=$("input[name='clientpmapping']:checked").val();
		var cmm=$("input[name='clientmdgmapping']:checked").val();
		console.log($("input[name='clientpmapping']:checked").val()+"===");
		console.log($("input[name='clientmdgmapping']:checked").val()+"===");
		$.get("projecttabledata.php?id="+id+"&deptid="+deptid+"&pirtype="+pirtype+"&cpm="+cpm+"&cmm="+cmm,function(data){
			var dataarr = data.split("~");
			$("#activitytr").html(dataarr[0]);
		$("#totrow").val(dataarr[1]);
			contracttypechange();
		});	
	}
	function getprojectlist(){
	
		var id=$("#pirno1").val();
		var projectid=$("#projectid").val();
		$.get("getprojectlist.php?id="+id+"&sel="+projectid,function(data){
			$("#projectname").html(data);
			if($("#projectid").val() != ''){
				var str_array = $("#projectid").val();
				$("#projectname").val(str_array).trigger("chosen:updated");			
			}
			else{
				$("#projectname").trigger("chosen:updated");
			}		
		});	
	}
	function checkclient(){
	
		$.ajax({
			url: 'getajaxpirdata.php',
			dataType: 'json',async: false,
			type: 'post',
			data: {"department_id":$("#department_id").val(),"subdepartment_id":$("#subdepartment_id").val(),"pirno":"",
				    "client_name":$("#client_name").val(),"is_client":1,"is_pir":0},
			success: function(data){
				if(data.clientcnt > 0){
					clientexists = false;
				}else{
					clientexists = true;
				}
			}
		});	
		return clientexists;
	}
	function checkproject(){
		var pirtype = $("input[name='pirtype']:checked").val();
		if(pirtype == 2){
			var pirno = $("#pirno1 option:selected").val();
					var projectlist =""
				$(".projectname").each(function(index){
					var projkey = index+1;
					if($("#projectid"+projkey).val() ==""){
						projectlist +=$(this).val()+",";
					}
				});

			//projectlist.
			$.ajax({
				url: 'checkproject.php',
				dataType: 'json',async: false,
				type: 'post',
				data:{"projectlist":projectlist,"selpir":pirno},
				success: function(data){
					if(data.proj_exists == 1){
						projexists = false;
					}else{
						projexists = true;
					}
				}
			});	
		
		}else{
			projexists = true;
		}
			return projexists;
	
	}
	function checkpirdata(){
		var newpir = "";
		if($(".newpir").val() != ""){
			newpir = $(".newpir").val().split("/")[1];
		}
		$.ajax({
			url: 'getajaxpirdata.php',
			dataType: 'json',async: false,
			type: 'post',
			data: {"department_id":$("#department_id").val(),"subdepartment_id":$("#subdepartment_id").val(),"pirno":newpir
				  ,"pirno1":$("#pirno1").val(),"client_id":$("#client_id").val(),"is_client":0,"is_pir":1},
			success: function(data){
		
				if(data != "" && $("#pirno1").val() != ""){
					if($(".newpir").val() != ""){
						if(data.pircnt > 0){
						pirexists = false;
						}else{
							pirexists = true;
						}
					}
					if($("#is_change").val() != 1 && data.contracttype !=""){
					$(".radioBtn")[data.contracttype].checked = true;
						/*if($("#noofresource").val() >0){
						}else{
							$("#noofresource").val(data.noofresource);
						}*/
					}
				}else if(data != "" && $(".newpir").val() != ""){
					if(data.pircnt > 0){
						pirexists = false;
					}else{
						pirexists = true;
					}
				}
			}
		});	
	return pirexists;
	}
	function getPIRMappinginfo(max_entry){
		$("#pirmaptable").html("");
			
			$(".projectname").each(function(key){
				var projkey = key+1;
				var pirmaptbl ='<tr><td height="40px" align="left"  colspan="2" width="20%"><b>Project Name:</b>'+$(this).val();
					if(projkey == 1){
					pirmaptbl +='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="showactivityform('+projkey+');">Add Activity</a>';
						
					}
					var projid = $("#projectid"+projkey).val();
					pirmaptbl +='<input type="hidden" id="totactrow_'+projkey+'" name="totactrow_'+projkey+'" />'+
					'<input type="hidden" id="projectid'+projkey+'" name="projectid'+projkey+'"  value="'+projid+'" />'+
					'<input type="hidden" id="activitycnt_'+projkey+'" name="activitycnt_'+projkey+'" value=""/>'+
					'</td>';
			    	pirmaptbl += '<tr/>';
					pirmaptbl += '<tr>';
				    pirmaptbl += '<td id="piractivitytr_'+projkey+'" height="40px" align="left"  colspan="2" width="20%">'+
								'</td></tr>';
				loadactivitytable(projkey,max_entry,$("#pirno1").val(),$("#projectid"+projkey).val());
				$("#pirmaptable").append(pirmaptbl);
			});
	}
	function loadactivitytable(projkey,max_entry,pirid1,projid1){
		var id=$("#editid").val();
		var pirno=$("#pirno1").val();
		$.post("activitytabledata.php",{id:id,pirno:pirno,tblindex:projkey,max_entry:max_entry,
										dept_id:$("#department_id").val(),subdept_id:$("#subdepartment_id").val(),
									   pirid:pirid1,projid:projid1,prjcnt:$(".projectname").length},function(data){
			var dataarr = data.split("~");
			$("#piractivitytr_"+projkey).html(dataarr[0]);
		
			$("#totactrow_"+projkey).val(dataarr[1]);
		});	
	}
function disablefields(id,value,index,type){
	var tblobj = $("#activitydetailstable_"+index);

	if(value!=''){
		if(type== 1){
			if(!tblobj.find("#budgettime"+id).hasClass("required")){
				tblobj.find("#budgettime"+id).addClass('required');
				tblobj.find("#budgettime"+id).attr('readonly', false);
			}
			//tblobj.find("#budgettime"+id).val('');
		}
		if(type==2){
			if(tblobj.find("#budgettime"+id).hasClass("required") && value == 'y'){
				tblobj.find("#budgettime"+id).removeClass('required');
				tblobj.find("#budgettime"+id).attr('readonly', true);
			
			}
		}
	}
}
	function setbgtimeformat(id,index){
			var tblobj = $("#activitydetailstable_"+index);
			if(!$("#row"+id).find('.bgttimetype').hasClass("btimesec") && $("#row"+id).find('.bgttimetype:checked').val() == 2){
				tblobj.find("#budgettime"+id).removeClass('timeformat');
				tblobj.find("#budgettime"+id).addClass('btimesec');
			}else if(!$("#row"+id).find('.bgttimetype').hasClass("timeformat") && $("#row"+id).find('.bgttimetype:checked').val() == 1){
				tblobj.find("#budgettime"+id).removeClass('btimesec');
				tblobj.find("#budgettime"+id).addClass('timeformat');
			}
	}
function changebudgettextbox(id,value,index){
	var tblobj = $("#activitydetailstable_"+index);

	if(value!=''){
		if(value=='y'){
				$("#row"+id).find('.bgttimetype').removeAttr('disabled');
			if(!tblobj.find("#budgettime"+id).hasClass("required")){
				tblobj.find("#budgettime"+id).addClass('required');
				tblobj.find("#budgettime"+id).attr('readonly', false);
				
			}
			setbgtimeformat(id,index);
			tblobj.find("#budgettime"+id).val("");
		}
		if(value=='n'){
			$("#row"+id).find('.bgttimetype').attr('disabled','disabled');
			if(tblobj.find("#budgettime"+id).hasClass("required")){
				tblobj.find("#budgettime"+id).removeClass('required');
				tblobj.find("#budgettime"+id).attr('readonly', true);
				if(tblobj.find("#budgettime"+id).val() > 0 && tblobj.find("#budgettime"+id).val() != ''){
					tblobj.find("#budgettime"+id).val('0');
				}else{
				tblobj.find("#budgettime"+id).val('0');
				}
			}
		}
	}
}
	function checkvaliddata(size1){
		if($('#activity_id'+size1+" option:selected").val() == "" || $('#activitytype'+size1+" option:selected").val() == "" || $('#calculatehours'+size1+" option:selected").val() == ""){
			$("#is_completed"+size1).prop("checked","");
			$.notify("Please fill proper data", "error");
			return false;
		}else{
			if($('#calculatehours'+size1+" option:selected").val() == 'y'){
				$.notify("Fill Budgeted Time","error");
				return false;
			}else{
				return true;
			}
		}
	}
	function disablerow(size1,type,tblindex){
		
		if(type == 1){
			$('#activity_id'+size1).removeAttr('disabled');
			$('#activitytype'+size1).removeAttr('disabled');
			$('#manhrs'+size1).removeAttr('disabled');
			if($('#calculatehours'+size1).val() == 'y' || $('#calculatehours'+size1).val() == ''){
				$('#calculatehours'+size1).removeAttr('disabled');
				$('#budgettime'+size1).removeAttr('disabled');
				$("#row"+size1).find('.bgttimetype').removeAttr('disabled');
			}
			
			disablefields(size1,$('#calculatehours'+size1).val(),tblindex,1);
		
		}else{
			var isvaliddata = checkvaliddata(size1);
			if(isvaliddata){
			$('#activity_id'+size1).attr('disabled','disabled');
			$('#activitytype'+size1).attr('disabled','disabled');
			$('#manhrs'+size1).attr('disabled','disabled');
			$('#calculatehours'+size1).attr('disabled','disabled');
			$("#row"+size1).find('.bgttimetype').attr('disabled','disabled');
			$('#budgettime'+size1).attr('disabled','disabled');
			
			disablefields(size1,$('#calculatehours'+size1).val(),tblindex,2);
			}
			
		}
		
	}
function changemanhrstextbox(id,value,index){

	
	var tblobj = $("#activitydetailstable_"+index);
	
	if(value!=''){
		if(value=='y'){
			$("#row"+id).find('.bgttimetype').removeAttr('disabled');
			if(!tblobj.find("#budgettime"+id).hasClass("required")){
				tblobj.find("#budgettime"+id).addClass('required');
				tblobj.find("#budgettime"+id).attr('readonly', false);
				tblobj.find("#calculatehours"+id).addClass('required');
				tblobj.find("#calculatehours"+id).removeAttr('disabled');
			}
			if(tblobj.find("#budgettime"+id).hasClass("required")){
				tblobj.find("#budgettime"+id).addClass('required');
				tblobj.find("#budgettime"+id).removeAttr('disabled');
				tblobj.find("#calculatehours"+id).addClass('required');
				tblobj.find("#calculatehours"+id).removeAttr('disabled');				
			}
			setbgtimeformat(id,index);
			tblobj.find("#calculatehours"+id).val('');
			tblobj.find("#budgettime"+id).val('');
		}
		if(value=='n'){
			tblobj.find("#budgettime"+id).attr('readonly', true);
			tblobj.find("#budgettime"+id).val('0');
			$("#row"+id).find('.bgttimetype').removeAttr('disabled');
			tblobj.find("#calculatehours"+id).val('n');
			if(tblobj.find("#calculatehours"+id).hasClass("required")){
				tblobj.find("#calculatehours"+id).removeClass('required');
				tblobj.find("#calculatehours"+id).attr('disabled', 'disabled');
				
			}			
			if(tblobj.find("#budgettime"+id).hasClass("required")){
				tblobj.find("#budgettime"+id).removeClass('required');
				tblobj.find("#budgettime"+id).removeClass('error');
				tblobj.find("#budgettime"+id).attr('disabled', 'disabled');
				if(tblobj.find("#budgettime"+id).val() > 0 && tblobj.find("#budgettime"+id).val() != ''){
					tblobj.find("#budgettime"+id).val('0');
				}
				
				
			}
		}
	}
}
	function addactivity(index,maximum_admin_entry,masterid){
		if($("#department_id").val()!=''){
				//	 e.preventDefault();    
			 	   var totaltr = $('#activitydetailstable_'+index+' >tbody >tr').length+1;
					 var content = $('#activitydetailstable_'+index+' tbody').children('tr:first'),
					 size = $('#activitydetailstable_'+index+' >tbody >tr').length + 1,
						element = null,    
						element = content.clone();
						if(size<=maximum_admin_entry){
						var size1 = index.toString()+size.toString();
							element.attr('id', 'row'+size1);
						
							element.find('.projactiveid').attr('id', 'projactiveid'+size1);
							element.find('.projactiveid').attr('name', 'projactiveid'+size1);
							element.find('#projactiveid'+size1).val("");
							element.find('.activity').attr('id', 'activity_id'+size1);
							element.find('.activity').attr('name', 'activity_id'+size1);
							element.find('.activity').val("");
							element.find('#activity_id'+size1).attr('onchange', 'checkactivity('+index+',this.value,'+size1+')');
							element.find('.activitytype').attr('id', 'activitytype'+size1);
							element.find('.activitytype').attr('name', 'activitytype'+size1);
							element.find('.calculatemanhrs').attr('id', 'manhrs'+size1);
							element.find('.calculatemanhrs').attr('name', 'manhrs'+size1);
							element.find('#manhrs'+size1).attr('onchange', 'changemanhrstextbox('+size1+',this.value,'+index+')');
							element.find('.calculatemanhrs').empty().append('<option value="">-Select-</option><option value="y">YES</option><option value="n">NO</option>');				
							element.find('.calculatehours').attr('id', 'calculatehours'+size1);
							element.find('.calculatehours').attr('name', 'calculatehours'+size1);
							element.find('#calculatehours'+size1).attr('onchange', 'changebudgettextbox('+size1+',this.value,'+index+')');
							element.find('.calculatehours').empty().append('<option value="">-Select-</option><option value="y">YES</option><option value="n">NO</option>');
							element.find('.budgettime').attr('id', 'budgettime'+size1);
							element.find('.budgettime').attr('name', 'budgettime'+size1);
							
							element.find('.bgttimetype').attr('id', 'bgttimetype'+size1);
							element.find('.bgttimetype').attr('name', 'bgttimetype'+size1);
							element.find('#bgttimetype'+size1).eq(0).attr('value', '1');
							element.find('#bgttimetype'+size1).eq(0).attr('onchange', 'changebgttimetype('+size1+',1)');
							
							element.find('.bgttimetype').attr('id', 'bgttimetype'+size1);
							element.find('.bgttimetype').attr('name', 'bgttimetype'+size1);
							element.find('#bgttimetype'+size1).eq(1).attr('value', '2');
							element.find('#bgttimetype'+size1).eq(1).attr('onchange', 'changebgttimetype('+size1+',2)');
							
							/*element.find('.apmapping').attr('id', 'apmappingy'+size1);
							element.find('.apmapping').attr('name', 'bgttimetype'+size1);
							element.find('#apmappingy'+size1).eq(1).attr('value', '2');
							element.find('#bgttimetype'+size1).eq(1).attr('onchange', 'changebgttimetype('+size1+',2)');
							
							element.find('.bgttimetype').attr('id', 'apmappingn'+size1);
							element.find('.bgttimetype').attr('name', 'bgttimetype'+size1);
							element.find('#apmappingn'+size1).eq(1).attr('value', '2');
							element.find('#bgttimetype'+size1).eq(1).attr('onchange', 'changebgttimetype('+size1+',2)');*/
							
							
							
							if(	element.find('.is_completed').length > 0){
								element.find('.is_completed').remove();
							}
							element.find('.budgettime').val('');
						
							if(masterid >0){
								var deleteimg = '<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record-activity" rowid="'+size+'" onclick="deleteactivity('+index+','+maximum_admin_entry+','+size+','+masterid+');"/>';
								element.find(".add-record-activity").after(deleteimg);
							}else{
								element.find('.delete-record-activity').attr('rowid', size);
								element.find('.delete-record-activity').attr('onclick', 'deleteactivity("'+index+'",'+maximum_admin_entry+','+size1+',0)');
							}
						
						//	$('#row'+size1).removeAttr("d");
						
							$("#totactrow_"+index).val(size);
							element.appendTo('#activitydetailstable_'+index);	
								disablerow(size1,1,index);
						}
					  else{
						$('<div>Only '+maximum_admin_entry+' can enter</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Ok": function() {
									$( "div" ).remove( ".ui-widget-overlay" );	
									$(this).dialog("close");
									return false;
								},
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
					  }
				}else{
						$('<div>Please Select Department</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Ok": function() {
									$( "div" ).remove( ".ui-widget-overlay" );	
									$(this).dialog("close");
									return false;
								},
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
					}			
	}
	function deleteactivity(tblindex,maximum_admin_entry,currentrid,masterid){
	// var id = obj.attr('rowid');
	 var id = currentrid;
	  if(id > 1){
	//  var targetDiv =obj.attr('targetDiv');
		
		$('<div>Are you sure  to delete ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
					var selrowid = tblindex.toString()+currentrid.toString();
					  $('#row'+selrowid).remove();
					//regnerate index number on table
					$('#activitydetailstable_'+tblindex+' tbody tr').each(function(index) {
					  var rowid = index+1;
						$(this).attr('id', 'row'+rowid);
						var rowid1 = tblindex.toString()+rowid.toString();
						$(this).find('.activity').attr('id', 'activity_id'+rowid1);
						$(this).find('.activity').attr('name', 'activity_id'+rowid1);
					
						$(this).find('.activitytype').attr('id', 'activitytype'+rowid1);
						$(this).find('.activitytype').attr('name', 'activitytype'+rowid1);
						$(this).find('.calculatemanhrs').attr('id', 'manhrs'+rowid1);
						$(this).find('.calculatemanhrs').attr('name', 'manhrs'+rowid1);						
						$(this).find('.calculatehours').attr('id', 'calculatehours'+rowid1);
						$(this).find('.calculatehours').attr('name', 'calculatehours'+rowid1);
						$(this).find('.budgettime').attr('id', 'budgettime'+rowid1);
						$(this).find('.budgettime').attr('name', 'budgettime'+rowid1);
						$(this).find('.bgttimetype').attr('id', 'bgttimetype'+rowid1);
						$(this).find('.bgttimetype').attr('name', 'bgttimetype'+rowid1);
					//	$(this).find('.is_completed').attr('id', 'is_completed'+rowid1);
						//$(this).find('.is_completed').attr('name', 'is_completed'+rowid1);
						$(this).find('.delete-record-activity').attr('rowid', rowid);
					
						$("#totactrow_"+tblindex).val(rowid);
					
					});
				 var totaltr = $('#activitydetailstable_'+tblindex+' >tbody >tr').length-1;
					$("#totactrow_"+tblindex).val(totaltr);
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return true;
					

				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
	  }
	  else{
		$('<div>You cant delete this row</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				},
			}
		});
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	  }
	}
	function showprojpreviewdata(){
		var previewhtml = "";var projhtml = "";
		var contract = "";var activitytbl ="";var pirmaptbl="";var pirno ="";
		previewhtml += '<table id="previewtblinfo" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">';
		previewhtml += '<tr>';
		previewhtml += '<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl"><b>Department:</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl">'+$("#department_id option:selected").text()+'</td>';
		previewhtml += '<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl"><b>Sub Department:</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl">'+$("#subdepartment_id option:selected").text()+'</td>';
		previewhtml += '</tr><tr>';
		if($("input[name=clienttype]:checked").val() == 1){
			var clientname = $("#client_name").val(); 
		}else{
			var clientname = $("#client_id option:selected").text(); 
		}
		previewhtml += '<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl"><b>Client Name:</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl">'+clientname+'</td>';
		var pirtype = $("input[name='pirtype']:checked").val();
		var contracttype = $("input[name='contracttype']:checked").val();
		var padmin = $("#padmin option:selected").text();
		if(contracttype == 0){
			 contract = "T&M"; 
		}else if(contracttype == 1){
			 contract = "Fixed Price";
		}else if(contracttype == 2){
			 contract = "Dedicated Team";
		}
		if(pirtype == 1){
			 pirno = $(".newpir").val();
		}else if(pirtype == 2){
			 pirno =  $("#pirno1 option:selected").text();
		}else if(pirtype == 3){
			//pirno = "<b>Previous PIR</b> :"+$("#pirno1 option:selected").text()+" <br/> <b>Renewal PIR</b> :"+$(".newpir").val();
		}
		if($("input[name='is_rework']:checked").val() == 1){
			var is_rework = "Yes";
		}else{
			var is_rework = "No";
		}
		if($("input[name='is_internalpir']:checked").val() == 1){
			var is_internalpir = "Yes";
		}else{
			var is_internalpir = "No";
		}	
		if(pirtype == 3){
			
		previewhtml += '<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl"><b>Previous PIR NO:</b>'+$("#pirno1 option:selected").text()+'</td>'+
			'<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl"><b>Renewal PIR NO:</b>'+$(".newpir").val()+'</td>';
		previewhtml += '</tr>';
		}else{
		previewhtml += '<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl"><b>PIR NO:</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%" class="previewtbl">'+pirno+'</td>';
		previewhtml += '</tr>';
		}
		
		previewhtml += '<tr>';
		previewhtml += '<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl"><b>Contract Type</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl">'+contract+'</td>'+
			'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl"><b>Project Admin</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl">'+padmin+'</td>';
		/*previewhtml += '<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl"><b>No of Resource</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl">'+$("#noofresource").val()+'</td>';*/
		previewhtml += '</tr><tr>'+
		'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl"><b>Is rework</b></td>'+
		'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl">'+is_rework+'</td>'+
			'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl"><b>Is Internal PIR</b></td>'+
			'<td height="40px" align="left"  colspan="2" width="10%"  class="previewtbl">'+is_internalpir+'</td>';
		previewhtml += '</tr>'+'<table>';
		
		previewhtml += '<br/><table><tr><b>Project Activity Details</b></tr>';

		$(".projectname").each(function(index){
	
			var projindex = index+1;
			var ttlvar = "totactrow_"+projindex;
			var totactivity = $("#"+ttlvar).val();
			pirmaptbl +='<tr><td height="40px" align="left" ><b>Project Name:</b>'+$(this).val();
		if($('input[name="is_partlinq_mapping'+projindex+'"]:checked').val() == '1'){
				pirmaptbl +='&nbsp;&nbsp;&nbsp;&nbsp;<b>Partlinq Mapping:</b>Yes';
			}
			if($("#noofresource"+projindex).val() != ""){
				pirmaptbl +='&nbsp;&nbsp;&nbsp;&nbsp;<b>No of resource:</b>'+$("#noofresource"+projindex).val();
			}
			pirmaptbl +='</td></tr>';
			pirmaptbl += '<tr>';
			pirmaptbl += '<td id="piractivitytr_'+projindex+'" height="40px" align="left"  colspan="2" width="20%">';
			activitytbl ="";
			 activitytbl += '<table id="previewactivitystable_'+projindex+'" class="projactivity" border="0" cellpadding="5" cellspacing="0"'+ 
					'align="center"  width="100%">';
			 activitytbl += '<thead>';
			 activitytbl += '<tr>'+
						 '<td width="16%"><b>Activity</b></td>'+
						 '<td width="10%"><b>Activity Type</b></td>'+
						 '<td width="14%"><b>Is Man Hours Mapped?</b></td>'+
						 '<td width="14%"><b>Calculate Budgeted Hours</b></td>'+
			 			 '<td width="14%"><b>Budgeted Time</b></td>';
			var projid = $("#projectid"+projindex).val();
			if(projid > 0){
				 activitytbl +='<td width="10%">'+
	 			 '<b>Completed?</b></td>';
			}
		//	activitytbl += '<td width="14%"><b>Partlinq Mapping</b></td>';
			 	activitytbl +='</tr>'+
			 			 '</thead>'+
						 '<tbody>';
			var rowindex = "";
	
			var totactivity1 =$('#activitydetailstable_'+projindex+' tbody tr').length;
			for(var k= 0;k<totactivity1;k++){
			
				var rowindex = k+1;
				var activity_id = "#activity_id"+projindex+rowindex+" option:selected";
				var activitytype ="#activitytype"+projindex+rowindex+" option:selected";
				var manhrs = "#manhrs"+projindex+rowindex+" option:selected";
				var budgettime = "#budgettime"+projindex+rowindex;
				var calculatehours = "#calculatehours"+projindex+rowindex+" option:selected";
				var is_completed = "#is_completed"+projindex+rowindex;
				//var is_completed = "#is_completed"+projindex+rowindex;
				 activitytbl += '<tr id="row'+rowindex+'">';
					if($(activity_id).val() != ""){
						if($(activity_id).text() != "-Select-"){
							var activity_name  = $(activity_id).text();
						}else{
							var activity_name  = "";
						}
						if($(activitytype).text() != "-Select-"){
							var activitytype1  = $(activitytype).text();
						}else{
							var activitytype1  = "";
						}
						if($(manhrs).text() != "-Select-"){
							var manhrs1  = $(manhrs).text();
						}else{
							var manhrs1  = "";
						}
						if($(calculatehours).text() != "-Select-"){
							var calculatehours1  = $(calculatehours).text();
						}else{
							var calculatehours1  = "";
						}
						if($(budgettime).text() != "-Select-"){
							var budgettime1  = $(budgettime).val();
						}else{
							var budgettime1  = "";
						}
						if($(is_completed).is(":checked")){
							var is_completed1  = "Yes";
						}else{
							var is_completed1  = "No";
						}
					/*	var apmapping = 'apmapping'+projindex+rowindex;
						var apmappingval = "No";
						if($("input[name='"+apmapping+"']:checked").val() == 1){
							var apmappingval = "Yes";
						}*/
						 activitytbl += '<td width="16%" row="'+rowindex+'">'+activity_name+'</td>';
						
						 activitytbl += '<td width="14%" row="'+rowindex+'">'+activitytype1+'</td>';
						 activitytbl += '<td width="14%" row="'+rowindex+'">'+manhrs1+'</td>';
						 activitytbl += '<td width="14%" row="'+rowindex+'">'+calculatehours1+'</td>';
						 activitytbl += '<td width="14%" row="'+rowindex+'">'+budgettime1+'</td>';
						 if(projid > 0){
							activitytbl += '<td width="16%" row="'+rowindex+'">'+is_completed1+'</td>';
						 }
						//activitytbl += '<td width="14%" row="'+rowindex+'">'+apmappingval+'</td>';
					}
				 activitytbl += '</tr>';
			}
			pirmaptbl +=activitytbl;
			pirmaptbl +='</tbody></table></td></tr>';

		});
		previewhtml += pirmaptbl;
		
	return previewhtml;
	}
	function projformsubmit(){
		
		var projdata  = $("#frm_projsetup").serializeArray();
		
		//if($("#editid").val() == 0){
			//var fname = "projsetup.php";
		//}else{
		var fname = "saveprojsetup.php";
		//}
			$.post(fname,projdata,function(data){
				var message ='<div>Added Successfully</div>';
					showalertmessage(message,1);
		    });	

	}
	function showalertmessage(msg,status){
		
		$("<div>"+msg+"</div>").dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"OK": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					if(status == 1){
						window.location.href = "setup.php";
					}
					
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );	
	}
	function beforesubmitinfo(){
		
		$('<div>Are you sure  to submit ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
					$(this).dialog("close");
					projformsubmit();
					
				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );	
	}
	
	function validatechosen(tblid){
		var ptype = "";
		if(tblid == "#pirtable"){
			 ptype = $("input[name='pirtype']:checked").val()
		}
		var isvalidsel = true;
		$(tblid).find(".chosen-select").each(function(index){
				if($(this).val() == "" && ((ptype == 1 && index ==1) || ptype == 2 || ptype == 3)){
					isvalidsel = false;
					$(tblid).find(".chosenerr").eq(index).css("display","block");
				}else{
					$(tblid).find(".chosenerr").eq(index).css("display","none");
				}
				
		});
		if(ptype == 3 && $(".newpir").val() !=""){
		
			if($(".newpir").val().split("/")[1] == ""){
			isvalidsel = false;
			}
			
		}
		return isvalidsel;
	}
	function showactivityform(projindex){
		var selactivitylen = $(".activity")[0].length;
		selactivitylen = selactivitylen+1;
		var department_id = $("#department_id option:selected").val();
		var subdepartment_id = $("#subdepartment_id option:selected").val();
		$("#dept_id").val(department_id);
		$("#dept_name").text($("#department_id option:selected").text());
		$("#subdept_id").val(subdepartment_id);
		$("#subdept_name").text($("#subdepartment_id option:selected").text());
		var sactivity = $("#sactivity").val();
		$("#activityform").dialog({  
			resizable: false,
			open: function(){
				
				 $(".ui-dialog-title").html("Add Activity");
			},close: function(){
				 $("#sactivity").val("");
				
			},			
			 width:600, modal: true,
			 buttons: {
            "Cancel": function() {
                 $("#activityform").dialog("close");
            },
            "Save": function(){
				if($("#activityform").valid()){
					$.ajax({
						url: 'addactivity.php',
						dataType: 'json',async: false,
						type: 'post',
						data: $("#activityform").serializeArray(),
						success: function(data){
						   if(data.msg == "success" && data.activity_name != "" && data.activity_id != ""){
						 		 $(".activity").append('<option value="'+data.activity_id+'">'+data.activity_name+'</option>');
								 $("#activityform").dialog("close");
								showalertmessage(data.successmsg,"");
							}else if(data.activity_id == 0){
								 $("#activityform").dialog("close");
								showalertmessage(data.successmsg,"");
							}
						}
					});	
				}else{
					return false;
				}
			}
			 }
		});
	}
	
	function checkallactivity(projindex,ischeck){
		$("#activitydetailstable_"+projindex).find(".is_completed").prop("checked",ischeck);
		if(ischeck){
			$("#activitydetailstable_"+projindex).find(".is_completed").val('1');
		}else{
			$("#activitydetailstable_"+projindex).find(".is_completed").val('0');
		}
		$("#activitydetailstable_"+projindex+" .is_completed").each(function(index){
			var k= index+1;
				k= projindex.toString()+k.toString();
			
			completecheck(k,projindex);
		});
		
	}
	function getpirmask(){
		var selnewpir = $(".newpir").val();
		var selpirlen = $("#subdepartment_id option:selected").text().length+1;
		var selnewpirarr = selnewpir.split("/");
		if(selnewpir == ""){
			var selpir =  $("#subdepartment_id option:selected").text()+"/";
		}else if(selnewpir.length >=selpirlen){
			var selpir = $("#subdepartment_id option:selected").text()+"/"+selnewpirarr[1];
		}else if(selnewpir.length < selpirlen){
			var selpir =  $("#subdepartment_id option:selected").text()+"/";
		}
	
		$(".newpir").val(selpir);

	}
	function changebgttimetype(id,val){
		$("#budgettime"+id).removeClass("timeformat");
		$("#budgettime"+id).removeClass("btimesec");
		if(val == 1){
			$("#budgettime"+id).addClass("timeformat");
		}else{
			$("#budgettime"+id).addClass("btimesec");
		}
		
		
	}
	function getpirlist(selpirno){
		getPIRListbyClient(selpirno);
		$(".newpir").hide();
		$(".newpir").val("");
		$(".pirerror").hide();
		
		$(".newpir").removeClass("error");
		$(".newpir").removeClass("required");
		$(".newpir").removeClass("alphanumericpir");
		$("lable[for='pirno']").css("display","none");
		if(selpirno != ""){
		$("#pirno1").val(selpirno).trigger("chosen:updated");	
		}
		$("#pirno1_chosen").show();
	}
	function completecheck(index,tblindex){
		if($("#is_completed"+index).is(":checked")){
			//disablerow(index,2,tblindex);
			$("#is_completed"+index).val('1');
			
			if($("#activitydetailstable_"+tblindex+" .is_completed:checked").length == $("#activitydetailstable_"+tblindex+" .is_completed").length){
				$("#projactivitychk_"+tblindex).prop('checked',true);
				$("#projactivitychk_"+tblindex).val('1');
			}else{
					$("#projactivitychk_"+tblindex).prop('checked',"");
					$("#projactivitychk_"+tblindex).val('0');
			}
			
		}else{
			disablerow(index,1,tblindex);
			$("#projactivitychk_"+tblindex).prop('checked',"");
			$("#projactivitychk_"+tblindex).val('0');
			$("#is_completed"+index).val('0');
		}
	}
	function checkactivity(selindex,selval,selidindex){
		var activityarr =[];
		var activityidsarr =[];
		var totactivity1 =$('#activitydetailstable_'+selindex+' tbody tr').length;
		for(var k= 0;k<totactivity1;k++){
			var rowindex = k+1;
			var activity_id = "#activity_id"+selindex+rowindex+" option:selected";
			if($(activity_id).val() != ""){
				activityarr.push($(activity_id).val());
				activityidsarr.push("#activity_id"+selindex+rowindex);
			}
		
		}
		hasDuplicates(activityarr,activityidsarr);
	}
	
	function hasDuplicates(arr,activityidsarr) {
    var counts = [];
	for (var i = 0; i <= arr.length; i++) {	
        if (counts[arr[i]] === undefined) {
            counts[arr[i]] = 1;
        } else {
         	$(activityidsarr[i]).prop('selectedIndex',0);;
			$.notify("Activity already exists,please select another", "error");
			return false;
        }
    }
	
    return true;
}
	
	function loadprojadmin(selprojadmin){
		var dept_id=$("#department_id option:selected").val();
		var subdept_id=$("#subdepartment_id option:selected").val();
			$.post("getprojadminlist.php",{dept_id:dept_id,subdept_id:subdept_id,id:selprojadmin},function(data){
			$("#padmin").html("");
			$("#padmin").html(data);
			$("#padmin").chosen().trigger("chosen:updated");
			
		});
	  }
	function changenoofresource(type){
		if(type ==1){
			if($(".noofresource").attr("disabled") == "disabled"){
				$(".noofresource").removeAttr("disabled");
			}
			$(".noofresource").addClass("number");
			$(".noofresource").addClass("required");
		}else{
			$(".noofresource").attr("disabled",true);
			$(".noofresource").val("");
			$(".noofresource").removeClass("number");
			$(".noofresource").removeClass("required");
		}
	}
	function contracttypechange(){
		
		 $("#addrecord").css("display","inline-block");
		if($("input[name=contracttype]:checked").val() == "2") {
			changenoofresource(1);
		}else if($("input[name=contracttype]:checked").val() == "1"){
			changenoofresource(2);
		}else{
			changenoofresource(2);
		}
	}
	
	function enablepartlinqmapping(){
		if($("input[name='clientpmapping']:checked").val() == 1){
		}
	}
		
</script>
</html>
<?php } ?>

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	
	function getbudgettime($dbase,$budtime){
		$val = $budtime['cnt'];
		if($budtime['manhrs']=='n'){
			return "NA";
		}
		else{
			if($budtime['activitytype']=='0'){
				$budgettime = $budtime['budgettime'] * 1;
				$btime = $dbase->secToTimeFormat($budgettime);
				if($btime=='00:00'){
					return "NULL";
				}
				else{
					return $btime;
				}				
			}
			else{
				$budgettime = $budtime['budgettime'] * $val;
				$btime = $dbase->secToTimeFormat($budgettime);
				if($btime=='00:00'){
					return "NULL";
				}
				else{
					return $btime;
				}				
			}

		}
	}
	function getdataentrydata($data,$dbase,$lastindex = 0,$index,$enablereadonly){
		$html .='<tr id="row'.$index.'">
		<td width="16%">
		<select id="pirmaster_id'.$index.'" name="pirmaster_id'.$index.'" class="required pir" onchange="getproject(this.id,this.value);">
		';
			$getuserDeptQuery = "SELECT department_ids,subdepartment_ids FROM employeelist WHERE ID = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND isactive='1' LIMIT 0,1";
			$getuserDeptResult = $dbase->executeQuery($getuserDeptQuery,"single");		
			if($getuserDeptResult['department_ids']!=''){
				$deptcsv = "'".implode( "','", explode(",", mysql_escape_string($getuserDeptResult['department_ids'])))."'";
				$deptcond = " AND p.department_id IN (".$deptcsv.")";
			}
			if($getuserDeptResult['subdepartment_ids']!=''){
				$subdeptcsv = "'".implode( "','", explode(",", mysql_escape_string($getuserDeptResult['subdepartment_ids'])))."'";
				$subdeptcond = " AND p.subdepartment_id IN (".$subdeptcsv.")";
			}			
			if($deptcond !=''){
				$cond .= $deptcond;
				if($subdeptcond !=''){
					$cond .= $subdeptcond;
				}
						
				//$pir_select = "SELECT p.pirno as id, (SELECT p2.pirno FROM pirlist p2 WHERE p2.id=p.pirno) as pirno FROM pirmaster p WHERE p.isActive ='1' ".$cond." GROUP BY p.pirno ORDER BY p.pirno ASC";
				$pir_select = "SELECT p.pirno as id,p2.pirno as pirno,(SELECT sd.subname FROM subdepartment sd WHERE sd.id=p.subdepartment_id) as subdept FROM pirmaster p,pirlist p2 WHERE p2.isActive='1' AND p2.id=p.pirno ".$cond." GROUP BY p.pirno ORDER BY p.pirno ASC";
				$pirlist = $dbase->executeQuery($pir_select,'multiple');
				if($data['pir'] >0){
				 $html .='<option value="" disabled>-Select-</option>';
					$required = "";
				}else{
					$html .='<option value="">-Select-</option>';
					$required = "required";
				}
				
				for($i=0;$i<count($pirlist);$i++){
						if($data['pir'] >0){
							if($pirlist[$i]['id'] ==  $data['pir']){
								$selected ="selected";
								$disableds = "";
							}else{
								$selected = "";
								$disableds = "disabled";
							}
						}else{
								$selected ="";
								$disableds = "";
						}
					$pir = $pirlist[$i]['subdept']."/".$pirlist[$i]['pirno'];
					$html .='<option value="'.$pirlist[$i]['id'].'"'.$selected.' '.$disableds.'>'.$pir.'</option>';
				}				
			}

		$html .= '</select>
		</td>
		<td width="16%">
		<select id="project_id'.$index.'" name="project_id'.$index.'" class="required project" onchange="getactivity(this.id,this.value);">';
		if($data['prjid'] >0){
			$disabled = "disabled";
		}else{
			$disabled = "";
		}
		$html .= '<option value="" '.$disabled.'>-Select-</option>';
		if($data['prjid'] >0){
			$html .='<option value="'.$data['prjid'].'" selected>'.$data['prjname'].'</option>';
		}
		$html .= '</select>		
		<td width="16%">
		<select id="activity_id'.$index.'" name="activity_id'.$index.'" class="required activity" onchange="callcalculatetime(this.id);">
		<option value="" '.$disabled.'>-Select-</option>';
		if($data['activity_id'] >0){
			
			$readonly = 'readonly="readonly"';
			$html .='<option value="'.$data['activity_id'].'" selected>'.$data['activity_name'].'</option>';
		}else{
			$readonly = "";
		}
		if($enablereadonly ==1){
			$ahreadonly = 'readonly="readonly"';
		}else{
			$ahreadonly ="";
		}
		if($data['actualhours'] != ""){
			$actualhours = $data['actualhours'];
		}else{
			$ahreadonly ="";
			$actualhours="";
		}
		$html .= '</select>		
		</td>
		<td width="14%">
		<input type="text" id="totalparts'.$index.'" name="totalparts'.$index.'" class="required number totalparts" onkeyup="calculatetime(this.id,this.value,1);" maxlength="10" size="10" value="'.$data['cnt'].'" '.$readonly.'/>
		</td>
		<td width="14%">
		<input type="password" id="calculatedhours'.$index.'" name="calculatedhours'.$index.'" class="required calculatedhours" readonly="readonly" placeholder = "HH:MM" maxlength="10" size="10"  value="'.$data['budgettime'].'" '.$readonly.'/>
		</td>
		<td width="14%">
		<input type="text" id="actualhours'.$index.'" name="actualhours'.$index.'" class="actualhours time24 required" placeholder = "HH:MM" onkeyup="checkcalculatetime(this.id,this.value);" value="'.$actualhours.'" maxlength="10" size="10" />
		</td>
		<td width="20%">';
		if(($data['activity_id'] >0 && $lastindex >0 ) || $data['activity_id'] == ""){
		$html .= '<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record"  rowid="'.$index.'"/>';
		$html .= '<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record" rowid="'.$index.'" '.$disabled.' />';
		}
		$html .= '</td>
	</tr>';
		return $html;
	}
	$emparr['empid'] = $_SESSION['timesheet']['USERCODE']; 
	$emparr['date'] = date("Y-m-d",strtotime($_GET['entrydate'])); 
//echo	$emparr['date'] = date("Y-m-d",strtotime('2-Nov-2020')); 
	$compltedparts = $master->getcompletedpartsinfo($emparr);
	$mdgcompltedparts = $master->getmdgcompletedparts($emparr);
	//SELECT * FROM `pir_activity` WHERE `pirmaster_id` =31 AND `partlinq_mapping`=0
	if($_GET){
		$id=$_GET['id'];
		$html = '';
$html .= '<table id="dataentrydetailstable" border="0" cellpadding="0" cellspacing="0" align="center"  width="100%">
<thead>
	<tr>
		<td width="16%"><b>PIR No</b></td>
		<td width="16%"><b>Project</b></td>
		<td width="16%"><b>Activity</b></td>
		<td width="14%"><b>No of Parts</b></td>
		<td width="14%"><b>Budgeted Hours ( HH:MM )</b></td>
		<td width="14%"><b>Actual Hours ( HH:MM )</b></td>
		<td width="20%"><b>Add / Del</b></td>
	</tr>
	</thead>
	<tbody>';
		$data['prjname'] = '';
	    $data['prjid'] = '';
        $data['cnt'] = '';
        $data['pir'] = '';
		$data['activity_id'] = '';
		$data['activity_name'] = '';
		$data['actualtime']='';
		$data['budgettime']='';
		$cnt  = count($compltedparts);	 $cnt =0;
		$cnt1 = count($mdgcompltedparts);$cnt1=0;
		if(count($mdgcompltedparts) >0){
			$index=0;
			for($s=0;$s<count($mdgcompltedparts);$s++){
				
				$pirmasterres = $dbase->executeQuery("SELECT pm.`projectname`,pm.`pirno`,
				(SELECT `pirno` FROM `pirlist` WHERE `id`=pm.`pirno`)as pirname,
				(SELECT `projectname` FROM `projectlist` WHERE `id` = pm.projectname)as prjname 
				FROM `pirmaster` as pm WHERE pm.`id` ='".$mdgcompltedparts[$s]['pmid']."' AND pm.`mdg_mapping`=1","single");
					$data['prjname'] = $pirmasterres['prjname'];
					$data['prjid'] = $pirmasterres['projectname'];
					$data['pir'] = $pirmasterres['pirno'];
					$data['cnt'] = $mdgcompltedparts[$s]['cnt'];
		
				$activityarr = $dbase->executeQuery(" SELECT n.* FROM (SELECT pa.*,a.name as activityname
				FROM `pir_activity` as pa,`activity` as a WHERE pa.pirmaster_id ='".$mdgcompltedparts[$s]['pmid']."' 
				AND pa.`activity_id` = a.id AND a.isActive=1 AND a.mentrytype='".$mdgcompltedparts[$s]['entrytype']."' AND a.mdg_mapping=1)n 
				
				","multiple");
				for($y=0;$y<count($activityarr);$y++){
					$index++;
					$data['activity_id'] = '';
					$data['activity_name'] = '';
					$data['budgettime'] = '';
					$data['manhrs'] = '';
					$data['activitytype'] = '';
					$data['activity_id'] = $activityarr[$y]['activity_id'];
					$data['activity_name'] = $activityarr[$y]['activityname'];
					$data['budgettime'] = $activityarr[$y]['budgettime'];
					$data['manhrs'] = $activityarr[$y]['manhrs'];
					$data['activitytype'] = $activityarr[$y]['activitytype'];
					//$actualtimearr = explode(":",$mdgcompltedparts[$s]['activetime']);
					$data['actualhours']   = "";
					$budgettime = getbudgettime($dbase,$data);
					$data['budgettime'] = $budgettime;
					
					if($s == (count($mdgcompltedparts)-1) && count($compltedparts) == 0){
						$lastindex =1;
					}else{
						$lastindex =0;
					}
			$html .= getdataentrydata($data,$dbase,$lastindex,$index,0);
				}
			}
		}
		if($cnt >0){ 
			if(count($mdgcompltedparts) == 0){
				$index=0;
			}
			for($s=0;$s<count($compltedparts);$s++){
				
				$pirmasterres = $dbase->executeQuery("SELECT pm.`projectname`,pm.`pirno`,
				(SELECT `pirno` FROM `pirlist` WHERE `id`=pm.`pirno`)as pirname,
				(SELECT `projectname` FROM `projectlist` WHERE `id` = pm.projectname)as prjname 
				FROM `pirmaster` as pm WHERE pm.`id` ='".$compltedparts[$s]['pmid']."' AND `is_partlinq_mapping`=1","single");
					$data['prjname'] = $pirmasterres['prjname'];
					$data['prjid'] = $pirmasterres['projectname'];
					$data['pir'] = $pirmasterres['pirno'];
					$data['cnt'] = $compltedparts[$s]['cnt'];
				$activityarr = $dbase->executeQuery("SELECT n.* FROM (SELECT pa.*,a.name as activityname
				FROM `pir_activity` as pa,`activity` as a WHERE pa.pirmaster_id ='".$compltedparts[$s]['pmid']."' 
				AND pa.`activity_id` = a.id AND a.isActive=1  AND a.entrytype='".$compltedparts[$s]['sEntryType']."' AND a.`partlinq_mapping`=1) n 
			
				","multiple");
				for($y=0;$y<count($activityarr);$y++){
					$index++;
					$data['activity_id'] = '';
					$data['activity_name'] = '';
					$data['budgettime'] = '';
					$data['manhrs'] = '';
					$data['activitytype'] = '';
					$data['activity_id'] = $activityarr[$y]['activity_id'];
					$data['activity_name'] = $activityarr[$y]['activityname'];
					$data['budgettime'] = $activityarr[$y]['budgettime'];
					//$data['budgettime'] = $activityarr[$y]['budgettime'];
					$data['manhrs'] = $activityarr[$y]['manhrs'];
					$data['activitytype'] = $activityarr[$y]['activitytype'];
					if($compltedparts[$s]['activetime'] != ""){
					$actualtimearr = explode(":",$compltedparts[$s]['activetime']);
					$data['actualhours']   = $actualtimearr[0].":".$actualtimearr[1];
					}
					$budgettime = getbudgettime($dbase,$data);
					$data['budgettime'] = $budgettime;
					
					if($s == (count($compltedparts)-1)){
						$lastindex =1;
					}else{
						$lastindex =0;
					}
			$html .= getdataentrydata($data,$dbase,$lastindex,$index,1);
				}
			}
		}else{
			$index =1;
		$html .= getdataentrydata($data,$dbase,"",$index,0);
		}
	$html .='<input type="hidden" name="totalactivity" id="totalactivity" value="'.$index.'" ></tbody>
</table>';
		echo $html;
	}
}
?>

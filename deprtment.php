<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$name = $_POST['name'];
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM department WHERE name='".trim($name)."'".$idcond;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: deprtment.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
			$departmentquery = "UPDATE department SET `name` = '".$name."' WHERE `id` ='".$editid."'";	
			}
			else{
			$departmentquery = "INSERT INTO department( `name`) VALUES ( '".$name."');";			
			}
			 $dbase->executeNonQuery($departmentquery);
			 if($editid !=''){
				header('Location: deprtment.php?m=2');
				exit();	
			 }
			 else{
				header('Location: deprtment.php?m=1');
				exit();				 
			 }			
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$departmentquery = "UPDATE department SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($departmentquery);
				header('Location: deprtment.php?m=4');
				exit();				
		}
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		else{
			$label = "Add";
			$button = "Add";
		}
		$dept_query = "SELECT * FROM department WHERE id='".trim($id)."'";
		$dept_result = $dbase->executeQuery($dept_query,"single");		
	}
	else{
		$label = "Add";
		$button = "Add";
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		table#departmenttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#departmenttable td, table#departmenttable th {
			border: 1px solid black;
		}	
		table#departmentlisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#departmentlisttable td, table#departmentlisttable th {
			border: 1px solid black;
			padding:5px;
		}		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php //include("adminmenu.php"); ?>
<?php include("userrolemenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_department" action="" method="POST" enctype="multipart/form-data">
<table id="departmenttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Inserted Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Department Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='4'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Department Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> Department</b></td></tr>		
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Department Name</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="name" id="name" value="<?php echo $dept_result['name']; ?>" class="required alphanumeric" /></td>
</tr>
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
		<?php } ?>
		<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
<td align="center" width="80%" valign="top">
	<div style="width: auto; min-height:250px; overflow-y:scroll;height:250px;">
	<table id="departmentlisttable" border="0" cellpadding="0" cellspacing="0" align="left" width="95%">
		<tr>
			<td width="50%" align="center"><b>Department Name</b></td>
			<td width="50%" align="center"><b>Action</b></td>
		</tr>
	<?php
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'";
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $deptResult[$i]['name'];?></td>
			<td align="left" style="padding-left:10px"><a href="deprtment.php?id=<?php echo $deptResult[$i]['id'];?>">Edit</a> | <a href="deprtment.php?del=1&id=<?php echo $deptResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
	</table>
	</div>	
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
	  $("#frm_department").validate();			
});	
</script>
</html>
<?php } ?>

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getRBUquery($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		if(strtotime($date)<strtotime(date('2021-07-01'))){
			$availhrs = NON_DEDICATED_PRESENT_HOURS_BEFORE;
		}
		else{
			$availhrs = NON_DEDICATED_PRESENT_HOURS_AFTER;
		}		
		$query = '';
		$query ="SELECT 
	m.empid,
	m.project_id,
	SEC_TO_TIME(m.actualseconds) as actualhours,
	SEC_TO_TIME(m.calculatedseconds) as billablehours,
	m.minimumseconds as presenthours
FROM 
	(
		SELECT 
			t1.`project_id`,
			(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
			SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
			SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
			IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."'),'09:00:00')	AS minimumseconds
		FROM 
			`timeentry` t1, 
			 pirmaster t2 
		WHERE 
			1=1  
			".$reworkcond."
			".$internalpircond."
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m 
WHERE 1=1".$cond;
		
		
		
		//$query = "SELECT m.empid,m.empname,m.deptid,m.edate,m.employee_id,m.project_id,m.pirmaster_id,m.totalparts,(SELECT d1.name FROM department d1 WHERE d1.id=m.deptid) as dept,m.subdept,m.entrydate,SEC_TO_TIME(m.actualseconds) as actualhours,SEC_TO_TIME(m.calculatedseconds) as billablehours,SEC_TO_TIME(m.onlineseconds) as onlinehours,SEC_TO_TIME(m.minimumseconds) as presenthours,SEC_TO_TIME((m.onlineseconds - m.dayseconds)) as shortagehours,CONCAT(ROUND(((m.calculatedseconds/m.minimumseconds)*100),2),'') as utilization,CONCAT(ROUND(((m.calculatedseconds/m.actualseconds)*100),2),'') as efficiency FROM (SELECT DATE_FORMAT(t1.`entrydate` , '%d-%b-%Y' ) as entrydate,t1.`entrydate` as edate , t1.`employee_id`, t1.`project_id`, t1.`pirmaster_id`,(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id=t1.employee_id) as empname,(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid, (SELECT u1.department_ids FROM employeelist u1 WHERE u1.id=t1.employee_id) as deptid, (SELECT u2.subdepartment_ids FROM employeelist u2 WHERE u2.id=t1.employee_id) as subdept, SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) as onlineseconds,(CASE WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) <= TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('04:15:00') WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) > TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('08:30:00') ELSE TIME_TO_SEC('08:30:00') END) AS minimumseconds,TIME_TO_SEC('09:30:00') AS dayseconds,SUM(t1.totalparts) as totalparts,t2.department_id,t2.subdepartment_id FROM `timeentry` t1, pirmaster t2  WHERE 1=1 AND t2.id=t1.pirmaster_id ".$subdeptcond." AND t1.isActive='1' ".$dtcond." AND t1.`entrydate`='".$date."' ".$group.") m WHERE 1=1".$cond;
		return $query;
	}	

	if($_POST){
		//print_r($_POST);exit;
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$deptid = $_POST['department_id'];
		$subdeptid = $_POST['subdepartment_id'];
		$pirmaster_id = $_POST['pirmaster_id'];
		$project_id = $_POST['project_id'];
		$kpis = $_POST['kpis'];
		$withdetails = "0";
		$subdepartment_ids = implode (",", $subdeptid);

		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$cond = '';
		$procond='';

		if($project_id != ''){
			$cond .= " AND m.project_id='".$project_id."'";
			$procond = " AND projectname='".$project_id."'";
		}		
		if($pirmaster_id != '' && $project_id != ''){
			$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'".$procond;
			$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
			$pirmasterid=$pirmasterdet['id'];			
			//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
			$group = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
		}
		elseif($pirmaster_id != ''){
			$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
			$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
			if($pirmastercntdet['pircnt']==1){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasterid=$pirmasterdet['id'];			
				//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
				$group = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";				
			}
			else{
				$pirmaster_select = "SELECT GROUP_CONCAT(id) as pircsv FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasteridcsv = $pirmasterdet['pircsv'];
				$group = "AND  t1.`pirmaster_id` IN (".$pirmasteridcsv.") GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
			}
		}		
		else{
			$group = " GROUP BY t1.`employee_id`";
		}		
		
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$subdeptcond = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
		}
		$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
		$dedicatedempcond = '';
		if($deptid != ''){
			$dedicatedempcond .= " AND department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$dedicatedempcond .= " AND subdepartment_id IN (".$subdepartment_ids.")";
		}
		if($pirmaster_id != ''){
			$dedicatedempcond .= " AND id='".$pirmaster_id."'";
		}		
		//$query="SELECT IFNULL(SUM(`noofresource`),0) as dedicatedresource FROM `pirlist` WHERE `isActive`='1' AND `contracttype`='2' ".$dedicatedempcond;
		if($pirmaster_id != '' && $project_id != ''){
			$dedicatedempcond1= "AND id='".$pirmasterid."'";
		}
		elseif($pirmaster_id != ''){
			$dedicatedempcond1= "AND pirno='".$pirmaster_id."'";
		}
		$query="SELECT IFNULL(`no_of_resource`,0) as dedicatedresource FROM `pirmaster` WHERE `isActive`='1' AND no_of_resource > 0 ".$dedicatedempcond1;
		$result = $dbase->executeQuery($query,'single');
		$totempcnt = $result['dedicatedresource'];			
		$datelist = $dbase->getDateLists($fromdate,$todate);
		
		if(count($datelist)>0){
			$overalldata = array();
			$dedicateddata = array();
			$nondedicateddata = array();
			
			for($j=0;$j<count($datelist);$j++){
				$query1 = '';
				$dtcond1 = "";
				$rwcond1 = " AND t1.is_rework='0'";
				$inpircond1 = " AND t1.is_internalpir='0'";
				unset($report1);				
				$query1 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond1,$rwcond1,$inpircond1);
				$result1 = $dbase->executeQuery($query1,'multiple');
				if(count($result1) > 0){
					$overalldata[] = $result1;
				}
				$query12 = '';
				$dtcond12 = "";
				$rwcond12 = " AND t1.is_rework='1'";
				$inpircond12 = " AND t1.is_internalpir='0'";
				unset($report12);				
				$query12 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond12,$rwcond12,$inpircond12);
				$result12 = $dbase->executeQuery($query12,'multiple');
				if(count($result12) > 0){
					$overallreworkdata[] = $result12;
				}
				$query13 = '';
				$dtcond13 = "";
				$rwcond13 = " AND t1.is_rework='0'";
				$inpircond13 = " AND t1.is_internalpir='1'";
				unset($report13);				
				$query13 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond13,$rwcond13,$inpircond13);
				$result13 = $dbase->executeQuery($query13,'multiple');
				if(count($result13) > 0){
					$overallinterpirdata[] = $result13;
				}				
				
				$query2 = '';
				$dtcond2 = " AND t1.is_dt='1'";
				$rwcond2 = " AND t1.is_rework='0'";
				$inpircond2 = " AND t1.is_internalpir='0'";				
				unset($report2);				
				$query2 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond2,$rwcond2,$inpircond2);
				$result2 = $dbase->executeQuery($query2,'multiple');
				if(count($result2) > 0){
					$dedicateddata[] = $result2;
				}					
				$query3 = '';
				$dtcond3 = " AND t1.is_dt='0'";
				$rwcond3 = " AND t1.is_rework='0'";
				$inpircond3 = " AND t1.is_internalpir='0'";				
				unset($report3);				
				$query3 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond3,$rwcond3,$inpircond3);
				$result3 = $dbase->executeQuery($query3,'multiple');	
				if(count($result3) > 0){
					$nondedicateddata[] = $result3;
				}
				
				$query121 = '';
				$dtcond121 = " AND t1.is_dt='0'";
				$rwcond121 = " AND t1.is_rework='1'";
				$inpircond121 = " AND t1.is_internalpir='0'";
				unset($report121);				
				$query121 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond121,$rwcond121,$inpircond121);
				$result121 = $dbase->executeQuery($query121,'multiple');
				if(count($result121) > 0){
					$nondedicatedreworkdata[] = $result121;
				}
				$query131 = '';
				$dtcond131 = " AND t1.is_dt='0'";
				$rwcond131 = " AND t1.is_rework='0'";
				$inpircond131 = " AND t1.is_internalpir='1'";
				unset($report131);				
				$query131 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond131,$rwcond131,$inpircond131);
				$result131 = $dbase->executeQuery($query131,'multiple');
				if(count($result131) > 0){
					$nondedicatedinterpirdata[] = $result131;
				}				
			}
				$overallactualhourssarr = array();
				$overallbillablehoursarr = array();
				$cnt1=0;
				for($i=0;$i<count($overalldata);$i++){
					for($j=0;$j<count($overalldata[$i]);$j++){
						$overallactualhourssarr[] = $overalldata[$i][$j]['actualhours'];
						$overallbillablehoursarr[] = $overalldata[$i][$j]['billablehours'];
						if($overalldata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins = $dbase->getminutes($overalldata[$i][$j]['presenthours']);
							$minimummins = $dbase->getminutes('06:00');
							if($presentmins <= $minimummins){
								$cnt1 += 0.5;
							}
							elseif($presentmins > $minimummins){
								$cnt1 += 1;
							}
							else{
								$cnt1 += 1;
							}
						}
						else{
							$cnt1 += 1;
						}
					}
				}
			
				$reworkcnt1=0;
				for($i=0;$i<count($overallreworkdata);$i++){
					for($j=0;$j<count($overallreworkdata[$i]);$j++){
						if($overallreworkdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins1 = $dbase->getminutes($overallreworkdata[$i][$j]['presenthours']);
							$minimummins1 = $dbase->getminutes('06:00');
							if($presentmins1 <= $minimummins1){
								$reworkcnt1 += 0.5;
							}
							elseif($presentmins1 > $minimummins1){
								$reworkcnt1 += 1;
							}
							else{
								$reworkcnt1 += 1;
							}
						}
						else{
							$reworkcnt1 += 1;
						}
					}
				}
			
				$interpircnt1=0;
				for($i=0;$i<count($overallinterpirdata);$i++){
					for($j=0;$j<count($overallinterpirdata[$i]);$j++){
						if($overallinterpirdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins2 = $dbase->getminutes($overallinterpirdata[$i][$j]['presenthours']);
							$minimummins2 = $dbase->getminutes('06:00');
							if($presentmins2 <= $minimummins2){
								$interpircnt1 += 0.5;
							}
							elseif($presentmins2 > $minimummins2){
								$interpircnt1 += 1;
							}
							else{
								$interpircnt1 += 1;
							}
						}
						else{
							$interpircnt1 += 1;
						}
					}
				}	
			
			
				$reworkcnt2=0;
				for($i=0;$i<count($nondedicatedreworkdata);$i++){
					for($j=0;$j<count($nondedicatedreworkdata[$i]);$j++){
						if($nondedicatedreworkdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins1 = $dbase->getminutes($nondedicatedreworkdata[$i][$j]['presenthours']);
							$minimummins1 = $dbase->getminutes('06:00');
							if($presentmins1 <= $minimummins1){
								$reworkcnt2 += 0.5;
							}
							elseif($presentmins1 > $minimummins1){
								$reworkcnt2 += 1;
							}
							else{
								$reworkcnt2 += 1;
							}
						}
						else{
							$reworkcnt2 += 1;
						}
					}
				}
			
				$interpircnt2=0;
				for($i=0;$i<count($nondedicatedinterpirdata);$i++){
					for($j=0;$j<count($nondedicatedinterpirdata[$i]);$j++){
						if($nondedicatedinterpirdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins2 = $dbase->getminutes($nondedicatedinterpirdata[$i][$j]['presenthours']);
							$minimummins2 = $dbase->getminutes('06:00');
							if($presentmins2 <= $minimummins2){
								$interpircnt2 += 0.5;
							}
							elseif($presentmins2 > $minimummins2){
								$interpircnt2 += 1;
							}
							else{
								$interpircnt2 += 1;
							}
						}
						else{
							$interpircnt2 += 1;
						}
					}
				}			
			

				$dedicatedactualhourssarr = array();
				$dedicatedbillablehoursarr = array();
				$dedicatedemp_array = array();
				for($i=0;$i<count($dedicateddata);$i++){
					for($j=0;$j<count($dedicateddata[$i]);$j++){
						$dedicatedemp_array[] = $dedicateddata[$i][$j]['empid'];
						$dedicatedactualhourssarr[] = $dedicateddata[$i][$j]['actualhours'];
						$dedicatedbillablehoursarr[] = $dedicateddata[$i][$j]['billablehours'];
					}
				}
				$dedicatedbilledhrsarr = array();
				//$tot_emp = count(array_unique($dedicatedemp_array));
				$tot_emp = $totempcnt;
				$dedicatedbilledhrsarr = ($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS);			

			
				$nondedicatedactualhourssarr = array();
				$nondedicatedbillablehoursarr = array();
				$cnt2=0;
				for($i=0;$i<count($nondedicateddata);$i++){
					for($j=0;$j<count($nondedicateddata[$i]);$j++){
						$nondedicatedactualhourssarr[] = $nondedicateddata[$i][$j]['actualhours'];
						$nondedicatedbillablehoursarr[] = $nondedicateddata[$i][$j]['billablehours'];
						if($nondedicateddata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins = $dbase->getminutes($nondedicateddata[$i][$j]['presenthours']);
							$minimummins = $dbase->getminutes('06:00');
							if($presentmins <= $minimummins){
								$cnt2 += 0.5;
							}
							elseif($presentmins > $minimummins){
								$cnt2 += 1;
							}
							else{
								$cnt2 += 1;
							}
						}
						else{
							$cnt2 += 1;
						}
					}
				}
			//echo $cnt1." - ".$reworkcnt1." - ".$interpircnt1;
			$reportdata1['utilization'] = ($dbase->addTime($overallbillablehoursarr,true) / ($cnt1 * $dbase->getminutes("08:30")))*100;
			//echo $dbase->addTime($nondedicatedbillablehoursarr,true)." - ".$dbase->addTime($dedicatedbilledhrsarr,true)." - ".$cnt1 * $dbase->getminutes("08:30");
			//$reportdata1['utilization'] = (($dbase->addTime($nondedicatedbillablehoursarr,true)+($dbase->addTime($dedicatedbilledhrsarr,true))) / ($cnt1 * $dbase->getminutes("08:30")))*100;
			$reportdata[] = $reportdata1;
			
			$reportdata3['utilization'] = (($dbase->addTime($dedicatedactualhourssarr,true)) / $dedicatedbilledhrsarr);
			$reportdata[] = $reportdata3;			
			
			$reportdata2['utilization'] = ($dbase->addTime($nondedicatedactualhourssarr,true) / ($dbase->addTime($nondedicatedbillablehoursarr,true)))*100;
			$reportdata[] = $reportdata2;

			$reportdata5['utilization'] = ($dbase->addTime($nondedicatedbillablehoursarr,true) / (($cnt2+$reworkcnt2+$interpircnt2) * $dbase->getminutes("08:30")))*100;
			$reportdata[] = $reportdata5;			
		}		
		
		$html = '';
		if(count($reportdata) > 0){
			$o=1;
			//$html .= '<div class="panel panel-default"><div class="panel-heading"><b>Dashboard</b></div><div class="panel-body">';
			$html .= '<div class="row">';
			for($y=0;$y<count($reportdata);$y++){
				$yy = $y+1;		
				if(round($reportdata[$y]['utilization'],0) > 100){
					$max = round($reportdata[$y]['utilization'],0);
				}
				else{
					$max = 100;
				}
				if($y == count($reportdata)-1){
					$html .='</div><div class="row">';
				}
				$html .= '<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading"><b>'.$reportname_array[$yy].'</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage'.$yy.'" ></canvas><div class="text-center" style="font-size:14px;">'.$reportname_array[$yy].' - <b>'.round($reportdata[$y]['utilization'],0).' %</b></div><div class="text-center" style="font-size:12px;">'.$reportformula_array[$yy].'</div>
									<script>
										var opts'.$yy.' = {
											lines: 12,
											angle: 0.05, 
											lineWidth: 0.19, 
											radiusScale: 1,
											pointer: {
												length: 0.5, 
												strokeWidth: 0.035, 
												color: "#000000"
											},
											  limitMax: false,
											  limitMin: false, 											
											generateGradient: true,
											highDpiSupport: true,
											staticLabels: {
												font: "10px sans-serif",
												labels: ['.round($reportdata[$y]['utilization'],0).'],
												color: "#000000", 
												fractionDigits: 0 
											},
										staticZones: [
											   {strokeStyle: "'.$utilization_legend_color['r'].'", min: 0, max: 80},
											   {strokeStyle: "'.$utilization_legend_color['a'].'", min: 80, max: 95},
											   {strokeStyle: "'.$utilization_legend_color['g'].'", min: 95, max: '.$max.'},
											],											
										};
										var target'.$yy.' = document.getElementById("utilization_guage'.$yy.'");
										var gauge'.$yy.' = new Gauge(target'.$yy.').setOptions(opts'.$yy.');
										gauge'.$yy.'.maxValue = 100;
										gauge'.$yy.'.setMinValue(0);
										gauge'.$yy.'.animationSpeed = 32;
										gauge'.$yy.'.set('.round($reportdata[$y]['utilization'],0).');
									</script>
							</div>
					</div>
				</div>';
				
			}	
			//$html .= '</div>';
			//$html .= '<div class="row">';
			$html .= '<div class="col-sm-4">
					<ul class="list-group">';
					foreach($report_legend as $key => $val){
						$expval = explode(" : ",$val); 
						$html .= '<li class="list-group-item" style="font-size:12px;"><b>'.$expval[0].'</b> : '.$expval[1].'</li>';
					}
					$html .= '</ul></div>';
			$html .= '<div class="col-sm-2">
					<ul class="list-group">';
					foreach($utilization_legend_color as $key => $val){
						$html .= '<li class="list-group-item" style="font-size:12px;"><div style="position:relative;padding-left:5px;padding-right:5px;float:left;text-align:center;">
											<span class="label label-default" style="background-color:'.$val.';">&nbsp;</span></div>'.$utilization_legend_label[$key].'</li>';
					}
					$html .= '</ul></div>';			
			$html .= '</div>';
		//	$html .= '</div></div>';
		}
		echo $html;exit;
	}
}
?>

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{ 
		$currentdatearr = explode(" ",date('F Y'));
		$currentmonth = date("m",strtotime($currentdatearr[0]));
		$currentyear = $currentdatearr[1];
			$currentyear =date("Y");
			$min = $currentyear - 1;
			$max = $currentyear+1;
			$yearlist .="<option value='".$min.'-'.$currentyear."'>".$min."-".$currentyear."</option>";
			for($i=$currentyear;$i<=$max; $i++){
				$j=$i+1;
				if($i== $currentyear){
				$yearlist .="<option value='".$i.'-'.$j."' selected>".$i."-".$j."</option>";
				}else{
					$yearlist .="<option value='".$i.'-'.$j."'>".$i."-".$j."</option>";
				}
			
			}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
	  <link type="text/css" href="css/chosen.css" rel="stylesheet" />
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	  <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	  <script type="text/javascript" src="js/chosen.jquery.js"></script>
	  <script type="text/javascript" src="js/notify.js"></script>
	  <script>
		  var montharr = '<?php echo json_encode($master->getmonths());?>';
		  var currentyear = '<?php echo $currentyear;?>';
		  var currentmonth = '<?php echo (int) $currentmonth;?>';
</script>
      <script type="text/javascript">
		  
		  function lockdata(stindex,index,isread){
			  var rtflag = true;var monthnpsflag = true;var quarternpsflag = true;
			  var achievedflag = true;
			  for(var s=stindex;s<=index;s++){
				 if($("#revenue_target_"+s).val() == "" || $("#revenue_target_"+s).val() == 0){
					 rtflag = false;
					 $.notify("Please enter Target($K)", "error");
					 return false;
				 }
			  }
			  for(var s=stindex;s<=index;s++){
				 if($("#month_nps_"+s).val() == "" || $("#month_nps_"+s).val() == 0){
					 monthnpsflag = false;
					 $.notify("Please enter  NPS-Month", "error");
					 return false;
				 }else if($("#month_nps_"+s).val() >100){
						   $.notify("Please enter NPS-Month less than 100", "error");
						  }
			  }
			 	 if($("#quarter_nps_"+index).val()  == "" || $("#quarter_nps_"+index).val() == 0){
					 quarternpsflag = false;
					 $.notify("Please enter NPS-Quarter", "error");
					 return false;
				 }else if($("#quarter_nps_"+index).val() >100){
						   $.notify("Please enter NPS-Quarter less than 100", "error");
						  }
			  
			  for(var s=stindex;s<=index;s++){
				 if($("#achieved_"+s).val() == "" || $("#achieved_"+s).val() == 0){
					 achievedflag = false;
					 $.notify("Please enter Actual($K)", "error");
					 return false;
				 }
			  }
			  if(rtflag && monthnpsflag && quarternpsflag && achievedflag){
	$('<div>Are you sure to lock the quarter.<br/>If Yes then no modification</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
			buttons: {
			"Yes": function() {
				
		 $.ajax({
				 url:"lockkpidata.php",
				 type:"POST",
			 	data:{kpiid:[$("#kpiid_"+(index-2)).val(),$("#kpiid_"+(index-1)).val(),$("#kpiid_"+index).val()],
					  monthid:[$("#monthid_"+(index-2)).val(),$("#monthid_"+(index-1)).val(),$("#monthid_"+index).val()],
					  revenue_target:[$("#revenue_target_"+(index-2)).val(),$("#revenue_target_"+(index-1)).val(),$("#revenue_target_"+index).val()],
					  month:[$("#monthid_"+(index-2)).val(),$("#monthid_"+(index-1)).val(),$("#monthid_"+index).val()],
					  year:[$("#year_"+(index-2)).val(),$("#year_"+(index-1)).val(),$("#year_"+index).val()],
					  achieved:[$("#achieved_"+(index-2)).val(),$("#achieved_"+(index-1)).val(),$("#achieved_"+index).val()],
					  month_nps:[$("#month_nps_"+(index-2)).val(),$("#month_nps_"+(index-1)).val(),$("#month_nps_"+index).val()],
					  quarter_nps:$("#quarter_nps_"+index).val()
					 },
					  dataType:"JSON",
					 success:function(data){
					 if(data != ""){
						 if(data.message == "success"){
							 $.notify("Saved successfully", "success");
							loadkpidata();
						 }else{
						 $.notify("Saved error", "error");
						 }
					 }
				 }
			 });
					$("div").remove(".ui-widget-overlay");
					$(this).dialog("close");
			},
			"No": function() {
				$("div").remove(".ui-widget-overlay");	
				$(this).dialog("close");
			 }
			}
		});	
		$('<div class="ui-widget-overlay"></div>').insertBefore(".ui-dialog");
			  }
		  }
	     $(document).ready(function() {
			 $(".chosen-select").chosen({"disable_search": true});
             $("#addkpiform").validate();
			jQuery.validator.addMethod("numchk",function(value, element){
					if (this.optional(element)) {
						return true;
					} else if (!/^[\d./-]*$/.test(value)) {
						return false;
					} 
					return true;
			}, "Allow digits");				 
         });
      </script> 
     		 
   </head>
    <body>
		<?php include("menu.php");?>
		<?php include_once("includebootstrap.php"); ?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'){ $deptid =1;?>
		<style>       
			#kpilisttbl table {
            width: 100%;
        }
	   #kpilisttbl thead, #kpitbdy, #kpilisttbl tr, #kpilisttbl td, #kpilisttbl th { display: block; }

       #kpilisttbl tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

       #kpilisttbl  thead #kpilisttbl th {
            height: 30px;
			background-color:blue
        }

        #kpitbdy {
            height: 580px;
            overflow-y: auto;
        }
        #kpilisttbl tbody td, #kpilisttbl thead  th {
            width: 120px;
            float: left;
        }
		#kpihead{
		background-color:#286090;color:#ffffff
		}
		.quartertd{width:15px}
		.ui-dialog-titlebar-close{
			display:none
		}

	
		</style>
<div class="container">
	
	<form id="frm_kpilist" name="frm_kpilist"  action="" method="post">
		 <div class="row">
			 <div class="col-sm-12">
			  <div class="col-sm-5">
				  <div class="form-group">
					<h3>KPI Data   <button type="button" id="backbtn" name="backbtn" onclick="window.history.back();" class="btn btn-primary btn-sm"> Back </button></h3>
				  </div>
				
			 </div>
			 <div class="col-sm-6" style="padding-top:20px">
				 <div class="col-sm-4"><label>Select Financial Year</label></div>
				 
				 <div class="col-sm-2">
			<select id="fyyear" name="fyyear" class="form-control chosen-select required" style="width:150px;" onchange="loadkpidata()">
				<?php echo $yearlist;?>
			</select>
			 </div>
			</div></div>
				</div> 
		<div class="row">
			<div class="container">
				<div class="col-sm-9">
			 <table class="table table-striped"id="kpilisttbl" style="border:1px solid #204d74 ">
			 <thead id="kpihead">
				<tr>
					<th style="width:60px">Term</th>
					<th>Month-Year</th>
					<th>Target ($K)</th>
					<th>Actual($K)</th>
					<th>NPS-Month</th>
					<th>NPS-Quarter</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="kpitbdy">
			</tbody>
			</table>
			</div>
				</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
			<div class="col-sm-4"></div>
			<div class="col-sm-6">
				 <input type="button" id="savebutton" name="savebutton" class="btn btn-primary" value="Save">
			</div>
			</div>
		</div>
	</form>
</div>
<script>
	function loadkpidata(){
			$.ajax({
				url:"getkpilist.php",
				type:"POST",
				data:{fyyear:$("#fyyear").val()},
				success:function(data){
					$("#kpitbdy").html(data);
					if($("#isreadonly").val() == 1 && $("#isunlock") == 0){
						$("#savebutton").hide();
					}else{
						$("#savebutton").show();
					}
				
			    }
			});
		
	}
		var months = [
    'January', 'February', 'March', 'April', 'May',
    'June', 'July', 'August', 'September',
    'October', 'November','December'
    ];
	function validateval(obj,selindex=""){
		if(obj.val() != "" && !/^[\d./-]*$/.test(obj.val())){
			obj.addClass("error");
			//obj.after("error");
			return false;
		}
	}
 $(document).ready(function(){
		loadkpidata();
		 $("#frm_kpilist").validate();
	 	jQuery.validator.addMethod("numchk",function(value, element){
					if (this.optional(element)) {
						return true;
					}else if (!/^[\d./-]*$/.test(value)) {
						return false;
					}
					return true;
			}, "Allow digits");	
	  
	 	jQuery.validator.addMethod("npschk",function(value, element){
					if (this.optional(element)) {
						return true;
					}else if (!/^[\d./-]*$/.test(value)) {
						return false;
					} else if (/^[\d./-]*$/.test(value)) {
						if(value >100){
						return false;
						}
					} 
					return true;
			}, "Enter value less than 100");
	 
	 $("#savebutton").click(function(){
		if($(":text.error").length == 0){
			   showconfirmmessage("Are you sure to submit ?",1);
		}else{
			return false;
		}
		
	 });
	});	

	function savedata(index,isread){
	if(isread == 0){
	/*if($("#monthid_"+index).val() >0 && $("#year_"+index).val() >0 && $("#revenue_target_"+index).val() >0 &&
	  $("#achieved_"+index).val()>0 && $("#month_nps_"+index).val() >0 && $("#month_nps_"+index).val() <=100 
	  ){*/
		if($("#monthid_"+index).val() >0 && $("#year_"+index).val() >0 && $("#revenue_target_"+index).val() >0
	  ){
		 $.ajax({
				 url:"savekpidata.php",
				 type:"POST",
				 data:{editid:$("#kpiid_"+index).val(),monthid:$("#monthid_"+index).val(),year:$("#year_"+index).val(),
					  revenue_target: $("#revenue_target_"+index).val() ,achieved:$("#achieved_"+index).val(),
					  month_nps:$("#month_nps_"+index).val(),quarter_nps:$("#quarter_nps_"+index).val(),rowedit:1},
					  dataType:"JSON",
					 success:function(data){
					 if(data != ""){
						 if(data.message == "success"){
							 $.notify("Saved successfully", "success");
						 }else{
						 $.notify("Saved error", "error");
						 }
					 }
				 }
			 });
	}else{
		var errormsg ="";
		if($("#revenue_target_"+index).val() == ""){
			errormsg += " Target ,";
		}
		if(errormsg == ""){
			errormsg +=" proper value(s)";
		}else{
		errormsg = errormsg.slice(0,-1);
		}
		 $.notify("Please fill "+errormsg, "error");
	}
	}
	}
	function submitkpiform(){
	
		 $.ajax({
				 url:"savekpidata.php",
				 type:"POST",
				 data:$("#frm_kpilist").serializeArray(),
				 dataType:"JSON",
				 success:function(data){
					 if(data != ""){
						 if(data.message == "success"){
							 
							 showmessage("Saved successfully",1);
						 }else{
							 showmessage("Save Error",1);
						 }
					 }
				 }
			 });
	}
	function showmessage(msg,type){
	$("<div>"+msg+"</div>").dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"OK": function() {
					$("div").remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					if(type == 1){
						loadkpidata();
					}
				}
			}
		});
	}
	function showconfirmmessage(msg,type){
	$('<div>'+msg+'</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
			buttons: {
			"Yes": function() {
			
				if(type == 1){
					submitkpiform();
				}else if(type ==2 ){
					loadkpidata();
				}
					$("div").remove(".ui-widget-overlay");
					$(this).dialog("close");
			},
			"No": function() {
				$("div").remove(".ui-widget-overlay");	
				$(this).dialog("close");
			 }
			}
		});	
		$('<div class="ui-widget-overlay"></div>').insertBefore(".ui-dialog");
	}
		</script>
		<?php }?>
</body>
</html>
<?php } ?>

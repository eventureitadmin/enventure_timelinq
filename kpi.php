<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
	<title>Enventure - Timesheet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!---<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />-->
	<link type="text/css" href="css/chosen.css" rel="stylesheet" />	
	<link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery_confirm.css" rel="stylesheet" />
	<link type="text/css" href="css/custom.css" rel="stylesheet" />
	<link type="text/css" href="css/css-circular-prog-bar.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jqueryui.js"></script>	   
	<script type="text/javascript" src="js/jquery_validate.js"></script>
	<script type="text/javascript" src="js/jquery_confirm.js"></script>
	<script type="text/javascript"  src="js/easyconfirm.js"></script>
	<script type="text/javascript" src="js/date.js"></script>
	<script type="text/javascript" src="js/chosen.jquery.js"></script>
	<!----<script type="text/javascript" src="js/bootstrap.js"></script>-->
	<script type="text/javascript" src="js/gauge.js"></script>
	<script type="text/javascript" src="js/chart2_8.js"></script>
	<script type="text/javascript" src="js/canvas.js"></script>
	<style>
		.panel-heading {
		text-align:center
		}
		.progress-circle .over50 .p89{
			background-color:"blue" ;
		}
	.over50 .first50-bar {
			background-color:"blue !important" ;
		}
		.value-bar {
			background-color:"blue";
		}
		
	</style>
</head>
<body>
	
<?php include("menu.php");?>
	
<?php if($_SESSION['timesheet']['ISADMIN']=='1'){ $deptid =1;?>
<div class="container-fluid">
<form id="frm_kpi"  action="" method="post">
	 <div class="row">
		 
		 <div class="col-sm-3" id="monthdiv">
		  <div class="form-group">
			<label for="monthid">Select Month</label>
			<select id="monthid" name="monthid" class="form-control chosen-select" style="width:300px;" >
			    <option value='01'>January</option>
				<option value='02'>February</option>
				<option value='03'>March</option>
				<option value='04'>April</option>
				<option value='05'>May</option>
				<option value='06'>June</option>
				<option value='07'>July</option>
				<option value='08'>August</option>
				<option value='09'>September</option>
				<option value='10'>October</option>
				<option value='11'>November</option>
				<option value='12'>December</option>

			</select>
		  </div>		 
	   </div>
		<div class="col-sm-3">
		  <div class="form-group">
			<label for="year">Select Year</label>
			<select id="year" name="year" class="form-control chosen-select" style="width:300px;">
			</select>
		  </div>		 
	   </div>
	
	<div class="col-sm-3">
		  <div class="form-group">
			  <label for="submit"></label><br>
			  <button type="button" id="submitbutton" name="submitbutton" onclick="submitform();" class="btn btn-primary"> Submit </button>
		  </div>		 
	  </div>
	
	
	</div> 	
	
</form>
</div>
<div id="chart-container" class="container-fluid">
	
<?php 
	
	$reportname_array = array("1"=>"Month Revenue","2"=>"Quater Revenue (QTD)","3"=>"Resource Billing Utilization","4"=>"Non Dedicated Project Efficiency
","5"=>"Dedicated Team Utilization");
	$rbu = array("CEG"=>89,"MRO"=>98,"EAS"=>100);
	$ndpu = array("CEG"=>89,"MRO"=>98);
	$dtu = array("Panasonic"=>89,"Vexos"=>108,"Fuji"=>53,"Kitron"=>78);
	$html ="";?>
	<div class="row"><div class="col-sm-3"><h2>KPI January 2020</h2></div></div>
	<div class="row">
	
	<div class="col-sm-3">
					<div class="panel panel-default">
						<div class="panel-heading"><b>January Revenue</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage1"></canvas>
									<div class="text-center" style="font-size:14px;">
							<?php echo $reportname_array[1];?>- <b>71 %</b></div>
								<div class="text-center" style="font-size:12px;"></div>
									<script>
										var opts1 = {
											lines: 12,
											angle: 0.05, 
											lineWidth: 0.19, 
											radiusScale: 1,
											pointer: {
												length: 0.5, 
												strokeWidth: 0.035, 
												color: "#000000"
											},
											  limitMax: false,
											  limitMin: false, 											
											generateGradient: true,
											highDpiSupport: true,
											staticLabels: {
												font: "10px sans-serif",
												labels: [71],
												color: "#000000", 
												fractionDigits: 0 
											},
										staticZones: [
											   {strokeStyle: "<?php echo $utilization_legend_color['r'];?>", min: 0, max: 80},
											   {strokeStyle: "<?php echo $utilization_legend_color['a'];?>", min: 80, max: 95},
											   {strokeStyle: "<?php echo $utilization_legend_color['g'];?>", min: 95, max:100},
											],											
										};
										var target1 = document.getElementById("utilization_guage1");
										var gauge1 = new Gauge(target1).setOptions(opts1);
										gauge1.maxValue = 100;
										gauge1.setMinValue(0);
										gauge1.animationSpeed = 32;
										gauge1.set(73);
									</script>
							</div>
					</div>
				</div>
	<div class="col-sm-3">
					<div class="panel panel-default">
						<div class="panel-heading"><b>Quarter Revenue</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage"></canvas>
									<div class="text-center" style="font-size:14px;">
							Quarter Revenue- <b>22%</b></div>
								<div class="text-center" style="font-size:12px;"></div>
									<script>
										var opts1 = {
											lines: 12,
											angle: 0.05, 
											lineWidth: 0.19, 
											radiusScale: 1,
											pointer: {
												length: 0.5, 
												strokeWidth: 0.035, 
												color: "#000000"
											},
											  limitMax: false,
											  limitMin: false, 											
											generateGradient: true,
											highDpiSupport: true,
											staticLabels: {
												font: "10px sans-serif",
												labels: [22],
												color: "#000000", 
												fractionDigits: 0 
											},
										staticZones: [
											   {strokeStyle: "<?php echo $utilization_legend_color['r'];?>", min: 0, max: 80},
											   {strokeStyle: "<?php echo $utilization_legend_color['a'];?>", min: 80, max: 95},
											   {strokeStyle: "<?php echo $utilization_legend_color['g'];?>", min: 95, max:100},
											],											
										};
										var target1 = document.getElementById("utilization_guage");
										var gauge1 = new Gauge(target1).setOptions(opts1);
										gauge1.maxValue = 100;
										gauge1.setMinValue(0);
										gauge1.animationSpeed = 32;
										gauge1.set(22);
									</script>
							</div>
					</div>
				</div>
	<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading"><b>Resource Billing Utilization</b><br/>
						</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-3"><p style="text-align:center;margin-left:50px">CEG</p>
										<div class="progress-circle over50 p60">
										   <span>60%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid red"></div>
											  <div class="value-bar" style="border: 0.45em solid red"></div>
										   </div>
										</div>
									</div>
									<div class="col-sm-3"><p style="text-align:center;margin-left:70px">MRO</p>
										<div class="progress-circle over50 p80" style="margin-left:40px">
										   <span>80%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid yellow"></div>
											  <div class="value-bar" style="border: 0.45em solid yellow"></div>
										   </div>
										</div>
									</div>
									<div class="col-sm-3" style="text-align:center;margin-left:60px"><p>EAS</p>
										<div class="progress-circle over50 p100" style="text-align:center;margin-right:30px">
										   <span>100%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid green"></div>
											  <div class="value-bar" style="border: 0.45em solid green"></div>
										   </div>
										</div>
									</div>
								</div>
							<p style="margin-left:140px">Billed hours / Available hours</p>
							</div>
					</div>
				</div>
	<?php $html .= '<div class="col-sm-2">
					<ul class="list-group">';
					foreach($utilization_legend_color as $key => $val){
						$html .= '<li class="list-group-item" style="font-size:12px;"><div style="position:relative;padding-left:5px;padding-right:5px;float:left;text-align:center;">
											<span class="label label-default" style="background-color:'.$val.';">&nbsp;</span></div>'.$utilization_legend_label[$key].'</li>';
					}
					$html .= '</ul></div>'; echo $html;?>
				
 </div>
	<!----<div class="row">
	<div class="col-sm-3">
					<div class="panel panel-default">
						<div class="panel-heading"><b>NPS</b></div>
							<div class="panel-body">
								<div class="row">
									
									<div class="col-sm-3" ><p style="text-align:center;margin-left:120px"></p>
										<div class="progress-circle over50 p100" style="margin-left:100px">
										   <span>10</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid #FFA500"></div>
											  <div class="value-bar" style="border: 0.45em solid #FFA500"></div>
										   </div>
										</div>
									</div>
								</div>
							
							</div>
					</div>
				</div>
	
	<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading"><b>Non Dedicated Project Efficiency</b></div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-3"><p style="text-align:center;margin-left:120px">CEG</p>
										<div class="progress-circle over50 p70" style="margin-left:100px">
										   <span>70%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid red"></div>
											  <div class="value-bar" style="border: 0.45em solid red"></div>
										   </div>
										</div>
									</div>
									<div class="col-sm-3"><p style="text-align:center;margin-left:130px">MRO</p>
										<div class="progress-circle over50 p100" style="margin-left:100px">
										   <span>100%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid green"></div>
											  <div class="value-bar" style="border: 0.45em solid green"></div>
										   </div>
										</div>
									</div>
								</div>
							
							</div>
					</div>
				</div>
		
	</div>--->
<?php $html ="";
			$html .= '<div class="row">';
			$html .= '<div class="col-sm-4">
					<ul class="list-group">';
					foreach($report_legend as $key => $val){
						$expval = explode(" : ",$val); 
						$html .= '<li class="list-group-item" style="font-size:12px;"><b>'.$expval[0].'</b> : '.$expval[1].'</li>';
					}
					$html .= '</ul></div>';echo $html;?>
	
		<div class="col-sm-5">
					<div class="panel panel-default">
						<div class="panel-heading"><b>Dedicated Team Utilization</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage3"></canvas><div class="text-center" style="font-size:14px;">Dedicated Team Utilization</div><div class="text-center" style="font-size:12px;"></div>
								<!-----	<script>
     

var ctx = document.getElementById("utilization_guage3").getContext('2d');

var original = Chart.defaults.global.legend.onClick;
Chart.defaults.global.legend.onClick = function(e, legendItem) {
  update_caption(legendItem);
  original.call(this, e, legendItem);
};

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Panasonic", "Vexos", "Fuji", "Kitron", "Decernis", "Aquitas", "Synovos-3BID"],
    datasets: [ {
      label: 'Billing',
      backgroundColor: "rgba(255,153,0,1)",
      data: [89, 108, 53, 73, 90, 50, 47],
    }]
  }
});

var labels = {
  "apples": true,
  "oranges": true
};

var caption = document.getElementById("caption");

var update_caption = function(legend) {
  labels[legend.text] = legend.hidden;

  var selected = Object.keys(labels).filter(function(key) {
    return labels[key];
  });

  var text = selected.length ? selected.join(" & ") : "nothing";

  caption.innerHTML = "The chart is displaying " + text;
};


							</script>--->
								<script>
var ctx = document.getElementById('utilization_guage3').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        //labels: ["Panasonic", "Vexos", "Fuji", "Kitron", "Decernis", "Aquitas", "Synovos-3BID","Synovos-OEM","Synovos-Expedite"],
        labels: ["Aquitas","Decernis","Fuji", "Kitron","Panasonic","Synovos-Expedite" , "Synovos-3BID","Synovos-OEM", "Vexos"],
       // labels: ["Panasonic", "Vexos", "Fuji", "Kitron", "Decernis", "Aquitas", "Synovos-3BID","Synovos-OEM","Synovos-Expedite"],
   
        datasets: [{
			
            label: ['Panasonic'],
             data: [50,80,53,73,89,103,47,0,108],
           //  data: [89, 108, 53, 73, 90, 50, 47,0,103],
			backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2",
							  "#b3ffff","ff0066"],
           /* backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],*/
            borderWidth: 1
        }]
    },
    options: {
		legend: {
    	display: false
    },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
							</div>
					</div>
				</div>
	
		<div class="row"><div class="col-sm-5">
					<div class="panel panel-default">
						<div class="panel-heading"><b>Non Dedicated Project Efficiency
</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage7"></canvas><div class="text-center" style="font-size:14px;">Dedicated Team Utilization</div><div class="text-center" style="font-size:12px;"></div>
						</div></div></div>
	

</div>
<?php } ?>
<div id="overlay">
  <div id="overlay-text"><h5><img src="images/busy.gif" />Graphs are getting generated.Please wait!</h5></div>
</div>	

<script type="text/javascript">
	function revenuetype(){alert('revenuetype');
			}
 $(document).ready(function(){
	 $('#overlay').css('visibility', 'hidden');
	  $("#frm_kpi").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			//$("#frm_kpi").submit();
		});
		
	 $(".chosen-select").chosen();
	 	getyear();
		$("#revenuetype").change(function(){
		 if($(this).val() == 1){
				$("#quaterdiv").css("display","none");
			 $("#quater_chosen").css("width","300px");
				$("#monthdiv").css("display","block");
			}else if($(this).val() == 2){
				$("#quaterdiv").css("display","block");
				$("#monthdiv").css("display","none");
				 $("#quater_chosen").css("width","300px");
			}
		});
	});	
	function getyear(){
	 var max = new Date().getFullYear();
   	 var min = max - 20;
		var years ="";
   		for(var i = max; i>=min; i--){
			years +="<option value='"+i+"'>"+i+"</option>";
	     }	 
		$("#year").html(years);
		$("#year").trigger("chosen:updated");
	 }
	/* Code By Webdevtrick ( https://webdevtrick.com ) */
function Circlle(el){
  $(el).circleProgress({fill: {color: '#ff5c5c'}})
    .on('circle-animation-progress', function(event, progress, stepValue){
					$(this).find('strong').text(String(stepValue.toFixed(2)).substr(2)+'%');
			});  
};

function submitform(){
	$("#chart-container").css("display","block");
	//$("#frm_dashboard").submit();
	/*var revenuetype = $("#revenuetype").val();
		if(revenuetype !='' && revenuetype != null){
			$('#overlay').css('visibility', 'visible');
			$.post("kpi.php",$.param($("#frm_kpi").serializeArray()), function(data, status){
				if(data!=''){
					$('#overlay').css('visibility', 'hidden');
				   $("#chart-container").html(data);
				}
				else{
					$.alert({
						title: 'Alert',
						content: 'No Records',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});	
				}
			});	
		}
		else{
			$("#chart-container").html('');
		}	*/
}
	
</script>	

<?php //} ?>
	</body>
	</html>
	<?php	/*	$html .= '<div class="row">';
			$html .= '<div class="col-sm-4">
					<ul class="list-group">';
					foreach($report_legend as $key => $val){
						$expval = explode(" : ",$val); 
						$html .= '<li class="list-group-item" style="font-size:12px;"><b>'.$expval[0].'</b> : '.$expval[1].'</li>';
					}
					$html .= '</ul></div>';
			$html .= '<div class="col-sm-2">
					<ul class="list-group">';
					foreach($utilization_legend_color as $key => $val){
						$html .= '<li class="list-group-item" style="font-size:12px;"><div style="position:relative;padding-left:5px;padding-right:5px;float:left;text-align:center;">
											<span class="label label-default" style="background-color:'.$val.';">&nbsp;</span></div>'.$utilization_legend_label[$key].'</li>';
					}
					$html .= '</ul></div>';			
			$html .= '</div>';*/
 }
?>

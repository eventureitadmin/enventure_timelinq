<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_GET){
		$entereddate=date('Y-m-d',strtotime($_GET['date']));
		$empid = $_GET['empid'];
		$html = '';
		$enteredlistQuery ="SELECT n.id,n.activity,n.totalparts,n.calculatedhours,n.actualhours,(SELECT p3.pirno FROM pirlist p3 WHERE p3.id=n.pirid) as pirno,(SELECT p4.projectname FROM projectlist p4 WHERE p4.id=n.proid) as project FROM (SELECT m.id,m.activity,m.totalparts,m.calculatedhours,m.actualhours,(SELECT p1.pirno FROM pirmaster p1 WHERE p1.id = m.`pirmaster_id`) as pirid,(SELECT p2.projectname FROM pirmaster p2 WHERE p2.id = m.`pirmaster_id`) as proid FROM (SELECT t.`id` , t.`pirmaster_id` , t.`activity_id` , (SELECT a.name FROM activity a WHERE a.id = t.activity_id) AS activity, t.`totalparts` , t.`calculatedhours` , t.`actualhours` FROM `timeentry` t WHERE t.`isActive` = '1' AND t.`entrydate` = '".$entereddate."' AND t.`employee_id` = '".trim(mysql_escape_string($empid))."') m ) n ORDER BY n.id ASC";
$enteredlistResult = $dbase->executeQuery($enteredlistQuery,'multiple');
$html .= '<table id="dataentereddetailstable" border="0" cellpadding="0" cellspacing="0" align="center"  width="100%">
<thead>
	<tr>
		<td width="16%"><b>PIR No</b></td>
		<td width="16%"><b>Project</b></td>
		<td width="16%"><b>Activity</b></td>
		<td width="14%"><b>No of Parts</b></td>
		<td width="14%"><b>Budgeted Hours ( HH:MM )</b></td>
		<td width="14%"><b>Actual Hours ( HH:MM )</b></td>
		<td width="20%"><b>Del</b></td>
	</tr>
	</thead>
	<tbody>';
for($j=0;$j<count($enteredlistResult);$j++){
	$html .= '<tr id="row1">
		<td width="16%">'.$enteredlistResult[$j]['pirno'].'</td>
		<td width="16%">'.$enteredlistResult[$j]['project'].'</td>
		<td width="16%">'.$enteredlistResult[$j]['activity'].'</td>
		<td width="14%">'.$enteredlistResult[$j]['totalparts'].'</td>
		<td width="14%">'.substr($enteredlistResult[$j]['calculatedhours'], 0, -3).'</td>
		<td width="14%">'.substr($enteredlistResult[$j]['actualhours'], 0, -3).'</td>
		<td width="20%"><img src="images/remove.png" height="16" width="16" alt="RemoveRow" onclick="deleterow('.$enteredlistResult[$j]['id'].')" /></td>
	</tr>';	
}
	$html .= '</tbody>
</table>';
		echo $html;
	}
}
?>
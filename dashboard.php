<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
	<title>Enventure - Timesheet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!----<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />-->
	<link type="text/css" href="css/chosen.css" rel="stylesheet" />	
	<link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery_confirm.css" rel="stylesheet" />
	<link type="text/css" href="css/custom.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jqueryui.js"></script>	   
	<script type="text/javascript" src="js/jquery_validate.js"></script>
	<script type="text/javascript" src="js/jquery_confirm.js"></script>
	<script type="text/javascript"  src="js/easyconfirm.js"></script>
	<script type="text/javascript" src="js/date.js"></script>
	<script type="text/javascript" src="js/chosen.jquery.js"></script>
	<!----<script type="text/javascript" src="js/bootstrap.js"></script>--->
	<script type="text/javascript" src="js/gauge.js"></script>
</head>
<body>
<?php include("menu.php");?>

<?php if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
	<td  width="80%" valign="top">
		
	<?php include_once("includebootstrap.php"); ?>
<div class="container-fluid">
<form id="frm_dashboard"  action="" method="post">
	 <div class="row">
	  <div class="col-sm-3">
		  <div class="form-group">
			<label for="email">From Date </label>
			<input type="text" id="from_date" name="from_date" class="form-control" value="<?php if($from_date==''){echo date('01-M-Y'); } else{ echo $from_date; }  ?>" />  
		  </div>		 
	   </div>
	  <div class="col-sm-3">
		  <div class="form-group ">
			<label for="email">To Date</label>
			<input type="text" id="to_date" name="to_date" class="form-control" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" />
		  </div>		 
	</div>
	  <div class="col-sm-3">
		  <div class="form-group">
			<label for="email">Select Department</label>
			<select id="department_id" name="department_id" class="form-control required"  onchange="getsubdepartment();">
				<option value="">-Select-</option>
				<?php
					$depart_cond = "";
					if($_SESSION['timesheet']['ISADMIN']=='0' || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){
						$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
					}																									  
					$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
					$deptResult = $dbase->executeQuery($deptQuery,"multiple");
					for($i=0;$i<count($deptResult);$i++){
						if($deptResult[$i]['id']==$deptid){
							$select = "selected";
						}
						else{
							$select = "";
						}
						echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
					}
				?>
				</select>
		  </div>		 
		</div>
	  <div class="col-sm-3">
		  <div class="form-group">
			<label for="email">Select Sub Department</label><br>
<select id="subdepartment_id" name="subdepartment_id[]" data-placeholder="Select Sub Department" class="form-control required chosen-select-multi" multiple style="width:200px;">
	</select>	
	<label for="subdepartment_id" class="error" style="display:none" >This field is required.</label>
		  </div>		 
	  </div>
	</div> 
	 <div class="row">
	  <div class="col-sm-3">
		  <div class="form-group">
			<label for="email">Select PIR</label>
			<select id="pirmaster_id" name="pirmaster_id" class="form-control chosen-select" style="width:250px;" onchange="getproject();">
			<option value="">-All-</option>
			</select>
		  </div>		 
	   </div>
	  <div class="col-sm-3">
		  <div class="form-group">
			<label for="email">Select Project</label>
			<select id="project_id" name="project_id" class="form-control">
				<option value="">-All-</option>
			</select>
		  </div>		 
	</div>
	<!--  <div class="col-sm-3">
		  <div class="form-group">
			<label for="email">Select KPI</label>
			<select id="kpis" name="kpis" class="form-control required" >
				<option value="">-Select-</option>
				<option value="1" <?php if($kpis=='1'){ ?>selected<?php } ?>>Billing Utilization</option>
				<option value="2" <?php if($kpis=='2'){ ?>selected<?php } ?>>Non Dedicated Project Efficiency</option>
				<option value="3" <?php if($kpis=='3'){ ?>selected<?php } ?>>Dedicated Team Utilization</option>
				<option value="4" <?php if($kpis=='4'){ ?>selected<?php } ?>>Non Dedicated Utilization</option>
				<option value="5" <?php if($kpis=='5'){ ?>selected<?php } ?>>Dedicated Team Bench Utilization</option>
			</select>
		  </div>		 
		</div>-->
	  <div class="col-sm-3">
		  <div class="form-group">
			  <label for="email"></label><br>
			  <button type="button" id="submitbutton" name="submitbutton" onclick="submitform();" class="btn btn-primary"> Submit </button>
		  </div>		 
	  </div>
	</div> 	
</form>
</div>
<div id="chart-container" class="container">
</div>
<div id="overlay">
  <div id="overlay-text"><h5><img src="images/busy.gif" />Graphs are getting generated.Please wait!</h5></div>
</div>
	</td></tr></table>
<?php } ?>
	
</body>
<script type="text/javascript">
 $(document).ready(function(){
	 $('#overlay').css('visibility', 'hidden');
 $('#from_date').datepicker({
	inline: true,
	dateFormat: 'dd-M-yy',
	maxDate:0,
	changeMonth: true,
	changeYear: true,
	yearRange: "-10:+0",
	onSelect: function(date){
		var dates = date.split('-');
		var month1 = dates[1].toLowerCase();
		var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
		month1 = months.indexOf(month1);
		var lastDate = new Date(dates[2], month1+1, 0);
		var y = lastDate.getFullYear(), m = lastDate.getMonth(), d = lastDate.getDate();
		m = (''+ (m+1)).slice(-2);
		var monname = months[m-1];
		var monthname = monname.charAt(0).toUpperCase() + monname.slice(1);
		$('#to_date').val(d+'-'+monthname+'-'+y);
	}	 
 });
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_dashboard").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_dashboard").submit();
		});
			getsubdepartment();
			//getpirdropdown();
			
			$("#excelreport").val('0');
	 $(".chosen-select").chosen();
	 $(".chosen-select-multi").chosen();
	 autoselectoption("#department_id"); 
		$('.chosen-select-multi').change(function () {
				$('#subdepartment_id > option').each(function() {
						$(".chosen-select-multi option[value='"+$(this).val()+"']").removeAttr('disabled',true).trigger("chosen:updated");
				});			
			if($(".chosen-select-multi option:selected").val()==""){
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").attr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			else{
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").removeAttr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			getpirdropdown();
		});	 
	 submitform();
	});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = '<?php echo $subdeptid;?>';
	$.get("getsubdepartmentadmin.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
		if(id != ''){
			$("#subdepartment_id").val('').trigger("chosen:updated");
			//$("#subdepartment_id").trigger('chosen:open');
			$('#subdepartment_id > option').each(function() {
				if($(".chosen-select-multi option:selected").val()==""){
					$('#subdepartment_id > option').each(function() {
						if($(this).val() != ''){
							$(".chosen-select-multi option[value='"+$(this).val()+"']").attr('disabled',true).trigger("chosen:updated");
						}
					});							
				}
				else{
					$('#subdepartment_id > option').each(function() {
						if($(this).val() != ''){
							$(".chosen-select-multi option[value='"+$(this).val()+"']").removeAttr('disabled',true).trigger("chosen:updated");
						}
					});							
				}
			});				
		}
		getpirdropdown();
	});
}	
function getexcel(){
	$("#excelreport").val('1');
	$("#frm_dashboard").submit();
	$("#excelreport").val('0');
}
function getpirdropdown(){
	var deptid = $("#department_id").val();
	var supdeptid = $("#subdepartment_id").val();
	var pirmaster_id = '<?php echo $pirmaster_id;?>';
	$.get("getpiradmin.php?deptid="+deptid+"&supdeptid="+supdeptid+"&pirmaster_id="+pirmaster_id+"&t=1",function(data){
		$("#pirmaster_id").html(data);
		$("#pirmaster_id").trigger("chosen:updated");
		getproject();
	});
}
function getproject(){
	var id = $("#pirmaster_id").val();
	var selid = '<?php echo $project_id;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#project_id").html(data);
		//$("#project_id").trigger("chosen:updated");
	});
}

function submitform(){
	//$("#frm_dashboard").submit();
	var depid = $("#department_id").val();
		if(depid !='' && depid != null){
			$('#overlay').css('visibility', 'visible');
			$.post("dashboardajax.php",$.param($("#frm_dashboard").serializeArray()), function(data, status){
				if(data!=''){
					$('#overlay').css('visibility', 'hidden');
				   $("#chart-container").html(data);
				}
				else{
					$.alert({
						title: 'Alert',
						content: 'No Records',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});	
				}
			});	
		}
		else{
			$("#chart-container").html('');
		}	
}


	
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-All-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}		
</script>	
</html>
<?php } ?>

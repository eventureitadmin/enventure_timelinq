<?php
include_once("config.php");
include_once("functions.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getRBUquery($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		if(strtotime($date)<strtotime(date('2021-07-01'))){
			$availhrs = NON_DEDICATED_PRESENT_HOURS_BEFORE;
		}
		else{
			$availhrs = NON_DEDICATED_PRESENT_HOURS_AFTER;
		}
		$query = '';
		$query = "SELECT m.empid,m.empname,m.deptid,m.edate,m.employee_id,m.project_id,m.pirmaster_id,m.totalparts,(SELECT d1.name FROM department d1 WHERE d1.id=m.deptid) as dept,m.subdept,m.entrydate,SEC_TO_TIME(m.actualseconds) as actualhours,SEC_TO_TIME(m.calculatedseconds) as billablehours,SEC_TO_TIME(m.onlineseconds) as onlinehours,SEC_TO_TIME(m.minimumseconds) as presenthours,m.static_availhrs,SEC_TO_TIME(m.minimumseconds-m.ndavailablesec) AS ndavailablehrs,SEC_TO_TIME((m.onlineseconds - m.dayseconds)) as shortagehours,CONCAT(ROUND(((m.calculatedseconds/m.minimumseconds)*100),2),'') as utilization,CONCAT(ROUND(((m.calculatedseconds/m.actualseconds)*100),2),'') as efficiency FROM (SELECT DATE_FORMAT(t1.`entrydate` , '%d-%b-%Y' ) as entrydate,t1.`entrydate` as edate , t1.`employee_id`, t1.`project_id`, t1.`pirmaster_id`,(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id=t1.employee_id) as empname,(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid, (SELECT u1.department_ids FROM employeelist u1 WHERE u1.id=t1.employee_id) as deptid, (SELECT u2.subdepartment_ids FROM employeelist u2 WHERE u2.id=t1.employee_id) as subdept, SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) as onlineseconds,(CASE WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) <= TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('04:15:00') WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) > TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('".$availhrs."') ELSE TIME_TO_SEC('".$availhrs."') END) AS minimumseconds,'".$availhrs."' AS static_availhrs,TIME_TO_SEC('09:30:00') AS dayseconds,(SELECT IFNULL(SUM(TIME_TO_SEC(t3.`actualhours`)),0) AS ndavailableseconds FROM `timeentry` t3 WHERE t3.`entrydate` = '".$date."' AND t3.`employee_id` = t1.employee_id AND t3.`isActive`='1' AND t3.`is_dt`='1' AND t3.`is_rework`='0' AND t3.`is_internalpir`='0') AS ndavailablesec,SUM(t1.totalparts) as totalparts,t2.department_id,t2.subdepartment_id FROM `timeentry` t1, pirmaster t2  WHERE 1=1  AND t2.id=t1.pirmaster_id ".$subdeptcond." AND t1.isActive='1' ".$dtcond." ".$reworkcond." ".$internalpircond." AND t1.`entrydate`='".$date."' ".$group.") m WHERE 1=1".$cond;
		return $query;
	}
	
	if($_POST){
		//print_r($_POST);exit;
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$deptid = $_POST['department_id'];
		$subdeptid = $_POST['subdepartment_id'];
		$pirmaster_id = $_POST['pirmaster_id'];
		$project_id = $_POST['project_id'];
		$kpis = $_POST['kpis'];
		$withdetails = $_POST['withdetails'];
		$excelreport = $_POST['excelreport'];
		$empid = $_POST['empid'];
		$subdepartment_ids = implode (",", $subdeptid);

		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$dedicatemonth=date('m',strtotime($to_date));
		$dedicatemyear=date('Y',strtotime($to_date));		
		$cond = '';
		$procond=''; 

		if($project_id != ''){
			$cond .= " AND m.project_id='".$project_id."'";
			$procond = " AND projectname='".$project_id."'";
		}	
		if($empid!= ''){
			$cond .= " AND m.employee_id='".$empid."'";
		}
		if($pirmaster_id != '' && $project_id != ''){
			$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'".$procond;
			$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
			$pirmasterid=$pirmasterdet['id'];			
			//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
			$group = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
		}
		elseif($pirmaster_id != ''){
			$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
			$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
			if($pirmastercntdet['pircnt']==1){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasterid=$pirmasterdet['id'];			
				//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
				$group = " AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";				
			}
			else{
				$pirmaster_select = "SELECT GROUP_CONCAT(id) as pircsv FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasteridcsv = $pirmasterdet['pircsv'];
				$group = " AND  t1.`pirmaster_id` IN (".$pirmasteridcsv.") GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
			}
		}		
		else{
			$group = " GROUP BY t1.`employee_id`";
		}		
		
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$subdeptcond = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
		}
		$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
		$dedicatedempcond = '';
		if($deptid != ''){
			$dedicatedempcond .= " AND department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$dedicatedempcond .= " AND subdepartment_id IN (".$subdepartment_ids.")";
		}
		if($pirmaster_id != ''){
			$dedicatedempcond .= " AND id='".$pirmaster_id."'";
		}
		//$query="SELECT IFNULL(SUM(`noofresource`),0) as dedicatedresource FROM `pirlist` WHERE `isActive`='1' AND `contracttype`='2' ".$dedicatedempcond;
		if($pirmaster_id != '' && $project_id != ''){
			$dedicatedempcond1= " AND pm.id='".$pirmasterid."'";
		}
		elseif($pirmaster_id != ''){
			$dedicatedempcond1= " AND pm.pirno='".$pirmaster_id."'";
		}
		$subcond ='';
		if($subdepartment_ids != ''){
			$subcond = " AND subdepartment_id IN (".$subdepartment_ids.")";
		}		
		//$query="SELECT IFNULL(SUM(`no_of_resource`),0) as dedicatedresource FROM `pirmaster` WHERE `isActive`='1' AND no_of_resource > 0 AND  department_id='".$deptid."' ".$subcond.$dedicatedempcond1;
		$query="SELECT IFNULL(SUM(rm.`no_of_resource`),0) AS dedicatedresource FROM `pirmaster` pm,`resourcemonth` rm WHERE rm.`isactive`='1' AND pm.isActive AND rm.`pirmaster_id`=pm.id AND rm.`monthval`='".$dedicatemonth."' AND rm.`yearval`='".$dedicatemyear."' AND pm.department_id='".$deptid."'".$subcond.$dedicatedempcond1;
		$result = $dbase->executeQuery($query,'single');
		$totempcnt = $result['dedicatedresource'];
		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){
			unset($reportdata);
				//kpi 1
				if($kpis=='1'){
							if($subdepartment_ids != ''){
								$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
							}
							else{
								$subcnd = '';
							}

								$reportdata1 = array();
								$subdeptcond1 = "AND t2.department_id = '".$deptid."'".$subcnd;
									for($j=0;$j<count($datelist);$j++){
										$reportdata1[$j]['date'] = $datelist[$j];
										$query12 = '';
										$dtcond12 = "AND t1.is_dt='0'";
										$rwcond12 =" AND t1.is_rework='0'";
										$intpircond12 = " AND t1.is_internalpir='0'";
										unset($report12);
										$query12 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond12,$rwcond12,$intpircond12);
										$report = $dbase->executeQuery($query12,'multiple');
										for($i=0;$i<count($report);$i++){
											$subdisplay = " AND sd.id IN (".$report[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
										$subdeptstr = substr($subdeptstr,0,-1);	
											$report[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata1[$j]['nddetails'] = $report;
										
										$query11 = '';
										$dtcond11 = "AND t1.is_dt='1'";
										$rwcond11 =" AND t1.is_rework='0'";
										$intpircond11 = " AND t1.is_internalpir='0'";
										unset($report11);
										$query11 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond11,$rwcond11,$intpircond11);
										$report11 = $dbase->executeQuery($query11,'multiple');
										for($i=0;$i<count($report11);$i++){
											$subdisplay = " AND sd.id IN (".$report11[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
										$subdeptstr = substr($subdeptstr,0,-1);	
											$report11[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata1[$j]['dddetails'] = $report11;
										//$reportdata1[$j]['details'] = array_merge($reportdata1[$j]['nddetails'],$reportdata1[$j]['ddtdetails']);										
										
										$query13 = '';
										$dtcond13 = '';
										$rwcond13 =" AND t1.is_rework='1'";
										$intpircond13 = " AND t1.is_internalpir='0'";										
										$query13 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond13,$rwcond13,$intpircond13);
										$report13 = $dbase->executeQuery($query13,'multiple');
										for($i=0;$i<count($report13);$i++){
											$subdisplay = " AND sd.id IN (".$report13[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
										$subdeptstr = substr($subdeptstr,0,-1);	
											$report13[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata1[$j]['reworkdetails'] = $report13;

										$query14 = '';
										$dtcond14 = '';
										$rwcond14 =" AND t1.is_rework='0'";
										$intpircond14 = " AND t1.is_internalpir='1'";										
										$query14 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond14,$rwcond14,$intpircond14);
										$report14 = $dbase->executeQuery($query14,'multiple');
										for($i=0;$i<count($report14);$i++){
											$subdisplay = " AND sd.id IN (".$report14[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
										$subdeptstr = substr($subdeptstr,0,-1);	
											$report14[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata1[$j]['internalpirdetails'] = $report14;											
									}
					$reportdata = $reportdata1;
				}
				//kpi 2
				if($kpis=='2'){
					unset($reportdata2);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata2 = array();
					$subdeptcond2 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
						$reportdata2[$j]['date'] = $datelist[$j];
						$query21 = '';
						$dtcond21 = "AND t1.is_dt='0'";
						$rwcond21 =" AND t1.is_rework='0'";
						$intpircond21 = " AND t1.is_internalpir='0'";						
						unset($report21);
						$query21 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond2,$dtcond21,$rwcond21,$intpircond21);
						$report21 = $dbase->executeQuery($query21,'multiple');
						
							for($i=0;$i<count($report21);$i++){
								
								$subdisplay = " AND sd.id IN (".$report21[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report21[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata2[$j]['details'] = $report21;
					}		
					$reportdata = $reportdata2;
				}			
				//kpi 3
				if($kpis=='3'){
				unset($reportdata3);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata3 = array();
					$subdeptcond3 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
					$dtcond31 = " AND t1.is_dt='1'";
					$rwcond31 =" AND t1.is_rework='0'";
					$intpircond31 = " AND t1.is_internalpir='0'";					
					$query31 = '';
					unset($report31);
					$query31 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond3,$dtcond31,$rwcond31,$intpircond31);
					$report31 = $dbase->executeQuery($query31,'multiple');
							for($i=0;$i<count($report31);$i++){
								$subdisplay = " AND sd.id IN (".$report31[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report31[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata3[$j]['details'] = $report31;	
					}
					$reportdata = $reportdata3;
				}
				//kpi 4
				if($kpis=='4'){
				unset($reportdata4);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata4 = array();
					$subdeptcond4 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
						$dtcond4 = " AND t1.is_dt='0'";
						$rwcond4 =" AND t1.is_rework='0'";
						$intpircond4 = " AND t1.is_internalpir='0'";						
					$query4 = '';
					unset($report4);
					$query4 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond4,$dtcond4,$rwcond4,$intpircond4);
					$report4 = $dbase->executeQuery($query4,'multiple');
							for($i=0;$i<count($report4);$i++){
								$subdisplay = " AND sd.id IN (".$report4[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report4[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata4[$j]['details'] = $report4;
										$query131 = '';
										$dtcond131 = " AND t1.is_dt='0'";
										$rwcond131 =" AND t1.is_rework='1'";
										$intpircond131 = " AND t1.is_internalpir='0'";										
										$query131 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond131,$rwcond131,$intpircond131);
										$report131 = $dbase->executeQuery($query131,'multiple');
										for($i=0;$i<count($report131);$i++){
											$subdisplay = " AND sd.id IN (".$report131[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
										$subdeptstr = substr($subdeptstr,0,-1);	
											$report131[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata4[$j]['reworkdetails'] = $report131;

										$query141 = '';
										$dtcond141 = " AND t1.is_dt='0'";
										$rwcond141 =" AND t1.is_rework='0'";
										$intpircond141 = " AND t1.is_internalpir='1'";										
										$query141 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond141,$rwcond141,$intpircond141);
										$report141 = $dbase->executeQuery($query141,'multiple');
										for($i=0;$i<count($report141);$i++){
											$subdisplay = " AND sd.id IN (".$report141[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
										$subdeptstr = substr($subdeptstr,0,-1);	
											$report141[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata4[$j]['internalpirdetails'] = $report141;						
						
					}
					$reportdata = $reportdata4;
				}
				//kpi 5
				if($kpis=='5'){
				unset($reportdata5);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata5 = array();
					$subdeptcond5 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
						$dtcond5 = " AND t1.is_dt='0'";
						$rwcond5 =" AND t1.is_rework='0'";
						$intpircond5 = " AND t1.is_internalpir='0'";						
					$query5 = '';
					unset($report5);
					$query5 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond5,$dtcond5,$rwcond5,$intpircond5);
					$report5 = $dbase->executeQuery($query5,'multiple');
							for($i=0;$i<count($report5);$i++){
								$subdisplay = " AND sd.id IN (".$report5[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report5[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata5[$j]['details'] = $report5;
					}			
					$reportdata = $reportdata5;
				}
			//echo "<pre>";
			//print_r($reportdata);exit;
		}
		
if($excelreport=='1'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  =>12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

	$fdate = $_POST['from_date'];
	$tdate = $_POST['to_date'];
	$reportname = '';
		if($kpis=='1'){
			$reportname = 'Resource Billing Utilization';
		}
		if($kpis=='2'){
			$reportname = 'Non Dedicated Project Efficiency';
		}
		if($kpis=='3'){
			$reportname = 'Dedicated Team Utilization';
		}
		if($kpis=='4'){
			$reportname = 'Non Dedicated Utilization';
		}	
	if($kpis != '1'){
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle($fdate." - ".$tdate);
	$row = 1;
		$col11 = "A{$row}";
		$col22 = "G{$row}";
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);


		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportname);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('EMP ID');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Employee Name');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Resource Type');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('Date');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Present Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Actual Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('Budget Hours');
		$row++;
		if(count($reportdata) > 0 && $reportdata != ''){
			if($kpis!='1'){
				$totparts = 0;
				$actualhourssarr = array();
				$billablehoursarr = array();
				$availablehoursarr = array();
				$emp_array = array();
				$cnt = 0;
				$totpresentdays = 0;
				$reworktotpresentdays = 0;
				$interpirtotpresentdays = 0;
				for($q=0;$q<count($reportdata);$q++){
					for($w=0;$w<count($reportdata[$q]['details']);$w++){
						$emp_array[] = $reportdata[$q]['details'][$w]['empid'];
						$presentdays = 0;
						$actualhourssarr[] = $reportdata[$q]['details'][$w]['actualhours'];
						$billablehoursarr[] = $reportdata[$q]['details'][$w]['billablehours'];
						$availablehoursarr[] = $reportdata[$q]['details'][$w]['presenthours'];
						$totparts += $reportdata[$q]['details'][$w]['totalparts'];
						if($reportdata[$q]['details'][$w]['presenthours']==$reportdata[$q]['details'][$w]['static_availhrs']){
							$presentdays += 1;
						}
						elseif($reportdata[$q]['details'][$w]['presenthours']=='04:15:00'){
							$presentdays += 0.5;
						}
						else{
							$presentdays += 0;
						}
						$totpresentdays += $presentdays;
						$cnt++;
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportdata[$q]['details'][$w]['empid']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($reportdata[$q]['details'][$w]['empname']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('');
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($reportdata[$q]['details'][$w]['entrydate']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue(substr($reportdata[$q]['details'][$w]['presenthours'],0,-3)." ( ".$presentdays." )");
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue(substr($reportdata[$q]['details'][$w]['actualhours'],0,-3));
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue(substr($reportdata[$q]['details'][$w]['billablehours'],0,-3));				
				$row++;						
					}
			if(count($reportdata[$q]['reworkdetails']) > 0){
					for($t=0;$t<count($reportdata[$q]['reworkdetails']);$t++){
						$reworkpresentdays = 0;
						if($reportdata[$q]['details'][$t]['presenthours']==$reportdata[$q]['details'][$t]['static_availhrs']){
							$reworkpresentdays += 1;
						}
						elseif($reportdata[$q]['details'][$t]['presenthours']=='04:15:00'){
							$reworkpresentdays += 0.5;
						}
						else{
							$reworkpresentdays += 0;
						}
						$reworktotpresentdays += $reworkpresentdays;						
					}
			}
			if(count($reportdata[$q]['internalpirdetails']) > 0){
					for($u=0;$u<count($reportdata[$q]['internalpirdetails']);$u++){
						$interpirpresentdays = 0;
						if($reportdata[$q]['details'][$u]['presenthours']==$reportdata[$q]['details'][$u]['static_availhrs']){
							$interpirpresentdays += 1;
						}
						elseif($reportdata[$q]['details'][$u]['presenthours']=='04:15:00'){
							$interpirpresentdays += 0.5;
						}
						else{
							$interpirpresentdays += 0;
						}
						$interpirtotpresentdays += $interpirpresentdays;						
					}
			}						
				}
			
			}
			
			//grand totoal sum 
			if($kpis!='1'){
						$col111 = "A{$row}";
						$col221 = "D{$row}";
						$concat1 =  $col111.":".$col221;	
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat1);	
						$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue("Total");
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->getActiveSheet()->getStyle($concat1)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
						$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($totpresentdays." - ".$reworktotpresentdays." - ".$interpirtotpresentdays);
						$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
						$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($dbase->addTime($actualhourssarr));
						$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($dbase->addTime($billablehoursarr));				
						$row++;						
					}
					
			// percentage
			
			
			if($kpis=='2'){
				$util = ($dbase->addTime($actualhourssarr,true) / ($dbase->addTime($billablehoursarr,true)))*100;
				$col111 = "A{$row}";
				$col221 = "D{$row}";
				$concat1 =  $col111.":".$col221;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat1);	
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue("Total Efficiency");
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle($concat1)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue("");
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($util);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue("");				
				$row++;
			}
			if($kpis=='3'){
				$dedicatedbilledhrsarr = array();
				$tot_emp = $totempcnt;
				$dedicatedbilledhrsarr = ($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS);
				$util = (($dbase->addTime($billablehoursarr,true)/($dedicatedbilledhrsarr*60))*100);
				$col111 = "A{$row}";
				$col221 = "D{$row}";
				$concat1 =  $col111.":".$col221;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat1);	
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue("Total Utilization");
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle($concat1)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue("");
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($util);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue("");				
				$row++;
			}
			if($kpis=='4'){
				$perdaymin = $dbase->getminutes("09:00");
				$util = (($dbase->addTime($billablehoursarr,true) / (($totpresentdays+$reworktotpresentdays+$interpirtotpresentdays)*$perdaymin))*100);
				$col111 = "A{$row}";
				$col221 = "D{$row}";
				$concat1 =  $col111.":".$col221;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat1);	
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue("Total Utilization");
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle($concat1)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue("");
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($util);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue("");				
				$row++;
			}			
			
		}	
	}else if($kpis == '1'){
		$overallindex =0;
		$objPHPExcel->setActiveSheetIndex($overallindex);
		$objPHPExcel->getActiveSheet()->setTitle($fdate." - ".$tdate);
		$row = 1;
		$col11 = "A{$row}";
		$col22 = "G{$row}";
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex($overallindex)->mergeCells($concat);


		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('A'.$row)->setValue("OVERALL:".$reportname);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('A'.$row)->setValue('EMP ID');
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('B'.$row)->setValue('Employee Name');
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('C'.$row)->setValue('Resource Type');
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('D'.$row)->setValue('Date');
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('E'.$row)->setValue('Present Hours');
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('F'.$row)->setValue('Actual Hours');
		$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('G'.$row)->setValue('Budget Hours');
		$row++;
		//echo "ddddd";exit;
		$kpirbtype=0;
		if(count($reportdata) > 0 && $reportdata != ''){
			if($kpirbtype == 0 || $kpirbtype == 1 || $kpirbtype ==2){
			
		$ndtotparts = 0;
		$ndactualhourssarr = array();
		$ndbillablehoursarr = array();
		$ndavailablehoursarr = array();
		$ndemp_array = array();
		$ndcnt = 0;
		$ndtotpresentdays = 0;
		$ndreworktotpresentdays = 0;
		$ndinterpirtotpresentdays = 0;
		
		$ddtotparts = 0;
		$ddactualhourssarr = array();
		$ddbillablehoursarr = array();
		$tmpddbillablehoursarr = array();
		$ddavailablehoursarr = array();	
		$ddemp_array = array();		
		$ddcnt = 0;
		$ddtotpresentdays = 0;
		$ddreworktotpresentdays = 0;
		$ddinterpirtotpresentdays = 0;	
		
		for($q=0;$q<count($reportdata);$q++){
			for($w=0;$w<count($reportdata[$q]['nddetails']);$w++){
				$ndemp_array[] = $reportdata[$q]['nddetails'][$w]['empid'];
				$ndpresentdays = 0;
				$ndactualhourssarr[] = $reportdata[$q]['nddetails'][$w]['actualhours'];
				$ndbillablehoursarr[] = $reportdata[$q]['nddetails'][$w]['billablehours'];
				$ndavailablehoursarr[] = $reportdata[$q]['nddetails'][$w]['ndavailablehrs'];
				$ndtotparts += $reportdata[$q]['nddetails'][$w]['totalparts'];
				if($reportdata[$q]['nddetails'][$w]['presenthours']==$reportdata[$q]['nddetails'][$w]['static_availhrs']){
					$ndpresentdays += 1;
				}
				elseif($reportdata[$q]['nddetails'][$w]['presenthours']=='04:15:00'){
					$ndpresentdays += 0.5;
				}
				else{
					$ndpresentdays += 0;
				}
				$ndtotpresentdays += $ndpresentdays;
				$ndcnt++;
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportdata[$q]['nddetails'][$w]['empid']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($reportdata[$q]['nddetails'][$w]['empname']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Non Dedicated');
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($reportdata[$q]['nddetails'][$w]['entrydate']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue(substr($reportdata[$q]['nddetails'][$w]['ndavailablehrs'],0,-3)." ( ".$ndpresentdays." )");
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue(substr($reportdata[$q]['nddetails'][$w]['actualhours'],0,-3));
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue(substr($reportdata[$q]['nddetails'][$w]['billablehours'],0,-3));				
				$row++;
			}
			for($w=0;$w<count($reportdata[$q]['dddetails']);$w++){
				$ddemp_array[] = $reportdata[$q]['dddetails'][$w]['empid'];
				$ddpresentdays = 0;
				$ddactualhourssarr[] = $reportdata[$q]['dddetails'][$w]['actualhours'];
				$ddbillablehoursarr[] = $reportdata[$q]['dddetails'][$w]['billablehours'];
				$tmpddbillablehoursarr[] = "09:00";
				$ddavailablehoursarr[] = $reportdata[$q]['dddetails'][$w]['presenthours'];
				$ddtotparts += $reportdata[$q]['dddetails'][$w]['totalparts'];
				if($reportdata[$q]['dddetails'][$w]['presenthours']==$reportdata[$q]['dddetails'][$w]['static_availhrs']){
					$ddpresentdays += 1;
				}
				elseif($reportdata[$q]['dddetails'][$w]['presenthours']=='04:15:00'){
					$ddpresentdays += 0.5;
				}
				else{
					$ddpresentdays += 0;
				}
				$ddtotpresentdays += $ddpresentdays;
				$ddcnt++;
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportdata[$q]['dddetails'][$w]['empid']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($reportdata[$q]['dddetails'][$w]['empname']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Dedicated');
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($reportdata[$q]['dddetails'][$w]['entrydate']);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue(substr($reportdata[$q]['dddetails'][$w]['presenthours'],0,-3)." ( ".$ddpresentdays." )");
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue(substr($reportdata[$q]['dddetails'][$w]['actualhours'],0,-3));
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue("09:00");				
				$row++;
			}				
		}			
			
				//grand totoal sum 
			
						$col1111 = "A{$row}";
						$col2211 = "D{$row}";
						$concat11 =  $col1111.":".$col2211;	
						$objPHPExcel->setActiveSheetIndex($overallindex)->mergeCells($concat11);
						$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue("Non Dedicated Total");
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->getActiveSheet()->getStyle($concat11)->applyFromArray($valueStyleArray);					
						$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
						$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue("");
						$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
						$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($dbase->addTime($ndactualhourssarr));
						$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($dbase->addTime($ndbillablehoursarr));				
						$row++;	
						$col1112 = "A{$row}";
						$col2212 = "D{$row}";
						$concat12 =  $col1112.":".$col2212;	
						$objPHPExcel->setActiveSheetIndex($overallindex)->mergeCells($concat12);	
						$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue("Dedicated Total");
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->getActiveSheet()->getStyle($concat12)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
						$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue("");
						$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
						$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($dbase->addTime($ddactualhourssarr));
						$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($dbase->addTime($tmpddbillablehoursarr));				
						$row++;							
			
		
			// percentage
			
				$perdaymin = $dbase->getminutes("09:00");
				$tot_emp = $totempcnt;
				$util =(($dbase->addTime($ndbillablehoursarr,true) + ($perdaymin*$tot_emp))/(($dbase->addTime($ndavailablehoursarr,true))+($perdaymin*$tot_emp))*100);
			$util = round($util,0)."%";
				$col111 = "A{$row}";
				$col221 = "D{$row}";
				$concat1 =  $col111.":".$col221;	
				$objPHPExcel->setActiveSheetIndex($overallindex)->mergeCells($concat1);	
				$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('A'.$row)->setValue("Total Utilization");
				$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle($concat1)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('E'.$row)->setValue("");
				$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('F'.$row)->setValue($util);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($overallindex)->getCell('G'.$row)->setValue("");				
				$row++;
			
			}
			//FPC
			if($kpirbtype == 0 || $kpirbtype == 1){
				$fpcindex =1;
		$objWorkSheet = $objPHPExcel->createSheet($fpcindex);
		$objWorkSheet->setTitle("FPC_".$fdate." - ".$tdate);
		$row = 1;
		$col11 = "A{$row}";
		$col22 = "G{$row}";
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex($fpcindex)->mergeCells($concat);


		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('A'.$row)->setValue("FPC :".$reportname);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('A'.$row)->setValue('EMP ID');
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('B'.$row)->setValue('Employee Name');
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('C'.$row)->setValue('Resource Type');
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('D'.$row)->setValue('Date');
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('E'.$row)->setValue('Present Hours');
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('F'.$row)->setValue('Actual Hours');
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('G'.$row)->setValue('Budget Hours');
		$row++;
				//data
					$ndtotparts = 0;
		$ndactualhourssarr = array();
		$ndbillablehoursarr = array();
		$ndavailablehoursarr = array();
		$ndemp_array = array();
		$ndcnt = 0;
		$ndtotpresentdays = 0;
		$ndreworktotpresentdays = 0;
		$ndinterpirtotpresentdays = 0;
		
		
		for($q=0;$q<count($reportdata);$q++){
			for($w=0;$w<count($reportdata[$q]['nddetails']);$w++){
				$ndemp_array[] = $reportdata[$q]['nddetails'][$w]['empid'];
				$ndpresentdays = 0;
				$ndactualhourssarr[] = $reportdata[$q]['nddetails'][$w]['actualhours'];
				$ndbillablehoursarr[] = $reportdata[$q]['nddetails'][$w]['billablehours'];
				$ndavailablehoursarr[] = $reportdata[$q]['nddetails'][$w]['ndavailablehrs'];
				$ndtotparts += $reportdata[$q]['nddetails'][$w]['totalparts'];
				if($reportdata[$q]['nddetails'][$w]['presenthours']==$reportdata[$q]['nddetails'][$w]['static_availhrs']){
					$ndpresentdays += 1;
				}
				elseif($reportdata[$q]['nddetails'][$w]['presenthours']=='04:15:00'){
					$ndpresentdays += 0.5;
				}
				else{
					$ndpresentdays += 0;
				}
				$ndtotpresentdays += $ndpresentdays;
				$ndcnt++;
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('A'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('A'.$row)->setValue($reportdata[$q]['nddetails'][$w]['empid']);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('B'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('B'.$row)->setValue($reportdata[$q]['nddetails'][$w]['empname']);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('C'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('C'.$row)->setValue('Non Dedicated');
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('D'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('D'.$row)->setValue($reportdata[$q]['nddetails'][$w]['entrydate']);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('E'.$row)->setValue(substr($reportdata[$q]['nddetails'][$w]['ndavailablehrs'],0,-3)." ( ".$ndpresentdays." )");
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('F'.$row)->setValue(substr($reportdata[$q]['nddetails'][$w]['actualhours'],0,-3));
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('G'.$row)->setValue(substr($reportdata[$q]['nddetails'][$w]['billablehours'],0,-3));				
				$row++;
			


			}			
		}	
				
				//grand totoal sum 
			
						$col1111 = "A{$row}";
						$col2211 = "D{$row}";
						$concat11 =  $col1111.":".$col2211;	
						$objPHPExcel->setActiveSheetIndex($fpcindex)->mergeCells($concat11);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('A'.$row)->setValue("FPC Total");
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle($concat11)->applyFromArray($valueStyleArray);					
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('E')->setWidth(20);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('E'.$row)->setValue($dbase->addTime($ndavailablehoursarr));
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('F')->setWidth(15);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('F'.$row)->setValue($dbase->addTime($ndactualhourssarr));
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('G')->setWidth(15);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('G'.$row)->setValue($dbase->addTime($ndbillablehoursarr));				
						$row++;	
			
		
			// percentage
			
				$perdaymin = $dbase->getminutes("09:00");
			$fpc = (($dbase->addTime($ndbillablehoursarr,true))/(($dbase->addTime($ndavailablehoursarr,true)))*100);
			$fpc = round($fpc,0)."%";
			
				$col111 = "A{$row}";
				$col221 = "D{$row}";
				$concat1 =  $col111.":".$col221;	
				$objPHPExcel->setActiveSheetIndex($fpcindex)->mergeCells($concat1);	
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('A'.$row)->setValue("FPC Total Utilization");
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle($concat1)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('E'.$row)->setValue("");
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('F'.$row)->setValue($fpc);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($fpcindex)->getCell('G'.$row)->setValue("");				
				$row++;
	
			}
			//DTU
			
			if($kpirbtype == 0 || $kpirbtype == 2){
				$dtuindex=2;
				$objWorkSheet = $objPHPExcel->createSheet($dtuindex);
				$objWorkSheet->setTitle("DTU_".$fdate." - ".$tdate);

				$row = 1;
				$col11 = "A{$row}";
				$col22 = "G{$row}";
				$concat =  $col11.":".$col22;	
				$objPHPExcel->setActiveSheetIndex($dtuindex)->mergeCells($concat);


				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('A'.$row)->setValue("DTU ".$reportname);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle($concat)->applyFromArray($headingStyleArray);
				$row++;
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('A'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('A'.$row)->setValue('EMP ID');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('B'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('B'.$row)->setValue('Employee Name');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('C'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('C'.$row)->setValue('Resource Type');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('D'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('D'.$row)->setValue('Date');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('E'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('E'.$row)->setValue('Present Hours');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('F'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('F'.$row)->setValue('Actual Hours');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('G'.$row)->applyFromArray($headingStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('G'.$row)->setValue('Budget Hours');
				$row++;
				
				$ddtotparts = 0;
		$ddactualhourssarr = array();
		$ddbillablehoursarr = array();
		$tmpddbillablehoursarr = array();
		$ddavailablehoursarr = array();	
		$ddemp_array = array();		
		$ddcnt = 0;
		$ddtotpresentdays = 0;
		$ddreworktotpresentdays = 0;
		$ddinterpirtotpresentdays = 0;	
		
		for($q=0;$q<count($reportdata);$q++){
			for($w=0;$w<count($reportdata[$q]['dddetails']);$w++){
				$ddemp_array[] = $reportdata[$q]['dddetails'][$w]['empid'];
				$ddpresentdays = 0;
				$ddactualhourssarr[] = $reportdata[$q]['dddetails'][$w]['actualhours'];
				$ddbillablehoursarr[] = $reportdata[$q]['dddetails'][$w]['billablehours'];
				$tmpddbillablehoursarr[] = "09:00";
				$ddavailablehoursarr[] = $reportdata[$q]['dddetails'][$w]['presenthours'];
				$ddtotparts += $reportdata[$q]['dddetails'][$w]['totalparts'];
				if($reportdata[$q]['dddetails'][$w]['presenthours']==$reportdata[$q]['dddetails'][$w]['static_availhrs']){
					$ddpresentdays += 1;
				}
				elseif($reportdata[$q]['dddetails'][$w]['presenthours']=='04:15:00'){
					$ddpresentdays += 0.5;
				}
				else{
					$ddpresentdays += 0;
				}
				$ddtotpresentdays += $ddpresentdays;
				$ddcnt++;
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('A'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('A'.$row)->setValue($reportdata[$q]['dddetails'][$w]['empid']);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('B'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('B'.$row)->setValue($reportdata[$q]['dddetails'][$w]['empname']);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('C'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('C'.$row)->setValue('Dedicated');
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('D'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('D'.$row)->setValue($reportdata[$q]['dddetails'][$w]['entrydate']);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('E'.$row)->setValue(substr($reportdata[$q]['dddetails'][$w]['presenthours'],0,-3)." ( ".$ddpresentdays." )");
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('F'.$row)->setValue(substr($reportdata[$q]['dddetails'][$w]['actualhours'],0,-3));
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('G'.$row)->setValue("09:00");				
				$row++;
			}				
		}			
		//grand totoal sum 
							
						$col1112 = "A{$row}";
						$col2212 = "D{$row}";
						$concat12 =  $col1112.":".$col2212;	
						$objPHPExcel->setActiveSheetIndex($dtuindex)->mergeCells($concat12);	
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('A'.$row)->setValue("DTU Total");
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle($concat12)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('E')->setWidth(20);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('E'.$row)->setValue("");
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('F')->setWidth(15);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('F'.$row)->setValue($dbase->addTime($ddactualhourssarr));
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('G')->setWidth(15);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
						$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('G'.$row)->setValue($dbase->addTime($tmpddbillablehoursarr));				
						$row++;							
			
		
			// percentage
			
			$perdaymin = $dbase->getminutes("09:00");
			$tot_emp = $totempcnt;
			$dedicatedbilledhrsarr = (($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS)*60);		
		$dtu = ((($dbase->addTime($ddbillablehoursarr,true))/($dedicatedbilledhrsarr))*100);
		$dtu = round($dtu,0)."%";
				$col111 = "A{$row}";
				$col221 = "D{$row}";
				$concat1 =  $col111.":".$col221;	
				$objPHPExcel->setActiveSheetIndex($dtuindex)->mergeCells($concat1);	
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('A'.$row)->setValue("DTU Total Utilization");
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle($concat1)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('E'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('E'.$row)->setValue("");
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('F'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('F'.$row)->setValue($dtu);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getStyle('G'.$row)->applyFromArray($valueStyleArray);
				$objPHPExcel->setActiveSheetIndex($dtuindex)->getCell('G'.$row)->setValue("");				
				$row++;
			


			}
		}
	
	}
	
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="export.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chart2_8.js" type="text/javascript"></script>
	   <script src="js/utils.js" type="text/javascript"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:14px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		
		table#reportdetailtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reportdetailtable td, table#reportdetailtable th {
			border: 1px solid white;
			 padding: 5px; 
		}	
		table#kpi1 {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#kpi1 td, table#kpi1 th {
			border: 1px solid black;
			 padding: 5px; 
		}			  
		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="1" align="center"  width="100%" >
<tr>
<td width="100%" colspan="4" ><b>KPI's</b></td>
</tr>
<tr>
<td width="25%" ><b>From Date</b><br/><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('01-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="25%" ><b>To Date</b><br/><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="25%" ><b>Select Department</b><br/><select id="department_id" name="department_id" class="required"  onchange="getsubdepartment();">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																									  
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		$select = "";
		for($i=0;$i<count($deptResult);$i++){
			echo '<option value="'.$deptResult[$i]['id'].'">'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select></td>
<td width="25%" ><b>Select Sub Department</b><br/><select id="subdepartment_id" name="subdepartment_id[]" data-placeholder="Select Sub Department" class="required chosen-select-multi" multiple style="width:180px;">
	</select>	
	<label for="subdepartment_id" class="error" style="display:none" >This field is required.</label>
</td>
</tr>
<tr>
<td width="25%" ><b>Select PIR</b><br/><select id="pirmaster_id" name="pirmaster_id" class="chosen-select" style="width:300px;" onchange="getproject();">
	<option value="">-All-</option>
	</select></td>
<td width="25%" ><b>Select Project</b><br/><select id="project_id" name="project_id">
	<option value="">-All-</option>
	</select></td>
<td width="12%" ><b>Select KPI</b><br/><select id="kpis" name="kpis" class="required" >
<option value="">-Select-</option>
<option value="1" <?php if($kpis=='1'){ ?>selected<?php } ?>>Resource Billing Utilization</option>
<option value="3" <?php if($kpis=='3'){ ?>selected<?php } ?>>Dedicated Team Utilization</option>	
<option value="2" <?php if($kpis=='2'){ ?>selected<?php } ?>>Non Dedicated Project Efficiency</option>
<option value="4" <?php if($kpis=='4'){ ?>selected<?php } ?>>Non Dedicated Utilization</option>
<!--<option value="5" <?php if($kpis=='5'){ ?>selected<?php } ?>>Dedicated Team Bench Utilization</option>-->
</select></td>
<td width="25%" ><b>Select User</b><br/><select id="empid" name="empid" class="chosen-select" style="width:300px;" >
<option value="">-Select-</option>
</select></td>
</tr>
<tr>
<td width="25%" ></td>
<td width="25%" ></td>
<td width="12%" ></td>
<td width="25%" ><input type="hidden" id="excelreport" name="excelreport"  /><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" /><!--<input type="checkbox" id="withdetails" name="withdetails" value='1' <?php if($withdetails=='1'){?> checked <?php } ?> /> With Details-->
<a href="javascript:void(0);" style="cursor: pointer;" onclick="getexcel()"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a>
</td>
</tr>
	<tr>
		<td colspan="4">
	<?php if(count($reportdata) > 0 && $reportdata != ''){ ?>
	   	<table id="kpi1" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%">
		<tr>
			<td width="5%" align="center"><b>Emp ID</b></td>
			<td width="15%" align="center"><b>Emp Name</b></td>
			<td width="15%" align="center"><b>Resource Type</b></td>
			<!--<td width="5%" align="center"><b>Dept</b></td>
			<td width="5%" align="center"><b>Sub Dept</b></td>-->
			<td width="10%" align="center"><b>Date</b></td>
			<!--<td width="7%" align="center"><b>Total Parts</b></td>-->
			<!--<td width="8%" align="center"><b>Online Hours</b></td>
			<td width="8%" align="center"><b>Shortage Hours</b></td>-->
			<td width="8%" align="center"><b>Available Hours</b></td>
			<td width="8%" align="center"><b>Actual Hours</b><span style="font-size:10px;"> Entered by user</span></td>
			<td width="8%" align="center"><b>Budgeted Hours</b></td>
		</tr>
<?php
		if($kpis=='1'){
		$ndtotparts = 0;
		$ndactualhourssarr = array();
		$ndbillablehoursarr = array();
		$ndavailablehoursarr = array();
		$ndemp_array = array();
		$ndcnt = 0;
		$ndtotpresentdays = 0;
		$ndreworktotpresentdays = 0;
		$ndinterpirtotpresentdays = 0;
		
		$ddtotparts = 0;
		$ddactualhourssarr = array();
		$ddbillablehoursarr = array();
		$tmpddbillablehoursarr = array();
		$ddavailablehoursarr = array();	
		$ddemp_array = array();		
		$ddcnt = 0;
		$ddtotpresentdays = 0;
		$ddreworktotpresentdays = 0;
		$ddinterpirtotpresentdays = 0;	
		
		for($q=0;$q<count($reportdata);$q++){
			for($w=0;$w<count($reportdata[$q]['nddetails']);$w++){
				$ndemp_array[] = $reportdata[$q]['nddetails'][$w]['empid'];
				$ndpresentdays = 0;
				$ndactualhourssarr[] = $reportdata[$q]['nddetails'][$w]['actualhours'];
				$ndbillablehoursarr[] = $reportdata[$q]['nddetails'][$w]['billablehours'];
				$ndavailablehoursarr[] = $reportdata[$q]['nddetails'][$w]['ndavailablehrs'];
				$ndtotparts += $reportdata[$q]['nddetails'][$w]['totalparts'];
				if($reportdata[$q]['nddetails'][$w]['presenthours']==$reportdata[$q]['nddetails'][$w]['static_availhrs']){
					$ndpresentdays += 1;
				}
				elseif($reportdata[$q]['nddetails'][$w]['presenthours']=='04:15:00'){
					$ndpresentdays += 0.5;
				}
				else{
					$ndpresentdays += 0;
				}
				$ndtotpresentdays += $ndpresentdays;
				$ndcnt++;		
		?>
		<tr>
			<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['empid']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['empname']; ?></td>
			<td valign="top">Non Dedicated</td>
			<!--<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['dept']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['subdept']; ?></td>-->
			<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['entrydate']; ?></td>
			<!--<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['totalparts']; ?></td>-->
			<!--<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['onlinehours']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['nddetails'][$w]['shortagehours']; ?></td>-->
			<td valign="top"><?php echo substr($reportdata[$q]['nddetails'][$w]['ndavailablehrs'],0,-3)." ( ".$ndpresentdays." )"; ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['nddetails'][$w]['actualhours'],0,-3); ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['nddetails'][$w]['billablehours'],0,-3); ?></td>
		</tr>	
		<?php
			}
			for($w=0;$w<count($reportdata[$q]['dddetails']);$w++){
				$ddemp_array[] = $reportdata[$q]['dddetails'][$w]['empid'];
				$ddpresentdays = 0;
				$ddactualhourssarr[] = $reportdata[$q]['dddetails'][$w]['actualhours'];
				$ddbillablehoursarr[] = $reportdata[$q]['dddetails'][$w]['billablehours'];
				$tmpddbillablehoursarr[] = "09:00";
				$ddavailablehoursarr[] = $reportdata[$q]['dddetails'][$w]['presenthours'];
				$ddtotparts += $reportdata[$q]['dddetails'][$w]['totalparts'];
				if($reportdata[$q]['dddetails'][$w]['presenthours']==$reportdata[$q]['dddetails'][$w]['static_availhrs']){
					$ddpresentdays += 1;
				}
				elseif($reportdata[$q]['dddetails'][$w]['presenthours']=='04:15:00'){
					$ddpresentdays += 0.5;
				}
				else{
					$ddpresentdays += 0;
				}
				$ddtotpresentdays += $ddpresentdays;
				$ddcnt++;		
		?>
		<tr>
			<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['empid']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['empname']; ?></td>
			<td valign="top">Dedicated</td>
			<!--<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['dept']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['subdept']; ?></td>-->
			<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['entrydate']; ?></td>
			<!--<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['totalparts']; ?></td>-->
			<!--<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['onlinehours']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['dddetails'][$w]['shortagehours']; ?></td>-->
			<td valign="top"><?php echo substr($reportdata[$q]['dddetails'][$w]['presenthours'],0,-3)." ( ".$ddpresentdays." )"; ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['dddetails'][$w]['actualhours'],0,-3); ?></td>
			<td valign="top"><?php echo "09:00"; ?></td>
		</tr>	
		<?php
			}			
		}
		}
		?>			
		<?php
		if($kpis!='1'){
		$totparts = 0;
		$actualhourssarr = array();
		$billablehoursarr = array();
		$availablehoursarr = array();
		$emp_array = array();
		$cnt = 0;
		$totpresentdays = 0;
		$reworktotpresentdays = 0;
		$interpirtotpresentdays = 0;
		for($q=0;$q<count($reportdata);$q++){
			for($w=0;$w<count($reportdata[$q]['details']);$w++){
				$emp_array[] = $reportdata[$q]['details'][$w]['empid'];
				$presentdays = 0;
				$actualhourssarr[] = $reportdata[$q]['details'][$w]['actualhours'];
				$billablehoursarr[] = $reportdata[$q]['details'][$w]['billablehours'];
				$availablehoursarr[] = $reportdata[$q]['details'][$w]['presenthours'];
				$totparts += $reportdata[$q]['details'][$w]['totalparts'];
				if($reportdata[$q]['details'][$w]['presenthours']==$reportdata[$q]['details'][$w]['static_availhrs']){
					$presentdays += 1;
				}
				elseif($reportdata[$q]['details'][$w]['presenthours']=='04:15:00'){
					$presentdays += 0.5;
				}
				else{
					$presentdays += 0;
				}
				$totpresentdays += $presentdays;
				$cnt++;		
		?>
		<tr>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['empid']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['empname']; ?></td>
			<td valign="top"><?php if($kpis=='3') { echo "Dedicated";}?></td>
			<!--<td valign="top"><?php echo $reportdata[$q]['details'][$w]['dept']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['subdept']; ?></td>-->
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['entrydate']; ?></td>
			<!--<td valign="top"><?php echo $reportdata[$q]['details'][$w]['totalparts']; ?></td>-->
			<!--<td valign="top"><?php echo $reportdata[$q]['details'][$w]['onlinehours']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['shortagehours']; ?></td>-->
			<td valign="top"><?php echo substr($reportdata[$q]['details'][$w]['presenthours'],0,-3)." ( ".$presentdays." )"; ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['details'][$w]['actualhours'],0,-3); ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['details'][$w]['billablehours'],0,-3); ?></td>
		</tr>	
		<?php
			}
			if(count($reportdata[$q]['reworkdetails']) > 0){
					for($t=0;$t<count($reportdata[$q]['reworkdetails']);$t++){
						$reworkpresentdays = 0;
						if($reportdata[$q]['details'][$t]['presenthours']==$reportdata[$q]['details'][$t]['static_availhrs']){
							$reworkpresentdays += 1;
						}
						elseif($reportdata[$q]['details'][$t]['presenthours']=='04:15:00'){
							$reworkpresentdays += 0.5;
						}
						else{
							$reworkpresentdays += 0;
						}
						$reworktotpresentdays += $reworkpresentdays;						
					}
			}
			if(count($reportdata[$q]['internalpirdetails']) > 0){
					for($u=0;$u<count($reportdata[$q]['internalpirdetails']);$u++){
						$interpirpresentdays = 0;
						if($reportdata[$q]['details'][$u]['presenthours']==$reportdata[$q]['details'][$u]['static_availhrs']){
							$interpirpresentdays += 1;
						}
						elseif($reportdata[$q]['details'][$u]['presenthours']=='04:15:00'){
							$interpirpresentdays += 0.5;
						}
						else{
							$interpirpresentdays += 0;
						}
						$interpirtotpresentdays += $interpirpresentdays;						
					}
			}			
		}
		}
		?>	
<?php if($kpis=='1'){ ?>			
<tr>
	<td width="10%" align="right" colspan="4"><b>Non Dedicated Total</b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($ndavailablehoursarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($ndactualhourssarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($ndbillablehoursarr); ?></b></td>
</tr>
<tr>
	<td width="10%" align="right" colspan="4"><b>Dedicated Total</b></td>
	<td width="8%" align="center"><b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($ddactualhourssarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($tmpddbillablehoursarr); ?></b></td>
</tr>
<?php } ?>
<?php if($kpis!='1'){ ?>			
<tr>
	<td width="10%" align="right" colspan="4"><b>Total</b></td>
	<td width="8%" align="center"><b><?php echo $totpresentdays; ?></b> - <b><?php echo $reworktotpresentdays; ?></b> - <b><?php echo $interpirtotpresentdays; ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($actualhourssarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($billablehoursarr); ?></b></td>
</tr>
<?php } ?>
	<?php if($kpis=='1'){ ?>			
	<?php 
			$perdaymin = $dbase->getminutes("09:00");
		$fpc = (($dbase->addTime($ndbillablehoursarr,true))/(($dbase->addTime($ndavailablehoursarr,true)))*100);
			$tot_emp = $totempcnt;
			$dedicatedbilledhrsarr = (($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS)*60);		
		$dtu = ((($dbase->addTime($ddbillablehoursarr,true))/($dedicatedbilledhrsarr))*100);
		$util =(($dbase->addTime($ndbillablehoursarr,true) + ($perdaymin*$tot_emp))/(($dbase->addTime($ndavailablehoursarr,true))+($perdaymin*$tot_emp))*100);			
	?>
<tr>
	<td width="10%" align="right" colspan="4">((Sum of budgeted hours of non dedicated resources)/((Sum of available hours of non dedicated resources))*100) <b>FPC </b></td>
	<td width="8%" align="center"><?php echo "((".$dbase->addTime($ndbillablehoursarr,true).")/(".$dbase->addTime($ndavailablehoursarr,true).")*100)"; ?></td>
	<td width="8%" align="center"><b><?php echo round($fpc,0)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>
<tr>
	<td width="10%" align="right" colspan="4">((Sum of budgeted hours of dedicated resources)/((no of days * tot emp * dedicated working hours)*60)*100) <b>DTU</b></td>
	<td width="8%" align="center"><?php echo "((".$dbase->addTime($ddbillablehoursarr,true).")/((".$workingdayscnt." * ".$tot_emp." * ".DEDICATED_WORKING_HOURS.")*60)*100)"; ?></td>
	<td width="8%" align="center"><b><?php echo round($dtu,0)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>
<tr>
	<td width="10%" align="right" colspan="4">((Sum of budgeted hours of non dedicated proejcts ) + (9* number of dedicated resources))/((Sum of available hours of non dedicated
 resources) +(9* number of dedicated resources)) <b>Total Utilization</b></td>
	<td width="8%" align="center"><?php echo "((".$dbase->addTime($ndbillablehoursarr,true)."+ (".$perdaymin." * ".$tot_emp.")) / ((".$dbase->addTime($ndavailablehoursarr,true).") + (".$tot_emp." * ".$perdaymin."))*100)"?></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>	
<?php } ?>
	<?php if($kpis=='2'){ ?>			
	<?php 
			$util = ($dbase->addTime($actualhourssarr,true) / ($dbase->addTime($billablehoursarr,true)))*100;
	?>
<tr>
	<td width="10%" align="right" colspan="4"><b>Efficiency</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"></td>
</tr>	
<?php } ?>		
	<?php if($kpis=='3'){ ?>			
	<?php 
			$dedicatedbilledhrsarr = array();
			// ($dbase->addTime($billablehoursarr,true)
			//$tot_emp = count(array_unique($emp_array));
			$tot_emp = $totempcnt;
			$dedicatedbilledhrsarr = ($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS);
			$util = (($dbase->addTime($billablehoursarr,true)/($dedicatedbilledhrsarr*60))*100);
	?>
<tr>
	<td width="10%" align="right" colspan="4"><b>Utilization</b></td>
	<td width="8%" align="center"><?php echo "(".$dbase->addTime($billablehoursarr,true)."/(".$workingdayscnt." * ".$tot_emp." * ".DEDICATED_WORKING_HOURS.")*60)*100"; ?></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"></td>
</tr>	
<?php } ?>
	<?php if($kpis=='4'){ ?>			
	<?php 
			$perdaymin = $dbase->getminutes("09:00");
			$util = (($dbase->addTime($billablehoursarr,true) / (($totpresentdays+$reworktotpresentdays+$interpirtotpresentdays)*$perdaymin))*100);
	?>
<tr>
	<td width="10%" align="right" colspan="4"><b>Utilization</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>	
<?php } ?>
	<?php if($kpis=='5'){ ?>			
	<?php 
			$perdaymin = $dbase->getminutes("09:00");
			$util = (($dbase->addTime($billablehoursarr,true) / ($totpresentdays*$perdaymin))*100);
	?>
<tr>
	<td width="10%" align="right" colspan="4"><b>Utilization</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>	
<?php } ?>				
	   </table>
		
	<?php } ?>		
		</td>
	</tr>
</table>
</form>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
	 getemployeelist();	
	 <?php if($deptid != ''){?>
		$('#department_id').val(<?php echo $deptid; ?>).change();
	 <?php } ?>
 $('#from_date').datepicker({
	inline: true,
	dateFormat: 'dd-M-yy',
	maxDate:0,
	changeMonth: true,
	changeYear: true,
	yearRange: "-10:+0",
	onSelect: function(date){
		var dates = date.split('-');
		var month1 = dates[1].toLowerCase();
		var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
		month1 = months.indexOf(month1);
		var lastDate = new Date(dates[2], month1+1, 0);
		var y = lastDate.getFullYear(), m = lastDate.getMonth(), d = lastDate.getDate();
		m = (''+ (m+1)).slice(-2);
		var monname = months[m-1];
		var monthname = monname.charAt(0).toUpperCase() + monname.slice(1);
		$('#to_date').val(d+'-'+monthname+'-'+y);
	}	 
});
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});
			getsubdepartment();
			//getpirdropdown();
			
			$("#excelreport").val('0');
	 $(".chosen-select").chosen();
	 $(".chosen-select-multi").chosen();
	 autoselectoption("#department_id"); 
		$('.chosen-select-multi').change(function () {
			if($(".chosen-select-multi option:selected").val()==""){
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").attr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			else{
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").removeAttr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			getpirdropdown();
		});	 
	});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = '<?php echo $subdepartment_ids;?>';
	$.get("getsubdepartmentadmin.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
		if(id != ''){
			$("#subdepartment_id").trigger("chosen:updated");
			//$("#subdepartment_id").trigger('chosen:open');
		}
		getpirdropdown();
	});
}
function getexcel(){
	$("#excelreport").val('1');
	$("#frm_details").submit();
	$("#excelreport").val('0');
}
function getpirdropdown(){
	var deptid = $("#department_id").val();
	var supdeptid = $("#subdepartment_id").val();
	var pirmaster_id = '<?php echo $pirmaster_id;?>';
	$.get("getpiradmin.php?deptid="+deptid+"&supdeptid="+supdeptid+"&pirmaster_id="+pirmaster_id+"&t=1",function(data){
		$("#pirmaster_id").html(data);
		$("#pirmaster_id").trigger("chosen:updated");
		getproject();
	});
}
function getproject(){
	var id = $("#pirmaster_id").val();
	var selid = '<?php echo $project_id;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#project_id").html(data);
		//$("#project_id").trigger("chosen:updated");
	});
}

function submitform(){
	$("#frm_details").submit();
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-All-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
function getemployeelist(){
	$.get("getemplist.php?t=1",function(data){
		$("#empid").html(data);
		$("#empid").trigger("chosen:updated");
	});	
}	
</script>
</html>
<?php } ?>

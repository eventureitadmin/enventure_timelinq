<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getRBUquery12($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		$query = '';
		$query ="SELECT 
	m.empid,
	m.project_id,
	SEC_TO_TIME(m.actualseconds) as actualhours,
	SEC_TO_TIME(m.calculatedseconds) as billablehours,
	m.minimumseconds as presenthours
FROM 
	(
		SELECT 
			t1.`project_id`,
			(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
			SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
			SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
			IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."'),'08:30:00')	AS minimumseconds
		FROM 
			`timeentry` t1, 
			 pirmaster t2 
		WHERE 
			1=1  
			".$reworkcond."
			".$internalpircond."
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m 
WHERE 1=1".$cond;
		return $query;
	}
	//if(count($_POST) > 0 && $_POST['from_date'] != "" && $_POST['to_date'] != ""){
	//$from_date = explode(" ",$_POST['from_date']); 
	//$fromdate = $from_date[1]."-".date("m",strtotime($from_date[0]))."-01";
	//$to_date = explode(" ",$_POST['to_date']);
	//$todate = $to_date[1]."-".date("m",strtotime($to_date[0]))."-31";
	//$deptid = 1;
	$cond111 = '';
	$dedicatedclientary = array();
	//$clientlistquery = "SELECT m.clientname, GROUP_CONCAT( m.pirno ) AS pirno, GROUP_CONCAT( m.id ) AS pirid FROM (SELECT (SELECT c.clientname FROM clientlist c WHERE c.id = p.`client_id` ) AS clientname, p.`pirno`, p.`id` FROM `pirlist` p WHERE p.`contracttype` =2 AND p.`isActive` =1 ) m GROUP BY m.clientname";
	//$clientlistquery = "(SELECT p.id AS pirid,(SELECT c.clientname FROM clientlist c WHERE c.id = p.`client_id` ) AS clientname, p.`graph_projname`, p.`pirno`, p.`id` FROM `pirlist` p WHERE p.`contracttype` =2 AND p.`isActive` =1 ) ORDER BY graph_projname";
	$clientlistquery = "SELECT m.clientname,m.graph_projname, GROUP_CONCAT( m.pirno ) AS pirno, GROUP_CONCAT( m.id ) AS pirid FROM ((SELECT (SELECT c.clientname FROM clientlist c WHERE c.id = p.`client_id` ) AS clientname, p.`graph_projname`, p.`pirno`, p.`id` FROM `pirlist` p WHERE p.`contracttype` =2 AND p.`isActive` =1 ) ORDER BY graph_projname) m GROUP BY m.graph_projname";
	$clientlist = $dbase->executeQuery($clientlistquery,'multiple');
	for($e=0;$e<count($clientlist);$e++){
		$dedicatedclientary[$e]['clientname'] = $clientlist[$e]['graph_projname'];
		$pirary = explode(",",$clientlist[$e]['pirid']);
		if(count($pirary) > 0){
			$utilization = 0;
			for($k=0;$k<count($pirary);$k++){
				$pirmaster_id = $pirary[$k];
				if($pirmaster_id != ''){
					$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
					$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
					if($pirmastercntdet['pircnt']==1){
						$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
						$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
						$pirmasterid=$pirmasterdet['id'];
						$group112 = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";				
					}
					else{
						$pirmaster_select = "SELECT GROUP_CONCAT(id) as pircsv FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
						$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
						$pirmasteridcsv = $pirmasterdet['pircsv'];
						$group112 = "AND  t1.`pirmaster_id` IN (".$pirmasteridcsv.") GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
					}
					
					if($deptid != ''){
						$subdeptcond112 = " AND t2.department_id = '".$deptid."'";
					}
					$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
					$dedicatedempcond = '';
					if($deptid != ''){
						$dedicatedempcond .= " AND department_id = '".$deptid."'";
					}
					if($pirmaster_id != ''){
						$dedicatedempcond .= " AND id='".$pirmaster_id."'";
					}		
					$dedicateresquery="SELECT IFNULL(SUM(`noofresource`),0) as dedicatedresource FROM `pirlist` WHERE `isActive`='1' AND `contracttype`='2' ".$dedicatedempcond;
					$dedicateresresult = $dbase->executeQuery($dedicateresquery,'single');
					$totempcnt = $dedicateresresult['dedicatedresource'];			
					$datelist = $dbase->getDateLists($fromdate,$todate);
					if(count($datelist)>0){
						$dedicateddata = array();
						for($j=0;$j<count($datelist);$j++){
							$query112 = '';
							$dtcond112 = " AND t1.is_dt='1'";
							$rwcond112 = " AND t1.is_rework='0'";
							$inpircond112 = " AND t1.is_internalpir='0'";				
							unset($report112);				
							$query112 = getRBUquery12($datelist[$j],$cond111,$group112,$subdeptcond112,$dtcond112,$rwcond112,$inpircond112);
							$result112 = $dbase->executeQuery($query112,'multiple');
							if(count($result112) > 0){
								$dedicateddata[] = $result112;
							}								
						}
						$dedicatedactualhourssarr = array();
						$dedicatedbillablehoursarr = array();
						for($i=0;$i<count($dedicateddata);$i++){
							for($j=0;$j<count($dedicateddata[$i]);$j++){
								$dedicatedactualhourssarr[] = $dedicateddata[$i][$j]['actualhours'];
								$dedicatedbillablehoursarr[] = $dedicateddata[$i][$j]['billablehours'];
							}
						}
						$dedicatedbilledhrsarr = array();
						$tot_emp = $totempcnt;
						$dedicatedbilledhrsarr[] = ($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS);						
					}
				}
				$utilization += floor((($dbase->addTime($dedicatedactualhourssarr,true)) / ($dbase->addTime($dedicatedbilledhrsarr,true)))*100);
			}
		}
		$dedicatedclientary[$e]['utilization'] = $utilization;
	}
		if(count($dedicatedclientary) > 0){
			$dedicatedarr = array("#17A2B8","#FFC107","#28A745","#DC3545","#9933ff","#ff1a66","#008080","#77b300","#e63900","#e600e6","#999966","#009999","#1aff1a","#ff9900","#339966","#0077b3","#ff8533");
			$dedicatedlabel = '';
			$dedicatedvalue = '';
			$dedicatedcolor = '';
			for($dd=0;$dd<count($dedicatedclientary);$dd++){
				$dedicatedlabel .= '"'.$dedicatedclientary[$dd]['clientname'].'",';
				$dedicatedvalue .= '"'.floor($dedicatedclientary[$dd]['utilization']).'",';
				$dedicatedcolor .= '"'.$dedicatedarr[$dd].'",';
			}
			$dedicatedlabel = substr($dedicatedlabel,0,-1);
			$dedicatedvalue = substr($dedicatedvalue,0,-1);
			$dedicatedcolor = substr($dedicatedcolor,0,-1);
		}
	//}
}
?>
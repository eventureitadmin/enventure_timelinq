<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
 function getQuarterByMonth($monthNumber) {
  return floor(($monthNumber - 1) / 3) + 1;
}
	//get quarter nps monthnum or get months based on quarter
	function getquarternums($month,$type=""){
		if($type == "nps"){
			$quarter2= "6";$quarter3= "9";$quarter4= "12";$quarter1= "3";
		}else{
			$quarter2= "4,5,6";$quarter3= "7,8,9";$quarter4= "10,11,12";$quarter1= "1,2,3";
		}
		 $qnum = getQuarterByMonth($month);
		if($qnum == 1){
			return $quarter1;
		}else if($qnum == 2){
			return $quarter2;
		}else if($qnum == 3){
			return $quarter3;
		}else if($qnum == 4){
			return $quarter4;
		}
	}
	//check from and to months are exists in same quarter
	function getquarter($frommonth,$tomonth){
		$is_quarter_eq = false;
		$fquarter =getQuarterByMonth((int)$frommonth);
		$tquarter = getQuarterByMonth((int)$tomonth);
		if($fquarter == $tquarter){
			$is_quarter_eq = true;
		}
		if($fquarter != $tquarter){
			$is_quarter_eq =false;
		}
		return $is_quarter_eq;
	}
	//Show color based on value
	function getcolor($selvalue){
		
		if($selvalue <= 79 || $selvalue ==""){
			$color = "red";
		}else if($selvalue >79 && $selvalue <= 89){
			$color = "orange";
		}else if($selvalue >89 && $selvalue <101){
			$color = "green";
		}else if($selvalue >=101 && $selvalue <=110){
			$color = "orange";
		}
		else if($selvalue >110){
			$color = "red";
		}
		return $color;
	}
	//Non dedicated project efficiency query
	function getRBUquery($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		if(strtotime($date)<strtotime(date('2021-07-01'))){
			$availhrs = NON_DEDICATED_PRESENT_HOURS_BEFORE;
		}
		else{
			$availhrs = NON_DEDICATED_PRESENT_HOURS_AFTER;
		}	
		$query = '';
		$query ="SELECT n.* FROM (SELECT 
		m.clientname, m.subdepartment_id,(SELECT cl.`clientname` FROM `clientlist` cl WHERE cl.`id` =m.clientname) as cname,(SELECT sd.`subname` FROM `subdepartment`sd WHERE sd.`id`= m.subdepartment_id)as sname,
	m.empid,
	m.project_id,
	SEC_TO_TIME(m.actualseconds) as actualhours,
	SEC_TO_TIME(m.calculatedseconds) as billablehours,
	m.minimumseconds as presenthours,m.ctype
FROM 
	(
		SELECT 
		(SELECT p.`contracttype` FROM `pirlist` p WHERE p.`id`=t2.`pirno` AND  p.`contracttype` =2)as ctype,
			t2.subdepartment_id,t2.clientname,
			t1.`project_id`,t3.`subname`,
			(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
			SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
			SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
			IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."'),'09:00:00')	AS minimumseconds
		FROM 
			`timeentry` t1, 
			 pirmaster t2 ,subdepartment t3
		WHERE 
			1=1
			".$reworkcond."
			".$internalpircond."			
			AND t3.show_kpirpt=1 AND t3.id=t2.subdepartment_id
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m )n
WHERE 1=1".$cond;
		return $query;
	}	

	if(count($_POST) > 0 && $_POST['from_date'] != "" && $_POST['to_date'] != ""){
		
		$from_date = explode(" ",$_POST['from_date']); 
		$fromdate = $from_date[1]."-".date("m",strtotime($from_date[0]))."-01";
		$to_date = explode(" ",$_POST['to_date']);
		$deptid = 1;
		 
		$frommonth = array_search(substr($from_date[0],0,3),$master->getmonths());
		$tomonth = array_search(substr($to_date[0],0,3),$master->getmonths());
		
		if($tomonth == date("m") && $to_date[1] == date("Y")){
			$day = date("d");
		}else{
			$day = date("d", mktime(0, 0, 0, $tomonth+1,0,date("Y")));
		}
		 if($tomonth == date("m")){
			 $todate = $to_date[1]."-".date("m",strtotime($to_date[0]))."-".($day-1);
		 }
		 else{
			 $todate = $to_date[1]."-".date("m",strtotime($to_date[0]))."-".($day);
		 }
		
		$fromyear = $from_date[1]; 
		$toyear = $to_date[1];
		$is_period =0;
		$months ="";
		$diff = abs(strtotime($fromdate) - strtotime($todate));
		$years = floor($diff / (365*60*60*24)); 
		$months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24));
		$kpireportname_array =array();
		$is_quarter_eq = getquarter($frommonth,$tomonth);
		//Same year and quarter
		if($fromyear == $toyear  && $months <=3 && $is_quarter_eq){
			 $qstr = getquarternums($frommonth,"nps");
			 $result = $dbase->executeQuery("SELECT `month_revenue`,month_nps,`quarter_nps` FROM `kpi` 
			 			WHERE dept_id =1 AND `month` ='".(int)$frommonth."' AND year='".$fromyear."'","single");
			 $month_revenue =$result['month_revenue'] ;
			 $quarter_nps =$dbase->executeQuery("SELECT quarter_nps as qn FROM `kpi` WHERE dept_id=".$deptid." 
			 AND month =".$qstr." AND year='".$fromyear."'","single")['qn'];
			 $months1 = substr($months1,0,-1);
			//quarter revenue(Sum of 3 month revenue /3)
			 $qstr1 = getquarternums($frommonth,"");
			 $quarter_revenue = $dbase->executeQuery("SELECT SUM(`month_revenue`) as qr FROM `kpi` 
			 WHERE dept_id =".$deptid." AND `month` IN(".$qstr1.") AND year='".$fromyear."'","single")['qr'];
				if($quarter_revenue >0){
					 $quarter_revenue = $quarter_revenue/3;
					 $quarter_revenue  = round($quarter_revenue,0);
				}else{
					$quarter_revenue =0;
				}
				if($quarter_nps >0){
					$quarter_nps = round(($quarter_nps),0);
				}else{
					$quarter_nps =0;
				}
		}
		// same year and month
		if($fromyear == $toyear && $frommonth == $tomonth){
			 $qstr2 = getquarternums($frommonth,"");								
			 $result = $dbase->executeQuery("SELECT `month_revenue`,month_nps,`quarter_nps` FROM `kpi` 
			 		WHERE dept_id =1 AND `month` ='".(int)$frommonth."' AND year='".$fromyear."'","single");
			 $month_revenue =$result['month_revenue'] ;
			
			 $quarter_revenue = $dbase->executeQuery("SELECT SUM(`month_revenue`) as qr FROM `kpi` 
			 	WHERE dept_id =".$deptid." AND `month` IN(".$qstr2.") AND year='".$fromyear."'","single")['qr'];
			
			$qstr3 = getquarternums($frommonth,"nps");	
			$quarter_nps = $dbase->executeQuery("SELECT `quarter_nps` as qr FROM `kpi`
			WHERE dept_id =".$deptid." AND `month`= ".$qstr3." AND year='".$fromyear."'","single")['qr'];

			$is_period =0;
			$reportname = "KPI ".$_POST['from_date'];

			if($quarter_revenue >0){
				$quarter_revenue = ($quarter_revenue/3);
			}else{
				$quarter_revenue =0;
			}
			//month revenue
			array_push($kpireportname_array," Revenue : ".$from_date[0]." ".$from_date[1]);
			$reportdata1['utilization'] = $month_revenue;
			$reportdata[] = $reportdata1;
			//quarter revenue
			
			array_push($kpireportname_array,"Quarter Revenue ".$from_date[1]);
			$reportdata2['utilization'] = $quarter_revenue;
			$reportdata[] = $reportdata2;
			
			$month_nps = $result['month_nps'];
			
		}else{
			//different year and month 
			$query = "SELECT SUM(`month_revenue`) as qr 
				FROM (SELECT * FROM `kpi` WHERE `year` BETWEEN '".$fromyear."' AND '".$toyear."')m 
				WHERE (m.`month` >='".(int) $frommonth."' AND m.`year`='".$fromyear."')"; 
				if($fromyear == $toyear){
					$query .= "  AND ";
				}else{
					$query .= " OR ";
				}
				$query .= "(m.`month` <='".(int) $tomonth."' AND m.`year`='".$toyear."')";
				$qrres = $dbase->executeQuery($query,"single");
				$period_revenue =$qrres['qr']/$months;
			
				$is_period =1;
				$reportname = "KPI ".$_POST['from_date']." to ".$_POST['to_date'];
				array_push($kpireportname_array," Revenue :".$_POST['from_date']." to ".$_POST['to_date']);
				$reportdata4['utilization'] = $period_revenue;
				$reportdata[] = $reportdata4;
			//if different month then month nps NA
				$month_nps = "NA";
				//Same quarter show quarter nps
				if($months <= 3 && $is_quarter_eq){
					 $quarter_nps = $quarter_nps;
				}else{
					//diffrent quarter 
					$quarter_nps = "NA";
				}
		}
		
		$cond = '';
		$procond='';
		$group = " GROUP BY t1.`employee_id`";
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		$dedicatedempcond = '';
		if($deptid != ''){
			$dedicatedempcond .= " AND department_id = '".$deptid."'";
		}
		
		$query="SELECT IFNULL(SUM(`noofresource`),0) as dedicatedresource FROM `pirlist` 
		WHERE `isActive`='1' AND `contracttype`='2' ".$dedicatedempcond;
		$result = $dbase->executeQuery($query,'single');
		$totempcnt = $result['dedicatedresource'];			
		$datelist = $dbase->getDateLists($fromdate,$todate);
		$rptarr = array("Billed Hours / Available Hours","Actual Hours / Billed Hours","Actual Hours Used / Billed Hours","Revenue Target/Achieved");
		if(count($datelist)>0){
			$overalldata = array();
			$nondedicateddata = array();
			
			for($j=0;$j<count($datelist);$j++){
				$cond = "  ORDER BY n.sname ASC ";
				$query3 = '';
				$dtcond3 = " AND t1.is_dt='0'";
				$rwcond3 = " AND t1.is_rework='0'";
				$inpircond3 = " AND t1.is_internalpir='0'";					
				unset($report3);				
				$query3 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond3,$rwcond3,$inpircond3);
				$result3 = $dbase->executeQuery($query3,'multiple');	
				if(count($result3) > 0){
					$nondedicateddata[] = $result3;
				}					
			}	
			$nondedicatedactualhourssarr = array();
			$nondedicatedbillablehoursarr = array();
			$cnt2=0;
			//Non dedicated Project Efficiency
			for($i=0;$i<count($nondedicateddata);$i++){
					for($j=0;$j<count($nondedicateddata[$i]);$j++){
						$subdepartment_name = $nondedicateddata[$i][$j]['sname'];
						$nondedicatedactualhourssarr[$subdepartment_name][] = $nondedicateddata[$i][$j]['actualhours'];
						$nondedicatedbillablehoursarr[$subdepartment_name][] = $nondedicateddata[$i][$j]['billablehours'];
						if($nondedicateddata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins = $dbase->getminutes($nondedicateddata[$i][$j]['presenthours']);
							$minimummins = $dbase->getminutes('06:00');
							if($presentmins <= $minimummins){
								$cnt2 += 0.5;
							}
							elseif($presentmins > $minimummins){
								$cnt2 += 1;
							}
							else{
								$cnt2 += 1;
							}
						}
						else{
							$cnt2 += 1;
						}
					}
				}
			//Report names
			array_push($kpireportname_array,"Month NPS");
			if($months <=3 && $is_quarter_eq && $is_period == 1){
				array_push($kpireportname_array,"Quarter NPS");
			}
			
			if($is_period == 0){
				if($month_nps >= 1){
					$reportdata3['utilization'] = $month_nps;
				}else{
					$reportdata3['utilization'] =  0;
				}
				/*if($months <=3 && $months >1){
					if($quarter_nps >= 1){
						$reportdataqn['utilization'] = $quarter_nps;
					}else{
						$reportdataqn['utilization'] =  0;
					}
				}*/
				
			}else{
				if($is_quarter_eq){
					if($months <=3){
						$reportdata3['utilization'] = "NA";
						$reportdataqn['utilization'] = $quarter_nps;
					}else{
						$reportdata3['utilization'] = "NA";
						$reportdataqn['utilization'] = "NA";
					}
				}else{
					$reportdata3['utilization'] = "NA";
				}
			}
			$rptnamearr =array("Resource Billing Utilization","Non Dedicated Project Efficiency","Dedicated Team Utilization");
			
			$reportdata[] = $reportdata3;
			if($is_quarter_eq){
				$reportdata[] = $reportdataqn;
			}
			
			$nondedicatedactualhrsarr = array();
			$projecteff ="";
			$nondedicatedval ="";
			$ndcolor ="";
			foreach($nondedicatedactualhourssarr as $key3=>$val3){
				$projecteff .='"'.$key3.'",';
				$nondedicatedval = round(($dbase->addTime($nondedicatedactualhourssarr[$key3],true) / ($dbase->addTime($nondedicatedbillablehoursarr[$key3],true)))*100);
				$nondedicatedarr .=$nondedicatedval.",";
				$ndcolor .='"'.getcolor($nondedicatedval).'",';
			}
		
			$ndcolor = substr($ndcolor,0,-1);
			$projecteff = substr($projecteff,0,-1);
			$nondedicatedarr = substr($nondedicatedarr,0,-1);
			
			$reportdata7['utilization'] = $nondedicatedactualhrsarr;
			$reportdata[] = $reportdata7;
						
		}		
		
		$html = '';
		if(count($reportdata) > 0){
			$o=1;
			$html .= '<div class="panel panel-default" id="kpitrptpanel">	
					<div class="panel-heading text-center"><b>'.$reportname.'</b>
					<button id="export" style="float:right;padding-top:-40px;display:none"
						class="btn btn-primary">Export</button></div>
			<div class="panel-body">';
				//If data exists then show first row graph
			if($is_period == 0){
				$checkrevnpsexist =0;
			$res =$dbase->executeQuery("SELECT `revenue_target`,`achieved`,`month_nps`,`month_revenue` FROM `kpi` WHERE `dept_id`=1 AND `month`=".$frommonth."  AND year=".$fromyear,"single");
				if($res['revenue_target'] >0 && $res['achieved'] >0 && $res['month_nps'] >0){
					$checkrevnpsexist =1;
				}
			}else{
				$checkrevnpsexist =1;
			}
			//if($checkrevnpsexist){
			for($y=0;$y<count($reportdata);$y++){		
				$yy = $y+1;		
				if(round($reportdata[$y]['utilization'],0) > 100){
					$max = round($reportdata[$y]['utilization'],0);
				}
				else{
					$max = 100;
				}
				//Month and quarter revenue
				if(($is_period == 1 && $y ==0) || ($is_period == 0 && $y ==0)){
					if($is_period == 0){
						$revenueformula = $rptname[3];
					}else{
						$revenueformula = 0;
					}
					
					$html .= '<div class="row">';
					if($is_period == 1){
						$html .= '<div class="col-sm-5">';
					}else{
						$html .= '<div class="col-sm-5">';
					}
					$html .='<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>
							<div class="panel-body">';
					if($is_period == 1){
						$html.="<div class='row'><div class='col-sm-2'></div><div class='col-sm-8'>";
					}
					$html .='	<canvas id="utilization_guage'.$yy.'" height="160"></canvas><div class="text-center" style="font-size:14px;">'.$kpireportname_array[$y].' - <b>'.round($reportdata[$y]['utilization'],0).'%</b></div>
									<script>
										var opts'.$yy.' = {
											lines: 12,
											angle: 0.05, 
											lineWidth: 0.19, 
											radiusScale: 1,
											pointer: {
												length: 0.5, 
												strokeWidth: 0.035, 
												color: "#000000"
											},
											  limitMax: false,
											  limitMin: false, 											
											generateGradient: true,
											highDpiSupport: true,
											staticLabels: {
												font: "10px sans-serif",
												labels: ['.round($reportdata[$y]['utilization'],0).'],
												color: "#000000", 
												fractionDigits: 0 
											},
										staticZones: [
											   {strokeStyle: "'.$kpi_legend_color['r'].'", min: 0, max: 80},
											   {strokeStyle: "'.$kpi_legend_color['o'].'", min: 80, max: 95},
											   {strokeStyle: "'.$kpi_legend_color['g'].'", min: 95, max: '.$max.'},
											],											
										};
										var target'.$yy.' = document.getElementById("utilization_guage'.$yy.'");
										var gauge'.$yy.' = new Gauge(target'.$yy.').setOptions(opts'.$yy.');
										gauge'.$yy.'.maxValue = 100;
										gauge'.$yy.'.setMinValue(0);
										gauge'.$yy.'.animationSpeed = 32;
										gauge'.$yy.'.set('.round($reportdata[$y]['utilization'],0).');
									</script>';
					if($is_period == 1){
						$html.="</div></div>";
					}
					$html.='</div>
					</div>
				</div>';
				}
				//Month and quarter NPS(Same quarter show quarter nps)
			
				if(($is_period ==1 && $y == 1) || 
				   ($is_period ==0 && $y == 2 && $months<=3 && $is_quarter_eq) || 
				   ($is_period ==1 && $y == 2  && $months<=3 && $is_quarter_eq) || 
				   ($is_period ==0 && $y == 3  && $months<=3 && $is_quarter_eq) ){
					//echo $is_period."===".$y."---".$reportdata[$y]['utilization']."====".$kpireportname_array[$y]."<br/>";
						if($reportdata[$y]['utilization'] == "NA" && $reportdata[$y]['utilization'] != "")
						{
							$npscolor="#d0d3d4";$y1=60;$z1=105;
						}else{
							$npscolor = getcolor($reportdata[$y]['utilization']);
						}
						if(is_numeric($reportdata[$y]['utilization'])){
							if($reportdata[$y]['utilization']<10 && $reportdata[$y]['utilization'] >=0){
								$y1=70;$z1=105;
							}else if($reportdata[$y]['utilization'] >10 && $reportdata[$y]['utilization'] <=100){
								$y1=60;$z1=105;
							}
						}
					
					if($reportdata[$y]['utilization'] >=0 && $kpireportname_array[$y] !=""){
					$html .='<div class="col-sm-2">
					<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>
							<div class="panel-body"><div class="row"><div class="col-sm-2">
								<canvas id="utilization_guage'.$yy.'" width=200 height="180"  ></canvas>
		
									<script>
										var canvas1 = document.getElementById("utilization_guage'.$yy.'");
										var context = canvas1.getContext("2d");
										context.beginPath();
										context.fillStyle = "'.$npscolor.'";
										context.strokeStyle = "black";
										context.font = "25px Verdana";
										context.lineWidth = 10;
										context.arc(80, 100, 80, 0, 2 * Math.PI, false);
										context.fill();
										context.beginPath();
										context.fillStyle = "black";
										context.fillText("'.$reportdata[$y]['utilization'].'", '.$y1.', '.$z1.');
										context.fill();
									</script></div></div>
							</div>
					</div>
				</div>';
					}
					if($is_period == 1 && $y == 2){
						$html .= "</div>";
					}if(!$is_quarter_eq){ 
						$html .= "</div>";
					}
					 if($is_period == 0 && $y == 3){
						 $html .= "</div>";
					 }
				}
				
		
			 }//for	
			//}
			include_once('kpiresourcebillinggraph.php');
					$html.='<div class="row" ><div class="col-sm-6"><div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$rptnamearr[0].'</b></div>';
				$html .='<div class="panel-body" style="height:660px;">';
							$html .='<div class="row"><div class="col-sm-12 text-center">'.$rptarr[0].'</div></div>';
							$html .='<div class="row">
							<div class="col-sm-12" style="margin-top:10px;">';
					$colorarr=array("red","yellow","green");
					$cindex=0;
					foreach($resourcebillingsubdeptary as $keyr=>$valr){
						if($keyr=='EAS'){
							$valr['utilization'] = '100';
						}
						$selval =round($valr['utilization'],0);
						$color = getcolor($selval);
						if($selval > 0){
							$class= "over50";
						}else{
							$class= "";
						}
					
					$html.='<div class="col-sm-3">
					<div class="panel-heading text-center" style="padding-left:50px">'.$keyr.'</div>
										<div class="progress-circle '.$class.' p'.$selval.'">
										   <span>'.$selval.'%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid '.$color.'"></div>
											  <div class="value-bar" style="border: 0.45em solid '.$color.'"></div>
										   </div>
										</div>
									</div>';
								}
								$html .='</div>
							</div>';
							$html .='<div class="row"><div class="col-sm-12 text-center">FPC - '.$rptarr[0].'</div></div>';
							$html .='<div class="row">
							<div class="col-sm-12" style="margin-top:10px;">';
					$colorarr=array("red","yellow","green");
					$cindex=0;
					foreach($resourcebillingfpc as $keyr=>$valr){
						$selval =round($valr['utilization'],0);
						$color = getcolor($selval);
						if($selval > 0){
							$class= "over50";
						}else{
							$class= "";
						}
					if($keyr=='EAS' && $selval==0){
					$html.='<div class="col-sm-3">
					<div class="panel-heading text-center" style="padding-left:50px"></div>
									</div>';							
					}
					else if($keyr=='PSG' && $selval==0){
					$html.='<div class="col-sm-3">
					<div class="panel-heading text-center" style="padding-left:50px"></div>
									</div>';							
					}
					else{
					$html.='<div class="col-sm-3">
					<div class="panel-heading text-center" style="padding-left:50px">'.$keyr.'</div>
										<div class="progress-circle '.$class.' p'.$selval.'">
										   <span>'.$selval.'%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid '.$color.'"></div>
											  <div class="value-bar" style="border: 0.45em solid '.$color.'"></div>
										   </div>
										</div>
									</div>';
					}
								}
								$html .='</div>
							</div>';							
							$html .='<div class="row"><div class="col-sm-12 text-center">DTU - Actual Hours / Billed Hours</div></div>';
							$html .='<div class="row">
							<div class="col-sm-12" style="margin-top:10px;">';
					$colorarr=array("red","yellow","green");
					$cindex=0;
					foreach($resourcebillingdtu as $keyr=>$valr){
						if($keyr=='EAS' && $frommonth=='1' && $tomonth=='1'){
							$valr['utilization'] = '68';
						}						
						$selval =round($valr['utilization'],0);
						$color = getcolor($selval);
						if($selval > 0){
							$class= "over50";
						}else{
							$class= "";
						}
					if($selval > '0'){
					$html.='<div class="col-sm-3">
					<div class="panel-heading text-center" style="padding-left:50px">'.$keyr.'</div>
										<div class="progress-circle '.$class.' p'.$selval.'">
										   <span>'.$selval.'%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid '.$color.'"></div>
											  <div class="value-bar" style="border: 0.45em solid '.$color.'"></div>
										   </div>
										</div>
									</div>';
					}
					else{
					$html.='<div class="col-sm-3">
					<div class="panel-heading text-center" style="padding-left:50px"></div>
									</div>';						
					}
								}
								$html .='</div>
							</div>					
					</div></div></div>';
				//Non dedicated project efficiency
			
					$html.='<div class="col-sm-5">
					<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$rptnamearr[1].'</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage'.$yy.'" width="500" height="250" ></canvas>
								<div class="text-center" style="font-size:14px;">'.$rptarr[2].'</div>
							<script>
							
						var ctx = document.getElementById("utilization_guage'.$yy.'").getContext("2d");
						var myChart = new Chart(ctx, {
							type: "horizontalBar",
		
							data: {
							
							labels: ['.$projecteff.'],
								datasets: [{
								backgroundColor: ['.$ndcolor.'],
								data: ['.$nondedicatedarr.'],
								borderWidth: 1, boxWidth: 10,
								}]
							},
							options: {
           						responsive: false,
           						maintainAspectRatio: false,							
								legend: {
								display: false
							},
								scales: {
								 xAxes: [{
										ticks: {
											min: 0 // Edit the value according to what you need
										}
									}],
									yAxes: [{
										barPercentage: 0.3,
										display: true,
                            
									}]
								},
								tooltips: {
												enabled: true,
												callbacks: {
														   label: function (tooltipItems, data) {
																return tooltipItems.xLabel + " %";
														   }	
														   }
											},
											hover: {
												animationDuration: 1
											},
											animation: {
											duration: 1,
											onComplete: function () {
												var chartInstance = this.chart,
													ctx = chartInstance.ctx;
													ctx.textAlign = "center";
													ctx.fillStyle = "rgba(0, 0, 0, 1)";
													ctx.textBaseline = "bottom";
													this.data.datasets.forEach(function (dataset, i) {
														var meta = chartInstance.controller.getDatasetMeta(i);
														meta.data.forEach(function (bar, index) {
															var data = dataset.data[index];
																ctx.fillText(data+"%", bar._model.x-25, bar._model.y+6);
														});
													});
												}
											}
							}
						});
						</script>
							</div>
					</div>
				</div>';

					$html .= '</div>';
			
		//Dedicated project efficincy
			include_once("kpidedicatedgraph.php");	
				$html .='<div class="row"><div class="col-sm-6">
						<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$rptnamearr[2].'</b></div>
						<div class="panel-body">
								<canvas id="dedicated_utilization_guage" width="500"></canvas>
								<div class="text-center" style="font-size:14px;">'.$rptarr[1].'</div>
								<script>
						var ctx = document.getElementById("dedicated_utilization_guage").getContext("2d");
						var myChart = new Chart(ctx, {
							type: "horizontalBar",
							width:600,
							data: {
							labels:['.$dedicatedlabel.'],
								datasets: [{
									data: ['.$dedicatedvalue.'],
									backgroundColor: ['.$dedicatedcolor.'],
									borderWidth: 1
								}]
							},
							options: {
								legend: {
								display: false
							},
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero: true
										}
									}]
								},
								tooltips: {
												enabled: true,
												  callbacks: {
														   label: function (tooltipItems, data) {
																return tooltipItems.xLabel + " %";
														   }	
														   }
											},
											hover: {
												animationDuration: 1
											},
											animation: {
											duration: 1,
											onComplete: function () {
												var chartInstance = this.chart,
													ctx = chartInstance.ctx;
													ctx.textAlign = "center";
													ctx.fillStyle = "rgba(0, 0, 0, 1)";
													ctx.textBaseline = "top";
													this.data.datasets.forEach(function (dataset, i) {
														var meta = chartInstance.controller.getDatasetMeta(i);
														meta.data.forEach(function (bar, index) {
															var data = dataset.data[index];
															ctx.fillText(data+"%", bar._model.x+14, bar._model.y-5);
														});
													});
												}
											}
							}
						});
						</script>
							</div>
							</div></div>';
					$legendhtml = '';
					$legendhtml .='<div class="col-sm-3">
									<ul class="list-group">';
					foreach($report_legend as $key => $val){
						$expval = explode(" : ",$val); 
						$legendhtml .= '<li class="list-group-item" style="font-size:12px;"><b>'.$expval[0].'</b> : '.$expval[1].'</li>';
					}
					$legendhtml .= '</ul></div>';

					$html.=$legendhtml.'</div>';
		}//rpt
		echo $html;exit;
	}
}
?>

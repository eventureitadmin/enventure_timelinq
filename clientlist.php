<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	$checkedn = "checked";
	$checkedy = "";
	
	$mdgcheckedn = "checked";
	$mdgcheckedy = "";
	
	$checkeda = "checked";
	$checkedia = "";
	$requiredcls ="";
	if($_GET){
		$id = $_GET['id'];
	
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		$client_query = "SELECT *,(SELECT `name` FROM `department` WHERE `id`=c.department_id) as dname,
		(SELECT `subname` FROM `subdepartment` WHERE `id`=c.subdepartment_id) as sdname FROM clientlist as c WHERE c.id='".trim($id)."'";
		$client_result = $dbase->executeQuery($client_query,"single");	
		if($client_result != ""){
			if($client_result['is_partlinq_mapping'] ==1){
				$checkedn = "";
				$requiredcls ="required";
				$checkedy = "checked";
			}
			if($client_result['mdg_mapping'] ==1){
				$mdgcheckedn = "";
				$requiredcls ="required";
				$mdgcheckedy = "checked";
			}
			if($client_result['isActive'] ==0){
				$checkeda = "";
				$checkedia = "checked";
			}
		}
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <script src="js/notify.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		
		table#clienttable,	table#clientlisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#clienttable td, table#clienttable th,table#clientlisttable td, table#clientlisttable th {
			border: 1px solid black;
			padding:5px;
		}	
	
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php //include("adminmenu.php"); ?>
<?php include("userrolemenu.php"); ?>
</td>
	<?php if(isset($_GET['id']) ){?>
<td align="center" width="80%" valign="top">
<form id="frm_client" action="clientupdate.php" method="POST" enctype="multipart/form-data">
<table id="clienttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 

<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Client Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Client Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> Client</b></td></tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department</b></td>
	<td height="40px" align="left" style="padding-left:10px"><?php echo $client_result['dname'];?></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px"><?php echo $client_result['sdname'];?></td>
</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client Name</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="name" id="name" value="<?php echo $client_result['clientname']; ?>" class="required alphanumeric" /></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Status</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="radio"  name="isActive" id="isActivea" value="1" <?php echo $checkeda;?>/>Active<input type="radio"  name="isActive" id="isActiveia" value="0" <?php echo $checkedia;?>/>Inactive</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Is Partlinq Mapping</b></td>
	<td height="40px" align="left" style="padding-left:10px">
		<input type="radio"  name="is_partlinq_mapping" id="is_partlinq_mappinga" value="1" <?php echo $checkedy;?> />Yes
		<input type="radio"  name="is_partlinq_mapping" id="is_partlinq_mappingn" value="0" <?php echo $checkedn;?> />No</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>MDG Mapping </b></td>
	<td height="40px" align="left" style="padding-left:10px">
		<input type="radio"  name="mdg_mapping" id="mdg_mappinga" value="1" <?php echo $mdgcheckedy;?> onchange="changemdgmapping();"/>Yes
		<input type="radio"  name="mdg_mapping" id="mdg_mappingn" value="0" <?php echo $mdgcheckedn;?> onchange="changemdgmapping();"/>No</td>
</tr>

<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
			<input type="hidden" name="subdept" id="subdept" value="<?php echo $client_result['subdepartment_id']; ?>" />
		<?php } else {?>
		<input type="hidden" name="subdept" id="subdept" value="" />
		<?php } ?>
		<input type="button" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
	<?php }?>
<td align="center" width="80%" valign="top">

		<?php if($_GET['m']=='1'){ ?>
	<b style="color:red;"><?php  echo "Updated Successfully"; ?></b>
	<?php }	?>
		<?php if($_GET['m']=='2'){ ?>
	<b style="color:red;"><?php  echo "Client Already Exists"; ?></b>
	<?php }	?>
		<?php if($_GET['m']=='3'){ ?>
	<b style="color:red;"><?php  echo "Client Deleted Successfully"; ?></b>
	<?php }	?>
	<!--<div style="width: auto; min-height:250px; overflow-y:scroll;height:250px;"></div>-->
	<table id="clientlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="25%" align="left"><b>Department</b></td>
			<td width="25%" align="left"><b>Sub Department</b></td>
			<td width="25%" align="left"><b>Client Name</b></td>
			<td width="25%" align="left"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND department_id='".$_SESSION['timesheet']['DEPART']."' AND subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$clientQuery = "SELECT c.id,c.clientname,(SELECT d.name FROM department d WHERE d.id=c.department_id) as deptname,(SELECT s.subname FROM subdepartment s WHERE s.id=c.subdepartment_id) as subdeptname FROM clientlist c WHERE c.isActive='1'".$depart_cond;
		$clientResult = $dbase->executeQuery($clientQuery,"multiple");
		for($i=0;$i<count($clientResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['clientname'];?></td>
			<td align="left" style="padding-left:10px"><?php if($_SESSION['timesheet']['ROLEID'] == SUPERADMIN_ROLE_ID || $_SESSION['timesheet']['ROLEID'] == ADMIN_ROLE_ID){?><a href="clientlist.php?id=<?php echo $clientResult[$i]['id'];?>">Edit</a> |<?php } ?><a href="clientlist.php?del=1&id=<?php echo $clientResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th>Department</th>
				<th>Sub Department</th>
				<th>Client Name</th>
                <th>Action</th>
            </tr>
        </tfoot>			
	</table>
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
	
$(document).ready(function(){
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
	  $("#frm_client").validate();	
    // Setup - add a text input to each footer cell
    $('#clientlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#clientlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
	
	$("#submit").click(function(){
		$("#frm_client").validate();
		
		$.ajax({
		type:"POST",
		url:"clientupdate.php",
		data:$('form').serializeArray(),
	  	success:function(data){
			var obj =JSON.parse(data);console.log(obj);
			if(obj.status ="success" && (obj.m ==1 || obj.m==3)){
				window.location.href = "clientlist.php?m="+obj.m;
			}else{
				$.notify(obj.msg,"error");
			}
		}
		});
		//$("#frm_client").submit();
	});
<?php if($id!=''){ ?>
getsubdepartment();
<?php } ?>	
	autoselectoption("#department_id");
});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = $("#subdept").val();
	$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
	});
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
</script>
</html>
<?php } ?>

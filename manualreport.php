<?php
include_once("config.php");
require_once('htmlexcel.php');
//$query = "SELECT m.empname,m.pirno,m.proname,m.actname,m.totalparts,m.calculatedhours,m.actualhours FROM (SELECT t.`employee_id`, (SELECT e.emp_name FROM employeelist e WHERE e.id=t.employee_id) as empname, t.`pirmaster_id`, (SELECT p.pirno FROM pirmaster p WHERE p.id=t.pirmaster_id) as pirno,(SELECT p.projectname FROM pirmaster p WHERE p.id=t.pirmaster_id) as proname, t.`activity_id`, (SELECT a.name FROM activity a WHERE a.id=t.activity_id) as actname, t.`totalparts`, t.`calculatedhours`, t.`actualhours`  FROM `timeentry` t WHERE t.isActive='1' AND t.`entrydate` = '2018-05-09' AND t.pirmaster_id='2' ORDER BY t.`pirmaster_id` ASC) m";

$startdate = '2018-05-09';
$enddate = '2018-05-16';
$datelist = $dbase->getDateLists($startdate,$enddate);
$html = '';
$html .= '<style>table#detailstable {empty-cells: show;border-collapse: collapse;font-size:12px;letter-spacing:1px;}table#detailstable td, table#detailstable th {border: 1px solid black;padding:5px;}</style>';
$css = 'table#detailstable {empty-cells: show;border-collapse: collapse;font-size:12px;letter-spacing:1px;}table#detailstable td, table#detailstable th {border: 1px solid black;padding:5px;}';
for($i=0;$i<count($datelist);$i++){
$html .= '<table id="detailstable" width="100%">';	
	$entrydate = $datelist[$i];
			$html .= '<tr>';
			$html .= '<td><b>Entry Date</b></td>';
			$html .= '<td>'.$dbase->dateFormatToDisplay($entrydate).'</td>';
			$html .= '</tr>';
	$projectlist = $dbase->getMultipleRows("pirmaster","WHERE isActive='1'");
	for($j=0;$j<count($projectlist);$j++){
		$pid = $projectlist[$j]['id'];
		$pname = $projectlist[$j]['projectname'];
		$pirno = $projectlist[$j]['pirno'];
		$checkproject = "SELECT COUNT( ID ) AS cnt FROM `timeentry` WHERE `pirmaster_id` = '".$pid."' AND `entrydate` = '".$entrydate."'";
		$checkprojectresult = $dbase->executeQuery($checkproject,"single");
		if($checkprojectresult['cnt'] > 0){
			$html .= '<tr>';
			$html .= '<td><b>Project Name (PIR No)</b></td>';
			$html .= '<td> '.$pname.' ( '.$pirno.' )</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="2">';
			$html .= '<table id="detailstable" width="100%">';
			$html .= '<tr>';
			$html .= '<td rowspan="2"><b>Employee Name ( Emp Code)</b></td>';
			$actquery = "SELECT t.`activity_id` AS actid, (SELECT a.name FROM activity a WHERE a.id = t.activity_id ) AS actname FROM `timeentry` t WHERE t.`entrydate` = '".$entrydate."' AND t.`pirmaster_id` = '".$pid."' GROUP BY t.`activity_id`";
			$actresult = $dbase->executeQuery($actquery,"multiple");
			for($k=0;$k<count($actresult);$k++){
				$html .= '<td colspan="3" align="center"><b>'.$actresult[$k]['actname'].'</b></td>';
			}		
			$html .= '</tr>';
			$html .= '<tr>';
			for($k1=0;$k1<count($actresult);$k1++){	
			$html .= '<td align="center"><b>Total Parts</b></td>';
			$html .= '<td align="center"><b>Budgeted Hours</b></td>';
			$html .= '<td align="center"><b>Actual Hours</b></td>';
			}
			$html .= '</tr>';
			$empquery = "SELECT t.`employee_id` AS empid, (SELECT e.emp_name FROM employeelist e WHERE e.id=t.employee_id) as empname, (SELECT e.emp_username FROM employeelist e WHERE e.id=t.employee_id) as empuser FROM `timeentry` t WHERE t.`entrydate` = '".$entrydate."' AND t.`pirmaster_id` ='".$pid."' GROUP BY t.`employee_id`";
			$empresult = $dbase->executeQuery($empquery,"multiple");
				for($l=0;$l<count($empresult);$l++){
					$html .= '<tr>';
					$html .= '<td >'.$empresult[$l]['empname'].' ( '.$empresult[$l]['empuser'].' )</td>';
					for($k2=0;$k2<count($actresult);$k2++){
						$timequery = "SELECT `totalparts` , `calculatedhours` , `actualhours` FROM `timeentry` WHERE `entrydate` = '".$entrydate."' AND `employee_id` ='".$empresult[$l]['empid']."' AND `pirmaster_id` ='".$pid."' AND `activity_id` = '".$actresult[$k2]['actid']."'";
						$timeresult = $dbase->executeQuery($timequery,"single");
					$html .= '<td>'.$timeresult['totalparts'].'</td>';
					$html .= '<td>'.$timeresult['calculatedhours'].'</td>';
					$html .= '<td>'.$timeresult['actualhours'].'</td>';
					}					
					$html .= '</tr>';
				}
			$html .= '</table>';
			$html .= '</td>';
			$html .= '</tr>';			
		}
	}
	$html .= '</table><br><br><br><br><br><br>';
}
$xls = new HtmlExcel();
$xls->setCss($css);
$xls->addSheet("Report", $html);
$xls->headers();
echo $xls->buildFile();
//echo $html;
?>
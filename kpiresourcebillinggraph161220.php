<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	
	function getresourcebillingRBUquery($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		$query = '';
		$query ="SELECT n.* FROM (SELECT 
			m.empid,
			m.project_id,
			SEC_TO_TIME(m.actualseconds) as actualhours,
			SEC_TO_TIME(m.calculatedseconds) as billablehours,
			m.minimumseconds as presenthours,m.subdepartment_id
			FROM 
				(
					SELECT 

						t2.subdepartment_id,t2.clientname,
						t1.`project_id`,
						(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
						SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
						SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
						IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND 	t2.log_date='".$date."'),'09:00:00') AS minimumseconds
		FROM 
			`timeentry` t1, 
			 pirmaster t2
		WHERE 
			1=1
			".$reworkcond."
			".$internalpircond."
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m )n
WHERE 1=1".$cond;
	
		return $query;
	}
	
	$cond111 = '';
	$group = " GROUP BY t1.`employee_id`";
	$resourcebillingsubdeptary = array();
	$subdeptquery = "SELECT id,subname FROM `subdepartment` WHERE `isActive`=1 AND `show_kpirpt`=1 AND department_id='".$deptid."' ORDER BY `subname` ASC";
	$subdeptlist = $dbase->executeQuery($subdeptquery,'multiple');
	for($e=0;$e<count($subdeptlist);$e++){
		$overallinterpirdata =array();
		$overalldata =array();
		$overallinterpirdata =array();
		$subdept = $subdeptlist[$e]['subname'];
		$subdeptlist[$e]['subname'] = $subdept;
		$cond = '';	
		$procond='';
		$group = " GROUP BY t1.`employee_id`";
		if($deptid != ''){
			 $subdeptcond = " AND t2.department_id = '".$deptid."' AND t2.subdepartment_id ='".$subdeptlist[$e]['id']."'";
		}

		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){
				for($j=0;$j<count($datelist);$j++){
				$query1 = '';
				$dtcond1 = "";
				$rwcond1 = " AND t1.is_rework='0'";
				$inpircond1 = " AND t1.is_internalpir='0'";
				unset($report1);				
				$query1 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond1,$rwcond1,$inpircond1);
				$result1 = $dbase->executeQuery($query1,'multiple');
				if(count($result1) > 0){
					$overalldata[] = $result1;
				}
				
				$query12 = '';
				$dtcond12 = "";
				$rwcond12 = " AND t1.is_rework='1'";
				$inpircond12 = " AND t1.is_internalpir='0'";
				unset($report12);				
				$query12 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond12,$rwcond12,$inpircond12);
				$result12 = $dbase->executeQuery($query12,'multiple');
				if(count($result12) > 0){
					$overallreworkdata[] = $result12;
				}
				$query13 = '';
				$dtcond13 = "";
				$rwcond13 = " AND t1.is_rework='0'";
				$inpircond13 = " AND t1.is_internalpir='1'";
				unset($report13);				
				$query13 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond13,$rwcond13,$inpircond13);
				$result13 = $dbase->executeQuery($query13,'multiple');
				if(count($result13) > 0){
					$overallinterpirdata[] = $result13;
				}				
				
				}
				$overallactualhourssarr = array();
				$overallactualhourssarr = array();
				$overallbillablehoursarr = array();
				$cnt1=0;
				for($i=0;$i<count($overalldata);$i++){
					for($j=0;$j<count($overalldata[$i]);$j++){
						$overallactualhourssarr[] = $overalldata[$i][$j]['actualhours'];
						$overallbillablehoursarr[] = $overalldata[$i][$j]['billablehours'];
						if($overalldata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins = $dbase->getminutes($overalldata[$i][$j]['presenthours']);
							$minimummins = $dbase->getminutes('06:00');
							if($presentmins <= $minimummins){
								$cnt1 += 0.5;
							}
							elseif($presentmins > $minimummins){
								$cnt1 += 1;
							}
							else{
								$cnt1 += 1;
							}
						}
						else{
							$cnt1 += 1;
						}
					}
				}
			
				$reworkcnt1=0;
				for($i=0;$i<count($overallreworkdata);$i++){
					for($j=0;$j<count($overallreworkdata[$i]);$j++){
						if($overallreworkdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins1 = $dbase->getminutes($overallreworkdata[$i][$j]['presenthours']);
							$minimummins1 = $dbase->getminutes('06:00');
							if($presentmins1 <= $minimummins1){
								$reworkcnt1 += 0.5;
							}
							elseif($presentmins1 > $minimummins1){
								$reworkcnt1 += 1;
							}
							else{
								$reworkcnt1 += 1;
							}
						}
						else{
							$reworkcnt1 += 1;
						}
					}
				}
			
				$interpircnt1=0;
				for($i=0;$i<count($overallinterpirdata);$i++){
					for($j=0;$j<count($overallinterpirdata[$i]);$j++){
						if($overallinterpirdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins2 = $dbase->getminutes($overallinterpirdata[$i][$j]['presenthours']);
							$minimummins2 = $dbase->getminutes('06:00');
							if($presentmins2 <= $minimummins2){
								$interpircnt1 += 0.5;
							}
							elseif($presentmins2 > $minimummins2){
								$interpircnt1 += 1;
							}
							else{
								$interpircnt1 += 1;
							}
						}
						else{
							$interpircnt1 += 1;
						}
					}
				}	
		//+$reworkcnt1+$interpircnt1
$resourcebillingsubdeptary[$subdept]['utilization'] = ($dbase->addTime($overallbillablehoursarr,true) / (($cnt1) * $dbase->getminutes("09:00")))*100;
		}		
	
	}
		
	}
?>

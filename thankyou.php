<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{

	$showlogouttime = 0;
	$datafill = 0;
	$time1 = date("H:i:s");
	$timearr = explode(":",$time1);
	$currentdate = date('Y-m-d');
	if(strlen($timearr[0])==1){
		$time = "0".$timearr[0].":".$timearr[1].":".$timearr[2];
	}
	else{
		$time = $timearr[0].":".$timearr[1].":".$timearr[2];
	}
	if($_POST){
		$empid = $_POST['empid'];
		$logtype = $_POST['logtype'];
		$currentdate = $_POST['currentdate'];
		$logid = $_POST['logid'];
		$ipaddress = $_POST['ipaddress'];
		if($logtype=='1'){
			$login_time = $_POST['login_time'];
			$logouttime = $_POST['logouttime'];
			$logout_comments = $_POST['logout_comments'];	
		}
		
		if($logtype=='0'){
			$logintime = $_POST['logintime'];
			$login_comments = $_POST['login_comments'];
		}
		if($logid=='0'){
			// insert
			$loginsert = "INSERT INTO `time_log` (`ID`, `emp_id`, `log_date`, `login_time`, `login_comments`, `login_ip`) VALUES (NULL, '".$empid."', '".$currentdate."', '".$currentdate." ".$logintime."', '".$login_comments."', '".$ipaddress."');";
			$dbase->executeNonQuery($loginsert);
			header("Location:index.php");
			exit;
		}
		else{
			//update
			$logupdate = "UPDATE `time_log` SET `logout_time`='".$currentdate." ".$logouttime."', `logout_comments`='".$logout_comments."', `logout_ip`='".$ipaddress."' WHERE ID='".$logid."'";
			$dbase->executeNonQuery($logupdate);
			$tottimequery = "SELECT login_time,logout_time FROM time_log WHERE ID='".$logid."'";
			$tottimeresult = $dbase->executeQuery($tottimequery,'single');
			$uptimediff = $dbase->timediff($tottimeresult['logout_time'],$tottimeresult['login_time'],false);
			$totalhours = $uptimediff['hrs'].":".$uptimediff['min'].":".$uptimediff['sec'];
			$timeupdate = "UPDATE `time_log` SET `totalhours`='".$totalhours."' WHERE ID='".$logid."'";
			$dbase->executeNonQuery($timeupdate);			
			header("Location:index.php");
			exit;			
		}
		
	}
	$empDetQuery = "SELECT * FROM employeelist WHERE ID = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND isactive='1'";
	$empDetResult = $dbase->executeQuery($empDetQuery,"single");
	$emplogDetQuery = "SELECT `ID`,`log_date`, `login_time`, `login_comments`, `logout_time`, `logout_comments`,`totalhours` FROM time_log WHERE `emp_id`='".$empDetResult['ID']."' AND log_date='".$currentdate."'";
	$emplogDetResult = $dbase->executeQuery($emplogDetQuery,"single");
	if($emplogDetResult['login_time'] =='' || $emplogDetResult['logout_time'] =='0000-00-00 00:00:00'){
		$datafill = 0;
		if($emplogDetResult['ID'] !=''){
			$showlogouttime = 1;
			$logid = $emplogDetResult['ID'];
		}
		else {
			$showlogouttime = 0;
			$logid = 0;
		}		
	}
	else{
		$timearr = explode(":",$emplogDetResult['totalhours']);
		$totaltime = $timearr[0]." Hrs ".$timearr[1]." Min ".$timearr[2]." Sec";
		$datafill = 1;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>	
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='0'){ ?>
<form id="frm_details" action="" method="post">
<table id="detailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="50%" >
<tr>
	<td colspan="2" align="center"><h2><b>Thank You</b></h2></td>
</tr>
<tr>
	<td colspan="2" align="center">Please click <a href="login.php?logout">here</a> to exit system</td>
</tr>
</table>
</form>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){	 
	  $("#frm_details").validate();	

	});	
</script>
</html>
<?php } ?>
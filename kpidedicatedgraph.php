<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
					$datestr=strtotime($todate);
					$resourcemonth=date("m",$datestr);
					$resourceyear=date("Y",$datestr);		
	function getRBUquery12($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		if(strtotime($date)<strtotime(date('2021-07-01'))){
			$availhrs = NON_DEDICATED_PRESENT_HOURS_BEFORE;
		}
		else{
			$availhrs = NON_DEDICATED_PRESENT_HOURS_AFTER;
		}		
		$query = '';
		$query ="SELECT 
				m.empid,
				m.project_id,
				SEC_TO_TIME(m.actualseconds) as actualhours,
				SEC_TO_TIME(m.calculatedseconds) as billablehours,
				m.minimumseconds as presenthours
			FROM 
				(
			SELECT 
			t1.`project_id`,
			(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
			SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
			SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
			IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."'),'08:30:00')	AS minimumseconds
		FROM 
			`timeentry` t1, 
			 pirmaster t2 
		WHERE 
			1=1  
			".$reworkcond."
			".$internalpircond."
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m 
WHERE 1=1".$cond;
		return $query;
	}

	$cond111 = '';
	$dedicatedclientary = array();
	//$clientlistquery = "SELECT m.clientname, GROUP_CONCAT( m.pirno ) AS pirno, GROUP_CONCAT( m.id ) AS pirid FROM (SELECT (SELECT c.clientname FROM clientlist c WHERE c.id = p.`client_id` ) AS clientname, p.`pirno`, p.`id` FROM `pirlist` p WHERE p.`contracttype` =2 AND p.`isActive` =1 ) m GROUP BY m.clientname";
	//$clientlistquery = "(SELECT p.id AS pirid,(SELECT c.clientname FROM clientlist c WHERE c.id = p.`client_id` ) AS clientname, p.`graph_projname`, p.`pirno`, p.`id` FROM `pirlist` p WHERE p.`contracttype` =2 AND p.`isActive` =1 ) ORDER BY graph_projname";
	// WHERE m.graph_projname='Panasonic'
	if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
		$tlcond= " AND id IN (SELECT pirno FROM pirmaster WHERE projadminid='".$_SESSION['timesheet']['ID']."')";
	}
	else{
		$tlcond= "";
	}
	$projnamecond="";
	//if($datestr){
		//$projnamecond = "WHERE 1=1 AND (m.graph_projname!='Oxford Instruments' AND m.graph_projname!='DMS')";
	//}
	$clientlistquery = "SELECT m.clientname,m.graph_projname, GROUP_CONCAT( m.pirno ) AS pirno, GROUP_CONCAT( m.id ) AS pirid FROM ((SELECT (SELECT c.clientname FROM clientlist c WHERE c.id = p.`client_id` ) AS clientname, p.`graph_projname`, p.`pirno`, p.`id` FROM `pirlist` p WHERE p.`contracttype` =2 AND p.`isActive` =1 ".$tlcond.") ORDER BY graph_projname) m ".$projnamecond." GROUP BY m.graph_projname";
	$clientlist = $dbase->executeQuery($clientlistquery,'multiple');
	for($e=0;$e<count($clientlist);$e++){
		$dedicatedclientary[$e]['clientname'] = $clientlist[$e]['graph_projname'];
		$pirary = explode(",",$clientlist[$e]['pirid']);
		if(count($pirary) > 0){
			$utilization = 0;
			for($k=0;$k<count($pirary);$k++){
				$pirmaster_id = $pirary[$k];
				if($pirmaster_id != ''){
					$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
					$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
					if($pirmastercntdet['pircnt']==1){
						$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
						$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
						$pirmasterid=$pirmasterdet['id'];
						$group112 = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
					if($deptid != ''){
						$subdeptcond112 = " AND t2.department_id = '".$deptid."'";
					}
					$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
					$dedicatedempcond = '';
					if($deptid != ''){
						$dedicatedempcond .= " AND department_id = '".$deptid."'";
					}
					if($pirmasterid != ''){
						$dedicatedempcond .= " AND id='".$pirmasterid."'";
					}
					
					//$dedicateresquery="SELECT IFNULL(`no_of_resource`,0) as dedicatedresource FROM `pirmaster` WHERE `isActive`='1' AND no_of_resource > 0 ".$dedicatedempcond;
					$dedicateresquery="SELECT IFNULL(`no_of_resource`,0) as dedicatedresource FROM `resourcemonth` WHERE `isactive`='1' AND no_of_resource > 0 AND pirmaster_id='".$pirmasterid."' AND monthval='".$resourcemonth."' AND yearval='".$resourceyear."'";
					$dedicateresresult = $dbase->executeQuery($dedicateresquery,'single');
					$totempcnt = $dedicateresresult['dedicatedresource'];
					$datelist = $dbase->getDateLists($fromdate,$todate);
					if(count($datelist)>0){
						$dedicateddata = array();
						for($j=0;$j<count($datelist);$j++){
							$query112 = '';
							$dtcond112 = " AND t1.is_dt='1'";
							$rwcond112 = " AND t1.is_rework='0'";
							$inpircond112 = " AND t1.is_internalpir='0'";				
							unset($report112);				
							$query112 = getRBUquery12($datelist[$j],$cond111,$group112,$subdeptcond112,$dtcond112,$rwcond112,$inpircond112);
							
							$result112 = $dbase->executeQuery($query112,'multiple');
							if(count($result112) > 0){
								$dedicateddata[] = $result112;
							}								
						}
						$dedicatedactualhourssarr = array();
						$dedicatedbillablehoursarr = array();
						for($i=0;$i<count($dedicateddata);$i++){
							for($j=0;$j<count($dedicateddata[$i]);$j++){
								$dedicatedactualhourssarr[] = $dedicateddata[$i][$j]['actualhours'];
								$dedicatedbillablehoursarr[] = $dedicateddata[$i][$j]['billablehours'];
							}
						}
						$dedicatedbilledhrsarr = array();
						$tot_emp = $totempcnt;
						$dedicatedbilledhrsarr = (($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS)*60);						
						$utilization += round(((($dbase->addTime($dedicatedbillablehoursarr,true)) / ($dedicatedbilledhrsarr))*100),0);
					}						
					}
					else{
						$tot_emp = 0;
						$utilization = 0;
						$dedicatedactualhourssarr = array();
						$dedicatedbillablehoursarr = array();
						$dedicatedbilledhrsarr=0;
						$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."' AND no_of_resource > 0";
						$pirmasterdet = $dbase->executeQuery($pirmaster_select,'multiple');
						for($k=0;$k<count($pirmasterdet);$k++){
						$tot_emp = 0;
						$dedicatedactualhourssarr = array();
						$dedicatedbillablehoursarr = array();
						$dedicatedbilledhrsarr=0;							
							$group112 = "AND  t1.`pirmaster_id` ='".$pirmasterdet[$k]['id']."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
							if($deptid != ''){
								$subdeptcond112 = " AND t2.department_id = '".$deptid."'";
							}
							$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
							$dedicatedempcond = '';
							if($deptid != ''){
								$dedicatedempcond .= " AND department_id = '".$deptid."'";
							}
							if($pirmasterdet[$k]['id'] != ''){
								$dedicatedempcond .= " AND id='".$pirmasterdet[$k]['id']."'";
							}		
							//$dedicateresquery="SELECT IFNULL(`no_of_resource`,0) as dedicatedresource FROM `pirmaster` WHERE `isActive`='1' ".$dedicatedempcond;
							$dedicateresquery="SELECT IFNULL(`no_of_resource`,0) as dedicatedresource FROM `resourcemonth` WHERE `isactive`='1' AND no_of_resource > 0 AND pirmaster_id='".$pirmasterdet[$k]['id']."' AND monthval='".$resourcemonth."' AND yearval='".$resourceyear."'";
							$dedicateresresult = $dbase->executeQuery($dedicateresquery,'single');
							$totempcnt = $dedicateresresult['dedicatedresource'];			
							$datelist = $dbase->getDateLists($fromdate,$todate);
							if(count($datelist)>0){
								$dedicateddata = array();
								for($j=0;$j<count($datelist);$j++){
									$query112 = '';
									$dtcond112 = " AND t1.is_dt='1'";
									$rwcond112 = " AND t1.is_rework='0'";
									$inpircond112 = " AND t1.is_internalpir='0'";				
									unset($report112);				
									$query112 = getRBUquery12($datelist[$j],$cond111,$group112,$subdeptcond112,$dtcond112,$rwcond112,$inpircond112);
									$result112 = $dbase->executeQuery($query112,'multiple');
									if(count($result112) > 0){
										$dedicateddata[] = $result112;
									}								
								}

								for($i=0;$i<count($dedicateddata);$i++){
									for($j=0;$j<count($dedicateddata[$i]);$j++){
										$dedicatedactualhourssarr[] = $dedicateddata[$i][$j]['actualhours'];
										$dedicatedbillablehoursarr[] = $dedicateddata[$i][$j]['billablehours'];
									}
								}
								$tot_emp = $totempcnt;
							}
							//echo $workingdayscnt." - ".$tot_emp." - ".DEDICATED_WORKING_HOURS."<br/>";
						$dedicatedbilledhrsarr = (($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS)*60);						
						$utilization += round(((($dbase->addTime($dedicatedbillablehoursarr,true)) / ($dedicatedbilledhrsarr))*100),0);
						}
					}
				}
				
			}
		}
		$dedicatedclientary[$e]['utilization'] = $utilization;
	}
		if(count($dedicatedclientary) > 0){
			$dedicatedarr = array("#17A2B8","#FFC107","#28A745","#DC3545","#9933ff","#ff1a66","#008080","#77b300","#e63900","#e600e6","#999966","#009999","#1aff1a","#ff9900","#339966","#0077b3","#ff8533");
			$dedicatedlabel = '';
			$dedicatedvalue = '';
			$dedicatedcolor = '';
			for($dd=0;$dd<count($dedicatedclientary);$dd++){
				/*if($dedicatedclientary[$dd]['clientname']=='IPoint' && $frommonth=='1' && $tomonth=='1'){
					$dedicatedclientary[$dd]['utilization'] = '101';
				}*/
				$dedicatedlabel .= '"'.$dedicatedclientary[$dd]['clientname'].'",';
				$dedicatedvalue .= '"'.round($dedicatedclientary[$dd]['utilization'],0).'",';
				$dedicatedcolor .= '"'.$dedicatedarr[$dd].'",';
			}
			$dedicatedlabel = substr($dedicatedlabel,0,-1);
			$dedicatedvalue = substr($dedicatedvalue,0,-1);
			$dedicatedcolor = substr($dedicatedcolor,0,-1);
		}
	//}
}
?>

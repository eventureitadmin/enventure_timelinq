<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
		if($_SESSION['timesheet']['ROLEID'] == HR_ROLE){
			header("Location:users.php");
		}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	  <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	  <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	<!---   <script type="text/javascript" src="js/bootstrap.js"></script>
	   <link type="text/css" href="css/bootstrap.css" rel="stylesheet" />--->
<script type="text/javascript">
var timetype = <?php echo TIME_TYPE; ?>;
 $(document).ready(function(){
	var time = new Date().toString("HH:mm:ss");
	if(timetype=='1'){
		$("#logintime").val(time);
		$("#logouttime").val(time);		
	}
	if(timetype=='2'){
		$("#logintime").val('<?php echo $time; ?>');
		$("#logouttime").val('<?php echo $time; ?>');
	}
	 //
	
	 	
});		
</script>	
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
	  #add_btn{
		  	border-radius: 10px 10px 10px 10px;
		  	background-color: #2184be;
		  	color:white;
		  	padding:10px;
		  float:left;
	  }
		table#userlisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#userlisttable td, table#userlisttable th {
			border: 1px solid black;
		}	
table{
  margin: 0 auto;
  width: 100%;
  clear: both;
  border-collapse: collapse;
  table-layout: fixed; 
  word-wrap:break-word; 
}
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php //if($_SESSION['timesheet']['ISADMIN']=='1' ){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" id="settingstbl">
	
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("userrolemenu.php"); ?>
</td>
	<!------Activity list---------->
	<td align="center" width="80%" valign="top">
<input type="button" name="add_btn" id="add_btn" value="Add Project" onclick="window.location.href='setup.php'" >
		<br/>
		<br/><br/>
				

		


	<table id="userlisttable" class="display" style="width:100%;">
		<thead>
		<tr>
			<td width="18%" align="left"><b>PIR No</b></td>
			<td width="18%" align="left"><b>Department</b></td>
			<td width="18%" align="left"><b>Sub Department</b></td>
			<td width="20%" align="left"><b>Client</b></td>
			<td width="20%" align="left"><b>Project</b></td>
			<td width="18%" align="left"><b>Action</b></td>
		</tr>
	</thead>
	<?php
		$pir_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$pir_cond = " AND p.department_id='".$_SESSION['timesheet']['DEPART']."' AND p.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")  AND  p.projadminid = ".$_SESSION['timesheet']['ID'];
		}																									   
		$pirQuery1 = "SELECT p.`id`, (SELECT p2.pirno FROM pirlist p2 WHERE p2.id=p.pirno) as pirno,(SELECT c.clientname FROM clientlist c WHERE c.id=p.clientname) as clientname,(SELECT p1.projectname FROM projectlist p1 WHERE p1.id=p.projectname) as projectname,(SELECT d.name FROM department d WHERE d.id=p.department_id) as deptname,(SELECT sd.subname FROM subdepartment sd WHERE sd.id=p.subdepartment_id) as subdeptname 
		, p.`isActive` as piractive FROM pirmaster p WHERE";
					if($_SESSION['timesheet']['ROLEID'] >= 2){
						$pirQuery1 .= " p.isActive='1'";
					}elseif($_SESSION['timesheet']['ROLEID'] == 1){
						$pirQuery1 .= " p.isActive IN('1','0')";
					}

		$pirQuery1 .= $pir_cond;
	
		$pirResult = $dbase->executeQuery($pirQuery1,"multiple");
		for($i=0;$i<count($pirResult);$i++){
	?>
		<tr>
			<td width="18%" align="left"><?php echo $pirResult[$i]['pirno'];?></td>
			<td width="18%" align="left"><?php echo $pirResult[$i]['deptname'];?></td>
			<td width="18%" align="left"><?php echo $pirResult[$i]['subdeptname'];?></td>
			<td width="20%" align="left"><?php echo $pirResult[$i]['clientname'];?></td>
			<td width="20%" align="left"><?php echo $pirResult[$i]['projectname'];?></td>
			<td width="10%" align="left"><a href="setup.php?id=<?php echo $pirResult[$i]['id'];?>">Edit</a> | <a href="pirmaster.php?del=1&id=<?php echo $pirResult[$i]['id'];?>">Delete</a> 
			<?php 
					if(($_SESSION['timesheet']['ROLEID'] >= 2 || $_SESSION['timesheet']['ROLEID'] == 1) &&  $pirResult[$i]['piractive'] == 1){?>
					| <a href="pirmaster.php?del=1&id=<?php echo $pirResult[$i]['id'];?>">Active</a> 
					<?php }elseif( $_SESSION['timesheet']['ROLEID'] == 1 &&   $pirResult[$i]['piractive'] == 0){?>
					| <a href="pirmaster.php?del=1&id=<?php echo $pirResult[$i]['id'];?>">Deactive</a> 
					<?php }
				?>	
				 
			</td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th width="18%" align="left">PIR No</th>
                <th width="10%" align="left">Department</th>
                <th width="10%" align="left">Sub Department</th>
                <th width="10%" align="left">Client</th>
                <th width="10%" align="left">Project</th>				
                <th width="10%" align="left">Action</th>
            </tr>
        </tfoot>		
	</table>	
</td>
</tr>	
	<!--------------->

</table>
<?php //} ?>
</body>
<script type="text/javascript">
$(document).ready(function(){	
	   // Setup - add a text input to each footer cell
    $('#userlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" size="15" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#userlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	  $("#frm_details").validate();			
});	
</script>
</html>
<?php } ?>

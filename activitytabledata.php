<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		if($_POST['projid'] != ""){
		
	 	$selpirmsater = "SELECT COUNT(*) as cnt ,id FROM `pirmaster` WHERE `pirno`=".$_POST['pirid']." AND `projectname`=".$_POST['projid'];
	 	$pirmasterres = $dbase->executeQuery($selpirmsater,"single");
			if($pirmasterres['cnt'] == 1){
				$id = $pirmasterres['id'];
			}else{
				$id = "";
			}
		
		}else{
			$id = "";
		}
		//$id = "";
		//$id=$_POST['id'];
		$pirno=$_POST['pirno'];
		$tblindex=trim($_POST['tblindex']);
		$maximum_admin_entry=$_POST['max_entry'];
//SELECT * FROM `pir_activity` WHERE `pirmaster_id` =31 AND `partlinq_mapping`=0
		 $act_select = "SELECT id,name FROM `activity` WHERE isActive='1' AND `department_id`='".$_POST['dept_id']."' AND `subdepartment_id`='".$_POST['subdept_id']."'";
		$actlist = $dbase->executeQuery($act_select,'multiple');//echo "<pre>";print_r($actlist);
			$activity_html = '';
			for($i=0;$i<count($actlist);$i++){
				$activity_html .= '<option value="'.$actlist[$i]['id'].' ">'.$actlist[$i]['name'].'</option>';
			}
		$html = '';
$html .= '<table id="activitydetailstable_'.$tblindex.'" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%">
<thead>
	<tr>
		
		<td width="16%"><b>Activity</b></td>
		<td width="10%"><b>Effort Basis</b></td>
		<td width="14%"><b>Is Man Hours Mapped?</b></td>
		<td width="14%"><b>Calculate Budgeted Hours</b></td>
		<td width="14%"><b>Budgeted Time</b></td>';
		if($id != ""){
			$html .='<td width="10%"><input type="checkbox" name="projactivitychk_'.$tblindex.'" id="projactivitychk_'.$tblindex.'" onclick="checkallactivity('.$tblindex.',this.checked)">';
			$html .='<b>Completed ?</b></td>';
		}
		//$html .='<td width="10%"><b>Partlinq Mapping</b></td>';
		$html .='<td width="10%"><b>Add / Del</b></td>
	</tr>
	</thead>
	<tbody>';
	if($id!= '' && $id!= 'undefined'){
		 $act_pir_select = "SELECT id,activity_id,budgettime,calculatehours,manhrs,is_completed,activitytype,budgettimetype FROM `pir_activity` WHERE isActive='1' AND `pirmaster_id`='".$id."' ORDER BY id ASC";
		$act_pir_list = $dbase->executeQuery($act_pir_select,'multiple');
		if(count($act_pir_list)>0){
			for($j=0;$j<count($act_pir_list);$j++){
					$disabled = '';
					$readonly = '';				
				if($act_pir_list[$j]['manhrs']=='n'){
					$disabled = 'disabled = "disabled"';
					$readonly = 'readonly = "readonly"';
				}
				$activity_html_edit = '';
				for($q=0;$q<count($actlist);$q++){
					if($actlist[$q]['id']==$act_pir_list[$j]['activity_id']){
						$select = 'selected';
					}
					else{
						$select = '';
					}
			
					$activity_html_edit .= '<option value="'.$actlist[$q]['id'].'" '.$select.'>'.$actlist[$q]['name'].'</option>';
				}
				$totalhoursselect = '';
				$peractivityselect = '';
				$completedcheck = '';
				if($act_pir_list[$j]['activitytype']=='0'){
					$totalhoursselect = "selected";
				}
				if($act_pir_list[$j]['activitytype']=='1'){
					$peractivityselect = "selected";
				}
				if($act_pir_list[$j]['is_completed']=='1'){
					$completedcheck = "checked";
					$is_completedval = $act_pir_list[$j]['is_completed'];
					$compdisabled ="";
					$compdisabled = 'disabled = "disabled"';
				}else{
					$compdisabled ="";
					$is_completedval = 0;
				}	
				$budgettimetype1 = "";
				$budgettimetype2 = "";
				
				if($act_pir_list[$j]['budgettimetype']=='1'){
					$budgettimetype2 = "checked";
					if($act_pir_list[$j]['manhrs']=='y' && $act_pir_list[$j]['calculatehours']=='y'){
						$budgetclass ="required btimesec";
					}else{
						$budgetclass ="";
					}
					$budgettime = $act_pir_list[$j]['budgettime'];
				}
				if($act_pir_list[$j]['budgettimetype']=='0'){
					$budgettimetype1 = "checked";
					if($act_pir_list[$j]['manhrs']=='y' && $act_pir_list[$j]['calculatehours']=='y'){
						$budgetclass ="required timeformat";
					}else{
						$budgetclass ="";
					}
				
					$budgettime = $dbase->secToTimeFormat($act_pir_list[$j]['budgettime']);
				}
				if($act_pir_list[$j]['budgettime'] == 0){
					$act_pir_list[$j]['budgettime'] ="0";
				}
				$k1=$j+1;
				$k = $tblindex.$k1;
				$html .= '<tr id="row'.$k.'"><input type="hidden" class="projactiveid" name="projactiveid'.$k.'" id="projactiveid'.$k.'" value="'.$act_pir_list[$j]['id'].'">
						<td width="14%">
						<select id="activity_id'.$k.'" name="activity_id'.$k.'" class="required activity chosen-select" onchange="checkactivity('.$tblindex.',this.value,'.$k.')" '.$compdisabled.'>
						<option value="">-Select-</option>'.$activity_html_edit.'
						</select>		
						</td>
						<td width="14%">
							<select id="activitytype'.$k.'" name="activitytype'.$k.'" class="required activitytype" '.$compdisabled.'>
							<option value="">-Select-</option>
								<option value="0" '.$totalhoursselect.'>Total Hours</option>
								<option value="1" '.$peractivityselect.'>Per Activity</option>
							</select>
						</td>
						<td width="14%">
							<select id="manhrs'.$k.'" name="manhrs'.$k.'" class="required calculatemanhrs" onchange="changemanhrstextbox('.$k.',this.value,'.$tblindex.');" '.$compdisabled.'>
							<option value="">-Select-</option>';
							$manhrs_select_y = '';
							$manhrs_select_n = '';
							if($act_pir_list[$j]['manhrs']=='y'){
								$manhrs_select_y = 'selected="selected"';
							}
							else if($act_pir_list[$j]['manhrs']=='n'){
								$manhrs_select_n = 'selected="selected"';
							}							
							$html .= '<option value="y" '.$manhrs_select_y.'>YES</option>
							<option value="n" '.$manhrs_select_n.'>NO</option>
							</select>
						</td>						
						<td width="14%">
							<select id="calculatehours'.$k.'" name="calculatehours'.$k.'" class="required calculatehours" '.$disabled.' onchange="changebudgettextbox('.$k.',this.value,'.$tblindex.');"'.$compdisabled.'>
							<option value="">-Select-</option>';
							$calculatehours_select_y = '';
							$calculatehours_select_n = '';
							if($act_pir_list[$j]['calculatehours']=='y'){
								$calculatehours_select_y = 'selected="selected"';
							}
							else if($act_pir_list[$j]['calculatehours']=='n'){
								$calculatehours_select_n = 'selected="selected"';
							}							
							$html .= '<option value="y" '.$calculatehours_select_y.'>YES</option>
							<option value="n" '.$calculatehours_select_n.'>NO</option>
							</select>
						</td>
					
						<td width="14%">
							<input type="radio" id="bgttimetype'.$k.'" class="bgttimetype" '.$disabled.' name="bgttimetype'.$k.'" value="1" style="display:inline-block"  onchange="changebgttimetype('.$k.',1)" '.$budgettimetype1.$compdisabled.'>HH:MM
							<input type="radio" id="bgttimetype'.$k.'" class="bgttimetype" '.$disabled.' name="bgttimetype'.$k.'" value="2" style="display:inline-block" onchange="changebgttimetype('.$k.',2)" '.$budgettimetype2.' '.$compdisabled.'>Seconds
							<input type="text" id="budgettime'.$k.'" name="budgettime'.$k.'" class=" budgettime '.$budgetclass.'" '.$disabled.' value="'.$budgettime.'" size="5" '.$compdisabled.' style="width:130px;padding:4px"/>
						</td>';
				if($id != ""){
						$html .='<td width="10%" id="iscompletedtd">
						<input type="checkbox" id="is_completed'.$k.'" name="is_completed'.$k.'" value="'.$is_completedval.'" '.$completedcheck.' class="is_completed" onchange="completecheck('.$k.','.$tblindex.')"/>
						</td>';
				}
						/*<td width="10%">
		<input type="radio" id="apmappingy'.$k.'" name="apmapping'.$k.'" class="apmapping"  value="1" style="display:inline-block" >Yes
			<input type="radio" id="apmappingn'.$k.'" name="apmapping'.$k.'" class="apmapping"  value="0" style="display:inline-block" checked>No
		</td>*/
						$html .='<td width="14%">
						<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record-activity" 
							onclick="addactivity('.$tblindex.','.$maximum_admin_entry.','.$id.');" />';
					if($id == ""){
						$html .= '<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record-activity" rowid="'.$k1.'" onclick="deleteactivity('.$tblindex.','.$maximum_admin_entry.','.$k.','.$id.');"/>';
					}
						$html .= '</td>
					</tr>';							
			}	
			$cnt=$k1;			
		}
		else{
			$cnt='1';
			$rw = $_POST['prjcnt'].'1';
		$html .= '<tr id="row'.$rw.'">
			<td width="16%">
			<select id="activity_id'.$rw.'" name="activity_id'.$rw.'" class="required activity " onchange="checkactivity(1,this.value,'.$rw.')">
			<option value="">-Select-</option>'.$activity_html.'
			</select>		
			</td>
			<td width="16%">
				<select id="activitytype'.$rw.'" name="activitytype'.$rw.'" class="required activitytype">
				<option value="">-Select-</option>
				<option value="0">Total Hours</option>
				<option value="1">Per Activity</option>
				</select>
			</td>				
			<td width="16%">
				<select id="manhrs'.$rw.'" name="manhrs'.$rw.'" class="required calculatemanhrs" onchange="changemanhrstextbox('.$rw.',this.value,'.$tblindex.');">
				<option value="">-Select-</option>
				<option value="y">YES</option>
				<option value="n">NO</option>
				</select>
					
			</td>				
			<td width="16%">
				<select id="calculatehours'.$rw.'" name="calculatehours'.$rw.'" class="required calculatehours" onchange="changemanhrstextbox('.$rw.',this.value,'.$tblindex.');">
				<option value="">-Select-</option>
				<option value="y">YES</option>
				<option value="n">NO</option>
				</select>
			</td>	
		
			<td width="16%">
				<input type="radio" id="bgttimetype'.$rw.'" name="bgttimetype'.$rw.'"  class="bgttimetype" onchange="changebgttimetype('.$rw.',1)" value="1" style="display:inline-block" >HH:MM
				<input type="radio" id="bgttimetype'.$rw.'" name="bgttimetype'.$rw.'" class="bgttimetype" onchange="changebgttimetype('.$rw.',2)" value="2" style="display:inline-block" checked>Seconds
				<input type="text" id="budgettime'.$rw.'" name="budgettime'.$rw.'" class="required btimesec budgettime " size="5" style="width:130px;padding:4px"/>
			</td>';
			if($id != ""){


				$html .='<td width="10%" id="iscompletedtd">
						<input type="checkbox" id="is_completed'.$rw.'" name="is_completed'.$rw.'" value="" class="is_completed" onchange="completecheck('.$rw.','.$tblindex.')"/>
						</td>
			';
			}
			/*$html .='<td width="10%">
				<input type="radio" id="apmappingy'.$rw.'" name="apmapping'.$rw.'" class="apmapping"  value="1" style="display:inline-block" >Yes
					<input type="radio" id="apmappingn'.$rw.'" name="apmapping'.$rw.'" class="apmapping"  value="0" style="display:inline-block" checked>No
				</td>*/
			$html .='<td width="16%">
			<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record-activity" onclick="addactivity('.$tblindex.','.$maximum_admin_entry.','.$id.');" />';
			if($id == ""){
			$html .='<img src="images/remove.png" height="16" width="16" alt="RemoveRow" onclick="deleteactivity('.$tblindex.','.$maximum_admin_entry.',1,'.$id.');" class="delete-record-activity" rowid="1" />';
			}
			$html .='</td>
		</tr>';				
		}

	}
	else{
		
		$cnt='1';
		$rw =  $_POST['tblindex'].'1';
		$html .= '<tr id="row'.$tblindex.'1">
		<input type="hidden" name="projactiveid'.$rw.'" class="projactiveid" id="projactiveid'.$rw.'">
		<td width="16%">
		<select id="activity_id'.$rw.'" name="activity_id'.$rw.'" class="required activity chosen-select" onchange="checkactivity(1,this.value,'.$rw.')">
		<option value="">-Select-</option>'.$activity_html.'
		</select>		
		</td>
			<td width="10%">
				<select id="activitytype'.$rw.'" name="activitytype'.$rw.'" class="required activitytype">
				<option value="">-Select-</option>
				<option value="0">Total Hours</option>
				<option value="1">Per Activity</option>
				</select>
			</td>		
			<td width="16%">
				<select id="manhrs'.$rw.'" name="manhrs'.$rw.'" class="required calculatemanhrs" onchange="changemanhrstextbox('.$rw.',this.value,'.$tblindex.');">
				<option value="">-Select-</option>
				<option value="y">YES</option>
				<option value="n">NO</option>
				</select>
			</td>		
			<td width="16%">
				<select id="calculatehours'.$rw.'" name="calculatehours'.$rw.'" class="required calculatehours" onchange="changebudgettextbox('.$rw.',this.value,'.$tblindex.');">
				<option value="">-Select-</option>
				<option value="y">YES</option>
				<option value="n">NO</option>
				</select>
			</td>
			
		<td width="10%">
			<input type="radio" id="bgttimetype'.$rw.'" name="bgttimetype'.$rw.'" class="bgttimetype" onchange="changebgttimetype('.$rw.',1)" value="1" style="display:inline-block" >HH:MM
			<input type="radio" id="bgttimetype'.$rw.'" name="bgttimetype'.$rw.'" class="bgttimetype" onchange="changebgttimetype('.$rw.',2)" value="2" style="display:inline-block" checked>Seconds
			<input type="text" id="budgettime'.$rw.'" name="budgettime'.$rw.'" class="required btimesec budgettime " size="5" style="display:inline-block;width:130px;padding:4px"  />
		</td>';
		/*	<td width="10%" id="iscompletedtd">
						<input type="checkbox" id="is_completed'.$rw.'" name="is_completed'.$rw.'" value="" class="is_completed" onchange="completecheck('.$rw.','.$tblindex.')"/>
						</td>
		';
		<td width="10%">
		<input type="radio" id="apmappingy'.$rw.'" name="apmapping'.$rw.'" class="apmapping"  value="1" style="display:inline-block" >Yes
			<input type="radio" id="apmappingn'.$rw.'" name="apmapping'.$rw.'" class="apmapping"  value="0" style="display:inline-block" checked>No
		</td>*/
		$html   .='<td width="16%">
		<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record-activity" onclick="addactivity('.$tblindex.','.$maximum_admin_entry.','.$id.');"/>';
		if($id == ""){
		$html .='<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record-activity"
		onclick="deleteactivity('.$tblindex.','.$maximum_admin_entry.',1,'.$id.');" rowid="1" />';
		}
		$html .='</td></tr>';			
	}

	$html .= '</tbody>
</table>';
		echo $html."~".$cnt;
	}
}
?>

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$department_id = $_POST['department_id'];
		$subdepartment_id = $_POST['subdepartment_id'];
		$client_id = $_POST['client_id'];
		$pirno = $_POST['pirno'];
		$contracttype = $_POST['contracttype'];
		$noofresource = $_POST['noofresource'];
		$totrow = $_POST['totrow'];
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM pirlist WHERE pirno='".trim($pirno)."'".$idcond;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: pir.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
				$pirquery = "UPDATE pirlist SET `department_id`='".$department_id."',`subdepartment_id`='".$subdepartment_id."',`client_id`='".$client_id."',`pirno` = '".$pirno."',`contracttype`='".$contracttype."',`noofresource`='".$noofresource."' WHERE `id` ='".$editid."'";
				$dbase->executeNonQuery($pirquery);
				for($i=1;$i<=$totrow;$i++){
					if( $_POST['projectname'.$i] !='' && $_POST['projectid'.$i] !=''){
						$projectlist_update = "UPDATE projectlist SET `projectname`='".$_POST['projectname'.$i]."' WHERE id='".$_POST['projectid'.$i]."'";
						$dbase->executeNonQuery($projectlist_update);
					}
					else{
						$projectlist_insert = "INSERT INTO `projectlist` ( `pir_id`, `projectname`) VALUES ( '".$editid."', '".$_POST['projectname'.$i]."');";
						$dbase->executeNonQuery($projectlist_insert);						
					}
				}				
			}
			else{
				$pirquery = "INSERT INTO pirlist( `department_id`,`subdepartment_id`,`client_id`,`pirno`,`contracttype`,`noofresource`) VALUES ( '".$department_id."','".$subdepartment_id."','".$client_id."','".$pirno."','".$contracttype."','".$noofresource."');";
				$dbase->executeNonQuery($pirquery);
				$pirid = mysql_insert_id();
				for($i=1;$i<=$totrow;$i++){
					if( $_POST['projectname'.$i] !=''){
						$projectlist_insert = "INSERT INTO `projectlist` ( `pir_id`, `projectname`) VALUES ( '".$pirid."', '".$_POST['projectname'.$i]."');";
						$dbase->executeNonQuery($projectlist_insert);
					}
				}				
			}			
			 if($editid !=''){
				header('Location: pir.php?m=2');
				exit();	
			 }
			 else{
				header('Location: pir.php?m=1');
				exit();				 
			 }			
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$pirquery = "UPDATE pirlist SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($pirquery);
				header('Location: pir.php?m=4');
				exit();				
		}
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		else{
			$label = "Add";
			$button = "Add";
		}
		$pir_query = "SELECT * FROM pirlist WHERE id='".trim($id)."'";
		$pir_result = $dbase->executeQuery($pir_query,"single");		
	}
	else{
		$label = "Add";
		$button = "Add";
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
	    <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		table#pirtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#pirtable td, table#pirtable th {
			border: 1px solid black;
		}	
		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php// include("adminmenu.php"); ?>
	<?php include("userrolemenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_pir" action="" method="POST" enctype="multipart/form-data">
<table id="pirtable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Inserted Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "PIR No Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='4'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "PIR No Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> PIR No</b></td></tr>		
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="department_id" name="department_id" class="required" onchange="getsubdepartment();">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																								   
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
			if($deptResult[$i]['id']==$pir_result['department_id']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="subdepartment_id" name="subdepartment_id" class="required">
	<option value="">-Select-</option>
	</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Client</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="client_id" name="client_id" class="required chosen-select" style="width:180px;">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND department_id='".$_SESSION['timesheet']['DEPART']."' AND subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$clientQuery = "SELECT c.id,c.clientname FROM clientlist c WHERE c.isActive='1'".$depart_cond;
		$clientResult = $dbase->executeQuery($clientQuery,"multiple");
		for($i=0;$i<count($clientResult);$i++){
			if($clientResult[$i]['id']==$pir_result['client_id']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$clientResult[$i]['id'].'" '.$select.'>'.$clientResult[$i]['clientname'].'</option>';
		}
	?>
	</select>
<label for="client_id" class="error" style="display:none" >This field is required.</label>	
	</td>
</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>PIR No</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="pirno" id="pirno" value="<?php echo $pir_result['pirno']; ?>" class="required alphanumericpir" /></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Contract Type</b></td>
	<td height="40px" align="left" style="padding-left:10px"> 
		<input class="radioBtn" type="radio" name="contracttype" value="0" <?php if($pir_result['contracttype']=='0'){ ?> checked <?php } ?> <?php if($id==''){ ?> checked <?php } ?>> T&M
		<input class="radioBtn" type="radio" name="contracttype" value="1" <?php if($pir_result['contracttype']=='1'){ ?> checked <?php } ?>> Fixed Price
		<input class="radioBtn" type="radio" name="contracttype" value="2" <?php if($pir_result['contracttype']=='2'){ ?> checked <?php } ?>> Dedicated Team
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>No of Resource</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="noofresource" id="noofresource" value="<?php echo $pir_result['noofresource']; ?>" class="number" /></td>
</tr>
<tr>
	<td id="activitytr" height="40px" align="left"  colspan="2" width="20%">
	
	</td>
</tr>	
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
			<input type="hidden" name="subdept" id="subdept" value="<?php echo $pir_result['subdepartment_id']; ?>" />
		<?php } else{?>
		<input type="hidden" name="subdept" id="subdept" value="" />
		<?php } ?>
		<input type="hidden" id="totrow" name="totrow" />
		<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
<td align="center" width="80%" valign="top">
	<br>
	<table id="pirlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="25%" align="center"><b>Department</b></td>
			<td width="25%" align="center"><b>Sub Department Name</b></td>			
			<td width="25%" align="center"><b>PIR No</b></td>
			<td width="25%" align="center"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$pir_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$pir_cond = " AND p.department_id='".$_SESSION['timesheet']['DEPART']."' AND p.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$pirQuery = "SELECT p.id,p.pirno,(SELECT d.name FROM department d WHERE d.id=p.department_id) as deptname,(SELECT s.subname FROM subdepartment s WHERE s.id=p.subdepartment_id) as subdeptname FROM pirlist p WHERE p.isActive='1'".$pir_cond;
		$pirResult = $dbase->executeQuery($pirQuery,"multiple");
		for($i=0;$i<count($pirResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['pirno'];?></td>
			<td align="left" style="padding-left:10px"><a href="pir.php?id=<?php echo $pirResult[$i]['id'];?>">Edit</a> | <a href="pir.php?del=1&id=<?php echo $pirResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
                <th>Department</th>
                <th>Sub Department</th>
				<th>PIR No</th>
                <th>Action</th>
            </tr>
        </tfoot>			
	</table>	
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
jQuery.validator.addMethod("alphanumericpir", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\/]+$/i.test( value );
}, 'Please enter only alphabets numbers and /.');	
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
jQuery.validator.addMethod("alphanumericspace", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');		
	  $("#frm_pir").validate();			
    // Setup - add a text input to each footer cell
    $('#pirlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#pirlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	$(".radioBtn").click(function() {
		$("#noofresource").attr("disabled", true);
		if ($("input[name=contracttype]:checked").val() == "2") {
			$("#noofresource").attr("disabled", false);
			$("#noofresource").select().focus();
		}
	});
	
	
	$(".chosen-select").chosen();
	
		$(document).delegate('.add-record', 'click', function(e) {
				 e.preventDefault();    
				 var content = $('#activitydetailstable tbody').children('tr:first'),
				 size = $('#activitydetailstable >tbody >tr').length + 1,
					element = null,    
					element = content.clone();
					if(size<=<?php echo MAXIMUM_ADMIN_ENTRY;?>){
						element.attr('id', 'row'+size);
						element.find('.projectname').attr('id', 'projectname'+size);
						element.find('.projectname').attr('name', 'projectname'+size);
						element.find('.projectname').val('');
						element.find('.delete-record').attr('rowid', size);
						element.appendTo('#activitydetailstable');					
						$("#totrow").val(size);
						<?php if($id!=''){ ?>
						 dropdownchange(size);
						<?php } ?>							
					}
				  else{
					$('<div>Only '+maximum_admin_entry+' can enter</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
								return false;
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				  }				  
		   });
	
   
	$(document).delegate('.delete-record', 'click', function(e) {
		e.preventDefault();  
	  var id = $(this).attr('rowid');
	  if(id > 1){
	  var targetDiv = $(this).attr('targetDiv');
		$('<div>Are you sure wants to delete ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
					  $('#row' + id).remove();
					//regnerate index number on table
					$('#activitydetailstable tbody tr').each(function(index) {
					  var rowid = index+1;
						$(this).attr('id', 'row'+rowid);
						$(this).find('.budgettime').attr('id', 'budgettime'+rowid);
						$(this).find('.budgettime').attr('name', 'budgettime'+rowid);
						$(this).find('.delete-record').attr('rowid', rowid);
						$("#totrow").val(rowid);
					});
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return true;
					

				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
	  }
	  else{
		$('<div>You cant delete this row</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				},
			}
		});
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	  }
	});	
	loadtable();
	autoselectoption("#department_id");
<?php if($id!=''){ ?>
	getsubdepartment();
	if ($("input[name=contracttype]:checked").val() == "2") {
		$("#noofresource").removeAttr("disabled");
	}	
<?php } else{?>
	$("#noofresource").attr("disabled", true);
<?php } ?>
});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = $("#subdept").val();
	$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
	});
}
function loadtable(pirtype= ""){
	if(pirtype == "" || pirtype == 1){
		var id=$("#editid").val();
		var deptid=$("#department_id").val();
		$.get("projecttable.php?id="+id+"&deptid="+deptid,function(data){
			var dataarr = data.split("~");
			$("#activitytr").html(dataarr[0]);
			$("#totrow").val(dataarr[1]);
		});	
	}
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}	
</script>
</html>
<?php } ?>

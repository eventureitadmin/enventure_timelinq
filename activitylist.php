<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	$usertypes = $master->getusertypes();
	$mdgusertypes = $master->getmldemousertypes();
	$checkedn = "checked";
	$checkedy = "";
	$checkeda = "checked";
	$checkedia = "";
	$mcheckedn = "checked";
	$mcheckedy = "";


	if($_GET){
		$id = $_GET['id'];
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		 $activity_query = "SELECT * FROM `activity` WHERE `id`='".trim($id)."'";
		$activity_result = $dbase->executeQuery($activity_query,"single");	
		if($activity_result != ""){
			if($activity_result['partlinq_mapping'] ==1){
				$checkedn = "";
				$checkedy = "checked";
			}
			if($activity_result['isActive'] ==0){
				$checkeda = "";
				$checkedia = "checked";
			}
			if($activity_result['mdg_mapping'] ==1){
				$mcheckedn = "";
				$mcheckedy = "checked";
			}
		
			
		}
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <script src="js/notify.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		
		table#clienttable,	table#clientlisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#clienttable td, table#clienttable th,table#clientlisttable td, table#clientlisttable th {
			border: 1px solid black;
			padding:5px;
		}	
	
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php //include("adminmenu.php"); ?>
<?php include("userrolemenu.php"); ?>
</td>
	<?php if(isset($_GET['id']) ){?>
<td align="center" width="80%" valign="top">
<form id="frm_activity" action="addactivity.php" method="POST" enctype="multipart/form-data">

<table id="clienttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 

<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Activity Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Activity Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> Activity</b></td></tr>
	<tr>
	<?php $deptres = $dbase->executeQuery("SELECT * FROM `department` WHERE `isActive`=1 ","multiple");?>
		
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
		<select onchange="getsubdepartment(this.value,'')" name="dept_id" id="dept_id">
			<option value="">-Select-</option>
			<?php if(count($deptres) >0){ 
					for($s=0;$s<count($deptres);$s++){
								if($deptres[$s]['id'] == $activity_result['department_id']){
									$selected = "selected";
								}else{
									$selected = "";
								}
							echo '<option value="'.$deptres[$s]['id'].'" '.$selected.'>'.$deptres[$s]['name'].'</option>';
					}
				}
			?>
		</select></td>
</tr>
<tr>
	<?php 
	
	?>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
		<select name="subdept_id" id="subdept_id">
			<option value="">-Select-</option>
			
		</select></td>
</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Activity</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="sactivity" id="sactivity" value="<?php echo $activity_result['name']; ?>" class="required alphanumeric" /></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Status</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="radio"  name="isActive" id="isActivea" value="1" <?php echo $checkeda;?>/>Active<input type="radio"  name="isActive" id="isActiveia" value="0" <?php echo $checkedia;?>/>Inactive</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Partlinq Mapping Required?</b></td>
	<td height="40px" align="left" style="padding-left:10px">
		<input type="radio"  name="partlinqmapping"  value="1"  <?php echo $checkedy;?> />Yes
		<input type="radio"  name="partlinqmapping"  value="0"  <?php echo $checkedn;?> />No</td>
</tr>

	
<tr>
	<td height="40px" align="left" class="entrytypetr" style="padding-left:10px" width="20%"><b>Select Entrytype</b></td>
	<td height="40px" align="left"  class="entrytypetr" style="padding-left:10px">
		<select name="entrytype"><option value="">-Select-</option>
		<?php if(count($usertypes)>0){
				for($k=0;$k<count($usertypes);$k++){
					if($usertypes[$k]['type'] == $activity_result['entrytype']){
						$selected = "selected";
					}else{
						$selected = "";
					}
					echo '<option value="'.$usertypes[$k]['type'].'" '.$selected.'>'.$usertypes[$k]['name'].'</option>';
				}
			}
		?>
			</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>MDG Mapping Required?</b></td>
	<td height="40px" align="left" style="padding-left:10px">
		<input type="radio"  name="mdgmapping"  value="1"  <?php echo $mcheckedy;?> />Yes
		<input type="radio"  name="mdgmapping"  value="0"  <?php echo $mcheckedn;?> />No</td>
</tr>	
<tr>
	<td height="40px" align="left" class="mdgentrytypetr" style="padding-left:10px" width="20%"><b>Select MDG Entrytype</b></td>
	<td height="40px" align="left"  class="mdgentrytypetr" style="padding-left:10px">
		<select name="mentrytype"><option value="">-Select-</option>
		<?php if(count($mdgusertypes)>0){
				for($k=0;$k<count($mdgusertypes);$k++){
					if($mdgusertypes[$k]['type'] == $activity_result['mentrytype']){
						$selected = "selected";
					}else{
						$selected = "";
					}
					echo '<option value="'.$mdgusertypes[$k]['type'].'" '.$selected.'>'.$mdgusertypes[$k]['name'].'</option>';
				}
			}
		?>
			</select>
	</td>
</tr>
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="activityeditid" id="activityeditid" value="<?php echo $id; ?>" />
			<input type="hidden" name="subdept" id="subdept" value="<?php echo $client_result['subdepartment_id']; ?>" />
		<?php } else {?>
		<input type="hidden" name="activityeditid" id="activityeditid" value="0" />
		<input type="hidden" name="subdept" id="subdept" value="" />
		<?php } ?>
		<input type="button" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
	<?php }?>
<td align="center" width="80%" valign="top">

		<?php if($_GET['m']=='1'){ ?>
	<b style="color:red;"><?php  echo "Updated Successfully"; ?></b>
	<?php }	?>
		<?php if($_GET['m']=='2'){ ?>
	<b style="color:red;"><?php  echo "Client Already Exists"; ?></b>
	<?php }	?>
		<?php if($_GET['m']=='3'){ ?>
	<b style="color:red;"><?php  echo "Client Deleted Successfully"; ?></b>
	<?php }	?>
	<!--<div style="width: auto; min-height:250px; overflow-y:scroll;height:250px;"></div>-->
	<table id="clientlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="25%" align="left"><b>Department</b></td>
			<td width="25%" align="left"><b>Subdepartment</b></td>
			<td width="25%" align="left"><b>Activity</b></td>
			<td width="25%" align="left"><b>Status</b></td>
			<td width="25%" align="left"><b>Partlinq Mapping</b></td>
			<td width="25%" align="left"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " WHERE department_id='".$_SESSION['timesheet']['DEPART']."' AND subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$clientQuery = "SELECT (SELECT `subname` FROM `subdepartment` WHERE `id` =(`subdepartment_id`))as subname,(SELECT `name` FROM `department` WHERE `id`=`department_id`)as dname,`name`,`partlinq_mapping`,`isActive`,`id` FROM `activity` ".$depart_cond;

		$clientResult = $dbase->executeQuery($clientQuery,"multiple");//echo"<pre>";print_r($clientResult);
		for($i=0;$i<count($clientResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['dname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['subname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['name'];?></td>
			<td align="left" style="padding-left:10px"><?php if($clientResult[$i]['isActive'] == 1){echo "Active";}else{echo "Inactive";}?></td>
			<td align="left" style="padding-left:10px"><?php if($clientResult[$i]['partlinq_mapping'] == 1){echo "Yes";}else{echo "No";}?></td>
			<td align="left" style="padding-left:10px"><?php if($_SESSION['timesheet']['ROLEID'] == SUPERADMIN_ROLE_ID || $_SESSION['timesheet']['ROLEID'] == ADMIN_ROLE_ID){?><a href="activitylist.php?id=<?php echo $clientResult[$i]['id'];?>">Edit</a> |<?php } ?><a href="activitylist.php?del=1&id=<?php echo $clientResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th>Department</th>
				<th>Sub Department</th>
				<th>Activity</th>
				<th>Status</th>
				<th>Partlinq Mapping</th>
                <th>Action</th>
            </tr>
        </tfoot>			
	</table>
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
	
$(document).ready(function(){
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
	  $("#frm_activity").validate();	
    // Setup - add a text input to each footer cell
    $('#clientlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#clientlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
	
	$("#submit").click(function(){
		$("#frm_activity").validate();
		$.ajax({
		type:"POST",
		url:"addactivity.php",
		data:$('#frm_activity').serializeArray(),
	  	success:function(data){
			var obj =JSON.parse(data);
			if(obj.status ="success" && (obj.m ==1 || obj.m==3)){
				window.location.href = "activitylist.php?m="+obj.m;
			}else{
				$.notify(obj.successmsg,"error");
			}
		}
		});
		//$("#frm_activity").submit();
	});
<?php if($id!=''){ ?>
	<?php if($activity_result['entrytype'] >0 && $activity_result['partlinq_mapping'] == 1){ ?>	
	$(".entrytypetr").css("display","table-cell");
	<?php }else{?>	
	$(".entrytypetr").css("display","none");
	<?php }?>
	
	<?php if($activity_result['mentrytype'] >0 && $activity_result['mdg_mapping'] == 1){ ?>	
	$(".mdgentrytypetr").css("display","table-cell");
	<?php }else{?>	
	$(".mdgentrytypetr").css("display","none");
	<?php }?>
getsubdepartment("<?php echo $activity_result['department_id']; ?>","<?php echo $activity_result['subdepartment_id']; ?>");
<?php }else{?>
	$(".entrytypetr").css("display","none");
	$(".mdgentrytypetr").css("display","none");
	<?php } ?>	
	autoselectoption("#dept_id");
        $('input:radio[name=partlinqmapping]').change(function () {
			if($("input[name='partlinqmapping']:checked").val() ==1){
				$(".entrytypetr").css("display","table-cell");
			}else{
				$(".entrytypetr").css("display","none");
			}
		});
	 $('input:radio[name=mdgmapping]').change(function () {
			if($("input[name='mdgmapping']:checked").val() ==1){
				$(".mdgentrytypetr").css("display","table-cell");
			}else{
				$(".mdgentrytypetr").css("display","none");
			}
		});

});	
function getsubdepartment(selid,subdept){
	if(selid == ""){
		var selid = $("#dept_id option:selected").val();
	}
	//var subdept = $("#subdept").val();
	$.get("getsubdepartmentpir.php?id="+selid+"&sel="+subdept,function(data){
		$("#subdept_id").html(data);
	});
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
	
</script>
</html>
<?php } ?>

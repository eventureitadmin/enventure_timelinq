
	var clientexists = true;
	var pirexists = true;
		$(document).ready(function(){
			
			jQuery.validator.addMethod("alphanumericpir", function(value, element) {
			  return this.optional( element ) || /^[a-zA-Z0-9\/]+$/i.test( value );
			}, 'Please enter only alphabets numbers and /.');	
			jQuery.validator.addMethod("alphanumeric", function(value, element) {
			  return this.optional( element ) || /^[a-zA-Z0-9]+$/i.test( value );
			}, 'Please enter only alphabets and numbers.');	
			jQuery.validator.addMethod("alphanumericspace", function(value, element) {
			  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
			}, 'Please enter only alphabets and numbers.');		
				
    // Setup - add a text input t
		var form = $("#frm_projsetup");
			//project setup wizard
			form.children("div").steps({
						enablePagination: true,
						headerTag: "h4",
						bodyTag: "section",
						transitionEffect: "slideLeft",
						labels: {
						current: "current step:",
						pagination: "Pagination",
						finish: "Finish",
						next: "Next",
						previous: "Previous"
					},
					onStepChanging: function (event, currentIndex, newIndex)
					{
						//console.log(currentIndex+""+newIndex);

						form.validate().settings.ignore = ":disabled,:hidden";
						if(form.valid()){
								if(currentIndex == 0 && newIndex == 1){
									if($("#client_name").val() != ""){
										var clientexists = checkclient();
										if(!clientexists){
											$("#client_name").after("<label class='error' id='client'>Client already exists</label>");
											return false;
										}else{
											$("#client").css("display","none");
										}
									}
								}
								var pirtype = $("input[name='pirtype']:checked").val();
								if(pirtype == 1 ){
										$("#pirno1_chosen").hide();
										$("#pirno1").hide();
								}else{
									$("#pirno1_chosen").show();
								}
							//2nd tab
							if(currentIndex == 1 && newIndex == 2){
								if((pirtype == 2 && $("#pirno1").val() != "") || (pirtype == 1 && $("#pirno").val() != "")){
									var pirexists = checkpirdata();
									if(!pirexists){
										$(".newpir").after("<label class='error' id='newpir'>PIR already exists</label>");
										return false;
									}else{
										$("#newpir").css("display","none");
									}
								}
								getPIRMappinginfo(maximum_admin_entry);
								
							}
								//3rd tab
							if(currentIndex == 2 && newIndex == 3){
								
							}
							return true;
						}else{
							return form.valid();
						}
					},
					onFinishing: function (event, currentIndex)
					{
						form.validate().settings.ignore = ":disabled";
						return form.valid();
					},
					onFinished: function (event, currentIndex)
					{
						alert("Submitted!");
					}
				});	
			  
				$(".chosen-select").chosen({search_contains: true});
		
			   
			//radio click
				$(".radioBtn").click(function() {
					$("#noofresource").attr("disabled", true);
					if ($("input[name=contracttype]:checked").val() == "2") {
						$("#noofresource").attr("disabled", false);
						$("#noofresource").select().focus();
					}else{
						$("#noofresource").val("");
					}
				});
				$(".clienttype").click(function() {
					var clienttype = $("input[name=clienttype]:checked").val();
				
					if($("input[name=clienttype]:checked").val() == 1){
						$("#newclient").show();
						$("#client_name").addClass("required alphanumeric");
						$("#existclient").hide();
					}else{
						$("#client_name").removeClass("required alphanumeric");
						$("#newclient").hide();
						$("#existclient").show();
					}
				});
			//radio click
			
				$(".pirtype").click(function() {
					if($("input[name=pirtype]:checked").val() == 2){
					   	$(".newpir").hide();
						$(".newpir").removeClass("required");
						$(".newpir").removeClass("alphanumericpir");
						$("#pirno1_chosen").show();
					 }else{
						$(".newpir").addClass("required");
						$(".newpir").addClass("alphanumericpir");
						$(".newpir").show();
						$("#pirno1_chosen").hide();
						$("#pirno1").val('').trigger("chosen:updated");
						$("#noofresource").val('');
					    $('.radioBtn')[0].checked = true;
						$("#pirno1").val('');
					 }
						 loadtable();
				});
				$(document).delegate('.add-record', 'click', function(e) {
					 e.preventDefault(); 
					 var content = $('#activitydetailstable tbody').children('tr:first'),
					 size = $('#activitydetailstable >tbody >tr').length + 1,
						element = null,    
						element = content.clone();
						if(size<= maximum_admin_entry){
							element.attr('id', 'row'+size);
							element.find('.projectname').attr('id', 'projectname'+size);
							element.find('.projectname').attr('name', 'projectname'+size);
							element.find('.projectname').val('');
							if($("input[name='pirtype']:checked").val() == 2){
							element.find(".add-record").after('<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record"'+ 
															  'rowid='+size+' />');
							}
							//element.find('.delete-record').attr('rowid', size);
							element.appendTo('#activitydetailstable');					
							$("#totrow").val(size);
							<?php if($id!=''){ ?>
							 dropdownchange(size);
							<?php } ?>							
						}
					  else{
						$('<div>Only '+maximum_admin_entry+' can enter</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Ok": function() {
									$( "div" ).remove( ".ui-widget-overlay" );	
									$(this).dialog("close");
									return false;
								},
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
					  }				  
			   });
	
   
			$(document).delegate('.delete-record', 'click', function(e) {
				e.preventDefault();  
			  var id = $(this).attr('rowid');

			  if(id > 1){
			  var targetDiv = $(this).attr('targetDiv');
				$('<div>Are you sure wants to delete ?</div>').dialog({
					resizable: false,
					open: function(){
					 $(".ui-dialog-title").html("Alert");
					},				
						buttons: {
						"Yes": function() {
							  $('#row' + id).remove();
							//regnerate index number on table
							$('#activitydetailstable tbody tr').each(function(index) {
							  var rowid = index+1;
								$(this).attr('id', 'row'+rowid);
								$(this).find('.budgettime').attr('id', 'budgettime'+rowid);
								$(this).find('.budgettime').attr('name', 'budgettime'+rowid);
								$(this).find('.delete-record').attr('rowid', rowid);
								$("#totrow").val(rowid);
							});
							$( "div" ).remove( ".ui-widget-overlay" );	
							$(this).dialog("close");
							return true;


						},
						"No": function() {
							$( "div" ).remove( ".ui-widget-overlay" );	
							$(this).dialog("close");
							return false;
						}
					}
				});	
				$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
			  }
			  else{
				$('<div>You cant delete this row</div>').dialog({
					resizable: false,
					open: function(){
					 $(".ui-dialog-title").html("Alert");
					},				
						buttons: {
						"Ok": function() {
							$( "div" ).remove( ".ui-widget-overlay" );	
							$(this).dialog("close");
							return false;
						},
					}
				});
				$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
			  }
			});	
	

			 loadtable();
	});
	function getsubdepartment(){
		var id=$("#department_id").val();
		var subdept = $("#subdept").val();
		$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
			$("#subdepartment_id").html("");
			$("#subdepartment_id").html(data);
			$("#subdepartment_id").chosen().trigger("chosen:updated");
		});
	}
	function autoselectoption(id){
		var length = ($(id+' > option').length - 1);
		if(length=='1'){
			$(id+" option").each(function () {
				if($(this).text() != "-Select-"){
					$(this).attr("selected", "selected").change();
				}
			});
		}
	}
	function changeclient(obj){
		var selclientname = $("#client_id option:selected" ).text();
		if(selclientname == "Other"){
			$("#existclient").hide();
			$("#newclient").show();
		}else{
			$("#existclient").show();
			$("#newclient").hide();
		}
	}
	
	function loadtable(){
		var pirtype = $("input[name='pirtype']:checked").val();
		
		var id=$("#pirno1").val();
		var deptid=$("#department_id").val();
		$.get("projecttabledata.php?id="+id+"&deptid="+deptid+"&pirtype="+pirtype,function(data){
			var dataarr = data.split("~");
			$("#activitytr").html(dataarr[0]);
			$("#totrow").val(dataarr[1]);
		});	
	}
	function getprojectlist(){
	
		var id=$("#pirno1").val();
		var projectid=$("#projectid").val();
		$.get("getprojectlist.php?id="+id+"&sel="+projectid,function(data){
			$("#projectname").html(data);
			if($("#projectid").val() != ''){
				var str_array = $("#projectid").val();
				$("#projectname").val(str_array).trigger("chosen:updated");			
			}
			else{
				$("#projectname").trigger("chosen:updated");
			}		
		});	
	}
	function checkclient(){
	
		$.ajax({
			url: 'getajaxpirdata.php',
			dataType: 'json',async: false,
			type: 'post',
			data: {"department_id":$("#department_id").val(),"subdepartment_id":$("#subdepartment_id").val(),"pirno":"",
				    "client_name":$("#client_name").val(),"is_client":1,"is_pir":0},
			success: function(data){
				if(data.clientcnt > 0){
					clientexists = false;
				}else{
					clientexists = true;
				}
			}
		});	
		return clientexists;
	}
	function checkpirdata(){
	
		$.ajax({
			url: 'getajaxpirdata.php',
			dataType: 'json',async: false,
			type: 'post',
			data: {"department_id":$("#department_id").val(),"subdepartment_id":$("#subdepartment_id").val(),"pirno":$(".newpir").val()
				  ,"pirno1":$("#pirno1").val(),"client_id":$("#client_id").val(),"is_client":0,"is_pir":1},
			success: function(data){
				
				if(data != "" && $("#pirno1").val() != ""){
					$(".radioBtn")[data.contracttype].checked = true;
					$("#noofresource").val(data.noofresource);
				}else if(data != "" && $(".newpir").val() != ""){
					if(data.pircnt > 0){
						pirexists = false;
					}else{
						pirexists = true;
					}
				}
			}
		});	
	return pirexists;
	}
	function getPIRMappinginfo(max_entry){
		$("#pirmaptable").html("");
			//loadactivitytable();
			$(".projectname").each(function(key){
				var pirmaptbl ='<tr><td height="40px" align="left"  colspan="2" width="20%"><b>Project Name:</b>'+$(this).val()+
					'<span style="float-right"><a href="#" >Add Activity</a></span></td>';
					pirmaptbl += '<td height="40px" align="left"  colspan="2" width="20%"></td>';
					pirmaptbl += '<input type="hidden" id="totrow" name="totrow_'+key+'" /><tr/>';
					pirmaptbl += '<tr>';
				    pirmaptbl += '<td id="piractivitytr_'+key+'" height="40px" align="left"  colspan="2" width="20%">'+
								'</td><td></td></tr>';
				loadactivitytable(key,max_entry);
				$("#pirmaptable").append(pirmaptbl);
			});
	}
	function loadactivitytable(key,max_entry){
		var id=$("#editid").val();
		var pirno=$("#pirno1").val();
		$.post("activitytabledata.php",{id:id,pirno:pirno,tblindex:key,max_entry:max_entry,dept_id:$("#department_id").val(),subdept_id:$("#subdepartment_id").val()},function(data){
			var dataarr = data.split("~");
			$("#piractivitytr_"+key).html(dataarr[0]);
			$("#totrow_"+key).val(dataarr[1]);
		});	
	}
	function dropdownchange(size){
		$('#activity_id'+size).val('');
		$('#activity_id'+size).removeAttr('selected');
		$('#activity_id'+size+' option[value=""]').attr('selected', true);
}
function changebudgettextbox(id,value){
	var id = id.replace("calculatehours", "");
	if(value!=''){
		if(value=='y'){
			if(!$("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).addClass('required');
				$("#budgettime"+id).attr('readonly', false);
			}
		}
		if(value=='n'){
			if($("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).removeClass('required');
				$("#budgettime"+id).attr('readonly', true);
				if($("#budgettime"+id).val() > 0 && $("#budgettime"+id).val() != ''){
					$("#budgettime"+id).val('0');
				}
			}
		}
	}
}
function changemanhrstextbox(id,value){
	var id = id.replace("manhrs", "");
	if(value!=''){
		if(value=='y'){
			if(!$("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).addClass('required');
				$("#budgettime"+id).attr('readonly', false);
				$("#calculatehours"+id).addClass('required');
				$("#calculatehours"+id).removeAttr('disabled');				
			}
			if($("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).addClass('required');
				$("#budgettime"+id).attr('readonly', false);
				$("#calculatehours"+id).addClass('required');
				$("#calculatehours"+id).removeAttr('disabled');				
			}			
		}
		if(value=='n'){
			if($("#calculatehours"+id).hasClass("required")){
				$("#calculatehours"+id).removeClass('required');
				$("#calculatehours"+id).attr('disabled', 'disabled');
			}			
			if($("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).removeClass('required');
				$("#budgettime"+id).attr('readonly', true);
				if($("#budgettime"+id).val() > 0 && $("#budgettime"+id).val() != ''){
					$("#budgettime"+id).val('0');
				}
			}
		}
	}
}
	function addactivity(index,maximum_admin_entry){
		if($("#department_id").val()!=''){
				//	 e.preventDefault();    
			console.log('#activitydetailstable_'+index+' tbody');
					 var content = $('#activitydetailstable_'+index+' tbody').children('tr:first'),
					 size = $('#activitydetailstable_'+index+' >tbody >tr').length + 1,
						element = null,    
						element = content.clone();
						if(size<=maximum_admin_entry){
							element.attr('id', 'row'+size);
							element.find('.activity').attr('id', 'activity_id'+size);
							element.find('.activity').attr('name', 'activity_id'+size);
							element.find('.calculatemanhrs').attr('id', 'manhrs'+size);
							element.find('.calculatemanhrs').attr('name', 'manhrs'+size);
							element.find('.calculatemanhrs').empty().append('<option value="">-Select-</option><option value="y">YES</option><option value="n">NO</option>');				
							element.find('.calculatehours').attr('id', 'calculatehours'+size);
							element.find('.calculatehours').attr('name', 'calculatehours'+size);
							element.find('.calculatehours').empty().append('<option value="">-Select-</option><option value="y">YES</option><option value="n">NO</option>');
							element.find('.budgettime').attr('id', 'budgettime'+size);
							element.find('.budgettime').attr('name', 'budgettime'+size);
							element.find('.budgettime').val('');
							element.find('.delete-record-activity').attr('rowid', size);
							element.appendTo('#activitydetailstable_'+index);
							//$('#activity_id'+size).chosen();
							//$('#activity_id'+size).val('').trigger("chosen:updated");	
							$("#totrow_"+index).val(size);
														
						}
					  else{
						$('<div>Only '+maximum_admin_entry+' can enter</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Ok": function() {
									$( "div" ).remove( ".ui-widget-overlay" );	
									$(this).dialog("close");
									return false;
								},
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
					  }
				}else{
						$('<div>Please Select Department</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Ok": function() {
									$( "div" ).remove( ".ui-widget-overlay" );	
									$(this).dialog("close");
									return false;
								},
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
					}			
	}
	function deleteactivity(index,maximum_admin_entry){
		console.log($(this));
	  var id = $(this).attr('rowid');
	  if(id > 1){
	  var targetDiv = $(this).attr('targetDiv');
		$('<div>Are you sure wants to delete ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
					  $('#row' + id).remove();
					//regnerate index number on table
					$('#activitydetailstable_'+index+' tbody tr').each(function(index) {
					  var rowid = index+1;
						$(this).attr('id', 'row'+rowid);
						$(this).find('.activity').attr('id', 'activity_id'+rowid);
						$(this).find('.activity').attr('name', 'activity_id'+rowid);
						$(this).find('.calculatemanhrs').attr('id', 'manhrs'+rowid);
						$(this).find('.calculatemanhrs').attr('name', 'manhrs'+rowid);						
						$(this).find('.calculatehours').attr('id', 'calculatehours'+rowid);
						$(this).find('.calculatehours').attr('name', 'calculatehours'+rowid);
						$(this).find('.budgettime').attr('id', 'budgettime'+rowid);
						$(this).find('.budgettime').attr('name', 'budgettime'+rowid);
						$(this).find('.delete-record').attr('rowid', rowid);
						$("#totrow").val(rowid);
					});
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return true;
					

				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
	  }
	  else{
		$('<div>You cant delete this row</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				},
			}
		});
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	  }
	}

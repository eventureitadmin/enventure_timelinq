<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$departmentquery = "UPDATE projectlist SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($departmentquery);
				header('Location: project.php?m=4');
				exit();				
		}
	
		$pro_query = "SELECT * FROM projectlist WHERE id='".trim($id)."'";
		$pro_result = $dbase->executeQuery($pro_query,"single");		
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		table#projecttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#projecttable td, table#projecttable th {
			border: 1px solid black;
		}	

	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php //include("adminmenu.php"); ?>
<?php include("userrolemenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">

	<table id="projectlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="50%" align="left"><b>Department</b></td>
			<td width="50%" align="left"><b>Sub Department</b></td>
			<td width="50%" align="left"><b>PIR No</b></td>
			<td width="50%" align="left"><b>Project Name</b></td>
			<td width="50%" align="left"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$pir_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$pir_cond = " AND p2.department_id='".$_SESSION['timesheet']['DEPART']."' AND p2.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																				   
		$proQuery = "SELECT p1.id, p1.projectname, p2.pirno, (SELECT d.name FROM department d WHERE d.id = p2.department_id) AS deptname, (SELECT s.subname FROM subdepartment s WHERE s.id = p2.subdepartment_id) AS subdeptname FROM `projectlist` p1, pirlist p2 WHERE p2.`isActive` = '1' AND p2.id = p1.`pir_id`".$pir_cond;
		$proResult = $dbase->executeQuery($proQuery,"multiple");
		for($i=0;$i<count($proResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['pirno'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['projectname'];?></td>
			<td align="left" style="padding-left:10px"><a href="project.php?del=1&id=<?php echo $proResult[$i]['id'];?>">Close</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th>Department</th>
				<th>Sub Department</th>
				<th>PIR No</th>
				<th>Project Name</th>
                <th>Action</th>
            </tr>
        </tfoot>		
	</table>
	</td>
	</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){

    // Setup - add a text input to each footer cell
    $('#projectlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#projectlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
});	
</script>
</html>
<?php } ?>
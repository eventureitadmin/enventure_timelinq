<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){
		header("Location:newkpi.php");
		exit;		
}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
	<title>Enventure - Timesheet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!----<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />--->

	<link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery_confirm.css" rel="stylesheet" />
	<link type="text/css" href="css/custom.css" rel="stylesheet" />
	<link type="text/css" href="css/css-circular-prog-bar.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jqueryui.js"></script>	   
	<script type="text/javascript" src="js/jquery_validate.js"></script>
	<script type="text/javascript" src="js/jquery_confirm.js"></script>
	<script type="text/javascript"  src="js/easyconfirm.js"></script>
	<script type="text/javascript" src="js/date.js"></script>
	<!----<script type="text/javascript" src="js/bootstrap.js"></script>-->
	<script type="text/javascript" src="js/gauge.js"></script>
	<script type="text/javascript" src="js/chart2_8.js"></script>
	<script type="text/javascript" src="js/canvas.js"></script>
	<style>
	.ui-datepicker-calendar 
      {
         display:none
      }
.table-style .today {background: #2A3F54; color: #ffffff;}
.table-style th:nth-of-type(7),td:nth-of-type(7) {color: blue;}
.table-style th:nth-of-type(1),td:nth-of-type(1) {color: blue;}
.table-style tr:first-child th{background-color:#F6F6F6; text-align:center; font-size: 15px;}
/*ul#userdashboardul {
    -webkit-column-count: 3;
    -moz-column-count: 3;
    column-count: 3;
}*/
	</style>
</head>
<body>
<?php include("menu.php");?>
	<?php include_once("includebootstrap.php"); ?>
<?php $deptid =1;?>
<div class="container-fluid">
<form id="frm_kpi"  action="" method="post">
	 <div class="row">
	  <div class="col-sm-2">
		  <div class="form-group">
			<input type="hidden" id="from_date" name="from_date" class="form-control " value="<?php echo date('Y-m-01'); ?>" />  
		  </div>		 
	   </div>
	  <div class="col-sm-2">
		  <div class="form-group ">
			<input type="hidden" id="to_date" name="to_date" class="form-control" value="<?php echo date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))); ?>" />
		  </div>		 
	</div>
	<input type="hidden" id="log_mon_yr" name="log_mon_yr" class="form-control " value="<?php echo date('Y-m'); ?>" />  
	  </div> 	
	
</form>
</div>
<div id="chart-container" class="container-fluid">
</div>

<div id="overlay">
  <div id="overlay-text"><h5><img src="images/busy.gif" />Graphs are getting generated.Please wait!</h5></div>
</div>	

<script type="text/javascript">
 $(document).ready(function(){
	 $(document).on("click", "a#previous", function(){
		var cur_mont_yr = $("#log_mon_yr").val().split("-");
		var new_mon =  (cur_mont_yr[1]-1).toString();
		if(new_mon.length=='1'){
			new_mon = '0'+new_mon; 
		}
		 var new_yr = cur_mont_yr[0];
		 $("#log_mon_yr").val(new_yr+"-"+new_mon);
		 submitform();
	 });
	 $(document).on("click", "a#next", function(){
		var cur_mont_yr1 = $("#log_mon_yr").val().split("-");
		var new_mon1 =  (parseInt (cur_mont_yr1[1])+parseInt(1)).toString();
		if(new_mon1.length=='1'){
			new_mon1 = '0'+new_mon1; 
		}
		 var new_yr1 = cur_mont_yr1[0];
		 $("#log_mon_yr").val(new_yr1+"-"+new_mon1);
		 submitform();
	 });	 
	 $('#overlay').css('visibility', 'visible');
		submitform();
});	
	
function submitform(){
	$("#chart-container").css("display","block");
		if($("#from_date").val() !='' && $("#to_date").val() != ""){
			$('#overlay').css('visibility', 'visible');
			$.post("homeajax.php",$.param($("#frm_kpi").serializeArray()), function(data, status){
				if(data!=''){
					$('#overlay').css('visibility', 'hidden');
				   $("#chart-container").html(data);
				}
				else{
					$.alert({
						title: 'Alert',
						content: 'No Records',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});	
				}
			});	

		}
		else{
			$("#chart-container").html('');
			$.alert({
						title: 'Alert',
						content: 'No Records',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});
		}

}
</script>	
</body>
</html>
<?php }
?>
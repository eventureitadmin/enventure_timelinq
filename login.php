<?php

include_once("config.php");
$useragent= $_SERVER['HTTP_USER_AGENT'];
preg_match('#\((.*?)\)#', $useragent, $match);
//print_r($match);
$browserarr = explode(";",$match[1]);
function getOS() { 
    global $useragent;
    $os_platform  = "Unknown OS Platform";
    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );
    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $useragent))
            $os_platform = $value;
    return $os_platform;
}

$os = strtolower(getOS());

$browserarrcnt = count($browserarr);
if($ua->is_mobile()){
		$_SESSION['timesheet']['ismobile'] = "1";
}
else{
	if(preg_match('/applewebkit|mobile|android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
		if($browserarrcnt<=3){
			if(!preg_match("/windows/i", $os) && !preg_match("/ubuntu/i", $os)){
				$_SESSION['timesheet']['ismobile'] = "1";
			}
		}
	}
	else{
		if($browserarrcnt<=3){
			if(!preg_match("/windows/i", $os) && !preg_match("/ubuntu/i", $os)){
				$_SESSION['timesheet']['ismobile'] = "1";
			}
		}
	}
}
if($_SESSION['timesheet']['ismobile']=="1"){
require_once 'mobiletest/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
	if($deviceType=='computer'){
		$_SESSION['timesheet']['ismobile']="0";
	}
}
if($_SESSION['timesheet']['ID']!=''){
	if(isset($_GET['logout'])){
		$_SESSION['timesheet']['ID']='';
		unset($_SESSION['timesheet']);
		header('Location: login.php');
		exit();	
	}
	else{
		if($_SESSION['timesheet']['ISADMIN']=='1'){
			header("Location:index.php");
			exit;			
		}
		else{
			header("Location:index.php");
			exit;
		}
	}
}
else{
	if($_POST){
		$emp_username = $_POST['emp_username'];
		$emp_password = $_POST['emp_password'];
		if($emp_username !=''){
			$loginQuery = "SELECT ID,emp_name,emp_password,isadmin,isprojectadmin,department_ids,subdepartment_ids,predate_logout,is_pwdreset,role_id,team_ids FROM employeelist WHERE emp_username = '".trim(mysql_escape_string($emp_username))."' AND isactive='1' LIMIT 0,1";
			$loginResult = $dbase->executeQuery($loginQuery,"single");
			if($loginResult['ID'] !=''){
				if($loginResult['emp_password']==mysql_escape_string($emp_password)){
					if($loginResult['is_pwdreset']=='0'){
						$_SESSION['timesheet']['ID']=$loginResult['ID'];
						header("Location:resetpwd.php");
						exit;						
					}
					$updatequery = "UPDATE employeelist SET lastlogin='".date('Y-m-d H:i:s')."' WHERE ID='".$loginResult['ID']."'";
					$dbase->executeNonQuery($updatequery);
					$_SESSION['timesheet']['ID']=$loginResult['ID'];
					$_SESSION['timesheet']['USERCODE']=$emp_username;
					$_SESSION['timesheet']['PREDATE_LOGOUT']=$loginResult['predate_logout'];
					$_SESSION['timesheet']['USERNAME']=$loginResult['emp_name'];
					$_SESSION['timesheet']['ISADMIN']=$loginResult['isadmin'];
					$_SESSION['timesheet']['ISPROJECTADMIN']=$loginResult['isprojectadmin'];
					$_SESSION['timesheet']['ROLEID']=$loginResult['role_id'];
					$_SESSION['timesheet']['DEPART']=$loginResult['department_ids'];
					$_SESSION['timesheet']['SUBDEPART_ARY']=explode(",",$loginResult['subdepartment_ids']);
					$_SESSION['timesheet']['SUBDEPART_CSV']=implode( ", ", explode(",",$loginResult['subdepartment_ids']));
					if($loginResult['team_ids'] != ''){
						$_SESSION['timesheet']['IS_TEAMIDS'] = '1';
						$_SESSION['timesheet']['TEAMIDS_CSV']=implode( ", ", explode(",",$loginResult['team_ids']));
					}
					else{
						$_SESSION['timesheet']['IS_TEAMIDS'] = '0';
					}
					/*if($loginResult['subdepartment_ids']=='2'){
						$_SESSION['timesheet']['NO_TIMESHEET'] = '0';
					}
					else{
						$_SESSION['timesheet']['NO_TIMESHEET'] = '1';
					}*/
					$_SESSION['timesheet']['NO_TIMESHEET']='1';
					if($_SESSION['timesheet']['ISADMIN']=='1'){
						header("Location:index.php");
						exit;			
					}
					else{
						if($_SESSION['timesheet']['ROLEID']== HR_ROLE){
							header("Location:users.php");
						}else{
							header("Location:index.php");
						}
						exit;	
					}
				}
				else{
					$msg= "Wrong Password";
				}
			}
			else{
				
				$msg="User not registered";
			}
		}
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
      <script type="text/javascript">
         $().ready(function() {
         	$("#frm_login").validate();
			 if($(window).width() < '768' || $(window).height() > '600'){
				 //alert('mobile');
			}			 
         });
                 
      </script>
      <style type="text/css">
         label.error { 
         margin-left:3px;
         color:red;
         }
         body{
         font-family:Arial;
		 font-size:12px;
         background:#fff url(/images/pattern.png) repeat top left;
         }
      </style>
   </head>
   <body>
      <form id="frm_login" action="" method="POST">
         <table border="0" cellpadding="0" cellspacing="0" align="center" width="50%" style="margin-top:150px">
            <tr>
               <td align="center" >
                  <img style="display: block;max-width: 180px;" src="images/logo.png"/>
               </td>
            </tr>
            <tr>
               <td align="center" valign="middle">
                  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
				  <?php if($msg!=''){?>
					<tr>
						<td align="center" colspan="2" >
							<label align="center" class="error"> <?php echo $msg; $msg=''; ?> </label>
						</td>
					</tr>
					<?php } ?>	
				  <?php if($_SESSION['timesheet']['ismobile']=='1'){?>
					<tr>
						<td align="center" colspan="2" >
							<label align="center" class="error" style="font-size: 30px;"> <?php echo "Mobile Browser Detected.<br/>Please login through Desktop / Laptop Browser"; unset($_SESSION['timesheet']['ismobile']); ?> </label>
						</td>
					</tr>
					<?php } else{ ?>						  
                     <tr>
                        <td align="right" width="50%" style="padding-right: 10px;">
                           Username &nbsp;&nbsp;
                        </td>
                        <td align="left">
                           <input name="emp_username" class="input2 required" id="emp_username" type="text" <?php if($emp_username !=''){ ?> value="<?php echo $emp_username;?>" <?php } ?> />
                        </td>
					</tr>
					
                   <tr>
                        <td align="right" width="50%" style="padding-right: 10px;">
                           Password &nbsp;&nbsp;
                        </td>
                        <td align="left">
                           <input name="emp_password" class="input2 required" id="sPassword" type="password" /> 
                        </td>
					</tr>					
                     <tr>
                        <td align="center" colspan="2"><br/>
                           <input name="btn_save" id="btn_save" value="  Login  " type="submit" />
                        </td>
                     </tr>
					  <?php } ?>
                  </table>
               </td>
            </tr>
         </table>
      </form>
   </body>
</html>
<?php } ?>

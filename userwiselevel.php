<?php
include_once("config.php");
include_once("functions.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getquery($datecond,$cond,$group,$selcolumn){
		$query = "SELECT 
	n.empid,
	n.empname,
	n.totalparts,
	n.calculatedhours,
	CONCAT(FLOOR((n.calculatedhours/3600)),':',floor((n.calculatedhours / 60) % 60),':',floor(n.calculatedhours % 60)) as totcalculatedhours,
	n.actualhours,
	CONCAT(FLOOR((n.actualhours/3600)),':',floor((n.actualhours / 60) % 60),':',floor(n.actualhours % 60)) as totactualhours".$selcolumn[0]."
FROM 
	(
		SELECT 
			t1.`employee_id`,
			(SELECT u3.emp_username FROM employeelist u3 WHERE u3.id = t1.employee_id) AS empid,
			(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id = t1.employee_id) AS empname,
			SUM(`totalparts`) AS totalparts,
			SUM(TIME_TO_SEC(t1.`calculatedhours`)) AS calculatedhours,
			SUM( TIME_TO_SEC( t1.`actualhours` ) ) AS actualhours".$selcolumn[1]."
		FROM `timeentry` t1
		WHERE t1.`isActive` = '1' ".$datecond." ".$cond."
		".$group."
	) n ";
		return $query;
	}
	
	if($_POST){
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$empid = $_POST['empid'];
		$pirmaster_id = $_POST['pirmaster_id'];
		$project_id = $_POST['project_id'];
		$activity_id = $_POST['activity_id'];
		$excelreport = $_POST['excelreport'];
		$withaverage = $_POST['withaverage'];
		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$cond = '';
		if($empid != ''){
			$cond .= " AND t1.employee_id='".$empid."'";
		}
		
		if($pirmaster_id != '' && $project_id==''){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'multiple');
				$pirmasterid = '';
				for($i=0;$i<count($pirmasterdet);$i++){
					if($i==(count($pirmasterdet)-1)){
						$pirmasterid .= "'".$pirmasterdet[$i]['id']."'";
					}
					else{
						$pirmasterid .= "'".$pirmasterdet[$i]['id']."',";
					}
				}
				$cond .= " AND t1.pirmaster_id IN (".$pirmasterid.")";
				$selcolumn[0]=",n.pirno";
				$selcolumn[1]=",(SELECT p.pirno FROM pirlist p,pirmaster pm WHERE pm.pirno=p.id AND pm.pirno='".$pirmaster_id."' GROUP BY pm.pirno) as pirno";
		}
		if($pirmaster_id != '' && $project_id!=''){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'multiple');
				$pirmasterid = '';
				for($i=0;$i<count($pirmasterdet);$i++){
					if($i==(count($pirmasterdet)-1)){
						$pirmasterid .= "'".$pirmasterdet[$i]['id']."'";
					}
					else{
						$pirmasterid .= "'".$pirmasterdet[$i]['id']."',";
					}
				}
				$cond .= " AND t1.pirmaster_id IN (".$pirmasterid.") AND t1.project_id='".$project_id."'";
				$selcolumn[0]=",n.pirno,n.proname";
				$selcolumn[1]=",(SELECT p.pirno FROM pirlist p,pirmaster pm WHERE pm.pirno=p.id AND pm.pirno='".$pirmaster_id."' GROUP BY pm.pirno) as pirno,(SELECT p.projectname FROM projectlist p WHERE p.id=t1.project_id) as proname";
		}
		if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'multiple');
				$pirmasterid = '';
				for($i=0;$i<count($pirmasterdet);$i++){
					if($i==(count($pirmasterdet)-1)){
						$pirmasterid .= "'".$pirmasterdet[$i]['id']."'";
					}
					else{
						$pirmasterid .= "'".$pirmasterdet[$i]['id']."',";
					}
				}
				$cond .= " AND t1.pirmaster_id IN (".$pirmasterid.") AND t1.project_id='".$project_id."' AND t1.activity_id='".$activity_id."'";
				$selcolumn[0]=",n.pirno,n.proname,n.activity";
				$selcolumn[1]=",(SELECT p.pirno FROM pirlist p,pirmaster pm WHERE pm.pirno=p.id AND pm.pirno='".$pirmaster_id."' GROUP BY pm.pirno) as pirno,(SELECT p.projectname FROM projectlist p WHERE p.id=t1.project_id) as proname,(SELECT a.name FROM activity a WHERE a.id=t1.activity_id) as activity";
		}			
		$group = " GROUP BY t1.`employee_id`";
		$datecond = " AND t1.`entrydate` BETWEEN '".$fromdate."' AND '".$todate."'";
			$reportdata = '';
			$query = getquery($datecond,$cond,$group,$selcolumn);
			$report = $dbase->executeQuery($query,'multiple');
			for($i=0;$i<count($report);$i++){
				$totcalculatedhours = $report[$i]['totcalculatedhours'];

				if($totcalculatedhours != '' ){
					$totcalculatedhoursarr = explode(":",$totcalculatedhours);
					$totcalculatedhoursnew = '';
					$totcalculatedhoursnew .= $totcalculatedhoursarr[0].":";					
					if(strlen($totcalculatedhoursarr[1])==1){
						$totcalculatedhoursnew .= "0".$totcalculatedhoursarr[1].":";	
					}
					else{
						$totcalculatedhoursnew .= $totcalculatedhoursarr[1].":";	
					}
					if(strlen($totcalculatedhoursarr[2])==1){
						$totcalculatedhoursnew .= "0".$totcalculatedhoursarr[2];	
					}
					else{
						$totcalculatedhoursnew .= $totcalculatedhoursarr[2];	
					}
				}
				$totactualhours = $report[$i]['totactualhours'];
				if($totactualhours != ''){
					$totactualhoursarr = explode(":",$totactualhours);
					$totactualhoursnew = '';
					$totactualhoursnew .= $totactualhoursarr[0].":";					
					if(strlen($totactualhoursarr[1])==1){
						$totactualhoursnew .= "0".$totactualhoursarr[1].":";	
					}
					else{
						$totactualhoursnew .= $totactualhoursarr[1].":";	
					}
					if(strlen($totactualhoursarr[2])==1){
						$totactualhoursnew .= "0".$totactualhoursarr[2];	
					}
					else{
						$totactualhoursnew .= $totactualhoursarr[2];	
					}
				}				
				$report[$i]['totcalculatedhours'] = $totcalculatedhoursnew;
				$report[$i]['totactualhours'] = $totactualhoursnew;
			}
			$reportdata = $report;
	}
if($excelreport=='1'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$detailHeadingStyleArray = array(
    'font'  => array(
        'bold'  => true,
		'italic'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
$detailValueStyleArray = array(
    'font'  => array(
		'italic'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}
function nextLetter(&$str) {
 $str = ('z' === $str ? 'a' : ++$str);
 return $str;
}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('User-wise Utilization');	
	$row = 1;
		$col11 = "A{$row}";
		if($pirmaster_id != '' && $project_id==''){
			$col22 = "F{$row}";
		}
		if($pirmaster_id != '' && $project_id!=''){
			$col22 = "G{$row}";
		}
		if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){
			$col22 = "H{$row}";
		}		
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('User-wise Utilization');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$letter = 'A';
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('EmpID');
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('Employee Name');
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('Total Parts');
		$letter = nextLetter($letter);
		if($pirmaster_id != '' && $project_id==''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('PIR No');
		$letter = nextLetter($letter);		
		}
		if($pirmaster_id != '' && $project_id!=''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('PIR No');
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('Project Name');
		$letter = nextLetter($letter);		
		}		
		if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('Activity');
		$letter = nextLetter($letter);
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('Actual Hours');
		$letter = nextLetter($letter);		
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('Budgeted Hours');		
		$row++;
$cnt = 0;
$partscnt = 0;
$totactualtimesarr = array();
$totcalculatedtimesarr = array();
for($q=0;$q<count($reportdata);$q++){
	$cnt++;
	$totactualtimesarr[] = $reportdata[$q]['totactualhours'];
	$totcalculatedtimesarr[] = $reportdata[$q]['totcalculatedhours'];
	$partscnt += $reportdata[$q]['totalparts'];	
		$letter = 'A';
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['empid']);
		$letter = nextLetter($letter);	
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['empname']);
		$letter = nextLetter($letter);	
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['totalparts']);
		$letter = nextLetter($letter);
		if($pirmaster_id != '' && $project_id==''){
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
			$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['pirno']);
			$letter = nextLetter($letter);			
		}
		if($pirmaster_id != '' && $project_id!=''){
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
			$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['pirno']);
			$letter = nextLetter($letter);
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
			$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['proname']);
			$letter = nextLetter($letter);
		}
		if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
			$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($reportdata[$q]['activity']);
			$letter = nextLetter($letter);
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($dbase->timetodisplay($reportdata[$q]['totactualhours']));
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($dbase->timetodisplay($reportdata[$q]['totcalculatedhours']));
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$row++;
}
		$col12 = "A{$row}";
		$col23 = "B{$row}";
		$concat =  $col12.":".$col23;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$letter = "C";
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($partscnt);
		$letter = nextLetter($letter);
		if($pirmaster_id != '' && $project_id==''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		}
		if($pirmaster_id != '' && $project_id!=''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		}
		if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);	
		}		
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($dbase->addTime($totactualtimesarr));
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue($dbase->addTime($totcalculatedtimesarr));
		$row++;
		$col12 = "A{$row}";
		$col23 = "B{$row}";
		$concat =  $col12.":".$col23;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Average');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$letter = "C";
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		if($pirmaster_id != '' && $project_id==''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		}
		if($pirmaster_id != '' && $project_id!=''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);
		}
		if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue('');
		$letter = nextLetter($letter);	
		}		
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue(round(($dbase->addTime($totactualtimesarr,true)/($cnt*510)),2)."%");
		$letter = nextLetter($letter);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle($letter.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($letter.$row)->setValue(round(($dbase->addTime($totcalculatedtimesarr,true)/($cnt*510)),2)."%");		

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="User-wise_utilization.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		
		table#reportdetailtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reportdetailtable td, table#reportdetailtable th {
			border: 1px solid white;
			 padding: 5px; 
		}		
		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
<td width="100%" colspan="3" ><b>User-wise Utilization</b></td>
</tr>
<tr>
<td width="12%" ><b>From Date</b></td>
<td width="12%" ><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('d-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>To Date</b></td>
<td width="12%" ><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="12%" ></td>
</tr>

<tr>
<td width="12%" ><b>Select PIR</b></td>
<td width="12%" >
	<select id="pirmaster_id" name="pirmaster_id" onchange="getproject();">
	<option value="">-Select-</option>
	</select>
</td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Project</b></td>
<td width="12%" >
	<select id="project_id" name="project_id" onchange="getactivity();">
	<option value="">-All-</option>
	</select>
</td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Activity</b></td>
<td width="12%" >
	<select id="activity_id" name="activity_id" >
	<option value="">-All-</option>
	</select>
</td>
<td width="12%" ><!--<input type="checkbox" id="withaverage" name="withaverage" value='1' <?php if($withaverage=='1'){?> checked <?php } ?> /> With Average--></td>
</tr>
<tr>
<td width="12%" ><b>Select Employee</b></td>
<td width="12%" ><select id="empid" name="empid" >
		<option value="">-All-</option>
		<?php
		$empcond = '';
		adminempdropdown($empcond,$empid);
		?>
		</select></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><input type="hidden" id="excelreport" name="excelreport"  /></td>
<td width="12%" ><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" /></td>
<td width="12%" ><a href="javascript:void(0);" onclick="getexcel();" style="cursor: pointer;"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a></td>
</tr>
</table>
</form>
</br>
<?php
if($fromdate != '' && $todate != ''){
 ?>
<table id="reporttable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
	<td width="5%" align="center"><b>Emp ID</b></td>
	<td width="15%" align="center"><b>Emp Name</b></td>
	<td width="7%" align="center"><b>Total Parts</b></td>
	<?php if($pirmaster_id != '' && $project_id==''){ ?>
	<td width="8%" align="center"><b>PIR No </b></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!=''){ ?>
	<td width="8%" align="center"><b>PIR No </b></td>
	<td width="8%" align="center"><b>Project Name </b></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){ ?>
	<td width="8%" align="center"><b>Activity</b></td>
	<?php } ?>		
	<td width="8%" align="center"><b>Actual Hours</b></td>
	<td width="8%" align="center"><b>Budgeted Hours</b></td>
</tr>
<?php
$cnt = 0;
$partscnt = 0;
$totactualtimesarr = array();
$totcalculatedtimesarr = array();
for($q=0;$q<count($reportdata);$q++){
	$cnt++;
	$totactualtimesarr[] = $reportdata[$q]['totactualhours'];
	$totcalculatedtimesarr[] = $reportdata[$q]['totcalculatedhours'];
	$partscnt += $reportdata[$q]['totalparts'];
?>
<tr>
	<td valign="top"><?php echo $reportdata[$q]['empid']; ?></td>
	<td valign="top"><?php echo $reportdata[$q]['empname']; ?></td>
	<td valign="top"><?php echo $reportdata[$q]['totalparts']; ?></td>
	<?php if($pirmaster_id != '' && $project_id==''){ ?>
	<td valign="top"><?php echo $reportdata[$q]['pirno']; ?></td>
	<?php } ?>		
	<?php if($pirmaster_id != '' && $project_id!=''){ ?>
	<td valign="top"><?php echo $reportdata[$q]['pirno']; ?></td>
	<td valign="top"><?php echo $reportdata[$q]['proname']; ?></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){ ?>
	<td valign="top"><?php echo $reportdata[$q]['activity']; ?></td>
	<?php } ?>		
	<td valign="top"><?php echo $dbase->timetodisplay($reportdata[$q]['totactualhours']); ?></td>
	<td valign="top"><?php echo $dbase->timetodisplay($reportdata[$q]['totcalculatedhours']); ?></td>
</tr>
<?php
}
?>
<tr>
	<td width="5%" align="center"></td>
	<td width="15%" align="center"></td>
	<?php if($pirmaster_id != '' && $project_id==''){ ?>
	<td width="8%"><b><?php echo $partscnt;?></b></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!=''){ ?>
	<td width="8%"><b><?php echo $partscnt;?></b></td>
	<td width="8%"></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){ ?>
	<td width="8%"></td>
	<?php } ?>		
	<td width="7%" align="center"><b>Total <?php //echo $cnt; ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($totactualtimesarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($totcalculatedtimesarr); ?></b></td>
</tr>
<tr>
	<td width="5%" align="center"></td>
	<td width="15%" align="center"></td>
	<?php if($pirmaster_id != '' && $project_id==''){ ?>
	<td width="8%"></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!=''){ ?>
	<td width="8%"></td>
	<td width="8%"></td>
	<?php } ?>	
	<?php if($pirmaster_id != '' && $project_id!='' && $activity_id!=''){ ?>

	<td width="8%"></td>
	<?php } ?>		
	<td width="7%" align="center"><b>Average <?php //echo $cnt; ?></b></td>
	<td width="8%" align="center"><b><?php echo round(($dbase->addTime($totactualtimesarr,true)/($cnt*510)),2)."%"; ?></b></td>
	<td width="8%" align="center"><b><?php echo round(($dbase->addTime($totcalculatedtimesarr,true)/($cnt*510)),2)."%"; ?></b></td>
</tr>
</table>
<br/>
<?php
}
?>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
 $('#from_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});
			getsubdepartment();
			//getpirdropdown();
			
			$("#excelreport").val('0');
	});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = '<?php echo $subdeptid;?>';
	$.get("getsubdepartmentadmin.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
		getpirdropdown();
	});
}
function getexcel(){
	if($("#pirmaster_id").val()==''){
		$('<div>Please Select PIR No</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$(this).dialog("close");
					$( "div" ).remove( ".ui-widget-overlay" );	
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	}
	else{	
	$("#excelreport").val('1');
	$("#frm_details").submit();
	$("#excelreport").val('0');
	}
}
function getpirdropdown(){
	var pirmaster_id = '<?php echo $pirmaster_id;?>';
	$.get("getpiradmin.php?pirmaster_id="+pirmaster_id+"&t=0",function(data){
		$("#pirmaster_id").html(data);
		getproject();
	});
}
function getproject(){
	var id = $("#pirmaster_id").val();
	var selid = '<?php echo $project_id;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#project_id").html(data);
		getactivity();
	});
}
function getactivity(){
	var pirid = $("#pirmaster_id").val();
	var proid = $("#project_id").val();
	var selid = '<?php echo $activity_id;?>';
	$.get("getactivityadmin.php?pirid="+pirid+"&proid="+proid+"&selid="+selid,function(data){
		$("#activity_id").html(data);
	});
}
function submitform(){
	if($("#pirmaster_id").val()==''){
		$('<div>Please Select PIR No</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$(this).dialog("close");
					$( "div" ).remove( ".ui-widget-overlay" );	
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	}
	else{
		$("#frm_details").submit();
	}
}
</script>
</html>
<?php } ?>

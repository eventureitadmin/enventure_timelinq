<?php 
	include_once('dbase.php');
	class master extends dbase{
	public $level= 0;
	public function getempdetails($id){
		$result='';
		$select = "SELECT ID,emp_name,designation,direct_rpt_id,indirect_rpt_id,role_id FROM employeelist WHERE id='".$id."' AND `isactive`=1 ";
		$result = $this->executeQuery($select,"single");
		return $result;
	}

	public function getchild($id,$returncnt=false){
		$result=array();
		if($id != ''){
			
		$select = "SELECT ID,emp_name,designation FROM employeelist WHERE direct_rpt_id='".$id."' AND `isactive`=1 ";
		$selresult = $this->executeQuery($select,"multiple");
		$cnt = count($selresult);
		if($returncnt){
			return $cnt;
		}
		if($cnt > 0){
			$i=0;
			//while ($row = mysql_fetch_array($query)) {
			foreach($selresult as $key=>$row){
				$result[$i]['ID'] = $row['ID'];
				$result[$i]['emp_name'] = $row['emp_name'];
				$result[$i]['designation'] = $row['designation'];
				$i++;
			}
		}
		return $result;
		}
	}

	public function getjsondata($res,$j=''){
		$jsondata = '';
		if($res['role_id'] == 2){
			$jsondata .= "'name': 'Management',";
		}else{
			$jsondata .= "'name': '".$res['emp_name']."',";
			$jsondata .= "'title': '".$res['designation']."',";
		}
		
		if($j != ''){
			$jsondata .= "'className': 'org-level".$j."',";
		}
		
		return $jsondata;
	}

	public function childloop($id,$j){
		$cnt = $this->getchild($id,true);
		if($cnt > 0){
			$j++;
			$jsondata .= "'children': [";
			$this->level = $j;
			$result = $this->getchild($id);
			for($i=0;$i<count($result);$i++){
				$res1 = $this->getempdetails($result[$i]['ID']);
				$jsondata .=  "{";
				$jsondata .= $this->getjsondata($res1,$j);
				
				$jsondata .= $this->childloop($res1['ID'],$j);
				$jsondata .=  "},";
			}
			$jsondata .= "]";
		}
		return $jsondata;
	}
	public function checkempstatus($emp_id){
		$isactive = true;
	 	$query = "SELECT COUNT(ID) as cnt FROM `employeelist` WHERE  `isactive`=0 AND ID =".$emp_id;
		$res = $this->executeQuery($query,"single");
		if($res['cnt'] > 0){
			$isactive = false;
		}
		return $isactive;
	}
	public function createtree($id){
			$j=1;
			$jsondata = '';
			$jsondata .=  "{";
			$res = $this->getempdetails($id);
			if($res['direct_rpt_id'] > 0 || $res['role_id'] == 2 || $res['role_id'] == 1){
					$res1 = $this->getempdetails($res['direct_rpt_id']);
				if( $res['role_id'] == 2 || $res['role_id'] == 1){
					$jsondata .= $this->getjsondata($res,$j);
				}else{
					$jsondata .= $this->getjsondata($res1,$j);
				}
				if($res['role_id'] > 2){
					$j++;
					$jsondata .= "'children': [{";
				}
			}
		
			if($res['role_id'] > 2){
				$jsondata .= $this->getjsondata($res,$j);
			
			}
			$jsondata .= $this->childloop($id,$j);
			if($res['direct_rpt_id'] > 0 && $res['role_id'] > 2){
				$jsondata .= "}]}";
			}else{
				$jsondata .= "}";
			}
			return $jsondata;
		}
		public function getkpidata($month,$year,$dept_id){
			$result ="";
			if($month > 0 && $year >0 && $dept_id >0){
			$query = "SELECT * FROM `kpi` WHERE `month`= '".$month."'AND `year`='".$year."'AND `dept_id`='".$dept_id."'";
			$result = $this->executeQuery($query,"single");
			}
			return $result;
		}
		public function getkpilist($fromyear,$toyear,$frommonth,$tomonth,$dept_id){
			$result="";$query = "";
			if($fromyear>0 && $toyear >0 && $frommonth > 0 && $tomonth >0){
				 $query = "SELECT * FROM (SELECT * FROM `kpi` WHERE `year` BETWEEN '".$fromyear."' AND '".$toyear."' AND `dept_id`='".$dept_id."')m WHERE m.`month` >='".(int)$frommonth."' AND m.year ='".$fromyear."'";
				if($fromyear == $toyear){
					$query .= " AND ";
				}else{
					$query .= " OR ";
				}
				$query .=" m.`month` <='".(int)$tomonth."' AND m.year ='".$toyear."' ORDER BY m.year DESC , m.month ASC";
				//echo $query;
				$result = $this->executeQuery($query,"multiple");
			}
			return $result;
		}
		
		public function savekpidata($id,$data){
			$savestatus   = false;
			if($data['month'] > 0 && $data['year'] >0 && $data['revenue_target'] >0){
			 	$query ="SELECT COUNT(id) as cnt,`id` FROM `kpi` WHERE 
				`dept_id`='".$data['dept_id']."' AND `month`='".$data['month']."' AND `year`='".$data['year']."'";
				$result = $this->executeQuery($query,"single");
				if($result['cnt'] == 1){
					$id =$result['id'];
				}
				if($id > 0 && $result['cnt'] == 1){
					 $updateqry = "UPDATE `kpi` SET `revenue_target`='".$data['revenue_target']."',`achieved`='".$data['achieved']."', `month_revenue`='".$data['month_revenue']."',`quarter_nps`='".$data['quarter_nps']."',`is_lock`='".$data['is_lock']."',`month_nps`='".$data['month_nps']."',`updated_by`='".$_SESSION['timesheet']['ID']."' WHERE  id='".$id."'";
					$savestatus  =  $this->query($updateqry);
				}else if($result['cnt'] == 0 && $id== 0){
					$insertqry = "INSERT INTO `kpi` (`month`,`year`,`dept_id`,`revenue_target`,`achieved`,`month_revenue`,`month_nps`,`quarter_nps`,`is_lock`,`created_by`,`created_date`) VALUES('".$data['month']."','".$data['year']."','".$data['dept_id']."','".$data['revenue_target']."','".$data['achieved']."','".$data['month_revenue']."','".$data['month_nps']."','".$data['quarter_nps']."','".$data['is_lock']."','".$_SESSION['timesheet']['ID']."','".date("Y-m-d h:i:s")."')";
					$savestatus = $this->query($insertqry);
				}
			}
			return $savestatus;
		}
		public function getmonths(){
			$months = array(
			"1"=>"Jan", "2"=>"Feb","3"=>"Mar","4"=>"Apr","5"=>"May",
			"6"=>"Jun", "7"=>"Jul", "8"=>"Aug", "9"=>"Sep",
			"10"=>"Oct", "11"=>"Nov","12"=>"Dec");
			return $months;
		}
		public function checkprefinancialyear($selyear){
			
			if($selyear < date("Y")){
				return true;
			}else{
				return false;
			}
		}
		public function savepartlinqdata($data){
			if(count($data) >0){
				$ch = curl_init("https://workbench.partlinq.com/savepartlinqdata.php"); 
				$str = http_build_query($data);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));		
				curl_setopt($ch, CURLOPT_POSTFIELDS,
							$str);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				echo $server_output = curl_exec($ch);
				curl_close ($ch);
				if($server_output){ 
						return true;
				}else{ 
					return false;
				}
			}
		}
		public function savemldemodata($data){
			if(count($data) >0){
				$ch = curl_init("http://14.141.34.75/mldemo/savemldemodata.php"); 
				$str = http_build_query($data);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));		
				curl_setopt($ch, CURLOPT_POSTFIELDS,
							$str);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				  $server_output = curl_exec($ch);
				curl_close ($ch);
				return true;
				
			}
		}
		public function getcompletedpartsinfo($data){
	
			if(count($data) >0){
			
				$ch = curl_init("https://workbench.partlinq.com/getcompletedpartsres.php");  
				$str = http_build_query($data);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));		
				curl_setopt($ch, CURLOPT_POSTFIELDS,
							$str);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
				curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
				curl_setopt($ch, CURLOPT_USERPWD, ":");  
				$result = curl_exec($ch); 
				 $json = json_decode($result, true);  
				curl_close($ch);  
				// echo $json;
				return $json;
			}
		}
			public function getusertypes(){
				$ch = curl_init("http://192.168.3.72/partlinq/getentrytypes.php");  
				curl_setopt($ch, CURLOPT_POST, 1);		
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
				curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
				curl_setopt($ch, CURLOPT_USERPWD, ":");  
				 $result = curl_exec($ch);  
				$json = json_decode($result, true);  
				curl_close($ch);  
				 
				return $json;
			
		}
		
			public function getmldemousertypes(){
				$ch = curl_init("https://mdg.partlinq.com/mldemo/getentrytypes.php");  
				curl_setopt($ch, CURLOPT_POST, 1);		
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
				curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
				curl_setopt($ch, CURLOPT_USERPWD, ":");  
				 $result = curl_exec($ch);  
				$json = json_decode($result, true);  
				curl_close($ch);  
				 
				return $json;
		
		}
		public function getmdgcompletedparts($data){
	
			if(count($data) >0){
			
				$ch = curl_init("http://14.141.34.75/mldemo/getcompletedpartsres.php");  
				$str = http_build_query($data);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));		
				curl_setopt($ch, CURLOPT_POSTFIELDS,
							$str);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
				curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
				curl_setopt($ch, CURLOPT_USERPWD, ":");  
				      $result = curl_exec($ch); 
				 $json = json_decode($result, true);  
				curl_close($ch);  
				// echo $json;
				return $json;
			}
		}
	}


?>

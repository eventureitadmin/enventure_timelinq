<?php
session_start();
class dbase {
	//number of rows affected by SQL query
	public $affected_rows = 0;
	
	public $link_id = 0;
	public $query_id = 0;
	public $dateCreated;
	public $timeCreated;
	
	public $dbData;
	public $validationErrors;
	public $returnDataToForm;
	
	public $validationInputMethod = 'POST';
	

	public $globalUserId;
	public $globalUserName;

	public $colSize = array ("1" => "8", "2" => "11", "3" => "30", "4" => "7", "5" => "10", "6" => "7" );
	public $colAlign = array ("1" => "L", "2" => "L", "3" => "L", "4" => "L", "5" => "R", "6" => "L" );
	
	//Constructor	
	function __construct() {
		$this->dateCreated = date("Y-m-d");
		$this->timeCreated = date("H:i:s");
		$this->datetimeCreated = date("Y-m-d H:i:s");
		$this->globalUserId = $_SESSION['timesheet']['ID'];
		$this->globalUserName = $_SESSION['timesheet']['USERNAME'];

	}
	
	//Magic Method to invoke Method Overloading.
	public function __call($name, $arguments) {
		switch ($name) {
			case 'validate' :
				switch (count ( $arguments )) {
					case 3 :
						$this->validateText ( $arguments [0], $arguments [1], $arguments [2], true, true );
						break;
					case 4 :
						$this->validateText ( $arguments [0], $arguments [1], $arguments [2], $arguments [3], true );
						break;
					case 5 :
						$this->validateText ( $arguments [0], $arguments [1], $arguments [2], $arguments [3], $arguments [4] );
						break;
					case 6 :
						$this->validatePassword ( $arguments [0], $arguments [1], $arguments [2], $arguments [3], $arguments [4], $arguments [5] );
						break;
					default :
						echo "ERROR: Invalid number of arguments or method does not exist";
				}
				break;
			default :
				echo "ERROR: Call to undefined function $name ";
		}
	}
	
	//Arguments -> input data, type = numeric/text/email/password, isRequired, isData, labelname, confirmpasswordinput  
	

	public function validateText($input, $labelName, $type, $isRequired, $isData) {
		if ($isRequired && empty ( $_POST [$input] )  && $_POST [$input] !== '0') {
			$this->validationErrors [] = $labelName . " is required";
		} else {
			if ($type == 'numeric') {
				if (is_numeric ( $_POST [$input] )) {
					if ($isData) {
						$this->dbData [$input] = $_POST [$input];
					}
				} else {
					$this->validationErrors [] = $labelName . " should be a number";
				}
			} else if ($type == 'email') {
				if (! empty ( $_POST [$input] )) {
					if (filter_var ( $_POST [$input], FILTER_VALIDATE_EMAIL )) {
						if ($isData) {
							$this->dbData [$input] = $_POST [$input];
						}
					} else {
						$this->validationErrors [] = $labelName . " is not a valid email address";
					}
				}
			} else if ($type == 'date') {
				if ($isData) {
					$this->dbData [$input] = $this->dateFormatToDbase ( $_POST [$input] );
				}
			
			} else {
				if ($isData) {
					$this->dbData [$input] = $_POST [$input];
				}
			}
			$this->returnDataToForm [$input] = $_POST [$input];
		}
	}
	
	public function validatePassword($input, $labelName, $type, $isRequired, $isData, $confirmpassword) {
		if ($isRequired && empty ( $_POST [$input] )) {
			$this->validationErrors [] = $labelName . " is required";
		} else {
			if ($_POST [$input] == $_POST [$confirmpassword]) {
				$this->dbData [$input] = md5 ( $_POST [$input] );
			} else {
				$this->validationErrors [] = $labelName . " mismatch";
			}
			$this->returnData = $_POST [$input];
		}
	}
	
	//Public Methods
	public function getSingleRow($table, $where_clause = "") {
		$SelectQuery = $this->createQuery ( $table, $where_clause );
		$result = $this->executeQuery ( $SelectQuery, 'single' );	
		return $result;
	}
	public function getMultipleRows($table, $where_clause = "") {
		$SelectQuery = $this->createQuery ( $table, $where_clause );
		$result = $this->executeQuery ( $SelectQuery, 'multiple' );
		return $result;
	}
	public function executeQuery($query, $type = 'multiple') {
		if (DEBUG_MODE) {
			$this->writeLog ( $query );
		}

		$result = mysql_query ( $query ) or die ( mysql_error () );
		$number = mysql_num_rows ( $result );
		if ($type == 'multiple') {
			while ( $row = mysql_fetch_array ( $result ) ) {
				$row_val [] = $row;
			}
		} else if ($type == 'single') {
			while ( $row = mysql_fetch_array ( $result ) ) {
				$row_val = $row;
			}
		} else {
			if ($number < 1) {
				$row_val = "";
			} else if ($number == 1) {
				while ( $row = mysql_fetch_array ( $result ) ) {
					$row_val = $row;
				}
			} else {
				while ( $row = mysql_fetch_array ( $result ) ) {
					$row_val [] = $row;
				}
			}
		}
		return $row_val;
	}
	public function getNumRows($table, $where_clause) {
		$SelectQuery = $this->createQuery ( $table, $where_clause );
		if (DEBUG_MODE) {
			$this->writeLog ( $SelectQuery );
		}
		$result = mysql_query ( $SelectQuery ) or die ( mysql_error () );
		$num = mysql_num_rows ( $result );
		return $num;
	}
	
	public function getNumRowswithQuery($sql) {
		if (DEBUG_MODE) {
			$this->writeLog ( $sql );
		}
		$result = mysql_query ( $sql ) or die ( mysql_error () );
		$num = mysql_num_rows ( $result );
		return $num;
	}	
	public function createQuery($table, $where_clause) {
		$SelectQuery = "SELECT * FROM $table ";
		if ($where_clause) {
			$SelectQuery .= $where_clause;
		}
		//echo $SelectQuery;exit;
		return $SelectQuery;
	}
	
	function is_leapyear($year) {
		return ($year % 4 == 0) ? ! ($year % 100 == 0 && $year % 400 != 0) : false;
	}
	function query_insert($table, $data) {
		$q = "INSERT INTO `" . $table . "` ";
		$v = '';
		$n = '';
		
		foreach ( $data as $key => $val ) {
			$n .= "`$key`, ";
			if (strtolower ( $val ) == 'null')
				$v .= "NULL, ";
			elseif (strtolower ( $val ) == 'now()')
				$v .= "NOW(), ";
			else
				$v .= "'" . $this->escape ( $val ) . "', ";
		}
		
		$q .= "(" . rtrim ( $n, ', ' ) . ") VALUES (" . rtrim ( $v, ', ' ) . ");";
		
		if ($this->query ( $q )) {
			return mysql_insert_id ();
		} else {
			return false;
		}
	
	}
	
	function query_insert1($table, $data) {
		$q = "INSERT INTO " . $table . " ";
		$v = '';
		$n = '';
		
		foreach ( $data as $key => $val ) {
			$n .= "`$key`, ";
			if (strtolower ( $val ) == 'null')
				$v .= "NULL, ";
			elseif (strtolower ( $val ) == 'now()')
				$v .= "NOW(), ";
			else
				$v .= "'" . $this->escape ( $val ) . "', ";
		}
		
		$q .= "(" . rtrim ( $n, ', ' ) . ") VALUES (" . rtrim ( $v, ', ' ) . ");";
		
		mysql_query ( $q ) or die ( mysql_error () );
	}
	
	function query_update($table, $data, $where = '1') {
		$q = "UPDATE `" . $table . "` SET ";
		
		foreach ( $data as $key => $val ) {
			if (strtolower ( $val ) == 'null')
				$q .= "`$key` = NULL, ";
			elseif (strtolower ( $val ) == 'now()')
				$q .= "`$key` = NOW(), ";
			else
				$q .= "`$key`='" . $this->escape ( $val ) . "', ";
		}
		
		$q = rtrim ( $q, ', ' ) . ' WHERE ' . $where . ';';
		return $this->query ( $q );
	
	}
	
	function query_update1($table, $data, $where = '1') {
		$q = "UPDATE " . $table . " SET ";
		
		foreach ( $data as $key => $val ) {
			if (strtolower ( $val ) == 'null')
				$q .= "`$key` = NULL, ";
			elseif (strtolower ( $val ) == 'now()')
				$q .= "`$key` = NOW(), ";
			else
				$q .= "`$key`='" . $this->escape ( $val ) . "', ";
		}
		
		$q = rtrim ( $q, ', ' ) . ' WHERE ' . $where . ';';
		return $this->query ( $q );
	
	}
	
	function query($sql) {
		// do query
		if ($this->checkforSession () == 'true') {
			if (DEBUG_MODE) {
				$this->writeLog ( $sql );
			}
			$this->query_id = @mysql_query ( $sql );
		} else {
			$this->redirect ( "login.php" );
		}
		if (! $this->query_id) {
			if (DEBUG_MODE) {
				$this->oops ( "<b>MySQL Query fail:</b> $sql" );
			}
		}
		
		$this->affected_rows = @mysql_affected_rows ();
		
		return $this->query_id;
	}
	
	function executeNonQuery($query) {
		if (DEBUG_MODE) {
			$this->writeLog ( $query );
		}
		return mysql_query ( $query );
	}
	
	function daysInMonth($month, $year) {
		$days = array (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
		return ($month == 2 && is_leapYear ( $year )) ? 29 : $days [$month - 1];
	}
	
	function checkEmail($emailaddress) {
		if (filter_var ( $emailaddress, FILTER_VALIDATE_EMAIL )) {
			return true;
		} else {
			return false;
		}
	}
	
	function sendMailold($message, $to, $from, $subject) {
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . ">\r\n";
		$html = "";
		$html .= $message;
		$html .= "";
		//echo $to."\r\n".$subject."\r\n".$html."\r\n".$headers."\r\n";
		if(mail ( $to, $subject, $html, $headers )){
                    return true;
                }
                else{
                    return false;
                }
	}
	
	function sendMail($message, $to, $to_name, $from, $from_name, $subject) {
		require_once('class.phpmailer.php');
		$mail             = new PHPMailer(); // defaults to using php "mail()"
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPAuth   = true;                  	// enable SMTP authentication
		$mail->Host       = SMTP_HOST; 			// SMTP server
		$mail->Username   = SMTP_USERNAME; 		// SMTP account username
		$mail->Password   = SMTP_PASSWORD;        	// SMTP account password
		$mail->Port       = SMTP_PORT;                  // set the SMTP port for the GMAIL server
		//$mail->SMTPDebug  = 2;                     	// enables SMTP debug information (for testing)
		$mail->AddReplyTo($from,$from_name);
		$mail->SetFrom($from, $from_name);
		$mail->AddAddress($to, $to_name);
		$mail->Subject    = $subject;
		$body             = $message;		
		$mail->MsgHTML($body);
		if(SEND_MAIL){
		  $mail->Send();
		}
	}	
	
	function generatePassword($length = 8) {
		// start with a blank password
		$password = "";
		// define possible characters
		$possible = "0123456789bcdfghjkmnpqrstvwxyz";
		// set up a counter
		$i = 0;
		// add random characters to $password until $length is reached
		while ( $i < $length ) {
			// pick a random character from the possible ones
			$char = substr ( $possible, mt_rand ( 0, strlen ( $possible ) - 1 ), 1 );
			// we don't want this character if it's already in the password
			if (! strstr ( $password, $char )) {
				$password .= $char;
				$i ++;
			}
		}
		// done!
		return $password;
	}
	
	function randomPassword($length = 8) {
		// start with a blank password
		$password = "";
		// define possible characters
		$possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		// set up a counter
		$i = 0;
		// add random characters to $password until $length is reached
		while ( $i < $length ) {
			// pick a random character from the possible ones
			$char = substr ( $possible, mt_rand ( 0, strlen ( $possible ) - 1 ), 1 );
			// we don't want this character if it's already in the password
			if (! strstr ( $password, $char )) {
				$password .= $char;
				$i ++;
			}
		}
		// done!
		return $password;
	}	
	
	function escape($string) {
		if (get_magic_quotes_gpc ())
			$string = stripslashes ( $string );
		return mysql_real_escape_string ( $string );
	}
	
	function dateFormat($in, $out, $date) {
		$reg = '^([-/\ \.Yymndj]{8,10})$';
		if (! @ereg ( $reg, $in . $out ))
			return 1;
		$date = @ereg_replace ( '[/\ \.]', '-', $date );
		$_date = explode ( '-', $date );
		$date = sprintf ( '%02u', $_date [0] ) . sprintf ( '%02u', $_date [1] ) . sprintf ( '%02u', $_date [2] );
		
		$in = @ereg_replace ( '[/\ \.-]', '', $in );
		for($i = 0; $i < strlen ( $in ); $i ++) {
			$v = $in [$i];
			
			if (@ereg ( '[Y]', $v ))
				$len = 4;
			elseif (@ereg ( '[ymndj]', $v ))
				$len = 2;
			
			$dt [$v] = substr ( $date, 0, $len );
			$date = substr ( $date, $len );
			
			if (@ereg ( '[Yy]', $v ))
				$v == 'y' ? $dt ['Y'] = ($dt [$v] <= date ( $v ) ? '20' . $dt [$v] : '19' . $dt [$v]) : $dt ['y'] = substr ( $dt [$v], 2 );
			elseif (@ereg ( '[mn]', $v ))
				$v == 'n' ? $dt ['m'] = $dt [$v] : $dt ['n'] = ( int ) $dt [$v];
			elseif (@ereg ( '[dj]', $v ))
				$v == 'j' ? $dt ['d'] = $dt [$v] : $dt ['j'] = ( int ) $dt [$v];
		}
		
		$j = 0;
		$o_dt = $sp = array ();
		for($i = 0; $i < strlen ( $out ); $i ++) {
			$v = $out [$i];
			
			if (! @ereg ( '[Yymndj]', $v ))
				$sp [$j ++] = $v;
			else
				$o_dt [] = $dt [$v];
		}
		
		$ret = $o_dt [0] . @$sp [0] . $o_dt [1] . @$sp [1] . $o_dt [2];
		
		return $ret;
	}
	
	function dateFormatToDbase($date) {
		return $this->dateFormat ( "d-m-Y", "Y-m-d", $date );
	}
	
	function dateFormatToDisplay($date) {
		return $this->dateFormat ( "Y-m-d", "d-m-Y", $date );
	}
	
	function timeFormatToDisplay($time,$case) {
		$gettime = strtotime($time);
		if($case=='u'){
		  $timeformat = "g:i A";
		}
		else{
		  $timeformat = "g:i a";
		}
		return date ( $timeformat, $gettime );
	}	
	
	function getCurrentMonthDates($keyVal) {
		$month = date ( 'm' );
		$year = date ( 'Y' );
		if ($keyVal == 1) {
			return ("01-" . $month . "-" . $year);
		} else if ($keyVal == 2) {
			$num = cal_days_in_month ( CAL_GREGORIAN, $month, $year );
			return ($num . "-" . $month . "-" . $year);
		}
	}
	
	function oops($msg = '') {
		if ($this->link_id > 0) {
			$this->error = mysql_error ( $this->link_id );
			$this->errno = mysql_errno ( $this->link_id );
		} else {
			$this->error = mysql_error ();
			$this->errno = mysql_errno ();
		}
		echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" style=\"background:white;color:black;width:80%;\">" . "<tr><th colspan=2>Database Error</th></tr>" . "<tr><td align=\"right\" valign=\"top\">Message:</td><td><?php echo $msg; ?></td></tr>";
		if (strlen ( $this->error ) > 0)
			echo '<tr><td align="right" valign="top" nowrap>MySQL Error:</td><td>' . $this->error . '</td></tr>';
		echo "<tr><td align=\"right\">Date:</td><td>";
		echo date ( "l, F j, Y a\\t g:i:s A" );
		echo "</td></tr>";
		echo "<tr><td align=\"right\">Script:</td><td><a href=\"";
		echo @$_SERVER ['REQUEST_URI'];
		echo "\">";
		echo @$_SERVER ['REQUEST_URI'];
		echo "</a></td></tr>";
		if (strlen ( @$_SERVER ['HTTP_REFERER'] ) > 0)
			echo '<tr><td align="right">Referer:</td><td><a href="' . @$_SERVER ['HTTP_REFERER'] . '">' . @$_SERVER ['HTTP_REFERER'] . '</a></td></tr>';
		echo "</table>";
	}
	function stringPad($data, $space, $align) {
		$count = strlen ( $data );
		if ($align == 'right') {
			$align = STR_PAD_LEFT;
		} else if ($align == 'middle') {
			$align = STR_PAD_BOTH;
		} else {
			$align = STR_PAD_RIGHT;
		}
		if ($count > $space) {
			//$data1 = substr($data,0,25);
			$data1 = substr ( $data, 0, $space );
			echo str_pad ( $data1, $space, " ", $align );
		} else {
			echo str_pad ( $data, $space, " ", $align );
		}
	}
		
	function stripos($haystack, $needle, $offset = 0) {
		return strpos ( strtolower ( $haystack ), strtolower ( $needle ), $offset );
	}
	function formfeed() {
		$result = chr ( 12 );
		return $result;
	}
	function reversePage() {
		for($i = 0; $i < 4; $i ++) {
			echo chr ( 27 ) . "j" . chr ( 72 );
		}
	}
	function boldText() {
		$result = chr ( 27 ) . chr ( 69 );
		return $result;
	}
	function normalText() {
		$result = chr ( 27 ) . chr ( 70 );
		return $result;
	}
	function biggerText() {
		$result = chr ( 14 );
		return $result;
	}
	function dateValidate($date1, $date2) {
		$date1 = strtotime ( $date1 );
		$date2 = strtotime ( $date2 );
		$today = strtotime ( date ( 'd-m-Y' ) );
		if ($date1 > $date2) {
			return ("Date Mismatch");
		} else {
			if ($date2 > $today) {
				return ("Future Date Occurred");
			}
		}
	}
	function spaceSkip() {
		$result = "";
	}
	
	function writeLog($text) {
	  if(LOG_QUERIES){
		@file_put_contents ( ROOT_DIR . "//logs//" . date ( "Y-m-d" ) . ".log", $text . "\n", FILE_APPEND );
	  }	
	}
	
	public function redirect($URL) {
		//session_destroy ();
		header ( "location:"  . $URL );
	}
	
	public function checkforSession() {
		// if($isadmin==1){
			// if ($_SESSION ['USER_LOGIN'] != '') {
				// return true;
			// } else {
				// return false;
			// }
		// }
		// else{
			// return true;
		// }
		return true;
	}
	public function timeDifference($date1, $date2) {
		$diff = abs ( strtotime ( $date2 ) - strtotime ( $date1 ) );
		$data ['years'] = floor ( $diff / (365 * 60 * 60 * 24) );
		$data ['months'] = floor ( ($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24) );
		$data ['days'] = floor ( ($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24) );
		$data ['hours'] = floor ( ($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60) );
		$data ['minuts'] = floor ( ($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60 );
		$data ['seconds'] = floor ( ($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60) );
		return $data;
	}
	public function printColumns($colNo, $content) {
		$contentLength = strlen ( $content );
		$align = $colAlign [$colNo];
		$columnLength = $colSize [$colNo];
		$returnContent = $content;
		for($i = 0; $i < ($columnLength - $contentLength); $i ++) {
			if ($align == "R") {
				$returnContent = " " + $returnContent;
			} else if ($align == "L") {
				$returnContent = $returnContent + " ";
			}
		}
		return $returnContent;
	}
	
	public function getDateLists($startdate, $enddate) {
		$date = array ();
		$init_date = strtotime ( $startdate );
		$dst_date = strtotime ( $enddate );
		$offset = $dst_date - $init_date;
		$dates = floor ( $offset / 60 / 60 / 24 ) + 1;
		for($i = 0; $i < $dates; $i ++) {
			$newdate = date ( "Y-m-d", mktime ( 12, 0, 0, date ( "m", strtotime ( $startdate ) ), (date ( "d", strtotime ( $startdate ) ) + $i), date ( "Y", strtotime ( $startdate ) ) ) );
			$date [] = $newdate;
		}
		return $date;
	}
	
	public function removeNumericKeys($inpArray) {
		$opArray = array ();
		foreach ( $inpArray as $key => $value ) {
			if (is_numeric ( $key )) {
				//unset($inpArray[$key]);
			} else {
				$opArray [$key] = $inpArray [$key];
			}
		}
		return $opArray;
	}
	
	public function splitStringAndNumber($value) {
		$outputArr = array ();
		preg_match_all ( "/[a-z]+|[A-Z]/", $value, $hashA );
		$outputArr ['chars'] = implode ( $hashA [0] );
		preg_match_all ( "/[0-9]/", $value, $hashA );
		$outputArr ['numbers'] = implode ( $hashA [0] );
		return $outputArr;
	}
	
	
	public function csv_file_to_mysql_table($source_file, $target_table, $max_line_length = 0) {
		if (($handle = fopen ( $source_file, "r" )) !== FALSE) {
			$insert_query_prefix = "INSERT INTO $target_table  VALUES";
			while ( ($data = fgetcsv ( $handle, $max_line_length, "," )) !== FALSE ) {
				while ( count ( $data ) < count ( $columns ) )
					array_push ( $data, NULL );
				$query = "$insert_query_prefix (" . join ( ",", $this->quote_all_array ( $data ) ) . ");";
				//echo $query;
				mysql_query ( $query );
			}
			fclose ( $handle );
		}
	}
	
	public function quote_all_array($values) {
		foreach ( $values as $key => $value )
			if (is_array ( $value ))
				$values [$key] = $this->quote_all_array ( $value );
			else
				$values [$key] = $this->quote_all ( $value );
		return $values;
	}
	
	public function quote_all($value) {
		if (is_null ( $value ))
			return "NULL";
		$value = "'" . mysql_real_escape_string ( $value ) . "'";
		return $value;
	}
	
	public function money_formatter($number){
		if(strstr($number,"-")){
			$number = str_replace("-","",$number);
			$negative = "-";
		}
		$split_number = @explode(".",$number);
		$rupee = $split_number[0];
		$paise = @$split_number[1];
		if(@strlen($rupee)>3){
			$hundreds = substr($rupee,strlen($rupee)-3);
			$thousands_in_reverse = strrev(substr($rupee,0,strlen($rupee)-3));
			for($i=0; $i<(strlen($thousands_in_reverse)); $i=$i+2){
				$thousands .= $thousands_in_reverse[$i].$thousands_in_reverse[$i+1].",";
			}
			$thousands = strrev(trim($thousands,","));
			$formatted_rupee = $thousands.",".$hundreds;
		}
		else{
			$formatted_rupee = $rupee;
		}
		if((int)$paise>0){
			$formatted_paise = ".".substr($paise,0,2);
		}
		else{
			 $formatted_paise = ".00";
		}
		return $negative.$formatted_rupee.$formatted_paise;
	} 
	
	public function dateswithweekdays($startDate, $endDate){
	   $days = (strtotime($endDate) - strtotime($startDate)) / 86400 + 1;
	   $startMonth = date("m", strtotime($startDate));
	   $startDay = date("d", strtotime($startDate));
	   $startYear = date("Y", strtotime($startDate));
	   for($i=0; $i<$days; $i++){
		   $dates[$i] = date("d-m-Y~l~w", mktime(0, 0, 0, $startMonth , ($startDay+$i), $startYear));
	   }
   return $dates;   
}

	public function sendSms($smsdata){
		$mobile  = rawurlencode($smsdata['mobile']);
		$message = rawurlencode($smsdata['message']);
		if($mobile !="" && $message !=""){
		  if(SEND_SMS){
			$fp = fopen(SMS_API_URL."&to=".$mobile."&message=".$message, "r");
			$response = stream_get_contents($fp);
			fpassthru($fp);
			fclose($fp);
//			if($response=="")
//			echo "Process Failed, Please check domain, username and password.";
//			else
//			echo "$response";	
			return substr($response,7);
		  }
		}
	}

	public function customwordwrap($text,$charactercount=30){
	  $text = wordwrap($text, $charactercount, "\n", true);
	  $text = htmlentities($text);
	  $text = nl2br($text);
	  return $text;
	}

	public function pre($array,$exit=false){
		echo "<pre>";
		print_r($array);
		if($exit){
			exit;
		}
	}
	
	public function trim_text($input, $length, $ellipses = true, $strip_html = true) {
		//strip tags, if desired
		if ($strip_html) {
			$input = strip_tags($input);
		}
	  
		//no need to trim, already shorter than trim length
		if (strlen($input) <= $length) {
			return $input;
		}
	  
		//find last space within length
		$last_space = strrpos(substr($input, 0, $length), ' ');
		$trimmed_text = substr($input, 0, $last_space);
	  
		//add ellipses (...)
		if ($ellipses) {
			$trimmed_text .= '...';
		}
	  
		return $trimmed_text;
	}	
	
	public function sendsmtpmailold($from,$tomailid,$subject,$msg){
		require_once(PAGEURL."/sendmail/smtp.php");
		require_once(PAGEURL."/sendmail/sasl.php");
		$smtp=new smtp_class;

		$smtp->host_name=SMTP_HOST;
		$smtp->host_port=SMTP_PORT;
		$smtp->ssl=1;                       
		$smtp->localhost="localhost";       
		$smtp->direct_delivery=0;           
		$smtp->timeout=60;                  
		$smtp->data_timeout=0;              
											  
		$smtp->debug=0;                     
		$smtp->html_debug=0;                
		$smtp->pop3_auth_host="";           
		$smtp->user=SMTP_USER;	
		$smtp->realm=SMTP_REALM;
		$smtp->password=SMTP_PASSWORD;
		$smtp->workstation="";              
		$smtp->authentication_mechanism="LOGIN"; 
		if($smtp->direct_delivery){
			if(!function_exists("GetMXRR")){
				include(PAGEURL."sendmail/getmxrr.php");			
				$_NAMESERVERS=array();
			}
		}
		if($smtp->SendMessage($from,array($tomailid),array("From: $from","To:" .$tomailid,"Subject: $subject","Date: ".strftime("%a, %d %b %Y %H:%M:%S %Z"),"Content-type:text/html;\r\n"),$msg)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function sendsmtpmail($valuearray){
		require_once(PAGEURL.'/Classes/phpmailer/PHPMailerAutoload.php');
		$mail = new PHPMailer(true);
		$mail->isSMTP();
		$mail->Host = 'smtp.office365.com';
		$mail->Port       = 587;
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth   = true;
		$mail->Username = 'pulse@enventure.com';
		$mail->Password = 'Env@12345';
		$mail->SetFrom(FROM_EMAILID, 'Pulse');	
		if(count($valuearray['to']) > 0){
			foreach($valuearray['to'] as $email => $name){
			   $mail->addAddress($email, $name);
			}			
		}
		// if(count($valuearray['cc']) > 0){
			// foreach($valuearray['cc'] as $email => $name){
			   // $mail->addCC($email, $name);
			// }			
		// }		
		//$mail->SMTPDebug  = 3;
		//$mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";}; //$mail->Debugoutput = 'echo';
		$mail->IsHTML(true);

		$mail->Subject = $valuearray['subject'];
		$mail->Body    = $valuearray['body'];

		if(!$mail->send()) {
			//echo 'Message could not be sent.';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
			return false;
		} else {
			//echo 'Message has been sent';
			return true;
		}	
	}

	public function check_in_range($start_date, $end_date, $date_from_user){
	  $start_ts = strtotime($start_date);
	  $end_ts = strtotime($end_date);
	  $user_ts = strtotime($date_from_user);
	  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	}
	
	public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	public function timediff($startdatetime,$enddatetime,$string = false){
		$date1 = strtotime($enddatetime);  
		$date2 = strtotime($startdatetime);  
		 
		$diff = abs($date2 - $date1);  

		$years = floor($diff / (365*60*60*24));  

		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
		 
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
		 
		$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
		  
		$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);  

		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60)); 


  
		if(strlen($days)==1){
			$days = "0".$days;
		}
		if(strlen($hours)==1){
			$hours = "0".$hours;
		}
		if(strlen($minutes)==1){
			$minutes = "0".$minutes;
		}
		if(strlen($seconds)==1){
			$seconds = "0".$seconds;
		}	
		$dataarr['day'] = $days;
		if($days > 0){
			$dataarr['hrs'] = (($days*24)+$hours);
		}
		else{
			$dataarr['hrs'] = $hours;
		}
		$dataarr['min'] = $minutes;
		$dataarr['sec'] = $seconds;
		$data = '';
		if($days > 0){
			$data .= $days." Days ";
		}
		if($hours > 0){
			$data .= $hours." Hrs ";
		}
		if($minutes > 0){
			$data .= $minutes." Min ";
		}
		else{
			$data .= "00 Min ";
		}
		if($seconds > 0){
			$data .= $seconds." Sec ";
		}
		else{
			$data .= "00 Sec ";
		}
		if($string){
			return $data;
		}
		else{
			return $dataarr;
		}
	}
	
	public function secToTimeFormat($seconds,$withseconds = false) {
	  $hours = floor($seconds / 3600);
	  if(strlen($hours)=='1'){
		  $hours = "0".$hours;
	  }     
	  $minutes = floor(($seconds / 60) % 60);
	  if(strlen($minutes)=='1'){
		  $minutes = "0".$minutes;
	  }  
	  $seconds = $seconds % 60;
	  if(strlen($seconds)=='1'){
		  $seconds = "0".$seconds;
	  }
	  if($withseconds){
		  return "$hours:$minutes:$seconds";
	  }
	  else{
		  return "$hours:$minutes";
	  }
	}	
	
	public function addTime($times,$returnmins=false) {
		$minutes = 0;
		foreach ($times as $time) {
			$minutes += $this->getminutes($time);
		}
		if($returnmins){
			return $minutes;
		}
		else{
			return $this->displayTimeFormat($minutes);
		}
	}	
	
	public function displayTimeFormat($minutes){
			$hours = floor($minutes / 60);
			$minutes -= $hours * 60;
			return sprintf('%02d:%02d', $hours, $minutes);			
	}
	
	public function getminutes($time,$issec= false){
		$min = 0;
		list($hour, $minute) = explode(':', $time);
		$min += $hour * 60;
		$min += $minute;
		if($issec){
			$min = $this->getseconds($min);
		}
		return $min;
	}
	public function getseconds($min){
		$sec = 0;
		$sec = $min * 60;
		return $sec;
	}
	
	public function timetodisplay($time){
		$minutes = $this->getminutes($time);
		return $this->displayTimeFormat($minutes);
	}
	
	public function getWorkingDays($startDate,$endDate,$holidays){
		// do strtotime calculations just once
		$endDate = strtotime($endDate);
		$startDate = strtotime($startDate);


		//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
		//We add one to inlude both dates in the interval.
		$days = ($endDate - $startDate) / 86400 + 1;

		$no_full_weeks = floor($days / 7);
		$no_remaining_days = fmod($days, 7);

		//It will return 1 if it's Monday,.. ,7 for Sunday
		$the_first_day_of_week = date("N", $startDate);
		$the_last_day_of_week = date("N", $endDate);

		//---->The two can be equal in leap years when february has 29 days, the equal sign is added here
		//In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
		if ($the_first_day_of_week <= $the_last_day_of_week) {
			if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
			if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
		}
		else {
			// (edit by Tokes to fix an edge case where the start day was a Sunday
			// and the end day was NOT a Saturday)

			// the day of the week for start is later than the day of the week for end
			if ($the_first_day_of_week == 7) {
				// if the start date is a Sunday, then we definitely subtract 1 day
				$no_remaining_days--;

				if ($the_last_day_of_week == 6) {
					// if the end date is a Saturday, then we subtract another day
					$no_remaining_days--;
				}
			}
			else {
				// the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
				// so we skip an entire weekend and subtract 2 days
				$no_remaining_days -= 2;
			}
		}

		//The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
	//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
	   $workingDays = $no_full_weeks * 5;
		if ($no_remaining_days > 0 )
		{
		  $workingDays += $no_remaining_days;
		}

		//We subtract the holidays
		foreach($holidays as $holiday){
			$time_stamp=strtotime($holiday);
			//If the holiday doesn't fall in weekend
			if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
				$workingDays--;
		}

		return $workingDays;
	}	
	public function getSecondsFromHMS($time) {
			$timeArr = array_reverse(explode(":", $time));    
			$seconds = 0;
			foreach ($timeArr as $key => $value) {
				if ($key > 2)
					break;
				$seconds += pow(60, $key) * $value;
			}
			return $seconds;
		}
}
?>

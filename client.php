<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$department_id = $_POST['department_id'];
		$subdepartment_id = $_POST['subdepartment_id'];
		$name = $_POST['name'];
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM clientlist WHERE clientname='".trim($name)."'".$idcond;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: client.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
			$clientquery = "UPDATE clientlist SET `clientname` = '".$name."',`department_id`='".$department_id."',`subdepartment_id`='".$subdepartment_id."' WHERE `id` ='".$editid."'";	
			}
			else{
			$clientquery = "INSERT INTO clientlist(`department_id`,`subdepartment_id`,`clientname`) VALUES ( '".$department_id."','".$subdepartment_id."','".$name."');";			
			}
			 $dbase->executeNonQuery($clientquery);
			 if($editid !=''){
				header('Location: client.php?m=2');
				exit();	
			 }
			 else{
				header('Location: client.php?m=1');
				exit();				 
			 }			
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$clientquery = "UPDATE clientlist SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($clientquery);
				header('Location: client.php?m=4');
				exit();				
		}
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		else{
			$label = "Add";
			$button = "Add";
		}
		$client_query = "SELECT * FROM clientlist WHERE id='".trim($id)."'";
		$client_result = $dbase->executeQuery($client_query,"single");		
	}
	else{
		$label = "Add";
		$button = "Add";
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		table#clienttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#clienttable td, table#clienttable th {
			border: 1px solid black;
		}	
		/*table#clientlisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#clientlisttable td, table#clientlisttable th {
			border: 1px solid black;
			padding:5px;
		}	*/	
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php //include("adminmenu.php"); ?>
<?php include("userrolemenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_client" action="" method="POST" enctype="multipart/form-data">
<table id="clienttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Inserted Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Client Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='4'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Client Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> Client</b></td></tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="department_id" name="department_id" class="required" onchange="getsubdepartment();">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																								   
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
			if($deptResult[$i]['id']==$client_result['department_id']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="subdepartment_id" name="subdepartment_id" class="required">
	<option value="">-Select-</option>
	</select>
	</td>
</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client Name</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="name" id="name" value="<?php echo $client_result['clientname']; ?>" class="required alphanumeric" /></td>
</tr>
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
			<input type="hidden" name="subdept" id="subdept" value="<?php echo $client_result['subdepartment_id']; ?>" />
		<?php } else {?>
		<input type="hidden" name="subdept" id="subdept" value="" />
		<?php } ?>
		<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
<td align="center" width="80%" valign="top">
	<!--<div style="width: auto; min-height:250px; overflow-y:scroll;height:250px;"></div>-->
	<table id="clientlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="25%" align="center"><b>Department</b></td>
			<td width="25%" align="center"><b>Sub Department</b></td>
			<td width="25%" align="center"><b>Client Name</b></td>
			<td width="25%" align="center"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND department_id='".$_SESSION['timesheet']['DEPART']."' AND subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$clientQuery = "SELECT c.id,c.clientname,(SELECT d.name FROM department d WHERE d.id=c.department_id) as deptname,(SELECT s.subname FROM subdepartment s WHERE s.id=c.subdepartment_id) as subdeptname FROM clientlist c WHERE c.isActive='1'".$depart_cond;
		$clientResult = $dbase->executeQuery($clientQuery,"multiple");
		for($i=0;$i<count($clientResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $clientResult[$i]['clientname'];?></td>
			<td align="left" style="padding-left:10px"><a href="client.php?id=<?php echo $clientResult[$i]['id'];?>">Edit</a> | <a href="client.php?del=1&id=<?php echo $clientResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th>Department</th>
				<th>Sub Department</th>
				<th>Client Name</th>
                <th>Action</th>
            </tr>
        </tfoot>			
	</table>
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
	  $("#frm_client").validate();	
    // Setup - add a text input to each footer cell
    $('#clientlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#clientlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
<?php if($id!=''){ ?>
getsubdepartment();
<?php } ?>	
	autoselectoption("#department_id");
});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = $("#subdept").val();
	$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
	});
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
</script>
</html>
<?php } ?>

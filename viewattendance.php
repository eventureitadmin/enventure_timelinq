<?php
include_once("config.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getquery($date,$empcsv,$usrcond){
		if($empcsv != ''){
			$emp_cond = " AND tl.emp_id IN (".$empcsv.")";
		}
		$query = "SELECT IFNULL( DATE_FORMAT( tl.`login_time` , '%d-%b-%Y %h:%i:%s %p' ) , 0 ) AS login_time, tl.`login_comments` , tl.`login_ip` , IFNULL( DATE_FORMAT( tl.`logout_time` , '%d-%b-%Y %h:%i:%s %p' ) , 0 ) AS logout_time, tl.`logout_comments` , tl.`logout_ip`, tl.`totalhours`, (SELECT e.emp_name FROM employeelist e WHERE e.ID = tl.emp_id) AS emp_name, (SELECT e.emp_username FROM employeelist e WHERE e.ID = tl.emp_id) AS emp_code FROM `time_log` tl WHERE tl.`log_date` = '".$date."' ".$emp_cond." ".$usrcond." ORDER BY tl.ID ASC";
		return $query;
	}
	
	function getohrmquery($date,$empcsv,$usrcond){
		if($empcsv != ''){
			$emp_cond = " AND tl.emp_id IN (".$empcsv.")";
		}
		$query = "SELECT IFNULL( DATE_FORMAT( tl.`log_date` , '%e-%b-%y' ) , 0 ) AS log_date,IFNULL( DATE_FORMAT( tl.`login_time` , '%H:%i' ) , 0 ) AS login_time, IFNULL( DATE_FORMAT( tl.`logout_time` , '%H:%i' ), 0 ) AS logout_time, tl.`totalhours`,(SELECT e.emp_username FROM employeelist e WHERE e.ID = tl.emp_id) AS emp_code FROM `time_log` tl WHERE tl.`log_date` = '".$date."' ".$emp_cond." ".$usrcond." ORDER BY tl.ID ASC";
		return $query;
	}	
	if($_POST){
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$empid = $_POST['empid'];
	}
		$cond = '';
		$usrcond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$user_cond = "";
			$user_cond = " AND department_ids='".$_SESSION['timesheet']['DEPART']."' AND subdepartment_ids IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
			$userQuery = "SELECT e.`ID` FROM employeelist e WHERE e.isActive='1' AND e.isadmin='0'".$user_cond;
			$userResult = $dbase->executeQuery($userQuery,"multiple");
			for($i=0;$i<count($userResult);$i++){
				$cond .= $userResult[$i]['ID'].",";
			}
			$cond = substr($cond,0,-1);
		}
		if($_SESSION['timesheet']['IS_TEAMIDS']=='1'){
			$cond = $_SESSION['timesheet']['TEAMIDS_CSV'];
		}
	
	if($empid != ''){
		$usrcond = " AND tl.emp_id='".$empid."'";
	}
if($_GET['fd'] != '' && $_GET['td'] != '' && $_GET['rt']=='1'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  =>12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

	$fdate = $_GET['fd'];
	$tdate = $_GET['td'];
	$empid = $_GET['eid'];
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle($dbase->dateFormatToDisplay($fdate)."-".$dbase->dateFormatToDisplay($tdate));	
$datelist = $dbase->getDateLists($fdate,$tdate);
if(count($datelist)>0){
	$row = 1;
for($j=0;$j<count($datelist);$j++){
		$col11 = "A{$row}";
		$col22 = "J{$row}";
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Report for the Date '.$dbase->dateFormatToDisplay($datelist[$j]));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Employee Code');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Employee Name');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Login Time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('Login Comments');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Login IP');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Logout Time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('Logout Comments');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('Logout IP');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue('Total Hours');
		$row++;
	if($empid != ''){
		$usrcond = " AND tl.emp_id='".$empid."'";
	}	
$query = getquery($datelist[$j],$cond,$usrcond);
$report = $dbase->executeQuery($query,'multiple');
for($i=0;$i<count($report);$i++){
		$totaltime = '';
		$timearr = explode(":",$report[$i]['totalhours']);
		if($timearr[0]=='00' && $timearr[1]=='00' && $timearr[2]=='00'){
			$totaltime = '';
		}
		else{
			//$totaltime = $timearr[0]." Hrs ".$timearr[1]." Min ".$timearr[2]." Sec";
			$totaltime = $timearr[0].":".$timearr[1];
		}
		/*$intime=$report[$i]['login_time'];
		$outtime=$report[$i]['logout_time'];
		if($report[$i]['login_time']!='0' && $report[$i]['logout_time']!='0'){
			$totaltime=(strtotime($report[$i]['logout_time'])-strtotime($report[$i]['login_time']));
		}
		else{
			$totaltime='';
		}
		*/
		$logouttime = '';
		if($report[$i]['logout_time'] != '0'){
			$logouttime = substr($report[$i]['logout_time'],11);
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($report[$i]['emp_code']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($report[$i]['emp_name']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue(substr($report[$i]['login_time'],11));
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($report[$i]['login_comments']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($report[$i]['login_ip']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($logouttime);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($report[$i]['logout_comments']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue($report[$i]['logout_ip']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($valueStyleArray);
		//$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue(round($totaltime/3600,2)." Hrs ");
	    $objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue($totaltime);
		$row++;
}		
$row++;
}
}
	
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="export.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}

if($_GET['fd'] != '' && $_GET['td'] != '' && $_GET['rt']=='2'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  =>12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

	$fdate = $_GET['fd'];
	$tdate = $_GET['td'];
	$empid = $_GET['eid'];
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle($dbase->dateFormatToDisplay($fdate)."-".$dbase->dateFormatToDisplay($tdate));	
$datelist = $dbase->getDateLists($fdate,$tdate);
if(count($datelist)>0){
	$row = 1;
for($j=0;$j<count($datelist);$j++){
	if($j==0){
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Date');	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Employee Code');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Employee Name');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('Login Time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Login Comments');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Logout Time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('Logout Comments');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('Total Hours');
		$row++;
	}
		if($empid != ''){
		$usrcond = " AND tl.emp_id='".$empid."'";
	}
$query = getquery($datelist[$j],$cond,$usrcond);
$report = $dbase->executeQuery($query,'multiple');
for($i=0;$i<count($report);$i++){
		$totaltime = '';
		$timearr = explode(":",$report[$i]['totalhours']);
		if($timearr[0]=='00' && $timearr[1]=='00' && $timearr[2]=='00'){
			$totaltime = '';
		}
		else{
			$totaltime = $timearr[0].":".$timearr[1];
		}

		$logouttime = '';
		if($report[$i]['logout_time'] != '0'){
			$logouttime = substr($report[$i]['logout_time'],11);
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($dbase->dateFormatToDisplay($datelist[$j]));		
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($report[$i]['emp_code']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue($report[$i]['emp_name']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue(substr($report[$i]['login_time'],11));
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($report[$i]['login_comments']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($logouttime);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($report[$i]['logout_comments']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($valueStyleArray);
	    $objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue($totaltime);
		$row++;
}		
//$row++;
}
}
	
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="export.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}	

if($_GET['fd'] != '' && $_GET['td'] != '' && $_GET['rt']=='3'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  =>10,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

	$fdate = $_GET['fd'];
	$tdate = $_GET['td'];
	$empid = $_GET['eid'];
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle($dbase->dateFormatToDisplay($fdate)."-".$dbase->dateFormatToDisplay($tdate));	
$datelist = $dbase->getDateLists($fdate,$tdate);
if(count($datelist)>0){
	$row = 1;
for($j=0;$j<count($datelist);$j++){
	if($j==0){
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Employee ID');	
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Empname');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Department');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('Date');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('In time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Out time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('Shift');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('Total Duration');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue('Status');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('J'.$row)->setValue('Remarks');		
		$row++;
	}
	if($empid != ''){
		$usrcond = " AND tl.emp_id='".$empid."'";
	}
$query = getohrmquery($datelist[$j],$cond,$usrcond);
$report = $dbase->executeQuery($query,'multiple');
for($i=0;$i<count($report);$i++){
	if($report[$i]['emp_code']=='4186'){
		$empcodedisplay = '4185';
	}
	else{
		$empcodedisplay=$report[$i]['emp_code'];
	}
	$empcode=$report[$i]['emp_code'];
	// get emp dept
	$dept_qry = "SELECT m.*,(SELECT name  FROM budget.`ohrm_job_category` WHERE `id` = m.eeo_cat_code) AS emp_dept FROM (SELECT `eeo_cat_code`,CONCAT( `emp_firstname` , ' ', `emp_middle_name`,' ', `emp_lastname`  ) emp_name FROM budget.`hs_hr_employee` WHERE `emp_number`='".$empcode."') m";
	$dept_res = $dbase->executeQuery($dept_qry,'single');
	$emp_name = $dept_res['emp_name'];
	$emp_dept = $dept_res['emp_dept'];
		$status = "";
		$totaltime = '';
		$timearr = explode(":",$report[$i]['totalhours']);
		if($timearr[0]=='00' && $timearr[1]=='00' && $timearr[2]=='00'){
			$totaltime = '';
		}
		else{
			$totaltime = $timearr[0].":".$timearr[1];
			$status = "Present";
		}

		$logouttime = '';
		
		if($totaltime != ''){
			$logouttime = $report[$i]['logout_time'];
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($empcodedisplay);		
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($emp_name);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue($emp_dept);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($report[$i]['log_date']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($report[$i]['login_time']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($logouttime);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue("FS");
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($valueStyleArray);
	    $objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue($totaltime);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($valueStyleArray);
	    $objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue($status);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($valueStyleArray);
	    $objPHPExcel->getActiveSheet()->getCell('J'.$row)->setValue('');		
		$row++;
}		
//$row++;
}
}
	
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="OHRM_attendance_format.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}		
	  </style>
	   <?php include_once("includebootstrap.php"); ?>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE || $_SESSION['timesheet']['IS_TEAMIDS']=='1' || $_SESSION['timesheet']['ROLEID']== HR_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>

<td width="12%" ><b>From Date</b></td>
<td width="12%" ><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('d-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="12%" ><b>To Date</b></td>
<td width="12%" ><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="12%" ><b>Select Employee</b></td>
<td width="12%" ><select id="empid" name="empid" class="chosen-select" style="width:300px;" >
		<option value="">-All-</option>
		</select></td>
<td width="12%" ><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" /></td>
<td width="12%" ><a href="viewattendance.php?fd=<?php echo $fromdate ;?>&td=<?php echo $todate ;?>&eid=<?php echo $empid;?>&rt=1" style="cursor: pointer;"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a> <a href="viewattendance.php?fd=<?php echo $fromdate ;?>&td=<?php echo $todate ;?>&eid=<?php echo $empid;?>&rt=2" style="cursor: pointer;"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a><a href="viewattendance.php?fd=<?php echo $fromdate ;?>&td=<?php echo $todate ;?>&eid=<?php echo $empid;?>&rt=3" style="cursor: pointer;"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a></td>
</tr>
</table>
</form>
</br>
<?php
if($fromdate != '' && $todate != ''){
$datelist = $dbase->getDateLists($fromdate,$todate);
if(count($datelist)>0){
for($j=0;$j<count($datelist);$j++){
 ?>
<table id="reporttable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
	<td width="22%" colspan="10"><b>Report for the Date <?php echo $dbase->dateFormatToDisplay($datelist[$j]); ?></b></td>
</tr>
<tr>
	<td width="2%" align="center"><b>SNo</b></td>
	<td width="11%" align="center"><b>Employee Name</b></td>
	<td width="9%" align="center"><b>Login Time</b></td>
	<td width="14%" align="center"><b>Login Comments</b></td>
	<td width="7%" align="center"><b>Login IP</b></td>
	<td width="9%" align="center"><b>Logout Time</b></td>
	<td width="14%" align="center"><b>Logout Comments</b></td>
	<td width="7%" align="center"><b>Logout IP</b></td>
	<td width="7%" align="center"><b>Total Hours</b></td>
</tr>
<?php
$query = getquery($datelist[$j],$cond,$usrcond);
$report = $dbase->executeQuery($query,'multiple');
for($i=0;$i<count($report);$i++){
		$totaltime = '';
		$timearr = explode(":",$report[$i]['totalhours']);
		if($timearr[0]=='00' && $timearr[1]=='00' && $timearr[2]=='00'){
			$totaltime = '';
		}
		else{
			//$totaltime = $timearr[0]." Hrs ".$timearr[1]." Min ".$timearr[2]." Sec";
			$totaltime = $timearr[0].":".$timearr[1];
		}
	/*$intime=$report[$i]['login_time'];
		$outtime=$report[$i]['logout_time'];
		if($report[$i]['login_time']!='0' && $report[$i]['logout_time']!='0'){
			$totaltime=(strtotime($report[$i]['logout_time'])-strtotime($report[$i]['login_time']));
		}
		else{
			$totaltime='f';
		}*/
?>
<tr>
	<td width="2%" valign="top"><?php echo $i+1; ?></td>
	<td width="11%" valign="top"><?php if($report[$i]['emp_name']!=''){echo substr($report[$i]['emp_name'],0,20);} ?></td>
	<td width="7%" valign="top"><?php if($report[$i]['login_time']!='0'){echo substr($report[$i]['login_time'],11);} ?></td>
	<td width="14%" valign="top"><?php if($report[$i]['login_comments']!=''){echo $report[$i]['login_comments'];} ?></td>
	<td width="7%" valign="top"><?php if($report[$i]['login_ip']!=''){echo $report[$i]['login_ip'];} ?></td>
	<td width="7%" valign="top"><?php if($report[$i]['logout_time']!='0'){echo substr($report[$i]['logout_time'],11);} ?></td>
	<td width="14%" valign="top"><?php if($report[$i]['logout_comments']!=''){echo $report[$i]['logout_comments'];} ?></td>
	<td width="7%" valign="top"><?php if($report[$i]['logout_ip']!=''){echo $report[$i]['logout_ip'];} ?></td>
	<!--<td width="7%" valign="top"><?php //if($totaltime!=''){//echo round($totaltime/3600,2)." Hrs ";} ?></td>-->
	<td width="9%" valign="top"><?php if($totaltime!=''){echo $totaltime;} ?></td>
</tr>
<?php 
}
?>
</table>
<br/>
<?php 
}
}
}
?>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
	 $(".chosen-select").chosen();
	 getemployeelist();	 
 $('#from_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});					
	});	
function submitform(){
	$("#frm_details").submit();
}	
	function getemployeelist(){
	$.get("getemplist.php?t=1",function(data){
		$("#empid").html(data);
		$("#empid").trigger("chosen:updated");
	});	
}
</script>
</html>
<?php } ?>

<?php
include_once("config.php");
include_once("functions.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{	
	if($_POST){
		//print_r($_POST);exit;
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$deptid = $_POST['department_id'];
		$subdeptid = $_POST['subdepartment_id'];
		$pirmaster_id = $_POST['pirmaster_id'];
		$project_id = $_POST['project_id'];
		$empid = $_POST['empid'];
		$hrstype = $_POST['hrstype'];
		$excelreport = $_POST['excelreport'];
		$subdepartment_ids = implode (",", $subdeptid);

		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$dedicatemonth=date('m',strtotime($to_date));
		$dedicatemyear=date('Y',strtotime($to_date));		
		$cond = '';
		$procond=''; 
		$pircond='';
		$repcond='';
		if($hrstype=='1'){
			$selcol = "actualhours";
		}
		else{
			$selcol = "calculatedhours";
		}
		if($project_id != ''){
			$procond = " AND projectname='".$project_id."'";
		}		
		if($pirmaster_id != '' && $project_id != ''){
			$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'".$procond;
			$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
			$pirmasterid=$pirmasterdet['id'];			
			$repcond = "AND  `pirmaster_id`='".$pirmasterid."'";
		}
		elseif($pirmaster_id != ''){
			$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
			$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
			if($pirmastercntdet['pircnt']==1){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasterid=$pirmasterdet['id'];			
				$repcond = " AND  `pirmaster_id`='".$pirmasterid."'";				
			}
			else{
				$pirmaster_select = "SELECT GROUP_CONCAT(id) as pircsv FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasteridcsv = $pirmasterdet['pircsv'];
				$repcond = " AND  `pirmaster_id` IN (".$pirmasteridcsv.") `";
			}
		}		
		else{
			$repcond = "";
		}		
		
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$subdeptcond = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
			$repcond = "AND `pirmaster_id` IN (SELECT id FROM pirmaster WHERE department_id='".$deptid."' AND subdepartment_id IN (".$subdepartment_ids."))";
		}
		$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
		$dedicatedempcond = '';
		if($deptid != ''){
			$dedicatedempcond .= " AND department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$dedicatedempcond .= " AND subdepartment_id IN (".$subdepartment_ids.")";
		}
		if($pirmaster_id != ''){
			$dedicatedempcond .= " AND id='".$pirmaster_id."'";
		}
		if($pirmaster_id != '' && $project_id != ''){
			$dedicatedempcond1= " AND pm.id='".$pirmasterid."'";
		}
		elseif($pirmaster_id != ''){
			$dedicatedempcond1= " AND pm.pirno='".$pirmaster_id."'";
		}
		$subcond ='';
		if($subdepartment_ids != ''){
			$subcond = " AND subdepartment_id IN (".$subdepartment_ids.")";
		}		
	
		$query="SELECT IFNULL(SUM(rm.`no_of_resource`),0) AS dedicatedresource FROM `pirmaster` pm,`resourcemonth` rm WHERE rm.`isactive`='1' AND pm.isActive AND rm.`pirmaster_id`=pm.id AND rm.`monthval`='".$dedicatemonth."' AND rm.`yearval`='".$dedicatemyear."' AND pm.department_id='".$deptid."'".$subcond.$dedicatedempcond1;
		$result = $dbase->executeQuery($query,'single');
		$totempcnt = $result['dedicatedresource'];
		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){
			unset($reportdata);
		}
		
if($excelreport=='1'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  =>10,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

	function nextLetter(&$str) {
	 $str = ('z' === $str ? 'a' : ++$str);
	 return $str;
	}
	
	$reportname = 'Userwise Report';	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle($from_date." - ".$to_date);	
	
	$row = 1;
		$col1 = "A";
		$col11 = "{$col1}{$row}";
		$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell($col11)->setValue('Employee Name / Date');
		$col1 = nextLetter($col1);		
		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){		
			for($j=0;$j<count($datelist);$j++){
				$col11 = "{$col1}{$row}";
				$col2 = nextLetter($col1);
				$col2 = nextLetter($col2);
				$col2 = nextLetter($col2);
				$col12 = "{$col2}{$row}";
				$concat =  $col11.":".$col12;
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue(date('d-M-y',strtotime($datelist[$j])));
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
				$col1 = nextLetter($col2);
			}
				$col11 = "{$col1}{$row}";
				$col2 = nextLetter($col1);
				$col2 = nextLetter($col2);
				$col2 = nextLetter($col2);
				$col12 = "{$col2}{$row}";
				$concat =  $col11.":".$col12;
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue("Total");
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
				$col1 = nextLetter($col2);
		$row++;	
		$col1 = "B";
			for($j=0;$j<(count($datelist)+1);$j++){
				$col11 = "{$col1}{$row}";
				$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue("Non Dedicated");
				$col1 = nextLetter($col1);	
				$col11 = "{$col1}{$row}";
				$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue("Dedicated");
				$col1 = nextLetter($col1);
				$col11 = "{$col1}{$row}";
				$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue("Internal PIR");
				$col1 = nextLetter($col1);
				$col11 = "{$col1}{$row}";
				$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue("Day Total");
				$col1 = nextLetter($col1);					
			}
		$empcond1 = '';
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
		if($_SESSION['timesheet']['DEPART'] != ''){
			$empcond1 .= " AND department_ids ='".$_SESSION['timesheet']['DEPART']."'";
		}
		
		if($_SESSION['timesheet']['SUBDEPART_CSV'] != ''){
			if(strlen($_SESSION['timesheet']['SUBDEPART_CSV'])=='1'){
				$empcond1 .= " AND (subdepartment_ids IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].") OR FIND_IN_SET(".$_SESSION['timesheet']['SUBDEPART_CSV'].",subdepartment_ids))";
			}
			else{
				$empcond1 .= " AND subdepartment_ids IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
			}
		}
		if($_SESSION['timesheet']['IS_TEAMIDS']=='1'){
			$empcond1 .= "AND ID IN (".$_SESSION['timesheet']['TEAMIDS_CSV'].")";
		}				
		}
		if($empid > 0 ){
			$empcond1 .= " AND ID='".$empid."'";
		}
		$emplist = adminemplist($empcond1);
		$row++;	
		$col1 = "A";
		$grdndhrsarr = array();
		$grddhrsarr = array();
		$grdiphrsarr = array();
		$grddayarr = array();		
			for($k=0;$k<count($emplist);$k++){
				$totndhrsarr = array();
				$totdhrsarr = array();
				$totiphrsarr = array();
				$totdayarr = array();				
				$col11 = "{$col1}{$row}";
				$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
				$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($emplist[$k]['emp_name']);
				$col1 = nextLetter($col1);
				for($t=0;$t<count($datelist);$t++){
				$dayhrsquery = "SELECT SEC_TO_TIME(m.ndhrs) AS ndhrs,SEC_TO_TIME(m.dhrs) AS dhrs,SEC_TO_TIME(m.iphrs) AS iphrs FROM (SELECT 
	(SELECT IFNULL(SUM(TIME_TO_SEC(".$selcol.")),0) FROM `timeentry` WHERE `isActive`='1' AND `employee_id`='".$emplist[$k]['ID']."' AND `entrydate`='".$datelist[$t]."' AND `is_dt`='0' AND `is_internalpir`='0' AND `is_rework`='0' ".$repcond.") AS ndhrs, 
	(SELECT IFNULL(SUM(TIME_TO_SEC(".$selcol.")),0) FROM `timeentry` WHERE `isActive`='1' AND `employee_id`='".$emplist[$k]['ID']."' AND `entrydate`='".$datelist[$t]."' AND `is_dt`='1'  AND `is_internalpir`='0' AND `is_rework`='0' ".$repcond.") AS dhrs, 
	(SELECT IFNULL(SUM(TIME_TO_SEC(".$selcol.")),0) FROM `timeentry` WHERE `isActive`='1' AND `employee_id`='".$emplist[$k]['ID']."' AND `entrydate`='".$datelist[$t]."' AND (`is_internalpir`='1' OR `is_rework`='1')".$repcond.") AS iphrs) m";
				$dayhrsresult = $dbase->executeQuery($dayhrsquery,'single');
				$daytotarr = array();
				$ndhrs = substr($dayhrsresult['ndhrs'],0,-3);
				$totndhrsarr[] = $ndhrs;
				$dhrs = substr($dayhrsresult['dhrs'],0,-3);
				$totdhrsarr[] = $dhrs;
				$iphrs = substr($dayhrsresult['iphrs'],0,-3);
				$totiphrsarr[] = $iphrs;
				$daytotarr[] = $ndhrs;
				$daytotarr[] = $dhrs;
				$daytotarr[] = $iphrs;
				if($ndhrs == '00:00'){
					$ndhrs = "";
				}
				if($dhrs == '00:00'){
					$dhrs = "";
				}
				if($iphrs == '00:00'){
					$iphrs = "";
				}
				$daytot = $dbase->addTime($daytotarr);
				$totdayarr[] = $daytot;
				if($daytot=='00:00'){
					$daytot = "";
				}
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($ndhrs);
					$col1 = nextLetter($col1);					
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dhrs);
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($iphrs);
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($daytot);
					$col1 = nextLetter($col1);					
				}
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($totndhrsarr));
					$col1 = nextLetter($col1);					
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($totdhrsarr));
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($totiphrsarr));
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($totdayarr));
					$col1 = nextLetter($col1);
		$grdndhrsarr[] = $dbase->addTime($totndhrsarr);
		$grddhrsarr[] =$dbase->addTime($totdhrsarr);
		$grdiphrsarr[] = $dbase->addTime($totiphrsarr);
		$grddayarr[] = $dbase->addTime($totdayarr);					
				$row++;
				$col1 = "A";
			}
					$col1 = "DQ";
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue("Total");
					$col1 = nextLetter($col1);					
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($grdndhrsarr));
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($grddhrsarr));
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($grdiphrsarr));
					$col1 = nextLetter($col1);
					$col11 = "{$col1}{$row}";
					$objPHPExcel->getActiveSheet()->getColumnDimension($col1)->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->applyFromArray($headingStyleArray);
					$objPHPExcel->getActiveSheet()->getStyle($col11)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getCell($col11)->setValue($dbase->addTime($grddayarr));
					$col1 = nextLetter($col1);					
					
		}

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="userdatewise_'.date('Y-m-d').'.xls"'); 
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');	
		exit;
	}		
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chart2_8.js" type="text/javascript"></script>
	   <script src="js/utils.js" type="text/javascript"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:14px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		
		table#reportdetailtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reportdetailtable td, table#reportdetailtable th {
			border: 1px solid white;
			 padding: 5px; 
		}	
		table#kpi1 {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#kpi1 td, table#kpi1 th {
			border: 1px solid black;
			 padding: 5px; 
		}			  
fieldset {
      overflow: hidden
    }
    
    .some-class {
      float: left;
      clear: none;
    }
    
    label {
      float: left;
      clear: none;
      display: block;
      padding: 0px 1em 0px 8px;
    }
    
    input[type=radio],
    input.radio {
      float: left;
      clear: none;
      margin: 2px 0 0 2px;
    }		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="1" align="center"  width="100%" >
<tr>
<td width="100%" colspan="4" ><b>PIR Userwise</b></td>
</tr>
<tr>
<td width="25%" ><b>From Date</b><br/><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('01-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="25%" ><b>To Date</b><br/><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="25%" ><b>Select Department</b><br/><select id="department_id" name="department_id" class="required"  onchange="getsubdepartment();">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																									  
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		$select = "";
		for($i=0;$i<count($deptResult);$i++){
			echo '<option value="'.$deptResult[$i]['id'].'">'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select></td>
<td width="25%" ><b>Select Sub Department</b><br/><select id="subdepartment_id" name="subdepartment_id[]" data-placeholder="Select Sub Department" class="required chosen-select-multi" multiple style="width:180px;">
	</select>	
	<label for="subdepartment_id" class="error" style="display:none" >This field is required.</label>
</td>
</tr>
<tr>
<td width="25%" ><b>Select PIR</b><br/><select id="pirmaster_id" name="pirmaster_id" class="chosen-select" style="width:300px;" onchange="getproject();">
	<option value="">-All-</option>
	</select></td>
<td width="25%" ><b>Select Project</b><br/><select id="project_id" name="project_id">
	<option value="">-All-</option>
	</select></td>
<td width="12%" ><b>Select User</b><br/><select id="empid" name="empid" class="chosen-select" style="width:250px;" >
<option value="">-Select-</option>
</select></td>
<td width="25%" >
<fieldset>
      <div class="some-class">
        <input type="radio" class="radio" name="hrstype" value="1" />
        <label for="y">Actual Hrs</label>
        <input type="radio" class="radio" name="hrstype" value="2" checked />
        <label for="z">Budget Hrs</label>
      </div>
    </fieldset>
<input type="hidden" id="excelreport" name="excelreport"  /><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" />
<!--<a href="javascript:void(0);" style="cursor: pointer;" onclick="getexcel()"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a>-->
</td>
</tr>
	<tr>
		<td colspan="4"><td>
	</tr>
</table>
</form>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
	 $(".chosen-select").chosen();
	 getemployeelist();	
	 <?php if($deptid != ''){?>
		$('#department_id').val(<?php echo $deptid; ?>).change();
	 <?php } ?>
 $('#from_date').datepicker({
	inline: true,
	dateFormat: 'dd-M-yy',
	maxDate:0,
	changeMonth: true,
	changeYear: true,
	yearRange: "-10:+0",
	onSelect: function(date){
		var dates = date.split('-');
		var month1 = dates[1].toLowerCase();
		var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
		month1 = months.indexOf(month1);
		var lastDate = new Date(dates[2], month1+1, 0);
		var y = lastDate.getFullYear(), m = lastDate.getMonth(), d = lastDate.getDate();
		m = (''+ (m+1)).slice(-2);
		var monname = months[m-1];
		var monthname = monname.charAt(0).toUpperCase() + monname.slice(1);
		$('#to_date').val(d+'-'+monthname+'-'+y);
	}	 
});
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});
			getsubdepartment();
			//getpirdropdown();
			
			$("#excelreport").val('0');
	 $(".chosen-select").chosen();
	 $(".chosen-select-multi").chosen();
	 //autoselectoption("#department_id"); 
		$('.chosen-select-multi').change(function () {
			if($(".chosen-select-multi option:selected").val()==""){
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").attr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			else{
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").removeAttr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			getpirdropdown();
		});	 
	});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = '<?php echo $subdepartment_ids;?>';
	$.get("getsubdepartmentadmin.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
		if(id != ''){
			$("#subdepartment_id").trigger("chosen:updated");
			//$("#subdepartment_id").trigger('chosen:open');
		}
		getpirdropdown();
	});
}
function getexcel(){
	$("#excelreport").val('1');
	$("#frm_details").submit();
	$("#excelreport").val('0');
}
function getpirdropdown(){
	var deptid = $("#department_id").val();
	var supdeptid = $("#subdepartment_id").val();
	var pirmaster_id = '<?php echo $pirmaster_id;?>';
	$.get("getpiradmin.php?deptid="+deptid+"&supdeptid="+supdeptid+"&pirmaster_id="+pirmaster_id+"&t=1",function(data){
		$("#pirmaster_id").html(data);
		$("#pirmaster_id").trigger("chosen:updated");
		getproject();
	});
}
function getproject(){
	var id = $("#pirmaster_id").val();
	var selid = '<?php echo $project_id;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#project_id").html(data);
		//$("#project_id").trigger("chosen:updated");
	});
}

function submitform(){
	$("#excelreport").val('1');
	$("#frm_details").submit();
	$("#excelreport").val('0');
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-All-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}	
	function getemployeelist(){
	$.get("getemplist.php?t=1",function(data){
		$("#empid").html(data);
		$("#empid").trigger("chosen:updated");
	});	
}
</script>
</html>
<?php } ?>

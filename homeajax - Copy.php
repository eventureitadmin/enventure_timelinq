<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	//Show color based on value
	function getcolor($selvalue,$userdashboard_legend_color){
		if($selvalue <= 79){
			$color = $userdashboard_legend_color['r'];
		}else if($selvalue >79 && $selvalue <= 95){
			$color = $userdashboard_legend_color['a'];
		}else if($selvalue >95){
			$color = $userdashboard_legend_color['g'];
		}
		return $color;
	}	
	
	function getUtilizationByUserID($empid,$date,$cond){
		$query = '';
		$query = "SELECT m.empid,m.empname,m.deptid,m.edate,m.employee_id,m.project_id,m.pirmaster_id,m.totalparts,(SELECT d1.name FROM department d1 WHERE d1.id=m.deptid) as dept,m.subdept,m.entrydate,SEC_TO_TIME(m.actualseconds) as actualhours,SEC_TO_TIME(m.calculatedseconds) as billablehours,SEC_TO_TIME(m.onlineseconds) as onlinehours,SEC_TO_TIME(m.minimumseconds) as presenthours,SEC_TO_TIME((m.onlineseconds - m.dayseconds)) as shortagehours,CONCAT(ROUND(((m.calculatedseconds/m.minimumseconds)*100),2),'') as utilization,CONCAT(ROUND(((m.calculatedseconds/m.actualseconds)*100),2),'') as efficiency FROM (SELECT DATE_FORMAT(t1.`entrydate` , '%d-%b-%Y' ) as entrydate,t1.`entrydate` as edate , t1.`employee_id`, t1.`project_id`, t1.`pirmaster_id`,(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id=t1.employee_id) as empname,(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid, (SELECT u1.department_ids FROM employeelist u1 WHERE u1.id=t1.employee_id) as deptid, (SELECT u2.subdepartment_ids FROM employeelist u2 WHERE u2.id=t1.employee_id) as subdept, SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) as onlineseconds,(CASE WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) <= TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('04:15:00') WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) > TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('08:30:00') ELSE TIME_TO_SEC('08:30:00') END) AS minimumseconds,TIME_TO_SEC('09:30:00') AS dayseconds,SUM(t1.totalparts) as totalparts,t2.department_id,t2.subdepartment_id FROM `timeentry` t1, pirmaster t2  WHERE 1=1 AND t2.id=t1.pirmaster_id AND t2.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].") AND t1.isActive='1'  AND t1.`entrydate`='".$date."' AND t1.employee_id='".$empid."' GROUP BY t1.`employee_id`) m WHERE 1=1".$cond;
		return $query;
	}
	
	if(count($_POST) > 0 && $_POST['from_date'] != "" && $_POST['to_date'] != ""){
		$fromdate = $_POST['from_date'];
		$todate = $_POST['to_date'];
		$html = '';
			$yy=1;
			$yestResult = $dbase->executeQuery("SELECT `entrydate` FROM `timeentry` WHERE `isActive`='1' AND employee_id='".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' GROUP BY `entrydate` ORDER BY `entrydate` DESC LIMIT 0,1",'single');
			$cond='';
			$yestQuery = getUtilizationByUserID(trim(mysql_escape_string($_SESSION['timesheet']['ID'])),$yestResult['entrydate'],$cond);
			$prevdayResult = $dbase->executeQuery($yestQuery,'single');
			$html .= '<div class="panel panel-default" id="kpitrptpanel">	
					<div class="panel-heading"><b>Dashboard</b></div>
			<div class="panel-body">';
					$html.='<div class="row" ><div class="col-sm-5"><div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$prevdayResult['entrydate'].' Utilization</b></div>';
				$html .='<div class="panel-body" style="height:299px;">
							<div class="row"><div class="col-sm-12" style="margin-top:40px;margin-left:70px;">';
					$html .='	<canvas id="utilization_guage'.$yy.'" height="160"></canvas><div  style="font-size:14px;margin-left:90px;">Utilization - <b>'.round($prevdayResult['utilization'],0).' %</b></div>
									<script>
										var opts'.$yy.' = {
											lines: 12,
											angle: 0.05, 
											lineWidth: 0.19, 
											radiusScale: 1,
											pointer: {
												length: 0.5, 
												strokeWidth: 0.035, 
												color: "#000000"
											},
											  limitMax: false,
											  limitMin: false, 											
											generateGradient: true,
											highDpiSupport: true,
											staticLabels: {
												font: "10px sans-serif",
												labels: ['.round($prevdayResult['utilization'],0).'],
												color: "#000000", 
												fractionDigits: 0 
											},
										staticZones: [
											   {strokeStyle: "'.$userdashboard_legend_color['r'].'", min: 0, max: 80},
											   {strokeStyle: "'.$userdashboard_legend_color['a'].'", min: 80, max: 95},
											   {strokeStyle: "'.$userdashboard_legend_color['g'].'", min: 95, max: '.$prevdayResult['utilization'].'},
											],											
										};
										var target'.$yy.' = document.getElementById("utilization_guage'.$yy.'");
										var gauge'.$yy.' = new Gauge(target'.$yy.').setOptions(opts'.$yy.');
										gauge'.$yy.'.maxValue = 100;
										gauge'.$yy.'.setMinValue(0);
										gauge'.$yy.'.animationSpeed = 32;
										gauge'.$yy.'.set('.round($prevdayResult['utilization'],0).');
									</script>';	
								$html .='</div>
							</div>
					</div></div></div>';
			$yy=2;

$yestResult1 = $dbase->executeQuery("SELECT `entrydate` FROM `timeentry` WHERE `isActive`='1' AND employee_id='".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' GROUP BY `entrydate` ORDER BY `entrydate` DESC LIMIT 1,7",'multiple');
$datelabel = '';
$datacolor = '';
$datavalue = '';
for($u=0;$u<count($yestResult1);$u++){
			$yestQuery1 = getUtilizationByUserID(trim(mysql_escape_string($_SESSION['timesheet']['ID'])),$yestResult1[$u]['entrydate'],$cond);
			$prevdayResult1 = $dbase->executeQuery($yestQuery1,'single');
			$datelabel .='"'.$prevdayResult1['entrydate'].'",';
			$datacolor .= '"'.getcolor(round($prevdayResult1['utilization'],0),$userdashboard_legend_color).'",';
			$datavalue .= '"'.round($prevdayResult1['utilization'],0).'",';			
}	
$datelabel = substr($datelabel,0,-1);
$datacolor = substr($datacolor,0,-1);
$datavalue = substr($datavalue,0,-1);
					$html.='<div class="col-sm-7">
					<div class="panel panel-default">
						<div class="panel-heading text-center"><b>Last 7 Days Utilization</b></div>
							<div class="panel-body">';
                  $html.='<div class="row">';
				  								 $html.='<canvas id="utilization_guage'.$yy.'" width="650" height="270" ></canvas>
								<div class="text-center" style="font-size:14px;"> </div>
							<script>
							
						var ctx = document.getElementById("utilization_guage'.$yy.'").getContext("2d");
						var myChart = new Chart(ctx, {
							type: "bar",
		
							data: {
							
							labels: ['.$datelabel.'],
								datasets: [{
								backgroundColor: ['.$datacolor.'],
								data: ['.$datavalue.'],
								borderWidth: 1, boxWidth: 10,
								}]
							},
							options: {
           						responsive: true,
           						maintainAspectRatio: true,							
								legend: {
								display: false
							},
								scales: {
								 xAxes: [{
										ticks: {
											min: 0 // Edit the value according to what you need
										}
									}],
									yAxes: [{
										barPercentage: 0.3,
										display: true,
										ticks: {
											beginAtZero: true
										},										
									}]
								},
								tooltips: {
												enabled: true,
												callbacks: {
														   label: function (tooltipItems, data) {
																return tooltipItems.yLabel + " %";
														   }	
														   }
											},
											hover: {
												animationDuration: 1
											},
											animation: {
											duration: 1,
											onComplete: function () {
												var chartInstance = this.chart,
													ctx = chartInstance.ctx;
													ctx.textAlign = "center";
													ctx.fillStyle = "rgba(0, 0, 0, 1)";
													ctx.textBaseline = "bottom";
													this.data.datasets.forEach(function (dataset, i) {
														var meta = chartInstance.controller.getDatasetMeta(i);
														meta.data.forEach(function (bar, index) {
															var data = dataset.data[index];
																ctx.fillText(data+"%", bar._model.x-5, bar._model.y);
														});
													});
												}
											}
							}
						});
						</script>';
                    $html.=' </div></div>';
					$html.='</div></div></div>';
					$html.='<div class="row" >';
					if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
						include_once("kpidedicatedgraph.php");
						$html .= '<div class="col-sm-7"><div class="panel panel-default">
						<div class="panel-heading text-center"><b>Dedicated Team Utilization</b></div>
							<div class="panel-body">';
							$html.='<div class="row">';
				  			$html.='<canvas id="dedicated_utilization_guage" width="500"></canvas>
								<div class="text-center" style="font-size:14px;"> </div>
							<script>';	
						$html.='var ctx = document.getElementById("dedicated_utilization_guage").getContext("2d");
						var myChart = new Chart(ctx, {
							type: "horizontalBar",
							width:600,
							data: {
							labels:['.$dedicatedlabel.'],
								datasets: [{
									data: ['.$dedicatedvalue.'],
									backgroundColor: ['.$dedicatedcolor.'],
									borderWidth: 1
								}]
							},
							options: {
								legend: {
								display: false
							},
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero: true
										}
									}]
								},
								tooltips: {
												enabled: true,
												  callbacks: {
														   label: function (tooltipItems, data) {
																return tooltipItems.xLabel + " %";
														   }	
														   }
											},
											hover: {
												animationDuration: 1
											},
											animation: {
											duration: 1,
											onComplete: function () {
												var chartInstance = this.chart,
													ctx = chartInstance.ctx;
													ctx.textAlign = "center";
													ctx.fillStyle = "rgba(0, 0, 0, 1)";
													ctx.textBaseline = "top";
													this.data.datasets.forEach(function (dataset, i) {
														var meta = chartInstance.controller.getDatasetMeta(i);
														meta.data.forEach(function (bar, index) {
															var data = dataset.data[index];
															ctx.fillText(data+"%", bar._model.x+14, bar._model.y-5);
														});
													});
												}
											}
							}
						});
						</script>';
                    $html.=' </div></div>';
					$html.='</div></div>';						
					}
			$html .= '<div class="col-sm-5">
					<ul id="userdashboardul" class="list-group">';
					foreach($userdashboard_legend_color as $key => $val){
						$html .= '<li class="list-group-item" style="font-size:12px;"><div style="position:relative;padding-left:5px;padding-right:5px;float:left;text-align:center;">
											<span class="label label-default" style="background-color:'.$val.';">&nbsp;</span></div>'.$userdashboard_legend_label[$key].'</li>';
					}
					$html .= '</ul></div>';						
					$html.='</div>';
					
					$html.='<div class="row" >';
			$yy=2;
	$ym = $_POST['log_mon_yr'];
	
// Check format
$timestamp = strtotime($ym,"-01");
if ($timestamp === false) {
	$timestamp = time();
}
$today = date('Y-m-j', time());
$html_title = date('M - Y', $timestamp);
$day_count = date('t', $timestamp);
$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));		
					$html.='<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading text-center"><b>Login Details</b></div>
							<div class="panel-body">';
                  $html.='<div class="row">
                     <div class="col-md-12">';
								  $html.='<div class="col-sm-12 well pull-right-lg" style="border:0px solid">
									<div class="col-md-12" style="padding:0px;">
									  <br>
										<table class="table table-bordered table-style table-responsive">
										  <tr>
											<th colspan="2"><a href="javascript:void(0);" id="previous"> << </a></th>
											<th colspan="3" align="middle">'.$html_title.'</th>
											<th colspan="2"><a href="javascript:void(0);" id="next"> >> </a></th>
										  </tr>
										  <tr>
											<th>S</th>
											<th>M</th>
											<th>T</th>
											<th>W</th>
											<th>T</th>
											<th>F</th>
											<th>S</th>
										  </tr>';
								$week = '';
								$week .= str_repeat('<td></td>', $str);
								for ( $day = 1; $day <= $day_count; $day++, $str++) {
									$logindet = "";
								$emplogDetQuery = "SELECT `ID`,`log_date`, `login_time`, `login_comments`, `logout_time`, `logout_comments`,`totalhours` FROM time_log WHERE `emp_id`='".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND log_date='".$ym."-".$day."'";
									$date = $ym.'-'.$day;
									$emplogDetResult = $dbase->executeQuery($emplogDetQuery,"single");	
									if($emplogDetResult['ID'] != ''){
										$logindet = "<span style='font-size:11px;'>In Time : </br>".date('d-M-Y h:i A',strtotime($emplogDetResult['login_time']))."</span></br>";
										if($emplogDetResult['logout_time'] != '0000-00-00 00:00:00'){
											$logindet .= "<span style='font-size:11px;'> Out Time : </br>".date('d-M-Y h:i A',strtotime($emplogDetResult['logout_time']))."</span></br>";
										}
										else{
										$logindet .= "<span style='font-size:11px;'> Out Time : </br></span></br>";
										}
										$timearr = explode(":",$emplogDetResult['totalhours']);
										//$totaltime = $timearr[0]." Hrs ".$timearr[1]." Min ".$timearr[2]." Sec";
										$totaltime = $timearr[0].":".$timearr[1];
										if($totaltime != '00:00'){
											$totaltime = $totaltime;
										}
										else{
											$totaltime = "";
										}
										$logindet .= "<span style='font-size:12px;'> Tot Hrs : <b>".$totaltime."</b></span></br>";
									}
									else{
										$logindet = "<span style='font-size:12px;'></span></br>";
										$logindet .= "<span style='font-size:12px;'></span></br>";
										$logindet .= "<span style='font-size:12px;'></span></br>";		
									}
									if ($today == $date) {
										$week .= '<td width="14%" class="today">'.$day."</br>".$logindet;
									} else {
										$week .= '<td width="14%">'.$day."</br>".$logindet;
									}
									$week .= '</td>';
									if ($str % 7 == 6 || $day == $day_count) {
										if($day == $day_count) {
											$week .= str_repeat('<td width="14%"></td>', 6 - ($str % 7));
										}
										$html .= '<tr>'.$week.'</tr>';
										$week = '';
									}
								}
										$html .='</table>
									</div>
								  </div>';					 
                    $html.=' </div></div>';
					$html.='</div></div>';					
					
					$html .= '</div></div>';
		echo $html;exit;
	}
}
?>

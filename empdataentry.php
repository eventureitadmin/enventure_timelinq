<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	   
	   <!---<script type="text/javascript" src="js/bootstrap.js"></script>-->
	  <!--- <link type="text/css" href="css/bootstrap.css" rel="stylesheet" />-->
		<script type="text/javascript">
		var timetype = <?php echo TIME_TYPE; ?>;
		var maximum_entry = <?php echo MAXIMUM_ENTRY; ?>;
		 $(document).ready(function(){
			var time = new Date().toString("HH:mm:ss");
			if(timetype=='1'){
				$("#logintime").val(time);
				$("#logouttime").val(time);		
			}
			if(timetype=='2'){
				$("#logintime").val('<?php echo $time; ?>');
				$("#logouttime").val('<?php echo $time; ?>');
			}
		});		
		</script>	
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#dataentrytable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#dataentrytable td, table#dataentrytable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		table#dataentrydetailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#dataentrydetailstable td, table#dataentrydetailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#datalisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#datalisttable td, table#datalisttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		table#dataentereddetailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#dataentereddetailstable td, table#dataentereddetailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<table id="datalisttable" border="0" cellpadding="0" cellspacing="0" align="center"  width="80%" >
<tr>
	<td width="10%"><b>Select Employee</b></td>
	<td width="22%"><select id="empid" name="empid" class="chosen-select" style="width:300px;" >
		<option value="">-Select-</option>
		</select></td>
	<td width="22%"></td>
</tr>	
<tr>
	<td width="10%"><b>Entry Date</b></td>
	<td width="22%"><input type="text" name="entereddate" id="entereddate" value="<?php  echo date('d-M-Y',strtotime(date('Y-m-d')));?>" class=" required" /></td>
	<td width="22%"><!--<input type="button" id="dataenteredbutton" value=" Submit " onclick="loadlisttable();" class="button button1" />--></td>
</tr>
<tr>
<td colspan="3" id="dataentered">

</td>
</tr>
</table>
</body>
<script type="text/javascript">
 $(document).ready(function(){
	 $(".chosen-select").chosen();
	 getemployeelist();
	 
	 $( "#entrydate" ).datepicker({
		inline: true,
		dateFormat: 'd-M-yy',
		maxDate: 0,
		showOn: 'button',
		buttonImageOnly: true,
		buttonImage: 'images/calendar.gif',
		changeMonth: true,
		changeYear: true,
		yearRange: "-10:+0",
		onSelect: function(dateText) {
		  $(this).change();
		},	
	 }).on("change", function() {
		 loadtable();
  });
  
	 $( "#entereddate" ).datepicker({
		inline: true,
		dateFormat: 'd-M-yy',
		maxDate: 0,
		showOn: 'button',
		buttonImageOnly: true,
		buttonImage: 'images/calendar.gif',
		changeMonth: true,
		changeYear: true,
		yearRange: "-10:+0",
		onSelect: function(dateText) {
		  $(this).change();
		},	
	 }).on("change", function() {
		 loadlisttable();
  });  
	 
	  $("#frm_dataentry").validate();
	  
	$(document).delegate('.add-record', 'click', function(e) {
		 e.preventDefault();    
		 var content = $('#dataentrydetailstable tbody').children('tr:first'),
		 size = $('#dataentrydetailstable >tbody >tr').length + 1,
			element = null,    
			element = content.clone();
			if(size<=<?php echo MAXIMUM_ENTRY;?>){
				element.attr('id', 'row'+size);
				element.find('.pir').attr('id', 'pirmaster_id'+size);
				element.find('.pir').attr('name', 'pirmaster_id'+size);
				element.find('.activity').attr('id', 'activity_id'+size);
				element.find('.activity').attr('name', 'activity_id'+size);
				element.find('.activity').empty().append('<option value="">-Select-</option>');
				element.find('.project').attr('id', 'project_id'+size);
				element.find('.project').attr('name', 'project_id'+size);
				element.find('.project').empty().append('<option value="">-Select-</option>');				
				element.find('.totalparts').attr('id', 'totalparts'+size);
				element.find('.totalparts').attr('name', 'totalparts'+size);
				element.find('.totalparts').val('');
				element.find('.calculatedhours').attr('id', 'calculatedhours'+size);
				element.find('.calculatedhours').attr('name', 'calculatedhours'+size);
				element.find('.calculatedhours').val('');
				element.find('.actualhours').attr('id', 'actualhours'+size);
				element.find('.actualhours').attr('name', 'actualhours'+size);
				element.find('.actualhours').val('');
				element.find('.delete-record').attr('rowid', size);
				element.appendTo('#dataentrydetailstable');
				<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
				$('#pirmaster_id'+size).change();
				<?php } ?>
				$("#totrow").val(size);				
			}
		  else{
			$('<div>Only '+maximum_entry+' can enter</div>').dialog({
				resizable: false,
				open: function(){
				 $(".ui-dialog-title").html("Alert");
				},				
					buttons: {
					"Ok": function() {
						$( "div" ).remove( ".ui-widget-overlay" );	
						$(this).dialog("close");
						return false;
					},
				}
			});	
			$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
		  }
   });
   
	$(document).delegate('.delete-record', 'click', function(e) {
		e.preventDefault();  
	  var id = $(this).attr('rowid');
	  if(id > 1){
	  var targetDiv = $(this).attr('targetDiv');
		$('<div>Are you sure wants to delete ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
					  $('#row' + id).remove();
					//regnerate index number on table
					$('#dataentrydetailstable tbody tr').each(function(index) {
					  var rowid = index+1;
						$(this).attr('id', 'row'+rowid);
						$(this).find('.pir').attr('id', 'pirmaster_id'+rowid);
						$(this).find('.pir').attr('name', 'pirmaster_id'+rowid);
						$(this).find('.activity').attr('id', 'activity_id'+rowid);
						$(this).find('.activity').attr('name', 'activity_id'+rowid);
						$(this).find('.project').attr('id', 'project_id'+rowid);
						$(this).find('.project').attr('name', 'project_id'+rowid);						
						$(this).find('.totalparts').attr('id', 'totalparts'+rowid);
						$(this).find('.totalparts').attr('name', 'totalparts'+rowid);
						$(this).find('.calculatedhours').attr('id', 'calculatedhours'+rowid);
						$(this).find('.calculatedhours').attr('name', 'calculatedhours'+rowid);
						$(this).find('.actualhours').attr('id', 'actualhours'+rowid);
						$(this).find('.actualhours').attr('name', 'actualhours'+rowid);
						$(this).find('.delete-record').attr('rowid', rowid);
						$("#totrow").val(rowid);
					});
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return true;
					

				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );	
	  }
	  else{
		$('<div>You cant delete this row</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				},
			}
		});
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	  }
	});
loadtable();
loadlisttable();

$.validator.addMethod("time24", function(value, element) {
	if(value != ''){
		if (!/^\d{1,2}:\d{2}$/.test(value)) return false;
		var parts = value.split(':');
		if (parts[0] > 23 || parts[1] > 59) return false;
		return true;		
	}
	else{
		return true;
	}
}, "Invalid time format.");
<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
  $('#frm_dataentry').bind('submit', function () {
		$('.pir').removeAttr('disabled'); 
		$('.project').removeAttr('disabled');  
		$('.activity').removeAttr('disabled');
  });
<?php } ?>
});	

function checkcalculatetime(id,value){
	var id = id.replace("actualhours", "");
	if($("#activity_id"+id).val() != ''){
		if($("#activity_id"+id).find(':selected').data('id')=='n' && $("#totalparts"+id).val() != 'NA' && $("#totalparts"+id).val() !='' && $("#totalparts"+id).val() > 0){
			if(value != ''){
				$("#calculatedhours"+id).val(value);
			}
		}
	}
}

function callcalculatetime(id){
	var id = id.replace("activity_id", "");
	var acttype = $("#activity_id"+id).find(':selected').data('type');
	if(acttype=='0'){
		$("#totalparts"+id).attr('readonly', true); 
		$("#totalparts"+id).val("1");
		calculatetime('totalparts'+id,'1','0');
	}
	else{
		if($("activity_id"+id).val()==''){
			calculatetime('totalparts'+id,$("#totalparts"+id).val(),'1');
		}
		else{
			calculatetime('totalparts'+id,$("#totalparts"+id).val(),'0');
		}
	}
}

function getactivity(id,value){
	var id = id.replace("project_id", "");
	var pirid = $("#pirmaster_id"+id).val();
	var proid = $("#project_id"+id).val();
	$.get("getactivity.php?pirid="+pirid+"&proid="+proid,function(data){
		$("#activity_id"+id).html(data);
		<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
			autoselectactivity();
		<?php } ?>		
	});
}

function getproject(id,value){
	var id = id.replace("pirmaster_id", "");
	$("#totalparts"+id).val('');
	$("#calculatedhours"+id).val('');
	$("#actualhours"+id).val('');
	$("#activity_id"+id).empty().append('<option value="">-Select-</option>');
	var selid = $("#pirmaster_id"+id).val();
	$.get("getproject.php?id="+selid,function(data){
		$("#project_id"+id).html(data);
		autoselectoption("#project_id"+id);
		<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
			autoselectproject();
		<?php } ?>			
	});
}

function calculatetime(id,value,check){
			var id = id.replace("totalparts", "");
			var value = value;
			var pirid = $("#pirmaster_id"+id).val();
			var proid = $("#project_id"+id).val();
			var actid = $("#activity_id"+id).val(); 
			if(pirid==''){
				//if(check=='1'){
					$(this).val('');
					$('<div>Please Select PIR</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$("#totalparts"+id).val('');
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				//}
			}
			else if(proid==''){
				//if(check=='1'){
					$(this).val('');
					$('<div>Please Select Project</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$("#totalparts"+id).val('');
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				//}
			}
			else if(actid==''){
				if(check=='1'){				
					$(this).val('');
					$('<div>Please Select Activity</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$("#totalparts"+id).val('');
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				}
			}
			else{
				$.get("getbudgettime.php?val="+value+"&pirid="+pirid+"&actid="+actid+"&proid="+proid,function(data){
					if(data == 'NA'){
						$("#totalparts"+id).removeClass("number");
						$("#totalparts"+id).val('NA');
						$("#calculatedhours"+id).removeClass("required");
						$("#calculatedhours"+id).val('NA');
					 }
					else{
						if(!$("#calculatedhours"+id).hasClass("required")){
							$("#calculatedhours"+id).addClass("required");
						}
						if(!$("#totalparts"+id).hasClass("required")){
							$("#totalparts"+id).addClass("required");
						}						
					if(data != 'NULL'){
						$("#calculatedhours"+id).val(data);
					}
					if(data=='NULL' && $("#activity_id"+id).find(':selected').data('id')=='y'){
						$("#totalparts"+id).val('');
						$("#calculatedhours"+id).val('00:00');
					}
					//if(data=='NULL'){
					//	$("#totalparts"+id).val('');
					//	$("#calculatedhours"+id).val('00:00');
					//}				
					checkcalculatetime('actualhours'+id,$("#actualhours"+id).val());
					}
				});					
			}
			
}

function loadtable(){
	$.get("dataentrytable.php?id=1",function(data){
		$("#dataentry").html(data);
		<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
			autoselectpirno();
		<?php } ?>			
	});	
	$("#totrow").val('1');
}

function loadlisttable(){
	var empid = $("#empid").val();
	var entereddate = $("#entereddate").val();
	$.get("empdataenteredtable.php?date="+entereddate+"&empid="+empid,function(data){
		$("#dataentered").html(data);
	});	
}

function deleterow(idval){
	$('<div>Do you want to Delete this row ?</div>').dialog({
		resizable: false,
		open: function(){
		 $(".ui-dialog-title").html("Please Confirm !");
		},				
			buttons: {
			"Yes": function() {
				$.post("deleterow.php",{idval:idval},function(data){
					if(data=='success'){
							$('<div>Row Deleted successfully</div>').dialog({
								resizable: false,
								open: function(){
								 $(".ui-dialog-title").html("Alert");
								},				
									buttons: {
									"Ok": function() {
										$(this).dialog("close");
									},
								}
							});							
					}
				});	
				$( "div" ).remove( ".ui-widget-overlay" );	
				loadlisttable();
				$(this).dialog("close");
			},
			"No": function() {
				$( "div" ).remove( ".ui-widget-overlay" );	
				$(this).dialog("close");
			}
		}
	});	
	$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
}

function autoselectpirno(){
$(".pir option").each(function () {
	var value1 = '';
	value1 = $(this).text();
	if (value1 == "EAS/0419") {
		$(this).attr("selected", "selected").change();
	}
});	
$('.pir').attr('disabled', 'disabled');
}
function autoselectproject(){
$(".project option").each(function () {
	var value2 = '';
	value2 = $(this).text();
	if (value2 == "Maximo Support") {
		$(this).attr("selected", "selected").change();
	}
});
$('.project').attr('disabled', 'disabled');
}
function autoselectactivity(){
$(".activity option").each(function () {
	var value3 = '';
	value3 = $(this).text();
	if (value3 == "Maximo Support") {
		$(this).attr("selected", "selected").change();
	}
});
$('.activity').attr('disabled', 'disabled');
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
function getemployeelist(){
	$.get("getemplist.php?t=1",function(data){
		$("#empid").html(data);
		$("#empid").trigger("chosen:updated");
	});	
}
</script>
</html>
<?php } ?>

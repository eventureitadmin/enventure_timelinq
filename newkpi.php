<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{ 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
	<title>Enventure - Timesheet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!----<link type="text/css" href="css/bootstrap.css" rel="stylesheet" />--->

	<link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery_confirm.css" rel="stylesheet" />
	<link type="text/css" href="css/custom.css" rel="stylesheet" />
	<link type="text/css" href="css/css-circular-prog-bar.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jqueryui.js"></script>	   
	<script type="text/javascript" src="js/jquery_validate.js"></script>
	<script type="text/javascript" src="js/jquery_confirm.js"></script>
	<script type="text/javascript"  src="js/easyconfirm.js"></script>
	<script type="text/javascript" src="js/date.js"></script>
	<!----<script type="text/javascript" src="js/bootstrap.js"></script>-->
	<script type="text/javascript" src="js/gauge.js"></script>
	<script type="text/javascript" src="js/chart2_8.js"></script>
	<script type="text/javascript" src="js/canvas.js"></script>
	<style>
	.ui-datepicker-calendar 
      {
         display:none
      }
	</style>
</head>
<body>
<?php include("menu.php");?>
	<?php include_once("includebootstrap.php"); ?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'){ $deptid =1;?>
<div class="container-fluid">
<form id="frm_kpi"  action="" method="post">
	 <div class="row">
	  <div class="col-sm-2">
		  <div class="form-group">
			<label for="email">From </label>
			<input type="text" id="from_date" name="from_date" class="form-control " value="<?php if($from_date==''){echo date('F Y'); } else{ echo $from_date; }  ?>" />  
		  </div>		 
	   </div>
	  <div class="col-sm-2">
		  <div class="form-group ">
			<label for="email">To </label>
			<input type="text" id="to_date" name="to_date" class="form-control" value="<?php if($to_date==''){echo date('F Y'); } else{ echo $to_date; }  ?>" />
		  </div>		 
	</div>
		<div class="col-sm-1">
			<label for="submitbutton"></label><br>
			  <button type="button" id="submitbutton" name="submitbutton" onclick="submitform();" 
					  class="btn btn-primary"> Submit </button>
	   </div>
		 <div class="col-sm-1">
			 <label for="addkpi"></label><br>
			  <button type="button" id="addkpi" name="addkpi" onclick="showkpilist()" 
					  class="btn btn-primary"> Add KPI </button>
	   </div>
	  </div> 	
	
</form>
</div>
<div id="chart-container" class="container-fluid">
</div>

<?php } ?>

<div id="overlay">
  <div id="overlay-text"><h5><img src="images/busy.gif" />Graphs are getting generated.Please wait!</h5></div>
</div>	

<script type="text/javascript">
var months = [
    'January', 'February', 'March', 'April', 'May',
    'June', 'July', 'August', 'September',
    'October', 'November','December'
    ];
	function showkpilist(){
		window.location.href="kpilist.php";
	}
 $(document).ready(function(){
	
	 $('#overlay').css('visibility', 'visible');
	 $("#submitbutton").trigger('click');
	  //$('#overlay').css('visibility', 'hidden');
		$('#from_date').datepicker({
		 changeMonth: true,
		 changeYear: true,
		 dateFormat: 'MM yy',
		yearRange: "-10:+0",
		maxDate:0,
		onChangeMonthYear:function(year, month, inst){	
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		   $('#from_date').val(months[month-1]+" "+iYear);
	
		},
		 onClose: function() {
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
			
		 },

		 beforeShow: function() {
		   if ((selDate = $(this).val()).length > 0) 
		   {
			  iYear = selDate.substring(selDate.length - 4, selDate.length);
			  iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
			  $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
			   $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
			  
		   }
		}
	  });
		$('#to_date').datepicker({
		 changeMonth: true,
		 changeYear: true,
		 dateFormat: 'MM yy',
		yearRange: "-10:+0",
		maxDate:0,
		onChangeMonthYear:function(year, month, inst){	var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
									
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$('#to_date').val(months[month-1]+" "+iYear);
		   // $('#to_date').datepicker("hide");
		},
		 onClose: function() {
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		 },

		 beforeShow: function() {
		   if ((selDate = $(this).val()).length > 0) 
		   {
			  iYear = selDate.substring(selDate.length - 4, selDate.length);
			  iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
			  $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
			   $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		   }
		}
	  });
	  
	});	
	
function submitform(){
	$("#chart-container").css("display","block");
	var fromyear ="";var toyear="";
	if($("#from_date").val() !='' && $("#to_date").val() != ""){
	var fromyear = $("#from_date").val().split(" ")[1];
	var toyear = $("#to_date").val().split(" ")[1];
	}
		if($("#from_date").val() !='' && $("#to_date").val() != "" && toyear >=fromyear ){
			/*var frmmonth = $("#from_date").val().split(" ")[0].replace(/^0+/, '');
			var tomonth = $("#to_date").val().split(" ")[0].replace(/^0+/, '');alert(frmmonth+"--"+tomonth);
			if(fromyear == toyear && months > months){
				$("#chart-container").html('');
			}else{*/
			$('#overlay').css('visibility', 'visible');
			$.post("kpiajax.php",$.param($("#frm_kpi").serializeArray()), function(data, status){
				if(data!=''){
					$('#overlay').css('visibility', 'hidden');
				   $("#chart-container").html(data);
				
				}
				else{
					$.alert({
						title: 'Alert',
						content: 'No Records',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});	
				}
			});	

		}else if(fromyear>toyear){
			$("#chart-container").html('');
			$.alert({
						title: 'Alert',
						content: 'From year should not be greater than To year',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});
			
		}
		else{
			$("#chart-container").html('');
			$.alert({
						title: 'Alert',
						content: 'No Records',
						animation: 'scale',
						closeAnimation: 'scale',
						onClose: function () {
						
						},							
						buttons: {
							okay: {
								text: 'Ok',
								btnClass: 'btn-blue'
							}
						}					
					});
		
		}

}
	
</script>	
</body>
</html>
<?php }
?>

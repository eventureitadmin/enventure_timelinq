<?php
include_once("config.php");
include_once("functions.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getRBUquery($date,$cond,$group,$subdeptcond,$dtcond){
		$query = '';
		$query = "SELECT m.empid,m.empname,m.deptid,m.edate,m.employee_id,m.project_id,m.pirmaster_id,m.totalparts,(SELECT d1.name FROM department d1 WHERE d1.id=m.deptid) as dept,m.subdept,m.entrydate,SEC_TO_TIME(m.actualseconds) as actualhours,SEC_TO_TIME(m.calculatedseconds) as billablehours,SEC_TO_TIME(m.onlineseconds) as onlinehours,SEC_TO_TIME(m.minimumseconds) as presenthours,SEC_TO_TIME((m.onlineseconds - m.dayseconds)) as shortagehours,CONCAT(ROUND(((m.calculatedseconds/m.minimumseconds)*100),2),'') as utilization,CONCAT(ROUND(((m.calculatedseconds/m.actualseconds)*100),2),'') as efficiency FROM (SELECT DATE_FORMAT(t1.`entrydate` , '%d-%b-%Y' ) as entrydate,t1.`entrydate` as edate , t1.`employee_id`, t1.`project_id`, t1.`pirmaster_id`,(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id=t1.employee_id) as empname,(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid, (SELECT u1.department_ids FROM employeelist u1 WHERE u1.id=t1.employee_id) as deptid, (SELECT u2.subdepartment_ids FROM employeelist u2 WHERE u2.id=t1.employee_id) as subdept, SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) as onlineseconds,(CASE WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) <= TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('04:15:00') WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) > TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('08:30:00') ELSE TIME_TO_SEC('08:30:00') END) AS minimumseconds,TIME_TO_SEC('09:30:00') AS dayseconds,SUM(t1.totalparts) as totalparts,t2.department_id,t2.subdepartment_id FROM `timeentry` t1, pirmaster t2  WHERE 1=1 AND t1.is_rework='0' AND t1.is_internalpir='0' AND t2.id=t1.pirmaster_id ".$subdeptcond." AND t1.isActive='1' ".$dtcond." AND t1.`entrydate`='".$date."' ".$group.") m WHERE 1=1".$cond;
		return $query;
	}
	
	if($_POST){
		//print_r($_POST);exit;
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$deptid = $_POST['department_id'];
		$subdeptid = $_POST['subdepartment_id'];
		$pirmaster_id = $_POST['pirmaster_id'];
		$project_id = $_POST['project_id'];
		$kpis = $_POST['kpis'];
		$withdetails = $_POST['withdetails'];
		$subdepartment_ids = implode (",", $subdeptid);

		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$cond = '';
		$procond=''; 

		if($project_id != ''){
			$cond .= " AND m.project_id='".$project_id."'";
			$procond = " AND projectname='".$project_id."'";
		}		
		if($pirmaster_id != '' && $project_id != ''){
			$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'".$procond;
			$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
			$pirmasterid=$pirmasterdet['id'];			
			//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
			$group = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
		}
		elseif($pirmaster_id != ''){
			$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
			$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
			if($pirmastercntdet['pircnt']==1){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasterid=$pirmasterdet['id'];			
				//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
				$group = " AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";				
			}
			else{
				$pirmaster_select = "SELECT GROUP_CONCAT(id) as pircsv FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasteridcsv = $pirmasterdet['pircsv'];
				$group = " AND  t1.`pirmaster_id` IN (".$pirmasteridcsv.") GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
			}
		}		
		else{
			$group = " GROUP BY t1.`employee_id`";
		}		
		
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$subdeptcond = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
		}
		$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
		$dedicatedempcond = '';
		if($deptid != ''){
			$dedicatedempcond .= " AND department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$dedicatedempcond .= " AND subdepartment_id IN (".$subdepartment_ids.")";
		}
		if($pirmaster_id != ''){
			$dedicatedempcond .= " AND id='".$pirmaster_id."'";
		}
		$query="SELECT IFNULL(SUM(`noofresource`),0) as dedicatedresource FROM `pirlist` WHERE `isActive`='1' AND `contracttype`='2' ".$dedicatedempcond;
		$result = $dbase->executeQuery($query,'single');
		$totempcnt = $result['dedicatedresource'];
		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){
			unset($reportdata);
				//kpi 1
				if($kpis=='1'){
							if($subdepartment_ids != ''){
								$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
							}
							else{
								$subcnd = '';
							}

								$reportdata1 = array();
								$subdeptcond1 = "AND t2.department_id = '".$deptid."'".$subcnd;
									for($j=0;$j<count($datelist);$j++){
										$reportdata1[$j]['date'] = $datelist[$j];
										$query12 = '';
										$dtcond12 = '';
										unset($report12);
										$query12 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond1,$dtcond12);
										$report = $dbase->executeQuery($query12,'multiple');
										for($i=0;$i<count($report);$i++){
											$subdisplay = " AND sd.id IN (".$report[$i]['subdept'].")";
											$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
											$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
											$subdeptstr = '';
											for($k=0;$k<count($subdeptResult);$k++){
												$subdeptstr .= $subdeptResult[$k]['subname'].",";
											}
											$subdeptstr = substr($subdeptstr,0,-1);	
											$report[$i]['subdept'] = $subdeptstr;
										}										
										$reportdata1[$j]['details'] = $report;							
									}
					$reportdata = $reportdata1;
				}
				//kpi 2
				if($kpis=='2'){
					unset($reportdata2);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata2 = array();
					$subdeptcond2 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
						$reportdata2[$j]['date'] = $datelist[$j];
						$query21 = '';
						$dtcond21 = "AND t1.is_dt='0'";
						unset($report21);
						$query21 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond2,$dtcond21);
						$report21 = $dbase->executeQuery($query21,'multiple');
						
							for($i=0;$i<count($report21);$i++){
								
								$subdisplay = " AND sd.id IN (".$report21[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report21[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata2[$j]['details'] = $report21;
					}		
					$reportdata = $reportdata2;
				}			
				//kpi 3
				if($kpis=='3'){
				unset($reportdata3);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata3 = array();
					$subdeptcond3 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
					$dtcond31 = " AND t1.is_dt='1'";
					$query31 = '';
					unset($report31);
					$query31 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond3,$dtcond31);
					$report31 = $dbase->executeQuery($query31,'multiple');
							for($i=0;$i<count($report31);$i++){
								$subdisplay = " AND sd.id IN (".$report31[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report31[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata3[$j]['details'] = $report31;	
					}
					$reportdata = $reportdata3;
				}
				//kpi 4
				if($kpis=='4'){
				unset($reportdata4);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata4 = array();
					$subdeptcond4 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
						$dtcond4 = " AND t1.is_dt='0'";
					$query4 = '';
					unset($report4);
					$query4 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond4,$dtcond4);
					$report4 = $dbase->executeQuery($query4,'multiple');
							for($i=0;$i<count($report4);$i++){
								$subdisplay = " AND sd.id IN (".$report4[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report4[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata4[$j]['details'] = $report4;
					}
					$reportdata = $reportdata4;
				}
				//kpi 5
				if($kpis=='5'){
				unset($reportdata5);
					if($subdepartment_ids != ''){
						$subcnd = " AND t2.subdepartment_id IN (".$subdepartment_ids.")";
					}
					else{
						$subcnd = '';
					}

					$reportdata5 = array();
					$subdeptcond5 = "AND t2.department_id = '".$deptid."'".$subcnd;
					for($j=0;$j<count($datelist);$j++){
						$dtcond5 = " AND t1.is_dt='0'";
					$query5 = '';
					unset($report5);
					$query5 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond5,$dtcond5);
					$report5 = $dbase->executeQuery($query5,'multiple');
							for($i=0;$i<count($report5);$i++){
								$subdisplay = " AND sd.id IN (".$report5[$i]['subdept'].")";
								$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
								$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
								$subdeptstr = '';
								for($k=0;$k<count($subdeptResult);$k++){
									$subdeptstr .= $subdeptResult[$k]['subname'].",";
								}
								$subdeptstr = substr($subdeptstr,0,-1);	
								$report5[$i]['subdept'] = $subdeptstr;
							}										
							$reportdata5[$j]['details'] = $report5;
					}			
					$reportdata = $reportdata5;
				}
			//echo "<pre>";
			//print_r($reportdata);exit;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chart2_8.js" type="text/javascript"></script>
	   <script src="js/utils.js" type="text/javascript"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:14px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		
		table#reportdetailtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reportdetailtable td, table#reportdetailtable th {
			border: 1px solid white;
			 padding: 5px; 
		}	
		table#kpi1 {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#kpi1 td, table#kpi1 th {
			border: 1px solid black;
			 padding: 5px; 
		}			  
		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="1" align="center"  width="100%" >
<tr>
<td width="100%" colspan="4" ><b>KPI's</b></td>
</tr>
<tr>
<td width="25%" ><b>From Date</b><br/><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('01-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="25%" ><b>To Date</b><br/><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="25%" ><b>Select Department</b><br/><select id="department_id" name="department_id" class="required"  onchange="getsubdepartment();">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																									  
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
			if($deptResult[$i]['id']==$deptid){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select></td>
<td width="25%" ><b>Select Sub Department</b><br/><select id="subdepartment_id" name="subdepartment_id[]" data-placeholder="Select Sub Department" class="required chosen-select-multi" multiple style="width:180px;">
	</select>	
	<label for="subdepartment_id" class="error" style="display:none" >This field is required.</label>
</td>
</tr>
<tr>
<td width="25%" ><b>Select PIR</b><br/><select id="pirmaster_id" name="pirmaster_id" class="chosen-select" style="width:300px;" onchange="getproject();">
	<option value="">-All-</option>
	</select></td>
<td width="25%" ><b>Select Project</b><br/><select id="project_id" name="project_id">
	<option value="">-All-</option>
	</select></td>
<td width="12%" ><b>Select KPI</b><br/><select id="kpis" name="kpis" class="required" >
<option value="">-Select-</option>
<option value="1" <?php if($kpis=='1'){ ?>selected<?php } ?>>Overall Billing Utilization</option>
<option value="3" <?php if($kpis=='3'){ ?>selected<?php } ?>>Dedicated Team Utilization</option>	
<option value="2" <?php if($kpis=='2'){ ?>selected<?php } ?>>Non Dedicated Project Efficiency</option>
<option value="4" <?php if($kpis=='4'){ ?>selected<?php } ?>>Non Dedicated Utilization</option>
<!--<option value="5" <?php if($kpis=='5'){ ?>selected<?php } ?>>Dedicated Team Bench Utilization</option>-->
</select></td>
<td width="25%" ><input type="hidden" id="excelreport" name="excelreport"  /><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" /><!--<input type="checkbox" id="withdetails" name="withdetails" value='1' <?php if($withdetails=='1'){?> checked <?php } ?> /> With Details--></td>
</tr>
	<tr>
		<td colspan="4">
	<?php if(count($reportdata) > 0 && $reportdata != ''){ ?>
	   	<table id="kpi1" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%">
		<tr>
			<td width="5%" align="center"><b>Emp ID</b></td>
			<td width="15%" align="center"><b>Emp Name</b></td>
			<!--<td width="5%" align="center"><b>Dept</b></td>
			<td width="5%" align="center"><b>Sub Dept</b></td>-->
			<td width="10%" align="center"><b>Date</b></td>
			<!--<td width="7%" align="center"><b>Total Parts</b></td>-->
			<!--<td width="8%" align="center"><b>Online Hours</b></td>
			<td width="8%" align="center"><b>Shortage Hours</b></td>-->
			<td width="8%" align="center"><b>Present Hours</b></td>
			<td width="8%" align="center"><b>Actual Hours</b><span style="font-size:10px;"> Entered by user</span></td>
			<td width="8%" align="center"><b>Budgeted Hours</b></td>
		</tr>
		<?php
		$totparts = 0;
		$actualhourssarr = array();
		$billablehoursarr = array();
		$availablehoursarr = array();
		$emp_array = array();
		$cnt = 0;
		$totpresentdays = 0;
		for($q=0;$q<count($reportdata);$q++){
			for($w=0;$w<count($reportdata[$q]['details']);$w++){
				$emp_array[] = $reportdata[$q]['details'][$w]['empid'];
				$presentdays = 0;
				$actualhourssarr[] = $reportdata[$q]['details'][$w]['actualhours'];
				$billablehoursarr[] = $reportdata[$q]['details'][$w]['billablehours'];
				$availablehoursarr[] = $reportdata[$q]['details'][$w]['presenthours'];
				$totparts += $reportdata[$q]['details'][$w]['totalparts'];
				if($reportdata[$q]['details'][$w]['presenthours']=='08:30:00'){
					$presentdays += 1;
				}
				elseif($reportdata[$q]['details'][$w]['presenthours']=='04:15:00'){
					$presentdays += 0.5;
				}
				else{
					$presentdays += 0;
				}
				$totpresentdays += $presentdays;
				$cnt++;		
		?>
		<tr>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['empid']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['empname']; ?></td>
			<!--<td valign="top"><?php echo $reportdata[$q]['details'][$w]['dept']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['subdept']; ?></td>-->
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['entrydate']; ?></td>
			<!--<td valign="top"><?php echo $reportdata[$q]['details'][$w]['totalparts']; ?></td>-->
			<!--<td valign="top"><?php echo $reportdata[$q]['details'][$w]['onlinehours']; ?></td>
			<td valign="top"><?php echo $reportdata[$q]['details'][$w]['shortagehours']; ?></td>-->
			<td valign="top"><?php echo substr($reportdata[$q]['details'][$w]['presenthours'],0,-3)." ( ".$presentdays." )"; ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['details'][$w]['actualhours'],0,-3); ?></td>
			<td valign="top"><?php echo substr($reportdata[$q]['details'][$w]['billablehours'],0,-3); ?></td>
		</tr>	
		<?php
			}
		}
		?>	
<tr>
	<td width="10%" align="right" colspan="3"><b>Total</b></td>
	<td width="8%" align="center"><b><?php echo $totpresentdays; ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($actualhourssarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($billablehoursarr); ?></b></td>
</tr>
	<?php if($kpis=='1'){ ?>			
	<?php 
			$perdaymin = $dbase->getminutes("08:30");
			$util = (($dbase->addTime($billablehoursarr,true) / ($totpresentdays*$perdaymin))*100);
	?>
<tr>
	<td width="10%" align="right" colspan="3"><b>Utilization</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>	
<?php } ?>
	<?php if($kpis=='2'){ ?>			
	<?php 
			$util = ($dbase->addTime($actualhourssarr,true) / ($dbase->addTime($billablehoursarr,true)))*100;
	?>
<tr>
	<td width="10%" align="right" colspan="3"><b>Efficiency</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"></td>
</tr>	
<?php } ?>		
	<?php if($kpis=='3'){ ?>			
	<?php 
			$dedicatedbilledhrsarr = array();
			// ($dbase->addTime($billablehoursarr,true)
			//$tot_emp = count(array_unique($emp_array));
			$tot_emp = $totempcnt;
			$dedicatedbilledhrsarr[] = ($workingdayscnt*$tot_emp*DEDICATED_WORKING_HOURS);
			$util = ($dbase->addTime($actualhourssarr,true)/$dbase->addTime($dedicatedbilledhrsarr,true))*100;
	?>
<tr>
	<td width="10%" align="right" colspan="3"><b>Utilization</b></td>
	<td width="8%" align="center"><?php echo $workingdayscnt." * ".$tot_emp." * ".DEDICATED_WORKING_HOURS; ?></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"></td>
</tr>	
<?php } ?>
	<?php if($kpis=='4'){ ?>			
	<?php 
			$perdaymin = $dbase->getminutes("08:30");
			$util = (($dbase->addTime($billablehoursarr,true) / ($totpresentdays*$perdaymin))*100);
	?>
<tr>
	<td width="10%" align="right" colspan="3"><b>Utilization</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>	
<?php } ?>
	<?php if($kpis=='5'){ ?>			
	<?php 
			$perdaymin = $dbase->getminutes("08:30");
			$util = (($dbase->addTime($billablehoursarr,true) / ($totpresentdays*$perdaymin))*100);
	?>
<tr>
	<td width="10%" align="right" colspan="3"><b>Utilization</b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo round($util,2)."%"; ?></b></td>
	<td width="8%" align="center"><b></b></td>
</tr>	
<?php } ?>				
	   </table>
		
	<?php } ?>		
		</td>
	</tr>
</table>
</form>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
 $('#from_date').datepicker({
	inline: true,
	dateFormat: 'dd-M-yy',
	maxDate:0,
	changeMonth: true,
	changeYear: true,
	yearRange: "-10:+0",
	onSelect: function(date){
		var dates = date.split('-');
		var month1 = dates[1].toLowerCase();
		var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
		month1 = months.indexOf(month1);
		var lastDate = new Date(dates[2], month1+1, 0);
		var y = lastDate.getFullYear(), m = lastDate.getMonth(), d = lastDate.getDate();
		m = (''+ (m+1)).slice(-2);
		var monname = months[m-1];
		var monthname = monname.charAt(0).toUpperCase() + monname.slice(1);
		$('#to_date').val(d+'-'+monthname+'-'+y);
	}	 
});
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});
			getsubdepartment();
			//getpirdropdown();
			
			$("#excelreport").val('0');
	 $(".chosen-select").chosen();
	 $(".chosen-select-multi").chosen();
	 autoselectoption("#department_id"); 
		$('.chosen-select-multi').change(function () {
			if($(".chosen-select-multi option:selected").val()==""){
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").attr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			else{
				$('#subdepartment_id > option').each(function() {
					if($(this).val() != ''){
						$(".chosen-select-multi option[value='"+$(this).val()+"']").removeAttr('disabled',true).trigger("chosen:updated");
					}
				});
			}
			getpirdropdown();
		});	 
	});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = '<?php echo $subdepartment_ids;?>';
	$.get("getsubdepartmentadmin.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
		if(id != ''){
			$("#subdepartment_id").trigger("chosen:updated");
			//$("#subdepartment_id").trigger('chosen:open');
		}
		getpirdropdown();
	});
}
function getexcel(){
	$("#excelreport").val('1');
	$("#frm_details").submit();
	$("#excelreport").val('0');
}
function getpirdropdown(){
	var deptid = $("#department_id").val();
	var supdeptid = $("#subdepartment_id").val();
	var pirmaster_id = '<?php echo $pirmaster_id;?>';
	$.get("getpiradmin.php?deptid="+deptid+"&supdeptid="+supdeptid+"&pirmaster_id="+pirmaster_id+"&t=1",function(data){
		$("#pirmaster_id").html(data);
		$("#pirmaster_id").trigger("chosen:updated");
		getproject();
	});
}
function getproject(){
	var id = $("#pirmaster_id").val();
	var selid = '<?php echo $project_id;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#project_id").html(data);
		//$("#project_id").trigger("chosen:updated");
	});
}

function submitform(){
	$("#frm_details").submit();
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-All-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}	
</script>
</html>
<?php } ?>

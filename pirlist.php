<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$pirno = $_POST['pirno'];
		$contracttype = $_POST['contracttype'];
		$noofresource = $_POST['noofresource'];
		$is_rework = 0;
		if(isset($_POST['is_rework'])){
			if($_POST['is_rework']  == 1){
				$is_rework = $_POST['is_rework'];
			}
		}
		
		$is_internalpir = 0;
		if(isset($_POST['is_internalpir'])){
			if($_POST['is_internalpir']  == 1){
				$is_internalpir = $_POST['is_internalpir'];
			}
		}		
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM pirlist WHERE  `department_id`=(SELECT department_id FROM pirlist WHERE id='".$editid."') 
		AND `subdepartment_id`=(SELECT subdepartment_id FROM pirlist WHERE id='".$editid."') AND pirno='".trim($pirno)."'".$idcond;//echo $check_query;exit;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: pirlist.php?m=2');
			exit();				
		}
		else{
			if($editid !=''){
				 $pirquery = "UPDATE pirlist SET `pirno` = '".$pirno."',`contracttype`='".$contracttype."',`noofresource`='".$noofresource."' ,`is_rework`='".$is_rework."',`is_internalpir`='".$is_internalpir."' WHERE `id` ='".$editid."'";
				
				$dbase->executeNonQuery($pirquery);
				header('Location: pirlist.php?m=1');
				exit();	
			 }
						
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$pirquery = "UPDATE pirlist SET `isActive` = '0'  WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($pirquery);
				header('Location: pirlist.php?m=3');
				exit();				
		}
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		$pir_query = "SELECT *,(SELECT `clientname` FROM `clientlist` WHERE `id`=p.client_id) as clientname,
		(SELECT `name` FROM `department` WHERE `id`=p.department_id) as dname,
		(SELECT `subname` FROM `subdepartment` WHERE `id`=p.department_id) as sdname
		FROM pirlist as p WHERE p.`id`='".trim($id)."'";
		$pir_result = $dbase->executeQuery($pir_query,"single");		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
	    <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		table#pirtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#pirtable td, table#pirtable th {
			border: 1px solid black;
		}	
		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php// include("adminmenu.php"); ?>
	<?php include("userrolemenu.php"); ?>
</td>
	<?php if(isset($_GET['id'])){?>
<td align="center" width="80%" valign="top">
<form id="frm_pir" action="" method="POST" enctype="multipart/form-data">
<table id="pirtable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "PIR No Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "PIR No Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> PIR No</b></td></tr>		
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Department</b></td>
	<td height="40px" align="left" style="padding-left:10px"><?php echo $pir_result['dname'];?></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px"><?php echo $pir_result['sdname'];?></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client</b></td>
	<td height="40px" align="left" style="padding-left:10px"><?php echo $pir_result['clientname'];?></td>
</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>PIR No</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="pirno" id="pirno" value="<?php echo $pir_result['pirno']; ?>" class="required alphanumericpir" /></td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Is rework</b></td>
	<td>
		<input type="radio" name="is_rework" value="1" class="is_rework" style="display:inline-block"  <?php if($pir_result['is_rework'] == '1'){?>checked<?php } ?> >Yes 
		<input type="radio" name="is_rework" class="is_rework " value="0" style="display:inline-block"  <?php if($pir_result['is_rework'] != '1'){?>checked<?php } ?>>No
	</td>

</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Is Internal PIR</b></td>
	<td>
		<input type="radio" name="is_internalpir" value="1" class="is_internalpir" style="display:inline-block"  <?php if($pir_result['is_internalpir'] == '1'){?>checked<?php } ?> >Yes 
		<input type="radio" name="is_internalpir" value="0" class="is_internalpir" style="display:inline-block"  <?php if($pir_result['is_internalpir'] != '1'){?>checked<?php } ?>>No
	</td>

</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Contract Type</b></td>
	<td height="40px" align="left" style="padding-left:10px"> 
		<input class="radioBtn" type="radio" name="contracttype" value="0" <?php if($pir_result['contracttype']=='0'){ ?> checked <?php } ?> <?php if($id==''){ ?> checked <?php } ?>> T&M
		<input class="radioBtn" type="radio" name="contracttype" value="1" <?php if($pir_result['contracttype']=='1'){ ?> checked <?php } ?>> Fixed Price
		<input class="radioBtn" type="radio" name="contracttype" value="2" <?php if($pir_result['contracttype']=='2'){ ?> checked <?php } ?>> Dedicated Team
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>No of Resource</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="noofresource" id="noofresource" value="<?php echo $pir_result['noofresource']; ?>" class="number" /></td>
</tr>
	
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
			<?php }?>
	<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
	<?php }?>
<td align="center" width="80%" valign="top">
	<br>
	
		<?php if($_GET['m']=='1'){ ?>
	<b style="color:red;"><?php  echo "Updated Successfully"; ?></b>
	<?php }	?>
		<?php if($_GET['m']=='2'){ ?>
	<b style="color:red;"><?php  echo "PIR No Already Exists"; ?></b>
	<?php }	?>
		<?php if($_GET['m']=='3'){ ?>
	<b style="color:red;"><?php  echo "PIR No Deleted Successfully"; ?></b>
	<?php }	?>
	<table id="pirlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="25%" align="center"><b>Department</b></td>
			<td width="25%" align="center"><b>Sub Department Name</b></td>			
			<td width="25%" align="center"><b>PIR No</b></td>
			<td width="25%" align="center"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$pir_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$pir_cond = " AND p.department_id='".$_SESSION['timesheet']['DEPART']."' AND p.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$pirQuery = "SELECT p.id,p.pirno,(SELECT d.name FROM department d WHERE d.id=p.department_id) as deptname,(SELECT s.subname FROM subdepartment s WHERE s.id=p.subdepartment_id) as subdeptname FROM pirlist p WHERE p.isActive='1'".$pir_cond;
		$pirResult = $dbase->executeQuery($pirQuery,"multiple");
		for($i=0;$i<count($pirResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['pirno'];?></td>
			<td align="left" style="padding-left:10px"><?php if($_SESSION['timesheet']['ROLEID'] == SUPERADMIN_ROLE_ID || $_SESSION['timesheet']['ROLEID'] == ADMIN_ROLE_ID){?><a href="pirlist.php?id=<?php echo $pirResult[$i]['id'];?>">Edit</a> | <?php }?><a href="pirlist.php?del=1&id=<?php echo $pirResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
                <th>Department</th>
                <th>Sub Department</th>
				<th>PIR No</th>
                <th>Action</th>
            </tr>
        </tfoot>			
	</table>	
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
jQuery.validator.addMethod("alphanumericpir", function(value, element) {
 return this.optional( element ) || /^[0-9]{4}[A-Z]{0,1}$/i.test(value);
}, 'Please enter only 4 numbers and 1 alphabet letter');	
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
jQuery.validator.addMethod("alphanumericspace", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');		
	  $("#frm_pir").validate();			
    // Setup - add a text input to each footer cell
    $('#pirlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#pirlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	$(".radioBtn").click(function() {
		$("#noofresource").attr("disabled", true);
		if ($("input[name=contracttype]:checked").val() == "2") {
			$("#noofresource").attr("disabled", false);
			$("#noofresource").select().focus();
		}
	});
	
	
	$(".chosen-select").chosen();
	
		
	autoselectoption("#department_id");
<?php if($id!=''){ ?>
	
	if ($("input[name=contracttype]:checked").val() == "2") {
		$("#noofresource").removeAttr("disabled");
	}	
<?php } else{?>
	$("#noofresource").attr("disabled", true);
<?php } ?>
});	


function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}	
	
</script>
</html>
<?php } ?>

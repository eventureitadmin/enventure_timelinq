<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
if($_SESSION['timesheet']['NO_TIMESHEET']=='0'){
		header("Location:dataentry.php");
		exit;		
}
if($_SESSION['timesheet']['ISADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE){
		header("Location:newkpi.php");
		exit;		
}	
	$showlogouttime = 0;
	$datafill = 0;
	$time1 = date("H:i:s");
	$timearr = explode(":",$time1);
	if($_SESSION['timesheet']['PREDATE_LOGOUT']=='1'){
		$chkpredatequery = "SELECT `ID` , `log_date` , `login_time` , `login_comments` , `logout_time` , `logout_comments` , `totalhours` FROM `time_log` WHERE `emp_id` = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND `logout_time`='0000-00-00 00:00:00' AND `totalhours`='00:00:00' AND deletestatus='0' AND log_date BETWEEN ( CURDATE() - INTERVAL 3 DAY ) AND CURDATE() ORDER BY ID DESC LIMIT 0 , 1 ";
		$chkpredateResult = $dbase->executeQuery($chkpredatequery,"single");
		if($chkpredateResult['login_time'] != '0000-00-00 00:00:00' && $chkpredateResult['logout_time'] =='0000-00-00 00:00:00'){
			$currentdate = $chkpredateResult['log_date'];
		}
		else{
			$currentdate = date('Y-m-d');
		}
	}
	else{
		$currentdate = date('Y-m-d');
	}

	if(strlen($timearr[0])==1){
		$time = "0".$timearr[0].":".$timearr[1].":".$timearr[2];
	}
	else{
		$time = $timearr[0].":".$timearr[1].":".$timearr[2];
	}
	if($_POST){
		$empid = $_POST['empid'];
		$logtype = $_POST['logtype'];
		$currentdate = $_POST['currentdate'];
		$logid = $_POST['logid'];
		$ipaddress = $_POST['ipaddress'];
		if($logtype=='1'){
			$login_time = $_POST['login_time'];
			$logouttime = $_POST['logouttime'];
			$logout_comments = $_POST['logout_comments'];	
		}
		
		if($logtype=='0'){
			$logintime = $_POST['logintime'];
			$login_comments = $_POST['login_comments'];
		}
		if($logid=='0'){
			$usercheck_select = "SELECT COUNT(ID) as cnt FROM `time_log` WHERE `emp_id` ='".$empid."' AND `log_date` = '".$currentdate."'";
			$usercheck_result = $dbase->executeQuery($usercheck_select,"single");
			if($usercheck_result['cnt']=='0'){
				// insert
				$loginsert = "INSERT INTO `time_log` (`ID`, `emp_id`, `log_date`, `login_time`, `login_comments`, `login_ip`) VALUES (NULL, '".$empid."', '".$currentdate."', '".$currentdate." ".$logintime."', '".$login_comments."', '".$ipaddress."');";
				$dbase->executeNonQuery($loginsert);
				header("Location:thankyou.php");
				exit;				
			}
		}
		else{
			//update
			if($logouttime != '' && $logouttime!='00:00:00'){
				$logupdate = "UPDATE `time_log` SET `logout_time`='".$currentdate." ".$logouttime."', `logout_comments`='".$logout_comments."', `logout_ip`='".$ipaddress."' WHERE ID='".$logid."'";
				$dbase->executeNonQuery($logupdate);
			}
			else{
				header("Location:index.php");
				exit;				
			}
			$tottimequery = "SELECT login_time,logout_time FROM time_log WHERE ID='".$logid."'";
			$tottimeresult = $dbase->executeQuery($tottimequery,'single');
			if($tottimeresult['logout_time'] != '' && $tottimeresult['logout_time'] != '0000-00-00 00:00:00'){
			$uptimediff = $dbase->timediff($tottimeresult['logout_time'],$tottimeresult['login_time'],false);
			$totalhours = $uptimediff['hrs'].":".$uptimediff['min'].":".$uptimediff['sec'];
			$timeupdate = "UPDATE `time_log` SET `totalhours`='".$totalhours."' WHERE ID='".$logid."'";
			$dbase->executeNonQuery($timeupdate);			
			//header("Location:login.php?logout");
			header("Location:home.php");
			exit;
			}
			else{
				header("Location:index.php");
				exit;
			}	
		}
		
	}
	$empDetQuery = "SELECT * FROM employeelist WHERE ID = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND isactive='1'";
	$empDetResult = $dbase->executeQuery($empDetQuery,"single");
	$emplogDetQuery = "SELECT `ID`,`log_date`, `login_time`, `login_comments`, `logout_time`, `logout_comments`,`totalhours` FROM time_log WHERE `emp_id`='".$empDetResult['ID']."' AND log_date='".$currentdate."'";
	$emplogDetResult = $dbase->executeQuery($emplogDetQuery,"single");
	if($emplogDetResult['login_time'] =='' || $emplogDetResult['logout_time'] =='0000-00-00 00:00:00'){
		$datafill = 0;
		if($emplogDetResult['ID'] !=''){
			//check timesheet entry 
			$timesheet_select = "SELECT COUNT( id ) AS cnt FROM `timeentry` WHERE isActive='1' AND `employee_id` = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND `entrydate` = '".$currentdate."'";
			$timesheet_Result = $dbase->executeQuery($timesheet_select,"single");
			if($timesheet_Result['cnt']=='0'){
				header("Location:dataentry.php");
				exit;				
			}
			$showlogouttime = 1;
			$logid = $emplogDetResult['ID'];
		}
		else {
			$showlogouttime = 0;
			$logid = 0;
		}		
	}
	else{
		$timearr = explode(":",$emplogDetResult['totalhours']);
		$totaltime = $timearr[0]." Hrs ".$timearr[1]." Min ".$timearr[2]." Sec";
		$datafill = 1;
	}
	if($_SESSION['timesheet']['PREDATE_LOGOUT']=='1'){
		$currentdate = date('Y-m-d');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
<script type="text/javascript">
var timetype = <?php echo TIME_TYPE; ?>;
 $(document).ready(function(){
	var time = new Date().toString("HH:mm:ss");
	if(timetype=='1'){
		updateTime();
				//$("#logintime").val(time);
				//$("#logouttime").val(time);		
	}
	if(timetype=='2'){
		setInterval(function() {
			$.get("getservertime.php",function(data){
				$("#logintime").val(data);
				$("#logouttime").val(data);
				});	
			}, 1000);
		//$("#logintime").val('<?php echo $time; ?>');
		//$("#logouttime").val('<?php echo $time; ?>');
	}
});		
</script>	
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		table#logtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#logtable td, table#logtable th {
			border: 1px solid black;
			 padding: 5px; 
		}			
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='0'){ ?>
<form id="frm_details" action="" method="post">
<table id="detailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="50%" >
<tr>
	<td width="10%"><b>Employee Name</b></td>
	<td width="22%"><?php echo $empDetResult['emp_name']; ?></td>
</tr>
<tr>
	<td width="10%"><b>Log Date</b></td>
	<td width="22%"><?php echo date('d-M-Y',strtotime($currentdate));  ?><input type="hidden" id="currentdate" name="currentdate" value="<?php echo $currentdate; ?>"/></td>
</tr>
<?php if($datafill=='0'){?>
<?php if($showlogouttime=='0'){?>
<tr>
	<td width="10%"><b>Login Time</b></td>
	<td width="22%"><input type="text" id="logintime" name="logintime" value="" readonly maxlength="8" size="8" /></td>
</tr>
<tr>
	<td width="10%" valign="top"><b>Comments</b></td>
	<td width="22%">
	<textarea id="login_comments" name="login_comments" rows="4" cols="30"></textarea></td>
</tr>
<?php } else { ?>
<tr>
	<td width="10%"><b>Login Time</b></td>
	<td width="22%"><?php echo date('d-M-Y h:i:s A',strtotime($emplogDetResult['login_time'])); ?><input type="hidden" id="login_time" name="login_time" value="<?php echo $emplogDetResult['login_time']; ?>"/></td>
</tr>
<?php } ?>
<?php if($showlogouttime=='1'){?>
<tr>
	<td width="10%"><b>Logout Time</b></td>
	<td width="22%"><input type="text" id="logouttime" name="logouttime" value="" readonly maxlength="8" size="8"  /></td>
</tr>
<tr>
	<td width="10%" valign="top"><b>Comments</b></td>
	<td width="22%">
	<textarea id="logout_comments" name="logout_comments" rows="4" cols="30"></textarea></td>
</tr>
<?php } ?>
<?php } else {?>
<tr>
	<td width="10%"><b>Login Time</b></td>
	<td width="22%"><?php echo date('d-M-Y h:i:s A',strtotime($emplogDetResult['login_time'])); ?></td>
</tr>
<tr>
	<td width="10%" valign="top"><b>Comments</b></td>
	<td width="22%">
	<?php echo $emplogDetResult['login_comments'];?></td>
</tr>

<tr>
	<td width="10%"><b>Logout Time</b></td>
	<td width="22%"><?php echo date('d-M-Y h:i:s A',strtotime($emplogDetResult['logout_time'])); ?></td>
</tr>
<tr>
	<td width="10%" valign="top"><b>Comments</b></td>
	<td width="22%">
	<?php echo $emplogDetResult['logout_comments']; ?></td>
</tr>
<tr>
	<td width="10%" valign="top"><b>Total Timings</b></td>
	<td width="22%">
	<?php echo "<b>".$totaltime."</b>"; ?></td>
</tr>
<?php } ?>
<tr>
	<td colspan="2" align="center">
	<?php if($datafill=='0'){?>
		<input type="hidden" id="empid" name="empid" value="<?php echo $empDetResult['ID']; ?>"/>
		<input type="hidden" id="logtype" name="logtype" value="<?php echo $showlogouttime; ?>"/>
		<input type="hidden" id="logid" name="logid" value="<?php echo $logid; ?>"/>
		<input type="hidden" id="ipaddress" name="ipaddress" value="<?php echo $dbase->get_client_ip(); ?>"/>
		<?php if($showlogouttime=='0'){?>
			<input type="button" id="loginbutton" name="loginbutton" value=" Start my Day " class="button button1 loginconfirm" />
		<?php } else {?>
			<input type="button" id="logoutbutton" name="logoutbutton" value=" End my Day " class="button button1 logoutconfirm" />
		<?php } ?>
		
	<?php } else { echo "<b>Time Log has been entered for the current date</b>"; } ?>
	</td>
</tr>
</table>
</form>
<br/>
<br/>
<table id="logtable" border="0" cellpadding="0" cellspacing="0" align="center"  width="50%" style="display:none;" >
<tr>
	<td colspan="5"><b>Last 7 Days Log Entry</b></td>
</tr>
<tr>
	<td width="16%"><b>Date</b></td>
	<td width="16%"><b>Day</b></td>
	<td width="16%"><b>Login Time</b></td>
	<td width="16%"><b>Logout Time</b></td>
	<td width="16%"><b>Total Hours</b></td>
</tr>
<?php
$html = '';
for($i=1; $i<=7; $i++){
$entrydate = date('Y-m-d',strtotime("-$i day"));
$datevalue = date('d-M-Y',strtotime($entrydate));
$dayvalue = date('l',strtotime($entrydate));
	$html .= '<tr>
	<td>'.$datevalue.'</td>
	<td>'.$dayvalue.'</td>';
	$emplogDetailQuery = "SELECT `ID`,IFNULL( DATE_FORMAT( `login_time` , '%d-%b-%Y %h:%i:%s %p' ) , 0 ) AS login_time, IFNULL( DATE_FORMAT( `logout_time` , '%d-%b-%Y %h:%i:%s %p' ) , 0 ) AS logout_time FROM time_log WHERE `emp_id`='".$_SESSION['timesheet']['ID']."' AND log_date='".$entrydate."'";
	$emplogDetailResult = $dbase->executeQuery($emplogDetailQuery,"single");
if($emplogDetailResult['ID']!= ''){
	$html .= '<td>'.substr($emplogDetailResult['login_time'],11).'</td>
	<td>'.substr($emplogDetailResult['logout_time'],11).'</td>';
	if($emplogDetailResult['login_time']!= '0' && $emplogDetailResult['logout_time']!= '0'){
	$html .= '<td>'.$dbase->timediff($emplogDetailResult['logout_time'],$emplogDetailResult['login_time'],true).'</td>';	
	}
	else{
	$html .= '<td></td>';		
	}
}	
else{
	$html .= '<td colspan="3" align="center">No data available</td>';	
}

	$html .= '</tr>';
}
echo $html;
?>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
	  $("#frm_details").validate();	
		$(".logoutconfirm").click(function() {
			var currentdate = $("#currentdate").val();
			var login_time = $("#login_time").val();
			var logouttime = $("#logouttime").val();
			var logout_time = currentdate+" "+logouttime;
				 $.post("checktotaltime.php",{login_time:login_time,logout_time:logout_time},function(data){
						if(data=='disallow'){
								$('<div>You have not completed the minimum time required time in office<br/>Are you sure wants to end your day ?</div>').dialog({
									resizable: false,
									open: function(){
									 $(".ui-dialog-title").html("Alert");
									},				
										buttons: {
										"Yes": function() {
											if ($.trim($('#logout_comments').val()).length < 1) {							
												$(this).dialog("close");
												$('<div>Logout Comments is Empty </br> Please fill the comments!</div>').dialog({
													resizable: false,
													open: function(){
													 $(".ui-dialog-title").html("Alert");
													},				
														buttons: {
														"Ok": function() {
															$(this).dialog("close");
															location.reload();
															//$('#logout_comments').focus();
														},
														Cancel: function() {
															$( "div" ).remove( ".ui-widget-overlay" );	
															$(this).dialog("close");
														}
													}
												});	
												$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
											}
											else{
												$(this).dialog("close");
												$("#frm_details").submit();
											}
										},
										Cancel: function() {
											location.reload();
											$(this).dialog("close");
										}
									}
								});
								$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
						}
						if(data=='allow'){
							$('<div>Do you want to Logout ?</div>').dialog({
								resizable: false,
								open: function(){
								 $(".ui-dialog-title").html("Please Confirm !");
								},				
									buttons: {
									"Yes": function() {
										$(this).dialog("close");
										$("#frm_details").submit();
									},
									"No": function() {
										location.reload();
										$(this).dialog("close");
									}
								}
							});	
							$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );							
						}
				 });			
			
		});	
		//$(".loginconfirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to Login ?', button: ['No','Yes']}});
		$(".loginconfirm").click(function() {
			var logintime = $("#logintime").val();
			$.post("checktime.php",{logintime:logintime},function(data){
				if(data=='showmsg'){
						$('<div>Oops!! You are late to office. Please make sure you are early tomorrow.</br> Do you want to Login ?</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Yes": function() {
									$("#frm_details").submit();
									$(this).dialog("close");
								},
								"No": function() {
									location.reload();
									$(this).dialog("close");
								}
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				}
				if(data=='nomsg'){
						$('<div>Do you want to Login ?</div>').dialog({
							resizable: false,
							open: function(){
							 $(".ui-dialog-title").html("Alert");
							},				
								buttons: {
								"Yes": function() {
									$("#frm_details").submit();
									$(this).dialog("close");

								},
								"No": function() {
									location.reload();
									$(this).dialog("close");
								}
							}
						});	
						$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				}				
			});
		});	
	});	
 	function updateTime(type){
		var time1 = new Date().toString("HH:mm:ss");
		console.log(type+" - "+time1);
		$("#logintime").val(time1);
		$("#logouttime").val(time1);			
		setTimeout(updateTime(type), 1000);
	}
function display_c(){
var refresh=1000; // Refresh rate in milli seconds
mytime=setTimeout('updateTime()',refresh)
}

function updateTime() {
var time1 = new Date().toString("HH:mm:ss");
$("#logintime").val(time1);
$("#logouttime").val(time1);
tt=display_c();
 }	
</script>
</html>
<?php } ?>

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	$html = "";
	function getquaterval($qindex,$qval){
		
		if($qindex== 3){
			$qindex= 0;
		}
		if($qindex ==2){$qval++;}
		$resarr["qindex"]= $qindex;
		$resarr["qval"]= $qval;
		return $resarr;
	}
	function getkpihtml($index,$month,$data,$qindex,$qval,$ispreviousfy){
		$html1 ="";$quarterval="";$startindex=0;
		if($qindex == 2){
			$quarterval ="Q".$qval;
		}else if($qindex == 1){
			$startindex = $index;
			$style ="border-top:2px solid #286090";
		}else if($qindex == 3){
				$startindex = $index-2;
			$style ="border-bottom:2px solid #286090";
			
		}
		if($data['is_lock'] == 1){
			$disabled ="disabled";
			$isread ="1";
		}else{
			$disabled = "";
			$isread ="0";
		}
				
				$html.='<tr id="rowid_'.$index.'" style="'.$style.'">';
				$html.='<input type="hidden" name="kpiid[]" id="kpiid_'.$index.'" value="'.$data["id"].'" class="kpiid">';
				$html.='<input type="hidden" name="monthid[]" id="monthid_'.$index.'" value="'.$data['month'].'" class="monthid">';
				$html .='<input type="hidden" name="year[]" id="year_'.$index.'" value="'.$data['year'].'" class="form-control input-sm kpiyear">';
			    $html .='<td style="width:60px;color:#286090"><b>'.$quarterval.'</b></td>';
				$html.='<td class="monthname">'.$data['mname'].'</td>';
			
				$html.='<td><input type="text" name="revenue_target[]" id="revenue_target_'.$index.'" value="'.$data['revenue_target'].'" class="form-control input-sm numchk kpirt required" onchange="validateval($(this))"'.$disabled.'></td>';
				$html.='<td><input type="text" name="achieved[]" id="achieved_'.$index.'" value="'.$data['achieved'].'" class="form-control input-sm numchk kpiachieved required" onchange="validateval($(this))" '.$disabled.'></td>';
				$html.='<td><input type="text" name="month_nps[]" id="month_nps_'.$index.'" value="'.$data['month_nps'].'" class="form-control input-sm npschk kpimnps required" onchange="validateval($(this),'.$index.')"'.$disabled.'></td>';
		if($qindex == 3){
				$html.='<td><input type="text" name="quarter_nps[]" id="quarter_nps_'.$index.'" value="'.$data['quarter_nps'].'" class="form-control input-sm npschk kpiqnps required" onchange="validateval($(this))"'.$disabled.'></td>';
		}else{$html .= '<td><input type="hidden" name="quarter_nps[]" id="quarter_nps_'.$index.'" value="0"></td>';}
	
		$html.='<td><img src="images/saveicon.jpeg" width="25px" height="20px" onclick="savedata('.$index.','.$isread.')">';
		if($qindex == 3 && $data['is_lock'] ==0){
		$html.=' <img src="images/lockimg.jpeg" width="25px" height="20px" onclick="lockdata('.$startindex.','.$index.','.$isread.')"'.$disabled.'>';
		}
		$html.='</td>';
		
				$html.='</tr>';
		return $html;
			
	}
	if(count($_POST) > 0 && $_POST['fyyear'] != "" && $_POST['fyyear'] >0){
		$montharr = $master->getmonths();
		$fyyear = explode("-",$_POST['fyyear']);//print_r($fyyear);
	    $ispreviousfy = $master->checkprefinancialyear(intval($fyyear[0]));
	    $iscurrenty = $master->checkprefinancialyear(intval($fyyear[1]));
		$html ='';
		if(!$ispreviousfy){
		$html.='<input type="hidden" name="isreadonly" id="isreadonly" value="0">';
		}else{
		$html.='<input type="hidden" name="isreadonly" id="isreadonly" value="1">';
		}
		$frommonth = 4;
		$tomonth = 3;$qval=1;
		$dept_id=1;$k=0;$data= array();$qindex=0;
		$isunlock = 0;
		for($s=$frommonth;$s<=12;$s++){
			$qarr = getquaterval($qindex,$qval);
			$qindex = $qarr["qindex"];
			$qval = $qarr["qval"];
			$qindex++;
		
			$result = $master->getkpidata($s,$fyyear[0],$dept_id);
			if($result !=""){
				$data["id"]= $result["id"];
				$data["month"] = $result["month"];
				$data["mname"]=$montharr[$s]."-".substr($fyyear[0],-2);
				$data["year"]= $fyyear[0];
				$data["revenue_target"]= $result["revenue_target"];
				$data["achieved"]=$result["achieved"];
				$data["month_nps"]=$result["month_nps"];
				$data["quarter_nps"]=$result["quarter_nps"];
				$data["is_lock"]=$result["is_lock"];
		
			}else{
				$data["id"] = 0;
				$data["month"] =$s;
				$data["mname"]= $montharr[$s]."-".substr($fyyear[0],-2);
				$data["year"]= $fyyear[0];
				$data["revenue_target"]="";
				$data["achieved"]="";
				$data["month_nps"]="";
				$data["quarter_nps"]="";
				$data["is_lock"]="";
				$isunlock =1;	
			}
			$html .=getkpihtml($k,$s,$data,$qindex,$qval,$ispreviousfy);
			$k++;
		}
		for($t=1;$t<=$tomonth;$t++){
			$qarr = getquaterval($qindex,$qval);
			$qindex = $qarr["qindex"];
			$qval = $qarr["qval"];
			$qindex++;
			$result = $master->getkpidata($t,$fyyear[1],$dept_id);
			if($result !=""){
				$data["id"]= $result["id"];
				$data["month"] = $result["month"];
				$data["mname"]=$montharr[$t]."-".substr($fyyear[1],-2);
				$data["year"]= $fyyear[1];
				$data["revenue_target"]= $result["revenue_target"];
				$data["achieved"]=$result["achieved"];
				$data["month_nps"]=$result["month_nps"];
				$data["quarter_nps"]=$result["quarter_nps"];
				$data["is_lock"]=$result["is_lock"];	
				
			}else{
				$data["id"] = 0;
				$data["month"] =$t;
				$data["mname"]= $montharr[$t]."-".substr($fyyear[1],-2);
				$data["year"]= $fyyear[1];
				$data["revenue_target"]="";
				$data["achieved"]="";
				$data["month_nps"]="";
				$data["quarter_nps"]="";
				$data["is_lock"]="";
				$isunlock =1;
			}
			$html .=getkpihtml($k,$s,$data,$qindex,$qval,$ispreviousfy);
			$k++;
		}

		
	}
	$html .="<input type='hidden' name='isunlock' id='isunlock' value='".$isunlock."'>";
	echo $html;
}

<?php
include_once("config.php");
include_once("functions.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getquery($date,$cond,$groupcond){
		$query = "SELECT (SELECT p1.pirno FROM pirlist p1 WHERE p1.id = b.pirno) as pirno,  (SELECT p2.projectname FROM projectlist p2 WHERE p2.id = b.projectname) as projectname, (SELECT c.clientname FROM clientlist c WHERE c.id = b.clientname) as clientname, SEC_TO_TIME( SUM( b.budgetseconds ) ) AS budgethours, SEC_TO_TIME( SUM( b.actualseconds ) ) AS actualhours, SEC_TO_TIME( SUM( b.deltaseconds ) ) AS deltahours FROM (SELECT n.pirmaster_id, n.pirno, n.clientname, n.projectname, n.budgetseconds, n.actualseconds, n.deltaseconds FROM ( SELECT m . * , (m.budgetseconds - m.actualseconds) AS deltaseconds FROM (SELECT t.`pirmaster_id` , (SELECT p.pirno FROM pirmaster p WHERE p.id = t.pirmaster_id) AS pirno, (SELECT p.clientname FROM pirmaster p WHERE p.id = t.pirmaster_id) AS clientname, (SELECT p.projectname FROM pirmaster p WHERE p.id = t.pirmaster_id) AS projectname, TIME_TO_SEC( `calculatedhours` ) AS budgetseconds, TIME_TO_SEC( `actualhours` ) AS actualseconds FROM `timeentry` t WHERE t.`entrydate` = '".$date."' )m )n )b WHERE 1=1 ".$cond." GROUP BY b.pirno".$groupcond;
		return $query;
	}
	if($_POST){
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$pirno = $_POST['pirno'];
		$proid = $_POST['proid'];
		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$cond = '';
		$groupcond = '';
		if($pirno != ''){
			$cond .= " AND b.pirno='".$pirno."'";
		}
		if($proid != ''){
			$cond .= " AND b.projectname='".$proid."'";
			$groupcond .= ',b.projectname';
		}		
	}
if($_GET['fd'] != '' && $_GET['td'] != ''){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

	$fdate = $_GET['fd'];
	$tdate = $_GET['td'];
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle($dbase->dateFormatToDisplay($fdate)."-".$dbase->dateFormatToDisplay($tdate));	
$datelist = $dbase->getDateLists($fdate,$tdate);
if(count($datelist)>0){
	$row = 1;
for($j=0;$j<count($datelist);$j++){
		$col11 = "A{$row}";
		$col22 = "J{$row}";
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Report for the Date '.$dbase->dateFormatToDisplay($datelist[$j]));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('SNo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Employee Name');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Login Time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('Login Comments');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Login IP');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Logout Time');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('Logout Comments');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('Logout IP');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue('Total Hours');
		$row++;
$query = getquery($datelist[$j],$cond);
$report = $dbase->executeQuery($query,'multiple');
for($i=0;$i<count($report);$i++){
		$totaltime = '';
		//$timearr = explode(":",$report[$i]['totalhours']);
		//if($timearr[0]=='00' && $timearr[1]=='00' && $timearr[2]=='00'){
		//	$totaltime = '';
		//}
		//else{
		//	$totaltime = $timearr[0]." Hrs ".$timearr[1]." Min ".$timearr[2]." Sec";
		//}
		$intime=$report[$i]['login_time'];
		$outtime=$report[$i]['logout_time'];
		$totaltime=(strtotime($report[$i]['logout_time'])-strtotime($report[$i]['login_time']));
		$logouttime = '';
		if($report[$i]['logout_time'] != '0'){
			$logouttime = substr($report[$i]['logout_time'],11);
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue(($i+1));
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($report[$i]['emp_name']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue(substr($report[$i]['login_time'],11));
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($report[$i]['login_comments']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($report[$i]['login_ip']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($logouttime);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($report[$i]['logout_comments']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue($report[$i]['logout_ip']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue(round($totaltime/3600,2)." Hrs ");
		$row++;
}		
$row++;
}
}
	
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="export.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
<td width="100%" colspan="3" ><b>Project Level Utilization</b></td>
</tr>
<tr>
<td width="12%" ><b>From Date</b></td>
<td width="12%" ><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('d-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>To Date</b></td>
<td width="12%" ><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select PIR</b></td>
<td width="12%" ><select id="pirno" name="pirno" onchange="getproject();" >
		<option value="">-All-</option>
		<?php
		$pircond = '';
		adminpirdropdown($pircond,$pirno);
		?>
		</select></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Project</b></td>
<td width="12%" ><select id="proid" name="proid" >
		<option value="">-All-</option>
		</select></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ></td>
<td width="12%" ><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" /></td>
<td width="12%" ></td>
</tr>
</table>
</form>
</br>
<?php
if($fromdate != '' && $todate != ''){
$datelist = $dbase->getDateLists($fromdate,$todate);
if(count($datelist)>0){
for($j=0;$j<count($datelist);$j++){
$query = getquery($datelist[$j],$cond,$groupcond);
$report = $dbase->executeQuery($query,'multiple');
if(count($report) > 0){
 ?>
<table id="reporttable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
	<td width="22%" colspan="10"><b>Report for the Date <?php echo $dbase->dateFormatToDisplay($datelist[$j]); ?></b></td>
</tr>
<tr>
	<td width="16%" align="center"><b>PIR No</b></td>
	<td width="16%" align="center"><b>Client Name</b></td>
	<?php if($groupcond != ''){?>
	<td width="16%" align="center"><b>Project Name</b></td>
	<?php } ?>
	<td width="16%" align="center"><b>Budgeted Hours</b></td>
	<td width="16%" align="center"><b>Actual Hours</b></td>
	<td width="16%" align="center"><b>Delta Hours</b></td>

</tr>
<?php
for($i=0;$i<count($report);$i++){
?>
<tr>
	<td valign="top"><?php echo $report[$i]['pirno']; ?></td>
	<td valign="top"><?php echo $report[$i]['clientname']; ?></td>
	<?php if($groupcond != ''){?>
	<td valign="top"><?php echo $report[$i]['projectname']; ?></td>
	<?php } ?>
	<td valign="top"><?php echo $report[$i]['budgethours']; ?></td>
	<td valign="top"><?php echo $report[$i]['actualhours']; ?></td>
	<td valign="top"><?php
	if (strpos($report[$i]['deltahours'], '-') !== false) {
		echo "<b style='color:red;'>".$report[$i]['deltahours']."</b>";
	}
	else{
		echo $report[$i]['deltahours'];
	}
	 ?></td>
</tr>
<?php 
}
?>
</table>
<br/>
<?php 
}
}
}
}
?>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
 $('#from_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});	
		getproject();	
	});	
	
function getproject(){
	var id = $("#pirno").val();
	var selid = '<?php echo $proid;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#proid").html(data);
	});
}
function submitform(){
	$("#frm_details").submit();
}
</script>
</html>
<?php } ?>
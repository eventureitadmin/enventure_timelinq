<?php
include_once("config.php");
include_once("functions.php");
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/IOFactory.php';
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function getquery($date,$cond,$group,$subdeptcond,$intcond,$partlinqcondn){
		if(strtotime($date)<strtotime(date('2021-07-01'))){
			$availhrs = NON_DEDICATED_PRESENT_HOURS_BEFORE;
		}
		else{
			$availhrs = NON_DEDICATED_PRESENT_HOURS_AFTER;
		}		
	/*	$query = "SELECT m.empid,m.empname,m.deptid,m.edate,m.employee_id,m.project_id,m.pirmaster_id,m.totalparts,(SELECT d1.name FROM department d1 WHERE d1.id=m.deptid) as dept,m.subdept,m.entrydate,SEC_TO_TIME(m.actualseconds) as actualhours,SEC_TO_TIME(m.calculatedseconds) as billablehours,SEC_TO_TIME(m.onlineseconds) as onlinehours,SEC_TO_TIME((m.onlineseconds - m.dayseconds)) as shortagehours,CONCAT(ROUND(((m.calculatedseconds/m.minimumseconds)*100),2),'') as utilization,CONCAT(ROUND(((m.calculatedseconds/m.actualseconds)*100),2),'') as efficiency FROM (SELECT DATE_FORMAT(t1.`entrydate` , '%d-%b-%Y' ) as entrydate,t1.`entrydate` as edate , t1.`employee_id`, t1.`project_id`, t1.`pirmaster_id`,t1.`activity_id`,(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id=t1.employee_id) as empname,(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid, (SELECT u1.department_ids FROM employeelist u1 WHERE u1.id=t1.employee_id) as deptid, (SELECT u2.subdepartment_ids FROM employeelist u2 WHERE u2.id=t1.employee_id) as subdept, SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) as onlineseconds,TIME_TO_SEC('08:30:00') AS minimumseconds,TIME_TO_SEC('09:30:00') AS dayseconds,SUM(t1.totalparts) as totalparts,t2.department_id,t2.subdepartment_id FROM `timeentry` t1, pirmaster t2  WHERE 1=1 AND t1.is_rework='0' AND t1.is_internalpir='0' AND t2.id=t1.pirmaster_id ".$subdeptcond." AND t1.isActive='1' AND t1.`entrydate`='".$date."' ".$group.") m WHERE 1=1".$cond;*/		
		$query = "SELECT m.empid,m.empname,m.deptid,m.edate,m.employee_id,m.project_id,m.pirmaster_id,m.totalparts,(SELECT d1.name FROM department d1 WHERE d1.id=m.deptid) as dept,m.subdept,m.entrydate,SEC_TO_TIME(m.actualseconds) as actualhours,SEC_TO_TIME(m.calculatedseconds) as billablehours,SEC_TO_TIME(m.onlineseconds) as onlinehours,SEC_TO_TIME(m.minimumseconds) as presenthours,m.static_availhrs,SEC_TO_TIME((m.onlineseconds - m.dayseconds)) as shortagehours,CONCAT(ROUND(((m.calculatedseconds/m.minimumseconds)*100),2),'') as utilization,CONCAT(ROUND(((m.calculatedseconds/m.actualseconds)*100),2),'') as efficiency FROM (SELECT DATE_FORMAT(t1.`entrydate` , '%d-%b-%Y' ) as entrydate,t1.`entrydate` as edate , t1.`employee_id`, t1.`project_id`, t1.`pirmaster_id`,t1.`activity_id`,(SELECT u3.emp_name FROM employeelist u3 WHERE u3.id=t1.employee_id) as empname,(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid, (SELECT u1.department_ids FROM employeelist u1 WHERE u1.id=t1.employee_id) as deptid, (SELECT u2.subdepartment_ids FROM employeelist u2 WHERE u2.id=t1.employee_id) as subdept, SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) as onlineseconds,(CASE WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) <= TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('04:15:00') WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) > TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('".$availhrs."') ELSE TIME_TO_SEC('".$availhrs."') END) AS minimumseconds,'".$availhrs."' AS static_availhrs,TIME_TO_SEC('09:30:00') AS dayseconds,SUM(t1.totalparts) as totalparts,t2.department_id,t2.subdepartment_id FROM `timeentry` t1, pirmaster t2  WHERE 1=1 ".$intcond.$partlinqcondn." AND t2.id=t1.pirmaster_id ".$subdeptcond." AND t1.isActive='1' AND t1.`entrydate`='".$date."' ".$group.") m WHERE 1=1".$cond;
		return $query;
	}
	
	function getdetailquery($entereddate,$emp_id,$project_id,$subdeptcond,$activity_id,$withinterpir,$is_partlinqdata){
		if($project_id!= ''){
			$procond = " AND t.project_id='".$project_id."'";
		}
		else{
			$procond = "";
		}
		if($activity_id != ''){
			$activity_cond = " AND t.`activity_id` = '".$activity_id."'";
		}
		else{
			$activity_cond = "";
		}
			if($withinterpir=='1'){
				$intcond1 .= "AND t.is_internalpir='1'"; 
			}
			else{
				$intcond1 .= "AND t.is_internalpir='0'";
			}
		if($is_partlinqdata == '1'){
			$intcond1 .= " AND t.mode ='1'";
		}
		$query = "SELECT n.id,n.activity,n.totalparts,n.calculatedhours,n.actualhours,(SELECT p3.pirno FROM pirlist p3 WHERE p3.id=n.pirid) as pirno,(SELECT p4.projectname FROM projectlist p4 WHERE p4.id=n.proid) as project FROM (SELECT m.id,m.activity,m.totalparts,m.calculatedhours,m.actualhours,(SELECT p1.pirno FROM pirmaster p1 WHERE p1.id = m.`pirmaster_id`) as pirid,(SELECT p2.projectname FROM pirmaster p2 WHERE p2.id = m.`pirmaster_id`) as proid FROM (SELECT t.`id` , t.`pirmaster_id` , t.`activity_id` , (SELECT a.name FROM activity a WHERE a.id = t.activity_id) AS activity, t.`totalparts` , t.`calculatedhours` , t.`actualhours`,t2.department_id,t2.subdepartment_id FROM `timeentry` t, pirmaster t2 WHERE 1=1 AND t.is_rework = '0' ".$intcond1." AND t2.id=t.pirmaster_id ".$subdeptcond." AND t.`isActive` = '1' AND t.`entrydate` = '".$entereddate."' AND t.`employee_id` = '".$emp_id."' ".$procond." ".$activity_cond.") m ) n ORDER BY n.id ASC";
		
		return $query;
	}
	if($_POST){
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		$empid = $_POST['empid'];
		$deptid = $_POST['department_id'];
		$subdeptid = $_POST['subdepartment_id'];
		$pirmaster_id = $_POST['pirmaster_id'];
		$project_id = $_POST['project_id'];
		$activity_id = $_POST['activity_id'];
		$excelreport = $_POST['excelreport'];
		$activityexcelreport = $_POST['activityexcelreport'];
		$withdetails = $_POST['withdetails'];
		$withinterpir = $_POST['withinterpir'];
		$is_partlinqdata = $_POST['is_partlinqdata'];
		$fromdate = date('Y-m-d',strtotime($from_date));
		$todate = date('Y-m-d',strtotime($to_date));
		$cond = '';
		$procond='';
		if($empid != ''){
			$cond .= " AND m.employee_id='".$empid."'";
		}
		else{
		if($_SESSION['timesheet']['IS_TEAMIDS']=='1'){
			$cond .= " AND m.employee_id IN (".$_SESSION['timesheet']['TEAMIDS_CSV'].")";
		}			
		}
		if($project_id != ''){
			$cond .= " AND m.project_id='".$project_id."'";
			$procond = " AND projectname='".$project_id."'";
		}		
		if($pirmaster_id != '' && $project_id != ''){
			$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'".$procond;
			$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
			$pirmasterid=$pirmasterdet['id'];			
			//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
			$group = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
		}
		elseif($pirmaster_id != ''){
			$pirmaster_select_cnt = "SELECT COUNT(id) as pircnt FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
			$pirmastercntdet = $dbase->executeQuery($pirmaster_select_cnt,'single');
			if($pirmastercntdet['pircnt']==1){
				$pirmaster_select = "SELECT id FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasterid=$pirmasterdet['id'];			
				//$cond .= " AND m.pirmaster_id='".$pirmasterid."'";
				$group = "AND  t1.`pirmaster_id`='".$pirmasterid."' GROUP BY t1.`pirmaster_id`, t1.`employee_id`";				
			}
			else{
				$pirmaster_select = "SELECT GROUP_CONCAT(id) as pircsv FROM pirmaster WHERE 1=1 AND pirno='".$pirmaster_id."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasteridcsv = $pirmasterdet['pircsv'];
				$group = "AND  t1.`pirmaster_id` IN (".$pirmasteridcsv.") GROUP BY t1.`pirmaster_id`, t1.`employee_id`";
			}
		}		
		else{
			$group = " GROUP BY t1.`employee_id`";
		}		
		
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		else{
			if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
					$subdeptcond = " AND t2.department_id = '".$_SESSION['timesheet']['DEPART']."'";
			}
		}
		if($subdeptid != ''){
			$subdeptcond = " AND t2.subdepartment_id ='".$subdeptid."'";
		}
		else{
			if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
				$subdeptcond = " AND t2.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
			}
		}
		
		//activity cond
		if($activity_id != ''){
			$cond .= " AND  m.activity_id='".$activity_id."'";
		}
		
		
		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){
			$reportdata = '';
		for($j=0;$j<count($datelist);$j++){
			$intcond = "AND t1.is_rework='0'";
			if($withinterpir=='1'){
				$intcond .= "AND t1.is_internalpir='1'"; 
			}
			else{
				$intcond .= "AND t1.is_internalpir='0'";
			}
			if($is_partlinqdata == '1'){
				$partlinqcondn = "AND t1.mode='1'"; 
			}
			else{
				$partlinqcondn = "";
			}
			$query = getquery($datelist[$j],$cond,$group,$subdeptcond,$intcond,$partlinqcondn);
			$report = $dbase->executeQuery($query,'multiple');
			for($i=0;$i<count($report);$i++){
				if($subdeptid != ''){
					$subdisplay = " AND sd.id = ".$subdeptid."";
				}
				else{
					$subdisplay = " AND sd.id IN (".$report[$i]['subdept'].")";
				}
				$subdeptQuery = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay;
				$subdeptResult = $dbase->executeQuery($subdeptQuery,"multiple");
				$subdeptstr = '';
				for($k=0;$k<count($subdeptResult);$k++){
					$subdeptstr .= $subdeptResult[$k]['subname'].",";
				}
				$subdeptstr = substr($subdeptstr,0,-1);	
				$report[$i]['subdept'] = $subdeptstr;
			}
			$reportdata[] = $report;
			if($withdetails==''){
			$intcond1 = "AND t1.is_rework='1'";
			if($withinterpir=='1'){
				$intcond1 .= "AND t1.is_internalpir='1'"; 
			}
			else{
				$intcond1 .= "AND t1.is_internalpir='0'";
			}
			if($is_partlinqdata=='1'){
				$partlinqcondn = "AND t1.mode='1'"; 
			}else{
				$partlinqcondn="";
			}
			
			$query1 = getquery($datelist[$j],$cond,$group,$subdeptcond,$intcond1,$partlinqcondn);
			$report1 = $dbase->executeQuery($query1,'multiple');
			for($f=0;$f<count($report1);$f++){
				if($subdeptid != ''){
					$subdisplay1 = " AND sd.id = ".$subdeptid."";
				}
				else{
					$subdisplay1 = " AND sd.id IN (".$report1[$f]['subdept'].")";
				}
				$subdeptQuery1 = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay1;
				$subdeptResult1 = $dbase->executeQuery($subdeptQuery1,"multiple");
				$subdeptstr1 = '';
				for($k=0;$k<count($subdeptResult1);$k++){
					$subdeptstr1 .= $subdeptResult1[$k]['subname'].",";
				}
				$subdeptstr1 = substr($subdeptstr1,0,-1);	
				$report1[$f]['subdept'] = $subdeptstr1;
			}
			$reportdata[$j]['reworkdetails'] = $report1;
			
			$intcond2 = "AND t1.is_rework='0' AND t1.is_internalpir='1'";
			$query2 = getquery($datelist[$j],$cond,$group,$subdeptcond,$intcond2);
			$report2 = $dbase->executeQuery($query2,'multiple');
			for($g=0;$g<count($report2);$g++){
				if($subdeptid != ''){
					$subdisplay2 = " AND sd.id = ".$subdeptid."";
				}
				else{
					$subdisplay2 = " AND sd.id IN (".$report2[$g]['subdept'].")";
				}
				$subdeptQuery2 = "SELECT subname FROM subdepartment sd WHERE 1=1".$subdisplay2;
				$subdeptResult2 = $dbase->executeQuery($subdeptQuery2,"multiple");
				$subdeptstr2 = '';
				for($k=0;$k<count($subdeptResult2);$k++){
					$subdeptstr2 .= $subdeptResult2[$k]['subname'].",";
				}
				$subdeptstr2 = substr($subdeptstr2,0,-1);	
				$report2[$g]['subdept'] = $subdeptstr2;
			}
			$reportdata[$j]['internalpirdetails'] = $report2;
		}
		}
		}
		
		if($withdetails=='1'){
			for($e=0;$e<count($reportdata);$e++){
				for($r=0;$r<count($reportdata[$e]);$r++){
					$detailquery = getdetailquery($reportdata[$e][$r]['edate'],$reportdata[$e][$r]['employee_id'],$project_id,$subdeptcond,$activity_id,$withinterpir,$is_partlinqdata);
					$detailreport = $dbase->executeQuery($detailquery,'multiple');
					$reportdata[$e][$r]['detail'] = $detailreport;
				}
			}			
		}

	}
if($excelreport=='1'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$detailHeadingStyleArray = array(
    'font'  => array(
        'bold'  => true,
		'italic'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
$detailValueStyleArray = array(
    'font'  => array(
		'italic'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 8,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Day-wise Utilization');	
	$row = 1;
		$col11 = "A{$row}";
		$col22 = "K{$row}";
		$concat =  $col11.":".$col22;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Day-wise Utilization');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$row++;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('EmpID');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Employee Name');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Dept');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('Sub Dept');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Date');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Total Parts');		
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('Online Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('Shortage Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue('Actual Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('J'.$row)->setValue('Budgeted Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('K'.$row)->setValue('Utilization %');
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('L'.$row)->setValue('Efficiency %');		
		$row++;
$totparts = 0;		
$totutilization = 0;
$totefficiency = 0;	
$actualhourssarr = array();
$billablehoursarr = array();
$cnt = 0;
for($q=0;$q<count($reportdata);$q++){
	for($w=0;$w<count($reportdata[$q]);$w++){
		$actualhourssarr[] = $reportdata[$q][$w]['actualhours'];
		$billablehoursarr[] = $reportdata[$q][$w]['billablehours'];		
		$totparts += $reportdata[$q][$w]['totalparts'];
		$totutilization += $reportdata[$q][$w]['utilization'];
		$totefficiency += $reportdata[$q][$w]['efficiency'];
		$cnt++;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportdata[$q][$w]['empid']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($reportdata[$q][$w]['empname']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue($reportdata[$q][$w]['dept']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($reportdata[$q][$w]['subdept']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($reportdata[$q][$w]['entrydate']);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($reportdata[$q][$w]['totalparts']);		
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($reportdata[$q][$w]['onlinehours']);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue($reportdata[$q][$w]['shortagehours']);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue($reportdata[$q][$w]['actualhours']);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('J'.$row)->setValue($reportdata[$q][$w]['billablehours']);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('K'.$row)->setValue($reportdata[$q][$w]['utilization']);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$row)->applyFromArray($valueStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('L'.$row)->setValue($reportdata[$q][$w]['efficiency']);			
		$row++;
		if($withdetails=='1' && count($reportdata[$q][$w]['detail']) > 0){
			$col22 = "A{$row}";
			$col23 = "B{$row}";
			$concat =  $col22.":".$col23;	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
			$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('PIR No');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailHeadingStyleArray);
			$col22 = "C{$row}";
			$col23 = "D{$row}";
			$concat =  $col22.":".$col23;	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
			$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Project');
			$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailHeadingStyleArray);
			$col22 = "E{$row}";
			$col23 = "F{$row}";
			$concat =  $col22.":".$col23;	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
			$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Activity');
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailHeadingStyleArray);
			$col22 = "G{$row}";
			$col23 = "H{$row}";
			$concat =  $col22.":".$col23;	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
			$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('No of Parts');
			$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailHeadingStyleArray);
			$col22 = "I{$row}";
			$col23 = "J{$row}";
			$concat =  $col22.":".$col23;	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
			$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue('Budgeted Hours');
			$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailHeadingStyleArray);
			$col22 = "K{$row}";
			$col23 = "L{$row}";
			$concat =  $col22.":".$col23;	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
			$objPHPExcel->getActiveSheet()->getCell('K'.$row)->setValue('Actual Hours');
			$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailHeadingStyleArray);			
			$row++;
			for($t=0;$t<count($reportdata[$q][$w]['detail']);$t++){
				$col31 = "A{$row}";
				$col32 = "B{$row}";
				$concat =  $col31.":".$col32;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);				
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['pirno']);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailValueStyleArray);
				$col31 = "C{$row}";
				$col32 = "D{$row}";
				$concat =  $col31.":".$col32;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);				
				$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['project']);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailValueStyleArray);
				$col31 = "E{$row}";
				$col32 = "F{$row}";
				$concat =  $col31.":".$col32;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);				
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['activity']);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailValueStyleArray);				
				$col31 = "G{$row}";
				$col32 = "H{$row}";
				$concat =  $col31.":".$col32;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);				
				$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['totalparts']);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailValueStyleArray);				
				$col31 = "I{$row}";
				$col32 = "J{$row}";
				$concat =  $col31.":".$col32;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);				
				$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['calculatedhours']);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailValueStyleArray);				
				$col31 = "K{$row}";
				$col32 = "L{$row}";
				$concat =  $col31.":".$col32;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);				
				$objPHPExcel->getActiveSheet()->getCell('K'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['actualhours']);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($detailValueStyleArray);
				$row++;
			}
				$col41 = "A{$row}";
				$col42 = "K{$row}";
				$concat =  $col41.":".$col42;	
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('');
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
				$row++;			
		}
}
}

		$col12 = "A{$row}";
		$col23 = "E{$row}";
		$concat =  $col12.":".$col23;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($totparts);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue($dbase->addTime($actualhourssarr));	
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('J'.$row)->setValue($dbase->addTime($billablehoursarr));		
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('K'.$row)->setValue($totutilization);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('L'.$row)->setValue($totefficiency);
		$row++;
		$totactualmin = $dbase->getminutes($dbase->addTime($actualhourssarr));
		$totbudgetmin = $dbase->getminutes($dbase->addTime($billablehoursarr));
		$perdaymin = $dbase->getminutes("09:00");
		$actualaverage = round(($totactualmin*100)/($cnt*$perdaymin),2)."%";
		$budgetaverage = number_format(round(($totbudgetmin*100)/($cnt*$perdaymin),2),2)."%";	
		$totactualmintues = $dbase->addTime($actualhourssarr,true);
		$totbudgetmintues = $dbase->addTime($billablehoursarr,true);
		$effaverage = number_format(round((($totbudgetmintues / $totactualmintues) * 100),2),2)."%";		
		$col12 = "A{$row}";
		$col23 = "E{$row}";
		$concat =  $col12.":".$col23;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Average');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('G'.$row)->setValue('');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('H'.$row)->setValue('');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('I'.$row)->setValue('');
		//$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, '=ROUND(('.$totactualmin.'*100)/('.$cnt.'*'.$perdaymin.'),2)');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($headingStyleArray);
		//$actualaverage
		$objPHPExcel->getActiveSheet()->getCell('J'.$row)->setValue('');	
		//$objPHPExcel->getActiveSheet()->setCellValue('J'.$row, '=ROUND(('.$totbudgetmin.'*100)/('.$cnt.'*'.$perdaymin.'),2)');		
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('K'.$row)->setValue($budgetaverage);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('L'.$row)->setValue($effaverage);			

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="day-wise_utilization.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}
if($activityexcelreport=='1'){
$objPHPExcel = new PHPExcel();
$headingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$detailHeadingStyleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$valueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
$detailValueStyleArray = array(
    'font'  => array(
        'color' => array('rgb' => '000000'),
        'size'  => 12,
        'name'  => 'Verdana'
    ),
   'borders' => array(
      'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
);
function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Activity-wise');	
	$row = 1;
			$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Name');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($detailHeadingStyleArray);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			
			$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue('Date');
			$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($detailHeadingStyleArray);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

			$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue('Activity');
			$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($detailHeadingStyleArray);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

			$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue('No of Parts');
			$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($detailHeadingStyleArray);

			$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue('Budgeted Hours');
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($detailHeadingStyleArray);

			$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue('Actual Hours');
			$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($detailHeadingStyleArray);			
			$row++;
$totparts = 0;
$activitytotparts = 0;	
$actualhourssarr = array();
$billablehoursarr = array();	
$cnt = 0;
for($q=0;$q<count($reportdata);$q++){
	for($w=0;$w<count($reportdata[$q]);$w++){	
		$totparts += $reportdata[$q][$w]['totalparts'];
		$actualhourssarr[] = $reportdata[$q][$w]['actualhours'];
		$billablehoursarr[] = $reportdata[$q][$w]['billablehours'];	
		if($withdetails=='1' && count($reportdata[$q][$w]['detail']) > 0){
			for($t=0;$t<count($reportdata[$q][$w]['detail']);$t++){
				$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($reportdata[$q][$w]['empname']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($detailValueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('B'.$row)->setValue($reportdata[$q][$w]['entrydate']);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($detailValueStyleArray);
				$objPHPExcel->getActiveSheet()->getCell('C'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['activity']);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($detailValueStyleArray);				
				$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['totalparts']);
				$activitytotparts +=$reportdata[$q][$w]['detail'][$t]['totalparts'];
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($detailValueStyleArray);				
				$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['calculatedhours']);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($detailValueStyleArray);				
				$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($reportdata[$q][$w]['detail'][$t]['actualhours']);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($detailValueStyleArray);
				$row++;
			}			
		}
}
}
		$col12 = "A{$row}";
		$col23 = "C{$row}";
		$concat =  $col12.":".$col23;	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($concat);
		$objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle($concat)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($activitytotparts);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('E'.$row)->setValue($dbase->addTime($actualhourssarr));	
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($headingStyleArray);
		$objPHPExcel->getActiveSheet()->getCell('F'.$row)->setValue($dbase->addTime($billablehoursarr));
		$row++;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Type: application/vnd.ms-excel'); 
header('Content-Disposition: attachment;filename="activities_wise.xls"'); 
header('Cache-Control: max-age=0');
$objWriter->save('php://output');	
exit;
}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#detailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#detailstable td, table#detailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#reporttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reporttable td, table#reporttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		
		table#reportdetailtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#reportdetailtable td, table#reportdetailtable th {
			border: 1px solid white;
			 padding: 5px; 
		}		
		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1' || $_SESSION['timesheet']['ROLEID']== ADMIN_ROLE || $_SESSION['timesheet']['IS_TEAMIDS']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="400px">
<?php include("reportmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_details" action="" method="post">
<table id="detailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
<td width="100%" colspan="3" ><b>Day-wise Utilization</b></td>
</tr>
<tr>
<td width="12%" ><b>From Date</b></td>
<td width="12%" ><input type="text" id="from_date" name="from_date" value="<?php if($from_date==''){echo date('d-M-Y'); } else{ echo $from_date; }  ?>" /></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>To Date</b></td>
<td width="12%" ><input type="text" id="to_date" name="to_date" value="<?php if($to_date==''){echo date('d-M-Y'); } else{ echo $to_date; }  ?>" /></td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Department</b></td>
<td width="12%" >
	<select id="department_id" name="department_id"  onchange="getsubdepartment();getemployeelist();">
	<option value="">-All-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																									   
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
			if($deptResult[$i]['id']==$deptid){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select>
		</td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Sub Department</b></td>
<td width="12%" >
<select id="subdepartment_id" name="subdepartment_id" onchange="getpirdropdown();getemployeelist();" >
<option value="">-All-</option>
</select>
</td>
<td width="12%" ></td>
</tr>	
<tr>
<td width="12%" ><b>Select PIR</b></td>
<td width="12%" >
	<select id="pirmaster_id" name="pirmaster_id" class="chosen-select" style="width:300px;" onchange="getproject();">
	<option value="">-All-</option>
	</select>
</td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Project</b></td>
<td width="12%" >
	<select id="project_id" name="project_id" onchange="getactivitylist();">
	<option value="">-All-</option>

	</select>
</td>
<td width="12%" ></td>
</tr>
<tr>
<td width="12%" ><b>Select Employee</b></td>
<td width="12%" ><select id="empid" name="empid" class="chosen-select" style="width:300px;" >
		<option value="">-All-</option>
		<?php
		//$empcond = '';
		//adminempdropdown($empcond,$empid);
		?>
		</select></td>
<td width="12%" ><input type="checkbox" id="is_partlinqdata" name="is_partlinqdata" value='1' <?php if($is_partlinqdata=='1'){?> checked <?php } ?> /> Show Partlinq Activities</td>
</tr>
<tr>
<td width="12%" ><b>Select Activity</b></td>
<td width="12%" ><select id="activity_id" name="activity_id" class="chosen-select" style="width:300px;" >
		<option value="">-All-</option>
		</select></td>
<td width="12%" ><input type="checkbox" id="withdetails" name="withdetails" value='1' <?php if($withdetails=='1'){?> checked <?php } ?> /> With Details<input type="checkbox" id="withinterpir" name="withinterpir" value='1' <?php if($withinterpir=='1'){?> checked <?php } ?> />Show Internal PIR</td>
</tr>	
<tr>
<td width="12%" ><input type="hidden" id="excelreport" name="excelreport"  /><input type="hidden" id="activityexcelreport" name="activityexcelreport"  /></td>
<td width="12%" ><input type="button" id="submitbutton" name="submitbutton" value=" Submit " onclick="submitform();" /></td>
<td width="12%" ><a href="javascript:void(0);" onclick="getexcel();" style="cursor: pointer;"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a>Employee Wise<br/><a href="javascript:void(0);" onclick="getactivityexcel();" style="cursor: pointer;"><img width="20px" height="20px" alt=" Export" src="images/excel.png"></a> Activities Wise</td>
</tr>

</table>
</form>
</br>
<?php
if($fromdate != '' && $todate != ''){
 ?>
<table id="reporttable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%" >
<tr>
	<td width="5%" align="center"><b>Emp ID</b></td>
	<td width="15%" align="center"><b>Emp Name</b></td>
	<td width="5%" align="center"><b>Dept</b></td>
	<td width="5%" align="center"><b>Sub Dept</b></td>
	<td width="10%" align="center"><b>Date</b></td>
	<td width="7%" align="center"><b>Total Parts</b></td>
	<td width="8%" align="center"><b>Online Hours</b></td>
	<td width="8%" align="center"><b>Shortage Hours</b></td>
	<td width="8%" align="center"><b>Actual Hours</b><span style="font-size:10px;"> Entered by user</span></td>
	<td width="8%" align="center"><b>Budgeted Hours</b></td>
	<td width="8%" align="center"><b>Utilization %</b><br/><span style="font-size:10px;letter-spacing:1px;">(BH/9hrs)*100</span></td>
	<td width="8%" align="center"><b>Efficiency %</b><br/><span style="font-size:10px;letter-spacing:1px;">(BH/AH)*100</span></td>
</tr>
<?php
$htmltotparts = 0;		
$totutilization = 0;
$totefficiency = 0;	
$actualhourssarr = array();
$billablehoursarr = array();
$cnt = 0;
$rewcnt = 0;
$intcnt = 0;
for($q=0;$q<count($reportdata);$q++){
	for($w=0;$w<count($reportdata[$q]);$w++){
		
		$actualhourssarr[] = $reportdata[$q][$w]['actualhours'];
		$billablehoursarr[] = $reportdata[$q][$w]['billablehours'];		
		$htmltotparts += $reportdata[$q][$w]['totalparts'];
		$totutilization += $reportdata[$q][$w]['utilization'];
		$totefficiency += $reportdata[$q][$w]['efficiency'];
		if($reportdata[$q][$w]['presenthours']==$reportdata[$q][$w]['static_availhrs']){
			$cnt += 1;
		}
		elseif($reportdata[$q][$w]['presenthours']=='04:15:00'){
			$cnt += 0.5;
		}
		else{
			$cnt += 0;
		}	
		//$cnt++;
	
if($reportdata[$q][$w]['empid'] != ''){		
?>
<tr>
	<td valign="top"><?php echo $reportdata[$q][$w]['empid']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['empname']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['dept']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['subdept']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['entrydate']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['totalparts']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['onlinehours']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['shortagehours']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['actualhours']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['billablehours']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['utilization']; ?></td>
	<td valign="top"><?php echo $reportdata[$q][$w]['efficiency']; ?></td>
</tr>
<?php if($withdetails=='1' && count($reportdata[$q][$w]['detail']) > 0){ ?>
<tr>
	<td valign="top" colspan="12">
<table id="reportdetailtable" border="0" cellpadding="0" cellspacing="0" align="center"  width="100%">
<thead>
	<tr>
		<td width="16%"><b>PIR No</b></td>
		<td width="16%"><b>Project</b></td>
		<td width="16%"><b>Activity</b></td>
		<td width="14%"><b>No of Parts</b></td>
		<td width="14%"><b>Budgeted Hours</b></td>
		<td width="14%"><b>Actual Hours</b></td>
	</tr>
	</thead>
	<tbody>
<?php for($t=0;$t<count($reportdata[$q][$w]['detail']);$t++){ ?>
	<tr>
		<td width="16%"><?php echo $reportdata[$q][$w]['detail'][$t]['pirno']; ?></td>
		<td width="16%"><?php echo $reportdata[$q][$w]['detail'][$t]['project']; ?></td>
		<td width="16%"><?php echo $reportdata[$q][$w]['detail'][$t]['activity']; ?></td>
		<td width="14%"><?php echo $reportdata[$q][$w]['detail'][$t]['totalparts']; ?></td>
		<td width="14%"><?php echo $reportdata[$q][$w]['detail'][$t]['calculatedhours']; ?></td>
		<td width="14%"><?php echo $reportdata[$q][$w]['detail'][$t]['actualhours']; ?></td>
	</tr>
<?php }  ?>
	<tr>
		<td width="16%" colspan="6">&nbsp;</td>
	</tr>
</tbody></table>	
	</td>
</tr>	
<?php } ?>
<?php
	}
	}
			if(count($reportdata[$q]['reworkdetails']) > 0){
					for($u=0;$u<count($reportdata[$q]['reworkdetails']);$u++){
						$interpirpresentdays = 0;
						if($reportdata[$q]['reworkdetails'][$u]['presenthours']==$reportdata[$q]['reworkdetails'][$u]['static_availhrs']){
							$rewcnt += 1;
						}
						elseif($reportdata[$q]['reworkdetails'][$u]['presenthours']=='04:15:00'){
							$rewcnt += 0.5;
						}
						else{
							$rewcnt += 0;
						}
					}
			}		
			if(count($reportdata[$q]['internalpirdetails']) > 0){
					for($u=0;$u<count($reportdata[$q]['internalpirdetails']);$u++){
						$interpirpresentdays = 0;
						if($reportdata[$q]['internalpirdetails'][$u]['presenthours']==$reportdata[$q]['internalpirdetails'][$u]['static_availhrs']){
							$intcnt += 1;
						}
						elseif($reportdata[$q]['internalpirdetails'][$u]['presenthours']=='04:15:00'){
							$intcnt += 0.5;
						}
						else{
							$intcnt += 0;
						}
					}
			}	
}
?>
<tr>
	<td width="10%" align="right" colspan="5"><b>Total</b></td>
	<td width="7%" align="center"><b><?php echo $htmltotparts; ?></b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($actualhourssarr); ?></b></td>
	<td width="8%" align="center"><b><?php echo $dbase->addTime($billablehoursarr); ?></b></td>
	<td width="8%" align="center"><b><?php //echo $totutilization; ?></b></td>
	<td width="8%" align="center"><b><?php //echo $totefficiency; ?></b></td>
</tr>
<?php 
		$totactualmin = $dbase->getminutes($dbase->addTime($actualhourssarr));
		$totbudgetmin = $dbase->getminutes($dbase->addTime($billablehoursarr));
		$perdaymin = $dbase->getminutes("09:00");
		//$actualaverage = round(($totactualmin*100)/(($cnt+$rewcnt+$intcnt)*$perdaymin),2)."%";
		$actualaverage = round(($totactualmin*100)/($cnt*$perdaymin),2)."%";
		//$budgetaverage = round(($totbudgetmin*100)/(($cnt+$rewcnt+$intcnt)*$perdaymin),2)."%";
		$budgetaverage = round(($totbudgetmin*100)/($cnt*$perdaymin),2)."%";
		$totactualmintues = $dbase->addTime($actualhourssarr,true);
		$totbudgetmintues = $dbase->addTime($billablehoursarr,true);
		$effaverage = round((($totbudgetmintues / $totactualmintues) * 100),2)."%";
	 	$activity_query = "SELECT budgettime FROM pir_activity WHERE isActive='1' AND activitytype='0' AND activity_id='".$activity_id."' AND pirmaster_id='".$pirmasterid."'";
		$activity_det = $dbase->executeQuery($activity_query,'single');
		$budgetedhrs_per_activity = $dbase->secToTimeFormat($activity_det['budgettime']);
?>
<tr>
	<td width="10%" align="right" colspan="5"><b>Average</b></td>
	<td width="7%" align="center"></td>
	<td width="8%" align="center"><?php echo $totbudgetmin." - ".$cnt." - ".$rewcnt." - ".$intcnt." - ".$perdaymin; ?></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"><b><?php //echo $actualaverage; ?></b></td>
	<td width="8%" align="center"><b><?php echo $budgetaverage; ?></b></td>
	<td width="8%" align="center"><b><?php echo $effaverage; ?></b></td>
</tr>
<?php if($activity_id != ''){ ?>
<tr>
	<td width="10%" align="right" colspan="9"><b>Total Budgeted Hours</b></td>

	<td width="8%" align="center"><b><?php echo $budgetedhrs_per_activity; ?></b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"></td>
</tr>
<tr>
	<td width="10%" align="right" colspan="9"><b>Total Actual Hours</b></td>

	<td width="8%" align="center"><b><?php echo $dbase->addTime($actualhourssarr); ?></b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"></td>
</tr>	
<tr>
	<td width="10%" align="right" colspan="9"><b>Delta</b></td>
	<?php
		$delta = $dbase->secToTimeFormat(($activity_det['budgettime'] - ($totactualmintues*60)));
		if (strpos($delta, '-') !== false) {
			$showdelta = str_replace("-","",$delta);
			$deltacolor = "red";
		}
		else{
			$showdelta = $delta;
			$deltacolor = "green";
		}
	?>
	<td width="8%" align="center"><b style="color:<?php echo $deltacolor; ?>"><?php echo $showdelta; ?></b></td>
	<td width="8%" align="center"></td>
	<td width="8%" align="center"></td>
</tr>
<?php } ?>
</table>
<br/>
<?php
}
?>
</td>
</tr>
</table>
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
 $('#from_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
  $('#to_date').datepicker({
	 inline: true,
	 dateFormat: 'dd-M-yy',
	 maxDate:0,
	 changeMonth: true,
	 changeYear: true,
	 yearRange: "-10:+0",
 });
	  $("#frm_details").validate();	
		$(".confirm").easyconfirm({locale: { title: 'Please Confirm !',text: 'Do you want to submit ?', button: ['No','Yes']}});
		$(".confirm").click(function() {
			$("#frm_details").submit();
		});
			getsubdepartment();
			//getpirdropdown();
			
			$("#excelreport").val('0');
	  $(".chosen-select").chosen();
	 autoselectoption("#department_id");
	 autoselectoption("#subdepartment_id");
	});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = '<?php echo $subdeptid;?>';
	$.get("getsubdepartmentadmin.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
		getpirdropdown();
		getemployeelist();
	});
}
function getexcel(){
	$("#excelreport").val('1');
	$("#frm_details").submit();
	$("#excelreport").val('0');
}
	
function getactivityexcel(){
	$("#activityexcelreport").val('1');
	$("#frm_details").submit();
	$("#activityexcelreport").val('0');
}
function getpirdropdown(){
	var deptid = $("#department_id").val();
	var supdeptid = $("#subdepartment_id").val();
	var pirmaster_id = '<?php echo $pirmaster_id;?>';
	$.get("getpiradmin1.php?deptid="+deptid+"&supdeptid="+supdeptid+"&pirmaster_id="+pirmaster_id+"&t=1",function(data){
		$("#pirmaster_id").html(data);
		$("#pirmaster_id").trigger("chosen:updated");
		getproject();
	});
}
function getproject(){
	var id = $("#pirmaster_id").val();
	var selid = '<?php echo $project_id;?>';
	$.get("getprojectadmin.php?id="+id+"&selid="+selid,function(data){
		$("#project_id").html(data);
		getactivitylist();
	});
}

function submitform(){
	$("#frm_details").submit();
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-All-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
function getemployeelist(){
	var deptid = $("#department_id").val();
	var supdeptid = $("#subdepartment_id").val();
	$.get("getempadmin.php?deptid="+deptid+"&supdeptid="+supdeptid+"&t=1",function(data){
		$("#empid").html(data);
		$("#empid").trigger("chosen:updated");
	});	
}
function getactivitylist(){
	var pirmaster_id = $("#pirmaster_id").val();
	var project_id = $("#project_id").val();
	var selid = '<?php echo $activity_id;?>';
	$.get("getactivityadmin.php?pirid="+pirmaster_id+"&proid="+project_id+"&selid="+selid,function(data){
		$("#activity_id").html(data);
		$("#activity_id").trigger("chosen:updated");
	});		
}
</script>
</html>
<?php } ?>

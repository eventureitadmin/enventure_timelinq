<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		

		$entrydate=date('Y-m-d',strtotime($_POST['entrydate']));
		$dedicatemonth=date('m',strtotime($_POST['entrydate']));
		$dedicatemyear=date('Y',strtotime($_POST['entrydate']));
		for($i=1;$i<=$_POST['totrow'];$i++){
			$is_dt = 0;
			if($_POST['pirmaster_id'.$i] !='' && $_POST['project_id'.$i] !='' && $_POST['activity_id'.$i] !='' && $_POST['totalparts'.$i] !='' && $_POST['calculatedhours'.$i] !='' && $_POST['actualhours'.$i] !=''){
				if($_POST['totalparts'.$i]=='NA'){
					$_POST['totalparts'.$i] = "1";
					$is_dt = 1;
				}
				//If PIR no is dedicated then set is_dt =1
				if($_POST['pirmaster_id'.$i] != "" && $_POST['project_id'.$i] != ""){
				$result = $dbase->executeQuery("SELECT rm.no_of_resource AS cnt FROM `pirmaster` pm,`resourcemonth` rm WHERE rm.pirmaster_id=pm.id AND pm.isActive='1' AND pm.`pirno`='".$_POST['pirmaster_id'.$i]."' AND pm.`projectname`='".$_POST['project_id'.$i]."' AND rm.monthval='".$dedicatemonth."' AND rm.yearval='".$dedicatemyear."' ","single");
					/*if($result['cnt'] == 1){
						$is_dt = 1;
					}*/
					if($result['cnt'] >0){
						$is_dt = 1;
					}					
				}
				if($_POST['calculatedhours'.$i]=='NA'){
					$_POST['calculatedhours'.$i] = $_POST['actualhours'.$i];
				}				
				$pirmaster_select = "SELECT id FROM pirmaster WHERE pirno='".$_POST['pirmaster_id'.$i]."' AND projectname='".$_POST['project_id'.$i]."'";
				$pirmasterdet = $dbase->executeQuery($pirmaster_select,'single');
				$pirmasterid=$pirmasterdet['id'];
				// check for same entry
			 	$checkrecord = "SELECT COUNT( id ) AS cnt,id FROM `timeentry` WHERE `entrydate` = '".$entrydate."' AND `employee_id` = '".$_SESSION['timesheet']['ID']."' AND `pirmaster_id` = '".$pirmasterid."' AND `project_id` = '".$_POST['project_id'.$i]."' AND `activity_id` = '".$_POST['activity_id'.$i]."' AND `totalparts` = '".$_POST['totalparts'.$i]."' AND isActive = '1' AND calculatedhours = '".$_POST['calculatedhours'.$i]."'";
				
				$checkrecord_Result = $dbase->executeQuery($checkrecord,"single");
				
					//rework
					$is_rework = $dbase->executeQuery("SELECT is_rework FROM `pirlist` WHERE id='".$_POST['pirmaster_id'.$i]."'",'single')[0];
					if($is_rework != 1){
						$is_rework =0;
					}
					
					//internal pir
					$is_internalpir = $dbase->executeQuery("SELECT is_internalpir FROM `pirlist` WHERE id='".$_POST['pirmaster_id'.$i]."'",'single')[0];
					if($is_internalpir != 1){
						$is_internalpir =0;
					}
				if($checkrecord_Result['cnt']==0){
						
					//print_r($_POST);
					if($checkrecord_Result['cnt'] == 1 && $checkrecord_Result['id']>0){
						 $datainsert = "UPDATE `timeentry` SET  is_actualhrs_modify='1',`pirmaster_id`='".$pirmasterid."',`project_id`='".$_POST['project_id'.$i]."', `activity_id`='".$_POST['activity_id'.$i]."',`is_dt`='".$is_dt."', `totalparts`='".$_POST['totalparts'.$i]."', `calculatedhours`='".$_POST['calculatedhours'.$i]."', `actualhours`='".$_POST['actualhours'.$i]."',`is_rework`='".$is_rework."',`is_internalpir`='".$is_internalpir."',`isActive`='1' WHERE id='".$checkrecord_Result['id']."'";
					}else{
					  $datainsert = "INSERT INTO `timeentry` (`entrydate`, `employee_id`, `pirmaster_id`,`project_id`, `activity_id`,`is_dt`, `totalparts`, `calculatedhours`, `actualhours`,`datecreated`,`is_rework`,`is_internalpir`,`isActive`) VALUES ('".$entrydate."', '".$_SESSION['timesheet']['ID']."', '".$pirmasterid."', '".$_POST['project_id'.$i]."','".$_POST['activity_id'.$i]."','".$is_dt."', '".$_POST['totalparts'.$i]."', '".$_POST['calculatedhours'.$i]."', '".$_POST['actualhours'.$i]."','".$dbase->datetimeCreated."','".$is_rework."','".$is_internalpir."','1');";
					}
					
					$dbase->executeNonQuery($datainsert);					
				}else if($checkrecord_Result['cnt'] == 1 && $checkrecord_Result['id']>0){
					
						  $datainsert = "UPDATE `timeentry` SET is_actualhrs_modify='1', `pirmaster_id`='".$pirmasterid."',`project_id`='".$_POST['project_id'.$i]."', `activity_id`='".$_POST['activity_id'.$i]."',`is_dt`='".$is_dt."', `totalparts`='".$_POST['totalparts'.$i]."', `calculatedhours`='".$_POST['calculatedhours'.$i]."', `actualhours`='".$_POST['actualhours'.$i]."',`is_rework`='".$is_rework."',`is_internalpir`='".$is_internalpir."',`isActive`='1' WHERE id='".$checkrecord_Result['id']."'";
						  $dbase->executeNonQuery($datainsert);	
				}	
				
			}
			
		}

		if($_SESSION['timesheet']['NO_TIMESHEET']=='1'){
			header("Location:index.php?m=1");
			exit;
		}
		else{
			//header("Location:login.php?logout");
			header("Location:dataentry.php");
			exit;				
		}				
	}

		if($_SESSION['timesheet']['PREDATE_LOGOUT']=='1'){
			$chkpredatequery = "SELECT `ID` , `log_date` , `login_time` , `login_comments` , `logout_time` , `logout_comments` , `totalhours` FROM `time_log` WHERE `emp_id` = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND `logout_time`='0000-00-00 00:00:00' AND `totalhours`='00:00:00' ORDER BY ID DESC LIMIT 0 , 1 ";
			$chkpredateResult = $dbase->executeQuery($chkpredatequery,"single");
			if($chkpredateResult['login_time'] != '0000-00-00 00:00:00' && $chkpredateResult['logout_time'] =='0000-00-00 00:00:00'){
				$currentdate = $chkpredateResult['log_date'];
				if(date('Y',strtotime($currentdate)) != date('Y')){
					$currentdate = date('Y-m-d');
				}
			}
			else{
				$currentdate = date('Y-m-d');
			}
		}
		else{
			$currentdate = date('Y-m-d');
		}

		if($_SESSION['timesheet']['NO_TIMESHEET']=='1'){
			//check time log entry 
			$logentry_select = "SELECT COUNT( id ) AS cnt FROM `time_log` WHERE `emp_id` = '".trim(mysql_escape_string($_SESSION['timesheet']['ID']))."' AND `log_date` = '".$currentdate."'";
			$logentry_Result = $dbase->executeQuery($logentry_select,"single");
			if($logentry_Result['cnt']==0){
				header("Location:index.php");
				exit;					
			}				
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	   <script type="text/javascript" src="js/notify.js"></script>
		<script type="text/javascript">
		var timetype = <?php echo TIME_TYPE; ?>;
		var maximum_entry = <?php echo MAXIMUM_ENTRY; ?>;
		 $(document).ready(function(){
			 jQuery.validator.addMethod("number",function(value, element){
					if (this.optional(element)) {
						return true;
					} else if (!/^\d*\.?\d{0,2}$/.test(value) || value == 0) {
						return false;
					} 
					return true;
			}, "Allow digits or decimals");
			var time = new Date().toString("HH:mm:ss");
			if(timetype=='1'){
				$("#logintime").val(time);
				$("#logouttime").val(time);		
			}
			if(timetype=='2'){
				$("#logintime").val('<?php echo $time; ?>');
				$("#logouttime").val('<?php echo $time; ?>');
			}
		});		
		</script>	
	  <style>
		#rcorners {
			border: 1px solid #73ad21;
			border-radius: 15px 15px 15px 15px;
			padding: 20px;
			box-shadow: 5px 5px 5px 3px #888;
			background-color: white;
		}
		table#dataentrytable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#dataentrytable td, table#dataentrytable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		table#dataentrydetailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#dataentrydetailstable td, table#dataentrydetailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}
		table#datalisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#datalisttable td, table#datalisttable th {
			border: 1px solid black;
			 padding: 5px; 
		}	
		table#msgtable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:1px;
		}

		table#msgtable td, table#msgtable th {
			/*border: 1px solid black;*/
			 padding: 5px; 
		}			  
		table#dataentereddetailstable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
		}

		table#dataentereddetailstable td, table#dataentereddetailstable th {
			border: 1px solid black;
			 padding: 5px; 
		}		
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='0'){ ?>
<form id="frm_dataentry" action="" method="post">
<table id="dataentrytable" border="0" cellpadding="0" cellspacing="0" align="center"  width="80%" >
<?php if($_GET['m']=='1'){ ?>
<tr>
	<td width="10%" colspan="2" align="center"><b style="color:red;"><?php echo "Time logged successfully !"; ?></b></td>
</tr>
<?php  } ?>
<tr>
	<td width="10%"><b>Entry Date</b></td>
	<td width="22%"><input type="text" name="entrydate" id="entrydate" value="<?php  echo date('d-M-Y',strtotime($currentdate));?>" class=" required" /></td>
</tr>
<tr>
<td colspan="2" id="dataentry">

</td>
</tr>
<tr>
	<td colspan="2" align="center">
	<input type="hidden" id="totrow" name="totrow" />
		<input type="submit" id="dataentrybutton" value=" Save " class="button button1" />
	</td>
</tr>
</table>
</form>
<br/>
<br/>
<table id="datalisttable" border="0" cellpadding="0" cellspacing="0" align="center"  width="80%" >
<tr>
	<td width="10%"><b>Entry Date</b></td>
	<td width="22%"><input type="text" name="entereddate" id="entereddate" value="<?php  echo date('d-M-Y',strtotime(date('Y-m-d')));?>" class=" required" /></td>
	<td width="22%"><!--<input type="button" id="dataenteredbutton" value=" Submit " onclick="loadlisttable();" class="button button1" />--></td>
</tr>
<tr>
<td colspan="3" id="dataentered">

</td>
</tr>
</table>
<br/>
<br/>	   
<table id="msgtable" border="0" cellpadding="0" cellspacing="0" align="center"  width="80%" >
<tr>
<td>
	<span style="color:red">After filling the all activities, please click <b>Start / End my Day</b> link from the menu.</span>
</td>
</tr>
</table>	   
	   
<?php } ?>
</body>
<script type="text/javascript">
 $(document).ready(function(){
	 $( "#entrydate" ).datepicker({
		inline: true,
		dateFormat: 'd-M-yy',
		maxDate: 0,
		showOn: 'button',
		buttonImageOnly: true,
		buttonImage: 'images/calendar.gif',
		changeMonth: true,
		changeYear: true,
		yearRange: "-10:+0",
		onSelect: function(dateText) {
		  $(this).change();
		},	
	 }).on("change", function() {
		 loadtable();
  });
  
	 $( "#entereddate" ).datepicker({
		inline: true,
		dateFormat: 'd-M-yy',
		maxDate: 0,
		showOn: 'button',
		buttonImageOnly: true,
		buttonImage: 'images/calendar.gif',
		changeMonth: true,
		changeYear: true,
		yearRange: "-10:+0",
		onSelect: function(dateText) {
		  $(this).change();
		},	
	 }).on("change", function() {
		 loadlisttable();
  });  
	 
	  $("#frm_dataentry").validate();
	  
	$(document).delegate('.add-record', 'click', function(e) {
		 e.preventDefault();    
		// var content = $('#dataentrydetailstable tbody').children('tr:first'),
		 var content = $('#dataentrydetailstable tr:last'),
		// var content = $('#dataentrydetailstable tbody tr:last-child'),
		 size = $('#dataentrydetailstable >tbody >tr').length + 1,
			element = null,    
			element = content.clone();console.log(size);
			if(size<=<?php echo MAXIMUM_ENTRY;?>){
			
				element.attr('id', 'row'+size);
				element.find('.pir').attr('id', 'pirmaster_id'+size);
				element.find('.pir').attr('name', 'pirmaster_id'+size);
		if(element.find('.pir option').attr('disabled')== 'disabled'){
				element.find('.pir option').removeAttr('disabled');
		}
		if(element.find('.pir option:selected').length >0){
				element.find('.pir option').removeAttr('selected');
		}
		
				element.find('.activity').attr('id', 'activity_id'+size);
				element.find('.activity').attr('name', 'activity_id'+size);
				element.find('.activity').empty().append('<option value="">-Select-</option>');
				element.find('.project').attr('id', 'project_id'+size);
				element.find('.project').attr('name', 'project_id'+size);
				element.find('.project').empty().append('<option value="">-Select-</option>');	
		if(element.find('.totalparts').attr('readonly')== 'readonly'){
				element.find('.totalparts').removeAttr('readonly');
	}
				element.find('.totalparts').attr('id', 'totalparts'+size);
				element.find('.totalparts').attr('name', 'totalparts'+size);
				element.find('.totalparts').val('');
				element.find('.calculatedhours').attr('id', 'calculatedhours'+size);
				element.find('.calculatedhours').attr('name', 'calculatedhours'+size);
				element.find('.calculatedhours').val('');
				element.find('.actualhours').attr('id', 'actualhours'+size);
				element.find('.actualhours').attr('name', 'actualhours'+size);
				element.find('.actualhours').removeAttr('readonly');
				element.find('.actualhours').val('');
				element.find('.delete-record').attr('rowid', size);
				element.appendTo('#dataentrydetailstable');
				<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
				$('#pirmaster_id'+size).change();
				<?php } ?>
				$("#totrow").val(size);				
			}
		  else{
			$('<div>Only '+maximum_entry+' can enter</div>').dialog({
				resizable: false,
				open: function(){
				 $(".ui-dialog-title").html("Alert");
				},				
					buttons: {
					"Ok": function() {
						$( "div" ).remove( ".ui-widget-overlay" );	
						$(this).dialog("close");
						return false;
					},
				}
			});	
			$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
		  }
   });
   
	$(document).delegate('.delete-record', 'click', function(e) {
		e.preventDefault();  
	  var id = $(this).attr('rowid');
	
	  if(id > 1 && $("#row"+id).find('.pir option').attr('disabled')!= 'disabled'){
	  var targetDiv = $(this).attr('targetDiv');
		$('<div>Are you sure wants to delete ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
						if($("#entryid"+id).val() > 0){
						$.post("deleterow.php",{idval:$("#entryid"+id).val()},function(data){
							if(data=='success'){
							loadlisttable();
							}
						});
						}
					  $('#row' + id).remove();
					//regnerate index number on table
					$('#dataentrydetailstable tbody tr').each(function(index) {
					  var rowid = index+1;
						$(this).attr('id', 'row'+rowid);
						$(this).find('.pir').attr('id', 'pirmaster_id'+rowid);
						$(this).find('.pir').attr('name', 'pirmaster_id'+rowid);
						$(this).find('.activity').attr('id', 'activity_id'+rowid);
						$(this).find('.activity').attr('name', 'activity_id'+rowid);
						$(this).find('.project').attr('id', 'project_id'+rowid);
						$(this).find('.project').attr('name', 'project_id'+rowid);						
						$(this).find('.totalparts').attr('id', 'totalparts'+rowid);
						$(this).find('.totalparts').attr('name', 'totalparts'+rowid);
						$(this).find('.calculatedhours').attr('id', 'calculatedhours'+rowid);
						$(this).find('.calculatedhours').attr('name', 'calculatedhours'+rowid);
						$(this).find('.actualhours').attr('id', 'actualhours'+rowid);
						$(this).find('.actualhours').attr('name', 'actualhours'+rowid);
						$(this).find('.delete-record').attr('rowid', rowid);
						$("#totrow").val(rowid);
					
					});
						
					
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return true;
					

				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );	
	  }
	  else{
		$('<div>You cant delete this row</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				},
			}
		});
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	  }
	});
loadtable();
loadlisttable();

$.validator.addMethod("time24", function(value, element) {
	if(value != ''){
		if (!/^\d{1,2}:\d{2}$/.test(value)) return false;
		var parts = value.split(':');
		if (parts[0] > 23 || parts[1] > 59) return false;
		return true;		
	}
	else{
		return true;
	}
}, "Invalid time format.");
<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
  $('#frm_dataentry').bind('submit', function () {
		$('.pir').removeAttr('disabled'); 
		$('.project').removeAttr('disabled');  
		$('.activity').removeAttr('disabled');
  });
<?php } ?>
});	

function checkcalculatetime(id,value){
	var id = id.replace("actualhours", "");
	if($("#activity_id"+id).val() != ''){
		if($("#activity_id"+id).find(':selected').data('id')=='n' && $("#totalparts"+id).val() != 'NA' && $("#totalparts"+id).val() !='' && $("#totalparts"+id).val() > 0){
			if(value != ''){
				$("#calculatedhours"+id).val(value);
			}
		}
	}
}

function callcalculatetime(id){
	var selid = id;
	var id = id.replace("activity_id", "");
	if(id !=""){
		for(var k=0;k<$(".activity").length;k++){
				
			if($("#activity_id"+id).val() == $(".activity").eq(k).val() && $("#project_id"+id).val() == $(".project").eq(k).val()
			  && $("#pirmaster_id"+id).val() == $(".pir").eq(k).val() && $(".activity").eq(k).attr("id")!=selid){
				$("#activity_id"+id).val("");
				$.notify("Activity already exists","error");
				return false;
			}
		}
	}
	var acttype = $("#activity_id"+id).find(':selected').data('type');
	if(acttype=='0'){
		$("#totalparts"+id).attr('readonly', true); 
		$("#totalparts"+id).val("1");
		calculatetime('totalparts'+id,'1','0');
	}
	else{
		if($("activity_id"+id).val()==''){
			calculatetime('totalparts'+id,$("#totalparts"+id).val(),'1');
		}
		else{
			calculatetime('totalparts'+id,$("#totalparts"+id).val(),'0');
		}
	}
}

function getactivity(id,value){
	var id = id.replace("project_id", "");
	var pirid = $("#pirmaster_id"+id).val();
	var proid = $("#project_id"+id).val();
	$.get("getactivity.php?pirid="+pirid+"&proid="+proid,function(data){
		$("#activity_id"+id).html(data);
		<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
			autoselectactivity();
		<?php } ?>		
	});
}

function getproject(id,value){
	var id = id.replace("pirmaster_id", "");
	$("#totalparts"+id).val('');
	$("#calculatedhours"+id).val('');
	$("#actualhours"+id).val('');
	$("#activity_id"+id).empty().append('<option value="">-Select-</option>');
	var selid = $("#pirmaster_id"+id).val();
	$.get("getproject.php?id="+selid,function(data){
		$("#project_id"+id).html(data);
		autoselectoption("#project_id"+id);
		<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
			autoselectproject();
		<?php } ?>			
	});
}

function calculatetime(id,value,check){
			var id = id.replace("totalparts", "");
			var value = value;
			var pirid = $("#pirmaster_id"+id).val();
			var proid = $("#project_id"+id).val();
			var actid = $("#activity_id"+id).val(); 
			if(pirid==''){
				//if(check=='1'){
					$(this).val('');
					$('<div>Please Select PIR</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$("#totalparts"+id).val('');
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				//}
			}
			else if(proid==''){
				//if(check=='1'){
					$(this).val('');
					$('<div>Please Select Project</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$("#totalparts"+id).val('');
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				//}
			}
			else if(actid==''){
				if(check=='1'){				
					$(this).val('');
					$('<div>Please Select Activity</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$("#totalparts"+id).val('');
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				}
			}
			else{
				$.get("getbudgettime.php?val="+value+"&pirid="+pirid+"&actid="+actid+"&proid="+proid,function(data){
					if(data == 'NA'){
						$("#totalparts"+id).removeClass("number");
						$("#totalparts"+id).val('NA');
						$("#calculatedhours"+id).removeClass("required");
						$("#calculatedhours"+id).val('NA');
					 }
					else{
						if(!$("#calculatedhours"+id).hasClass("required")){
							$("#calculatedhours"+id).addClass("required");
						}
						if(!$("#totalparts"+id).hasClass("required")){
							$("#totalparts"+id).addClass("required");
						}						
					if(data != 'NULL'){
						$("#calculatedhours"+id).val(data);
					}
					if(data=='NULL' && $("#activity_id"+id).find(':selected').data('id')=='y' && $("#totalparts"+id).val() >0  && $("#totalparts"+id).val().indexOf("0") !=-1){
						$("#totalparts"+id).val('');
						$("#calculatedhours"+id).val('00:00');
					}
					//if(data=='NULL'){
					//	$("#totalparts"+id).val('');
					//	$("#calculatedhours"+id).val('00:00');
					//}				
					checkcalculatetime('actualhours'+id,$("#actualhours"+id).val());
					}
				});					
			}
			
}

function loadtable(){
	$.get("dataentrytable.php?id=1&entrydate="+$("#entrydate").val(),function(data){
		$("#dataentry").html(data);
		<?php if($_SESSION['timesheet']['NO_TIMESHEET']=='0') { ?>
			autoselectpirno();
		<?php } ?>		
	$("#totrow").val($("#totalactivity").val());
	});	
//("#totalactivity").val();
	
}

function loadlisttable(){
	var entereddate = $("#entereddate").val();
	$.get("dataenteredtable.php?date="+entereddate,function(data){
		$("#dataentered").html(data);
	});	
}

function deleterow(idval){
	$('<div>Do you want to Delete this row ?</div>').dialog({
		resizable: false,
		open: function(){
		 $(".ui-dialog-title").html("Please Confirm !");
		},				
			buttons: {
			"Yes": function() {
				$.post("deleterow.php",{idval:idval},function(data){
					if(data=='success'){
						
							$('<div>Row Deleted successfully</div>').dialog({
								resizable: false,
								open: function(){
								 $(".ui-dialog-title").html("Alert");
								},				
									buttons: {
									"Ok": function() {
										$(this).dialog("close");
									},
								}
							});							
					}
				});	
				$( "div" ).remove( ".ui-widget-overlay" );	
				loadtable();
				loadlisttable();
				$(this).dialog("close");
			},
			"No": function() {
				$( "div" ).remove( ".ui-widget-overlay" );	
				$(this).dialog("close");
			}
		}
	});	
	$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
}

function autoselectpirno(){
$(".pir option").each(function () {
	var value1 = '';
	value1 = $(this).text();
	if (value1 == "EAS/0419") {
		$(this).attr("selected", "selected").change();
	}
});	
$('.pir').attr('disabled', 'disabled');
}
function autoselectproject(){
$(".project option").each(function () {
	var value2 = '';
	value2 = $(this).text();
	if (value2 == "Maximo Support") {
		$(this).attr("selected", "selected").change();
	}
});
$('.project').attr('disabled', 'disabled');
}
function autoselectactivity(){
$(".activity option").each(function () {
	var value3 = '';
	value3 = $(this).text();
	if (value3 == "Maximo Support") {
		$(this).attr("selected", "selected").change();
	}
});
$('.activity').attr('disabled', 'disabled');
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
</script>
</html>
<?php } ?>

<?php
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	function pirdropdown($cond){
		 global $dbase;
		$pir_select = "SELECT id,pirno,clientname,projectname FROM pirmaster WHERE isActive ='1' ".$cond;
		$pirlist = $dbase->executeQuery($pir_select,'multiple');
		for($i=0;$i<count($pirlist);$i++){
			//$pir = $pirlist[$i]['clientname']." - ".$pirlist[$i]['projectname']." ( ".$pirlist[$i]['pirno']." )";
			$pir = $pirlist[$i]['pirno'];
			$html .='<option value="'.$pirlist[$i]['id'].'">'.$pir.'</option>';
		}
		return $html;
	}
	
	function adminpirdropdown_old($cond,$selected,$return=false){
		global $dbase;
		$pir_select = "SELECT id,pirno,clientname,projectname FROM pirmaster WHERE isActive ='1' ".$cond;
		$pirlist = $dbase->executeQuery($pir_select,'multiple');
		for($i=0;$i<count($pirlist);$i++){
			$pir = $pirlist[$i]['clientname']." - ".$pirlist[$i]['projectname']." ( ".$pirlist[$i]['pirno']." )";
			//$pir = $pirlist[$i]['pirno'];
			if($selected==$pirlist[$i]['id']){
				$select = "selected='selected'";
			}
			else{
				$select = "";
			}
			if(count($pirlist)=='1'){
				$select = "selected='selected'";
			}			
			$html .='<option value="'.$pirlist[$i]['id'].'" '.$select.'>'.$pir.'</option>';
		}
		if($return){
			return $html;
		}
		else{
			echo $html;
		}
	}
	
	function adminpirdropdown($cond,$selected,$return=false){
		global $dbase;
		 $pir_select = "SELECT p.pirno as id, (SELECT p2.pirno FROM pirlist p2 WHERE p2.id=p.pirno) as pirno, (SELECT p3.clientname FROM clientlist p3 WHERE p3.id=p. clientname ) as client, (SELECT p4.projectname FROM projectlist p4 WHERE p4.id=p.projectname) as project FROM pirmaster p WHERE p.isActive IN('1','0') ".$cond." GROUP BY p.pirno ORDER BY p.pirno";
		$pirlist = $dbase->executeQuery($pir_select,'multiple');
		for($i=0;$i<count($pirlist);$i++){
			if($selected==$pirlist[$i]['id']){
				$select = "selected='selected'";
			}
			else{
				$select = "";
			}
			if(count($pirlist)=='1'){
				$select = "selected='selected'";
			}				
			$pir = $pirlist[$i]['pirno']." ( ".$pirlist[$i]['project']." )";
			//$pir = $pirlist[$i]['pirno'];
			$html .='<option value="'.$pirlist[$i]['id'].'" '.$select.'>'.$pir.'</option>';
		}	
		if($return){
			return $html;
		}
		else{
			echo $html;
		}		
	}
	
	function adminempdropdown($cond,$selected,$return=false){
		global $dbase;
		$emp_select = "SELECT `ID` , `emp_name` , `emp_username` FROM `employeelist` WHERE `isactive` = '1' AND `isadmin` = '0'".$cond;
		$emplist = $dbase->executeQuery($emp_select,'multiple');
		for($i=0;$i<count($emplist);$i++){
			if($selected==$emplist[$i]['ID']){
				$select = "selected='selected'";
			}
			else{
				$select = "";
			}			
			$empname = $emplist[$i]['emp_name']." ( ".$emplist[$i]['emp_username']." )";
			$html .='<option value="'.$emplist[$i]['ID'].'" '.$select.'>'.$empname.'</option>';
		}	
		if($return){
			return $html;
		}
		else{
			echo $html;
		}		
	}
	
	function adminemplist($cond){
		global $dbase;
		$emp_select = "SELECT `ID` , `emp_name` , `emp_username` FROM `employeelist` WHERE `isactive` = '1' AND `isadmin` = '0'".$cond;
		$emplist = $dbase->executeQuery($emp_select,'multiple');
		return $emplist;
	}	
}
?>

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	$save_status = false;
	if(count($_POST) > 0){
		//if(isset($_POST['rowedit']) && $_POST['monthid'] >0 && $_POST['year'] >0 && $_POST['revenue_target'] >0 && 
		//  $_POST['achieved']>0 && $_POST['month_nps'] >0 ){
		if(isset($_POST['rowedit']) && $_POST['monthid'] >0 && $_POST['year'] >0 && $_POST['revenue_target'] >0 ){
			$data =array();
			if($_POST['editid'] != "" && $_POST['editid'] >0){
				$editid = $_POST['editid'];
			}else{$editid =0;}
				$data['dept_id'] =1;
				$data['month'] = $_POST['monthid'];
				$data['year'] = $_POST['year'];
				$data['revenue_target'] = $_POST['revenue_target'];
				$data['achieved'] = $_POST['achieved'];
				$data['month_nps'] = $_POST['month_nps'];
				$data['quarter_nps'] = $_POST['quarter_nps'];
				$data['is_lock'] = 0;
			if($_POST['achieved'] >0 && $_POST['revenue_target'] >0){
				$data['month_revenue'] = ($_POST['achieved']/$_POST['revenue_target']) *100;
			}else{
				$data['month_revenue'] = 0;
			}
				$save_status = $master->savekpidata($editid,$data);
		}else{
			
			if(count($_POST['kpiid']) > 0){
			for($k=0;$k<count($_POST['kpiid']);$k++){
				$data =array();
				$data['dept_id'] =1;
				$data['month'] = $_POST['monthid'][$k];
				$data['year'] = $_POST['year'][$k];
				$data['revenue_target'] = $_POST['revenue_target'][$k];
				$data['achieved'] = $_POST['achieved'][$k];
				$data['month_nps'] = $_POST['month_nps'][$k];
				$data['quarter_nps'] = $_POST['quarter_nps'][$k];
				$data['is_lock'] = 0;
				if($_POST['revenue_target'][$k] > 0  && $_POST['year'][$k] > 0 &&  $_POST['monthid'][$k] >0){
					if($_POST['achieved'][$k] > 0 && $_POST['revenue_target'][$k] >0){
					$data['month_revenue'] = ($_POST['achieved'][$k]/$_POST['revenue_target'][$k]) *100;
					}else{
						$data['month_revenue'] = 0;
					}
				
					if($_POST['kpiid'][$k] >0 || $_POST['kpiid'][$k] != ""){
					$save_status = $master->savekpidata($_POST['kpiid'][$k],$data);
					}else{
						$save_status = $master->savekpidata(0,$data);
					}
				}
			}
		}
			
		}
		if($save_status){
			echo '{"message":"success"}';
		}else{
			echo '{"message":"failure"}';
		}
	}
}

<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$name = $_POST['name'];
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM projectlist WHERE projectname='".trim($name)."'".$idcond;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: project.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
			$departmentquery = "UPDATE projectlist SET `projectname` = '".$name."' WHERE `id` ='".$editid."'";	
			}
			else{
			$departmentquery = "INSERT INTO projectlist( `projectname`) VALUES ( '".$name."');";			
			}
			 $dbase->executeNonQuery($departmentquery);
			 if($editid !=''){
				header('Location: project.php?m=2');
				exit();	
			 }
			 else{
				header('Location: project.php?m=1');
				exit();				 
			 }			
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$departmentquery = "UPDATE projectlist SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($departmentquery);
				header('Location: project.php?m=4');
				exit();				
		}
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		else{
			$label = "Add";
			$button = "Add";
		}
		$pro_query = "SELECT * FROM projectlist WHERE id='".trim($id)."'";
		$pro_result = $dbase->executeQuery($pro_query,"single");		
	}
	else{
		$label = "Add";
		$button = "Add";
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		table#projecttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#projecttable td, table#projecttable th {
			border: 1px solid black;
		}	
		/*table#projectlisttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#projectlisttable td, table#projectlisttable th {
			border: 1px solid black;
			padding:5px;
		}	*/	
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php //include("adminmenu.php"); ?>
<?php include("userrolemenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<!--<form id="frm_project" action="" method="POST" enctype="multipart/form-data">
<table id="projecttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Inserted Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Projectlist Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='4'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Projectlist Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> Project</b></td></tr>		
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Project Name</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="name" id="name" value="<?php echo $pro_result['projectname']; ?>" class="required alphanumeric" /></td>
</tr>
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
		<?php } ?>
		<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>-->
	<table id="projectlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="50%" align="left"><b>Department</b></td>
			<td width="50%" align="left"><b>Sub Department</b></td>
			<td width="50%" align="left"><b>PIR No</b></td>
			<td width="50%" align="left"><b>Project Name</b></td>
			<td width="50%" align="left"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$pir_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$pir_cond = " AND p2.department_id='".$_SESSION['timesheet']['DEPART']."' AND p2.subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																				   
		$proQuery = "SELECT p1.id, p1.projectname, p2.pirno, (SELECT d.name FROM department d WHERE d.id = p2.department_id) AS deptname, (SELECT s.subname FROM subdepartment s WHERE s.id = p2.subdepartment_id) AS subdeptname FROM `projectlist` p1, pirlist p2 WHERE p2.`isActive` = '1' AND p2.id = p1.`pir_id`".$pir_cond;
		$proResult = $dbase->executeQuery($proQuery,"multiple");
		for($i=0;$i<count($proResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['pirno'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $proResult[$i]['projectname'];?></td>
			<td align="left" style="padding-left:10px"><!--<a href="project.php?id=<?php echo $proResult[$i]['id'];?>">Edit</a> |--> <a href="project.php?del=1&id=<?php echo $proResult[$i]['id'];?>">Close</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th>Department</th>
				<th>Sub Department</th>
				<th>PIR No</th>
				<th>Project Name</th>
                <th>Action</th>
            </tr>
        </tfoot>		
	</table>
	</td>
	</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
	  $("#frm_project").validate();	
    // Setup - add a text input to each footer cell
    $('#projectlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#projectlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
});	
</script>
</html>
<?php } ?>

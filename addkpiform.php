<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	$currentyear =date("Y");
	$min = $currentyear - 20;
	$yearlist ="<option value='0'>-Select-</option>";
	for($i=$currentyear;$i>=$min; $i--){
	$yearlist .= "<option value='".$i."'>".$i."</option>";
	}
	$savestatus = false;
if(isset($_POST['year']) && $_POST['year'] >0 && $_POST['month_revenue'] >0 && $_POST['quarter_revenue'] >0)
{
	$selqry = "SELECT * FROM `kpi` WHERE `month`='".$_POST['month']."'  AND `year`='".$_POST['year']."'";
  $res = $dbase->executeQuery($selqry,"single");
	if($res['cnt'] == 0)
	{
		$savestatus = $dbase->query("INSERT INTO `kpi` (`month`,`year`,`dept_id`,`revenue_target`,`achieved`,`month_revenue`,`quarter_revenue`,`nps`,`created_by`,`created_date`) VALUES('".$_POST['month']."','".$_POST['year']."','1','".$_POST['month_revenue']."','".$_POST['quarter_revenue']."','".$_POST['nps']."','".$_SESSION['timesheet']['ID']."','".date("Y-m-d h:i:s")."')");
		if(!$savestatus){$msg=" Insert error";}else{$msg=" Data has been added successfully";}
	}else if($res['cnt'] == 1){
		$savestatus = $dbase->query("UPDATE `kpi` SET `month_revenue`='".$_POST['month_revenue']."',`quarter_revenue`='".$_POST['quarter_revenue']."',`nps`='".$_POST['nps']."',`updated_by`='".$_SESSION['timesheet']['ID']."' WHERE  id='".$res['id']."'");
		if(!$savestatus){$msg=" Update error";}else{$msg=" Data has been updated successfully";}
	}
      
 }else if(isset($_POST) && count($_POST) > 0){
	$msg ="Please provide ";
	if($_POST['year'] =="" || $_POST['year'] ==0){
		$msg .= " Year,";
	}
	if($_POST['month_revenue'] =="" || $_POST['month_revenue'] ==0){
		$msg .= " Month Revenue,";
	}
	if($_POST['quarter_revenue'] =="" || $_POST['quarter_revenue'] ==0){
		$msg .= " Quarter Revenue,";
	}
	if($_POST['nps'] =="" || $_POST['nps'] ==0){
		$msg .= " NPS ,";
	}
	$msg = substr($msg,0,-1);
}else{$msg ="";}     
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	   <link type="text/css" href="css/bootstrap.css" rel="stylesheet" />
	  <link type="text/css" href="css/chosen.css" rel="stylesheet" />	
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
      <link href="css/custom.css" rel="stylesheet">
	   <script type="text/javascript" src="js/chosen.jquery.js"></script>
	   	<script type="text/javascript" src="js/bootstrap.js"></script>
	  <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script>
	   
	function monthchange(){//alert('change');
		$("#errormsg").css("display","none");
		 $.ajax({
				            url: "checkkpi.php",
	                        dataType: 'json',
				            type: 'POST',
				            data:{month:$("#month").val(),year:$("#year").val()},
				            success: function(data){
				                if(data.msg == "success"){
                                  $("#month_revenue").val(data.mr);
								   $("#quarter_revenue").val(data.qr);
									$("#nps").val(data.nps);
                                }
				            }
			            });
	}
	   </script>
      <script type="text/javascript">
		  function revenuechnage{
		  }
         $(document).ready(function() {
			 
			 $("#errormsg").css("display","none");
			 <?php if(isset($_POST) && count($_POST) >0){?>
			 $("#errormsg").css("display","block");
			<?php }?>
			 $(".chosen-select").chosen();
	$("#save").click(function(){alert('save');
		 $("#addkpiform").validate();
			jQuery.validator.addMethod("pwcheck",function(value, element){
					if (this.optional(element)) {
						return true;
					} else if (!/[A-Z]/.test(value)) {
						return false;
					} else if (!/[a-z]/.test(value)) {
						return false;
					} else if (!/[0-9]/.test(value)) {
						return false;
					} else if (!/[\d=!\-@._*]/.test(value)){
						return false;
					}
					return true;
			}, "Password must be atleast 8 characters with Uppercase lowercase number and special characters");				 
         });
	});
            
      </script> 
      <style type="text/css">
         label.error { 
         margin-left:3px;
         color:red;
         }
		  .lbltd{
			  padding-left:10px;
		  }
	  </style>		 
   </head>
    <body>
    <form id='addkpiform' method='post'>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" >
<tr>
<td align="center" style="vertical-align: middle;height: 40px;" colspan="3" class="headertd" >
<p class="headertda"><?php echo "Add KPI Form";?></p>
</td>
</tr>
<?php if ($msg!=''){?>
<tr>
<td align="center" style="vertical-align: middle;height: 40px;" colspan="3" >
<p style="color: red;" id="errormsg"><?php echo $msg;?></p>
</td>
</tr>
<?php } ?>
<tr>
	<td class="lbltd">Select Month</td>
<td align="left" style="vertical-align: middle;" height="40px" >

			<select id="month" name="month" class="form-control chosen-select" style="width:200px;" onchange="monthchange()">
			    <option value='01'>January</option>
				<option value='02'>February</option>
				<option value='03'>March</option>
				<option value='04'>April</option>
				<option value='05'>May</option>
				<option value='06'>June</option>
				<option value='07'>July</option>
				<option value='08'>August</option>
				<option value='09'>September</option>
				<option value='10'>October</option>
				<option value='11'>November</option>
				<option value='12'>December</option>

			</select>
</td>
<td align="left" style="vertical-align: middle;" height="40px" >
<br />
<label for="month_revenue" style="display: none;" class="error">This field is required.</label>
</td>
</tr>

<tr>
	<td class="lbltd">Select Year</td>
	<td align="left" style="vertical-align: middle;" height="40px" >
	<select id="year" name="year" class="form-control chosen-select" style="width:200px;" onchange="monthchange()">
		<?php echo $yearlist;?>
			</select>
	</td>
	<td align="left" style="vertical-align: middle;" height="40px" >
	<br />
	<label for="month_revenue" style="display: none;" class="error">This field is required.</label>
	</td>
</tr>
<tr>
		<td class="lbltd">Revenue Traget</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
			 <input ty[e="text"  name="revenue_target" id="revenue_target" value="" onchange="revenuechnage()" class="input2 required pwcheck" >
		</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<br />
		<label for="revenue_target" style="display: none;" class="error">This field is required.</label>
		</td>
</tr>
	<tr>
		<td class="lbltd">Achieved</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
			 <input ty[e="text"  name="achieved" id="achieved" value="" onchange="revenuechnage()" class="input2 required pwcheck" >
		</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<br />
		<label for="achieved" style="display: none;" class="error">This field is required.</label>
		</td>
</tr>

<tr>
		<td class="lbltd">Month Revenue ( %)</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
			 <input ty[e="text"  name="month_revenue" id="month_revenue" value="" readonly>
		</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		</td>
</tr>

<tr>
		<td class="lbltd">Quarter Revenue ( %)</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<input ty[e="text"  name="quarter_revenue" id="quarter_revenue" value="" readonly>
		</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		</td>
</tr>
	
<tr>
	<td class="lbltd">Select NPS type</td>
	<td align="left" style="vertical-align: middle;" height="40px" >
	<select id="npstype" name="npstype" class="form-control chosen-select" style="width:200px;" onchange="npschange()">
		<option value="">Select</option>
		<option value="1">Month</option>
		<option value="2">Quarter</option>
	</select>
	</td>
	<td align="left" style="vertical-align: middle;" height="40px" >
	<br />
	<label for="month_revenue" style="display: none;" class="error">This field is required.</label>
	</td>
</tr>
	<tr>
		<td class="lbltd" id="npsmonth"> Month NPS ($k)</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<input ty[e="text"  name="nps" id="nps" value="">
		</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<br />
		<label for="nps" style="display: none;" class="error">This field is required.</label>
		</td>
	</tr>
	<tr>
		<td class="lbltd" id="npsquarter" style="display:none"> Quarter NPS ($k)</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<input ty[e="text"  name="nps_quarter" id="nps_quarter" value="">
		</td>
		<td align="left" style="vertical-align: middle;" height="40px" >
		<br />
		<label for="nps" style="display: none;" class="error">This field is required.</label>
		</td>
	</tr>
<tr>
<td align="center" style="vertical-align: middle;" colspan="2" >
<input type="button" name="save" class="btn btn-primary" id="save" value="Submit"/>
</td>
</tr>
</table>
</form>
</body>
</html>
<?php } ?>

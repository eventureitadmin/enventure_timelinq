<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$department_id = $_POST['department_id'];
		$subdepartment_id = $_POST['subdepartment_id'];
		$name = $_POST['name'];
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM activity WHERE name='".trim($name)."'".$idcond;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: activity.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
			$departmentquery = "UPDATE activity SET `name` = '".$name."',`department_id`='".$department_id."',`subdepartment_id`='".$subdepartment_id."' WHERE `id` ='".$editid."'";	
			}
			else{
			$departmentquery = "INSERT INTO activity(`name`,`department_id`,`subdepartment_id`) VALUES ( '".$name."','".$department_id."','".$subdepartment_id."');";			
			}
			 $dbase->executeNonQuery($departmentquery);
			 if($editid !=''){
				header('Location: activity.php?m=2');
				exit();	
			 }
			 else{
				header('Location: activity.php?m=1');
				exit();				 
			 }			
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$departmentquery = "UPDATE activity SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($departmentquery);
				header('Location: activity.php?m=4');
				exit();				
		}		
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		else{
			$label = "Add";
			$button = "Add";
		}
		$subdept_query = "SELECT * FROM activity WHERE id='".trim($id)."'";
		$subdept_result = $dbase->executeQuery($subdept_query,"single");		
	}
	else{
		$label = "Add";
		$button = "Add";
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	  <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>	   
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	  <style>
		table#subdepartmenttable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#subdepartmenttable td, table#subdepartmenttable th {
			border: 1px solid black;
		}	
	
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php include("adminmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_subdepartment" action="" method="POST" enctype="multipart/form-data">
<table id="subdepartmenttable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Inserted Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Activity Already Exists"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='4'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Activity Deleted Successfully"; ?></b></td></tr>
<?php }	?>	
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> Activity</b></td></tr>		
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="department_id" name="department_id" class="required" onchange="getsubdepartment();">
	<option value="">-Select-</option>
	<?php
		$depart_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$depart_cond = " AND id = '".$_SESSION['timesheet']['DEPART']."'";
		}																								   
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'".$depart_cond;
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
			if($deptResult[$i]['id']==$subdept_result['department_id']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="subdepartment_id" name="subdepartment_id" class="required">
	<option value="">-Select-</option>
	</select>
	</td>
</tr>	
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Activity Name</b></td>
	<td height="40px" align="left" style="padding-left:10px"><input type="text" style="width:170px" name="name" id="name" value="<?php echo $subdept_result['name']; ?>" class="required alphanumeric" /></td>
</tr>
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
			<input type="hidden" name="subdept" id="subdept" value="<?php echo $subdept_result['subdepartment_id']; ?>" />
		<?php }
			else{
		?>
		<input type="hidden" name="subdept" id="subdept" value="" />
		<?php } ?>
		<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
<td align="center" width="80%" valign="top">
	<table id="subdepartmentlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="30%" align="left"><b>Activity Name</b></td>
			<td width="30%" align="left"><b>Department Name</b></td>
			<td width="30%" align="left"><b>Sub Department Name</b></td>
			<td width="30%" align="left"><b>Action</b></td>
		</tr>
		</thead>
	<?php
		$act_cond = "";
		if($_SESSION['timesheet']['ISPROJECTADMIN']=='1'){
			$act_cond = " AND department_id='".$_SESSION['timesheet']['DEPART']."' AND subdepartment_id IN (".$_SESSION['timesheet']['SUBDEPART_CSV'].")";
		}																								   
		$activityQuery = "SELECT a.id,a.name,(SELECT d.name FROM department d WHERE d.id=a.department_id) as deptname,(SELECT s.subname FROM subdepartment s WHERE s.id=a.subdepartment_id) as subdeptname FROM activity a WHERE a.isActive='1'".$act_cond;
		$activityResult = $dbase->executeQuery($activityQuery,"multiple");
		for($i=0;$i<count($activityResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $activityResult[$i]['name'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $activityResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $activityResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><a href="activity.php?id=<?php echo $activityResult[$i]['id'];?>">Edit</a> | <a href="activity.php?del=1&id=<?php echo $activityResult[$i]['id'];?>">Delete</a></td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
                <th>Activity Name</th>
                <th>Department Name</th>
                <th>Sub Department Name</th>
                <th>Action</th>
            </tr>
        </tfoot>		
	</table>
</td>
</tr>	
	</table>	
<?php } ?>
</body>
<script type="text/javascript">
$(document).ready(function(){
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s\/]+$/i.test( value );
}, 'Please enter only alphabets and numbers.');	
	  $("#frm_subdepartment").validate();	
<?php if($id!=''){ ?>
getsubdepartment();
<?php } ?>	
    // Setup - add a text input to each footer cell
    $('#subdepartmentlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#subdepartmentlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
	autoselectoption("#department_id");
});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = $("#subdept").val();
	$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
	});
}
function autoselectoption(id){
	var length = ($(id+' > option').length - 1);
	if(length=='1'){
		$(id+" option").each(function () {
			if($(this).text() != "-Select-"){
				$(this).attr("selected", "selected").change();
			}
		});
	}
}
</script>
</html>
<?php } ?>

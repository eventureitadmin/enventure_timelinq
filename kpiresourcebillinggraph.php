<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	
	function getresourcebillingRBUquery($date,$cond,$group,$subdeptcond,$dtcond,$reworkcond,$internalpircond){
		if(strtotime($date)<strtotime(date('2021-07-01'))){
			$availhrs = NON_DEDICATED_PRESENT_HOURS_BEFORE;
		}
		else{
			$availhrs = NON_DEDICATED_PRESENT_HOURS_AFTER;
		}	
		$query = '';
		$query ="SELECT n.* FROM (SELECT 
			m.empid,
			m.project_id,
			SEC_TO_TIME(m.actualseconds) as actualhours,
			SEC_TO_TIME(m.calculatedseconds) as billablehours,
			SEC_TO_TIME(m.minimumseconds) as presenthours,
			m.static_availhrs,
			SEC_TO_TIME(m.minimumseconds-m.ndavailablesec) AS ndavailablehrs,
			m.subdepartment_id
			FROM 
				(
					SELECT 

						t2.subdepartment_id,t2.clientname,
						t1.`project_id`,
						(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
						SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
						SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
						IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND 	t2.log_date='".$date."'),'09:00:00') AS minimumseconds1,
(CASE WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) <= TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('04:15:00') WHEN TIME_TO_SEC((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."')) > TIME_TO_SEC('06:00:00') && (SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."') != '00:00:00' THEN TIME_TO_SEC('".$availhrs."') ELSE TIME_TO_SEC('".$availhrs."') END) AS minimumseconds,'".$availhrs."' AS static_availhrs,
(SELECT IFNULL(SUM(TIME_TO_SEC(t3.`actualhours`)),0) AS ndavailableseconds FROM `timeentry` t3 WHERE t3.`entrydate` = '".$date."' AND t3.`employee_id` = t1.employee_id AND t3.`isActive`='1' AND t3.`is_dt`='1' AND t3.`is_rework`='0' AND t3.`is_internalpir`='0') AS ndavailablesec					
		FROM 
			`timeentry` t1, 
			 pirmaster t2
		WHERE 
			1=1
			".$reworkcond."
			".$internalpircond."
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m )n
WHERE 1=1".$cond;
	
		return $query;
	}
	
	$cond111 = '';
	$group = " GROUP BY t1.`employee_id`";
	$resourcebillingsubdeptary = array();
	$resourcebillingfpc = array();
	$resourcebillingdtu = array();
	$subdeptquery = "SELECT id,subname FROM `subdepartment` WHERE `isActive`=1 AND `show_kpirpt`=1 AND department_id='".$deptid."' ORDER BY `subname` ASC";
	$subdeptlist = $dbase->executeQuery($subdeptquery,'multiple');
	for($e=0;$e<count($subdeptlist);$e++){
		$overallinterpirdata =array();
		$ndoveralldata =array();
		$ddoveralldata =array();
		$overallinterpirdata =array();
		$subdeptid = $subdeptlist[$e]['id'];
		$subdept = $subdeptlist[$e]['subname'];
		$subdeptlist[$e]['subname'] = $subdept;
		$cond = '';	
		$procond='';
		$group = " GROUP BY t1.`employee_id`";
		if($deptid != ''){
			 $subdeptcond = " AND t2.department_id = '".$deptid."' AND t2.subdepartment_id ='".$subdeptlist[$e]['id']."'";
		}

		$datelist = $dbase->getDateLists($fromdate,$todate);
		if(count($datelist)>0){
				for($j=0;$j<count($datelist);$j++){
				$query1 = '';
				$dtcond1 = "AND t1.is_dt='0'";
				$rwcond1 = " AND t1.is_rework='0'";
				$inpircond1 = " AND t1.is_internalpir='0'";
				unset($report1);				
				$query1 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond1,$rwcond1,$inpircond1);
				$result1 = $dbase->executeQuery($query1,'multiple');
				if(count($result1) > 0){
					$ndoveralldata[] = $result1;
				}
				
				$query12 = '';
				$dtcond12 = "";
				$rwcond12 = " AND t1.is_rework='1'";
				$inpircond12 = " AND t1.is_internalpir='0'";
				unset($report12);				
				$query12 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond12,$rwcond12,$inpircond12);
				$result12 = $dbase->executeQuery($query12,'multiple');
				if(count($result12) > 0){
					$overallreworkdata[] = $result12;
				}
				$query13 = '';
				$dtcond13 = "";
				$rwcond13 = " AND t1.is_rework='0'";
				$inpircond13 = " AND t1.is_internalpir='1'";
				unset($report13);				
				$query13 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond13,$rwcond13,$inpircond13);
				$result13 = $dbase->executeQuery($query13,'multiple');
				if(count($result13) > 0){
					$overallinterpirdata[] = $result13;
				}				
				$query11 = '';
				$dtcond11 = "AND t1.is_dt='1'";
				$rwcond11 =" AND t1.is_rework='0'";
				$intpircond11 = " AND t1.is_internalpir='0'";
				unset($report11);
				$query11 = getresourcebillingRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond11,$rwcond11,$intpircond11);
				$report11 = $dbase->executeQuery($query11,'multiple');
				//echo $datelist[$j]." - ".$subdeptlist[$e]['subname']." - ".count($report11);
				//echo "<br/>";
				if(count($report11) > 0){
					$ddoveralldata[] = $report11;
				}										
				}
				
				$ndoverallactualhourssarr = array();
				$ndoverallbillablehoursarr = array();
				$ndoverallavailablehoursarr = array();
				$ndcnt1=0;
				for($i=0;$i<count($ndoveralldata);$i++){
					for($j=0;$j<count($ndoveralldata[$i]);$j++){
						$ndpresentdays = 0;
						$ndoverallactualhourssarr[] = $ndoveralldata[$i][$j]['actualhours'];
						$ndoverallbillablehoursarr[] = $ndoveralldata[$i][$j]['billablehours'];
						$ndoverallavailablehoursarr[] = $ndoveralldata[$i][$j]['ndavailablehrs'];
						$ndcnt1++;
					}
					
				}
				$ddoverallactualhourssarr = array();
				$ddoverallbillablehoursarr = array();
				$tmpddbillablehoursarr = array();
				$ddcnt1=0;
				for($i=0;$i<count($ddoveralldata);$i++){
					for($j=0;$j<count($ddoveralldata[$i]);$j++){
						$ddoverallactualhourssarr[] = $ddoveralldata[$i][$j]['actualhours'];
						$ddoverallbillablehoursarr[] = $ddoveralldata[$i][$j]['billablehours'];
						$tmpddbillablehoursarr[] = "09:00";
						$ddcnt1++;
					}
				}	
				
				$reworkcnt1=0;
				for($i=0;$i<count($overallreworkdata);$i++){
					for($j=0;$j<count($overallreworkdata[$i]);$j++){
						if($overallreworkdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins1 = $dbase->getminutes($overallreworkdata[$i][$j]['presenthours']);
							$minimummins1 = $dbase->getminutes('06:00');
							if($presentmins1 <= $minimummins1){
								$reworkcnt1 += 0.5;
							}
							elseif($presentmins1 > $minimummins1){
								$reworkcnt1 += 1;
							}
							else{
								$reworkcnt1 += 1;
							}
						}
						else{
							$reworkcnt1 += 1;
						}
					}
				}
			
				$interpircnt1=0;
				for($i=0;$i<count($overallinterpirdata);$i++){
					for($j=0;$j<count($overallinterpirdata[$i]);$j++){
						if($overallinterpirdata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins2 = $dbase->getminutes($overallinterpirdata[$i][$j]['presenthours']);
							$minimummins2 = $dbase->getminutes('06:00');
							if($presentmins2 <= $minimummins2){
								$interpircnt1 += 0.5;
							}
							elseif($presentmins2 > $minimummins2){
								$interpircnt1 += 1;
							}
							else{
								$interpircnt1 += 1;
							}
						}
						else{
							$interpircnt1 += 1;
						}
					}
				}	
		//+$reworkcnt1+$interpircnt1

				$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
				$dedicatemonth=date('m',strtotime($todate));
				$dedicatemyear=date('Y',strtotime($todate));				
				$dedsubcond ='';
				if($subdeptid != ''){
					$dedsubcond = " AND pm.subdepartment_id ='".$subdeptid."'";
				}		
				//$query="SELECT IFNULL(SUM(`no_of_resource`),0) as dedicatedresource FROM `pirmaster` WHERE `isActive`='1' AND no_of_resource > 0 AND  department_id='".$deptid."' ".$subcond;
				$query123="SELECT IFNULL(SUM(rm.`no_of_resource`),0) AS dedicatedresource FROM `pirmaster` pm,`resourcemonth` rm WHERE rm.`isactive`='1' AND pm.isActive AND rm.`pirmaster_id`=pm.id AND rm.`monthval`='".$dedicatemonth."' AND rm.`yearval`='".$dedicatemyear."' AND pm.department_id='".$deptid."'".$dedsubcond;
				$result123 = $dbase->executeQuery($query123,'single');
				$totempcnt = $result123['dedicatedresource'];				
				$dedicatedbilledhrsarr = (($workingdayscnt*$totempcnt*DEDICATED_WORKING_HOURS)*60);		
		//echo $subdept." - ".$dbase->addTime($ndoverallavailablehoursarr,true)." - ".$totempcnt." - ".$ndcnt1;
		//echo "<br/>";				
$resourcebillingsubdeptary[$subdept]['utilization'] = ((($dbase->addTime($ndoverallbillablehoursarr,true) + ($dbase->getminutes("09:00")*$totempcnt))/(($dbase->addTime($ndoverallavailablehoursarr,true))+($dbase->getminutes("09:00")*$totempcnt)))*100);

		}	
// FPC
			if($subdept != 'EAS'){
				$resourcebillingfpc[$subdept]['utilization'] =  (($dbase->addTime($ndoverallbillablehoursarr,true))/(($dbase->addTime($ndoverallavailablehoursarr,true)))*100);
			}
			else{
				$resourcebillingfpc[$subdept]['utilization'] = '0';
			}		

//DTU 
				$resourcebillingdtu[$subdept]['utilization'] = ((($dbase->addTime($ddoverallbillablehoursarr,true))/($dedicatedbilledhrsarr))*100);
	
	}
		
	}
?>

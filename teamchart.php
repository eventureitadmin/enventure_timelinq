<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_SESSION['timesheet']['ISADMIN']=='1'){
		$res = $dbase->executeQuery("SELECT ID FROM `employeelist` WHERE `isactive`=1 AND `role_id`=2 AND direct_rpt_id  >= 0","single");
		$loginid = $res['ID'];
	}else{
		$loginid = $_SESSION['timesheet']['ID'];
	}
		
		$output = $master->createtree($loginid);
	
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	 <link rel="stylesheet" href="css/orgchart/jquery.orgchart.css">
	  <link rel="stylesheet" href="css/orgchart/style.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
	<?php 
	   if($master->level > 0){
		   $color =array("#008B8B","#FF6600","#FF66FF","#669900","#CC3300");
			$levelcnt = $master->level;
			 echo  '<style>';
			for($i=0;$i<$levelcnt;$i++){
				$j=$i+1;
				echo	'.orgchart .org-level'.$j.' .title { background-color: '.$color[$i].'; }';
				echo	'.orgchart .org-level'.$j.' .content { border-color: '.$color[$i].'; }';

		   }
		  echo  '</style>';
		}
	   ?>
	
	</head>
	<body>
		<?php include("menu.php");?>
	  <div id="chart-container" style="overflow:none"></div>
	  <script type="text/javascript" src="js/jquery1.8.js"></script>
	  <script type="text/javascript" src="js/orgchart/jquery.orgchart.js"></script>

	 <!---  <script type="text/javascript" src="js/bootstrap.js"></script>
	   <link type="text/css" href="css/bootstrap.css" rel="stylesheet" />-->
	  <script type="text/javascript">
			$(function() {
				var datascource =<?php echo $output;?>;

				$('#chart-container').orgchart({
				  'data' :  datascource,
					'collapsed': false,
				  'nodeContent': 'title'
				});

			  });
	  </script>
	  
	  </body>
	</html>
<?php }?>

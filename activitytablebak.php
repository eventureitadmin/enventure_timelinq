<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_GET){
		$id=$_GET['id'];
		$deptid=$_GET['deptid'];
		if($deptid != ''){
		$act_select = "SELECT id,name FROM `activity` WHERE isActive='1' AND `department_id`='".$deptid."'";
		$actlist = $dbase->executeQuery($act_select,'multiple');
			$activity_html = '';
			for($i=0;$i<count($actlist);$i++){
				$activity_html .= '<option value="'.$actlist[$i]['id'].'">'.$actlist[$i]['name'].'</option>';
			}
		}
		$html = '';
$html .= '<table id="activitydetailstable" border="0" cellpadding="5" cellspacing="0" align="center"  width="100%">
<thead>
	<tr>
		
		<td width="16%"><b>Activity</b></td>
		<td width="16%"><b>Calculate Budgeted Hours</b></td>
		<td width="16%"><b>Budgeted Time(Seconds)</b></td>
		<td width="16%"><b>Add / Del</b></td>
	</tr>
	</thead>
	<tbody>';
	if($id!= '' && $id!= 'undefined'){
		$act_pir_select = "SELECT id,activity_id,budgettime,calculatehours FROM `pir_activity` WHERE isActive='1' AND `pirmaster_id`='".$id."' ORDER BY id ASC";
		$act_pir_list = $dbase->executeQuery($act_pir_select,'multiple');
		if(count($act_pir_list)>0){
			for($j=0;$j<count($act_pir_list);$j++){
				$activity_html_edit = '';
				for($q=0;$q<count($actlist);$q++){
					if($actlist[$q]['id']==$act_pir_list[$j]['activity_id']){
						$select = 'selected';
					}
					else{
						$select = '';
					}
					$activity_html_edit .= '<option value="'.$actlist[$q]['id'].'" '.$select.'>'.$actlist[$q]['name'].'</option>';
				}				
				$k=$j+1;
				$html .= '<tr id="row'.$k.'">
						<td width="16%">
						<select id="activity_id'.$k.'" name="activity_id'.$k.'" class="required activity">
						<option value="">-Select-</option>'.$activity_html_edit.'
						</select>		
						</td>
						<td width="16%">
							<select id="calculatehours'.$k.'" name="calculatehours'.$k.'" class="required calculatehours" onchange="changebudgettextbox(this.id,this.value);">
							<option value="">-Select-</option>';
							$calculatehours_select_y = '';
							$calculatehours_select_n = '';
							if($act_pir_list[$j]['calculatehours']=='y'){
								$calculatehours_select_y = 'selected="selected"';
							}
							else if($act_pir_list[$j]['calculatehours']=='n'){
								$calculatehours_select_n = 'selected="selected"';
							}							
							$html .= '<option value="y" '.$calculatehours_select_y.'>YES</option>
							<option value="n" '.$calculatehours_select_n.'>NO</option>
							</select>
						</td>							
						<td width="16%">
						<input type="text" id="budgettime'.$k.'" name="budgettime'.$k.'" class="required number budgettime" value="'.$act_pir_list[$j]['budgettime'].'" />
						</td>
						<td width="16%">
						<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record" />
						<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record" rowid="'.$k.'" />
						</td>
					</tr>';							
			}	
			$cnt=$k;			
		}
		else{
			$cnt='1';
		$html .= '<tr id="row1">
			<td width="16%">
			<select id="activity_id1" name="activity_id1" class="required activity">
			<option value="">-Select-</option>'.$activity_html.'
			</select>		
			</td>
			<td width="16%">
				<select id="calculatehours1" name="calculatehours1" class="required calculatehours" onchange="changebudgettextbox(this.id,this.value);">
				<option value="">-Select-</option>
				<option value="y">YES</option>
				<option value="n">NO</option>
				</select>
			</td>			
			<td width="16%">
			<input type="text" id="budgettime1" name="budgettime1" class="required number budgettime" />
			</td>
			<td width="16%">
			<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record" />
			<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record" rowid="1" />
			</td>
		</tr>';				
		}

	}
	else{
		$cnt='1';
	$html .= '<tr id="row1">
		<td width="16%">
		<select id="activity_id1" name="activity_id1" class="required activity">
		<option value="">-Select-</option>'.$activity_html.'
		</select>		
		</td>
			<td width="16%">
				<select id="calculatehours1" name="calculatehours1" class="required calculatehours" onchange="changebudgettextbox(this.id,this.value);">
				<option value="">-Select-</option>
				<option value="y">YES</option>
				<option value="n">NO</option>
				</select>
			</td>			
		<td width="16%">
		<input type="text" id="budgettime1" name="budgettime1" class="required number budgettime" />
		</td>
		<td width="16%">
		<img src="images/add.png" height="16" width="16" alt="AddRow" class="add-record" />
		<img src="images/remove.png" height="16" width="16" alt="RemoveRow" class="delete-record" rowid="1" />
		</td>
	</tr>';			
	}

	$html .= '</tbody>
</table>';
		echo $html."~".$cnt;
	}
}
?>
<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
 function getQuarterByMonth($monthNumber) {
  return floor(($monthNumber - 1) / 3) + 1;
}
	function getquarter($frommonth,$tomonth){
		$is_quarter_eq = false;
		$fquarter =getQuarterByMonth((int)$frommonth);
		$tquarter = getQuarterByMonth((int)$tomonth);
		if($fquarter == $tquarter){
			$is_quarter_eq = true;
		}
		if($fquarter != $tquarter){
			$is_quarter_eq =false;
		}
		return $is_quarter_eq;
	}
	function getcolor($selvalue){
		if($selvalue <= 79){
			$color = "red";
		}else if($selvalue >79 && $selvalue <= 89){
			$color = "orange";
		}else if($selvalue >89 && $selvalue <101){
			$color = "green";
		}else if($selvalue >=101 && $selvalue <=110){
			$color = "orange";
		}
		else if($selvalue >110){
			$color = "red";
		}
		return $color;
	}
	function getRBUquery($date,$cond,$group,$subdeptcond,$dtcond){
		$query = '';
		$query ="SELECT n.* FROM (SELECT 
		m.clientname, m.subdepartment_id,(SELECT cl.`clientname` FROM `clientlist` cl WHERE cl.`id` =m.clientname) as cname,(SELECT sd.`subname` FROM `subdepartment`sd WHERE sd.`id`= m.subdepartment_id)as sname,
	m.empid,
	m.project_id,
	SEC_TO_TIME(m.actualseconds) as actualhours,
	SEC_TO_TIME(m.calculatedseconds) as billablehours,
	m.minimumseconds as presenthours,m.ctype
FROM 
	(
		SELECT 
		(SELECT p.`contracttype` FROM `pirlist` p WHERE p.`id`=t2.`pirno` AND  p.`contracttype` =2)as ctype,
			t2.subdepartment_id,t2.clientname,
			t1.`project_id`,t3.`subname`,
			(SELECT u4.emp_username FROM employeelist u4 WHERE u4.id=t1.employee_id) as empid,  
			SUM(TIME_TO_SEC( t1.`actualhours`)) AS actualseconds,
			SUM(TIME_TO_SEC( t1.`calculatedhours`)) AS calculatedseconds,
			IFNULL((SELECT t2.totalhours FROM time_log t2 WHERE t2.deletestatus = '0' AND t2.emp_id=t1.employee_id AND t2.log_date='".$date."'),'08:30:00')	AS minimumseconds
		FROM 
			`timeentry` t1, 
			 pirmaster t2 ,subdepartment t3
		WHERE 
			1=1 AND t3.show_kpirpt=1 AND t3.id=t2.subdepartment_id
		AND 
			t2.id=t1.pirmaster_id 
		".$subdeptcond." 
		AND 
			t1.isActive='1' 
		".$dtcond."			
		AND 
			t1.`entrydate`='".$date."' 
		".$group."
	) m )n
WHERE 1=1".$cond;
		return $query;
	}	

	if(count($_POST) > 0 && $_POST['from_date'] != "" && $_POST['to_date'] != ""){
		
		$from_date = explode(" ",$_POST['from_date']); 
		$fromdate = $from_date[1]."-".date("m",strtotime($from_date[0]))."-01";
		$to_date = explode(" ",$_POST['to_date']);
		$todate = $to_date[1]."-".date("m",strtotime($to_date[0]))."-31";
		$deptid = 1;
		$frommonth = date("m",strtotime($from_date[0])); $tomonth = date("m",strtotime($to_date[0]));
		$fromyear = $from_date[1]; $toyear = $to_date[1];
		$is_period =0;
		$months ="";
		 $diff = abs(strtotime($fromdate) - strtotime($todate));
		 $years = floor($diff / (365*60*60*24)); 
		 $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24));
	
			$kpireportname_array =array();
		    $is_quarter_eq = getquarter($frommonth,$tomonth);
			if($fromyear == $toyear  && $months <=3 && $is_quarter_eq){
				$limit = 3;
			for($i=1;$i<$limit;$i++){
				$m = (int)$frommonth+$i;
				$months1 .=$m.",";
			}
													
			 $result = $dbase->executeQuery("SELECT `month_revenue`,month_nps,`quarter_nps` FROM `kpi` WHERE dept_id =1 AND `month` ='".(int)$frommonth."' AND year='".$fromyear."'","single");
$month_revenue =$result['month_revenue'] ;
$quarter_nps =$result['quarter_nps'] ;

			 $months1 = substr($months1,0,-1);
			 $quarter_revenue = $month_revenue+$dbase->executeQuery("SELECT SUM(`month_revenue`) as qr FROM `kpi` WHERE dept_id =1 AND `month` IN(".$months1.") AND year='".$fromyear."'","single")['qr'];
	
			 $quarter_nps = $quarter_nps+$dbase->executeQuery("SELECT SUM(`quarter_nps`) as qn FROM `kpi` WHERE dept_id =1 AND `month` IN(".$months1.") AND year='".$fromyear."'","single")['qn'];
			
		
			if($quarter_revenue >0){
				$quarter_revenue = round(($quarter_revenue/3),0);
			}else{
				$quarter_revenue =0;
			}
		   if($quarter_nps >0){
				$quarter_nps = round(($quarter_nps/3),0);
			}else{
				$quarter_nps =0;
			}
			}
		if($fromyear == $toyear && $frommonth == $tomonth){
			$limit = 3;
			for($i=1;$i<$limit;$i++){
				$m = (int)$frommonth+$i;
				$months1 .=$m.",";
			}
											
											
			 $result = $dbase->executeQuery("SELECT `month_revenue`,month_nps,`quarter_nps` FROM `kpi` WHERE dept_id =1 AND `month` ='".(int)$frommonth."' AND year='".$fromyear."'","single");
$month_revenue =$result['month_revenue'] ;

			 $months1 = substr($months1,0,-1);
			 $quarter_revenue = $month_revenue+$dbase->executeQuery("SELECT SUM(`month_revenue`) as qr FROM `kpi` WHERE dept_id =1 AND `month` IN(".$months1.") AND year='".$fromyear."'","single")['qr'];
	
			$is_period =0;$reportname = "KPI ".$_POST['from_date'];
		
			if($quarter_revenue >0){
				$quarter_revenue = ($quarter_revenue/3);
			}else{
				$quarter_revenue =0;
			}
		array_push($kpireportname_array," Revenue : ".$from_date[0]." ".$from_date[1]);
		$reportdata1['utilization'] = $month_revenue;
			$reportdata[] = $reportdata1;
			array_push($kpireportname_array,"Quarter Revenue ".$from_date[1]);
		$reportdata2['utilization'] = $quarter_revenue;
			$reportdata[] = $reportdata2;
			$month_nps = $result['month_nps'];
			$quarter_nps = $result['quarter_nps'];
			
		}else{
	$query = "SELECT SUM(`month_revenue`) as qr FROM (SELECT * FROM `kpi` WHERE `year` BETWEEN '".$fromyear."' AND '".$toyear."')m WHERE (m.`month` >='".(int) $frommonth."' AND m.`year`='".$fromyear."')"; 
			if($fromyear == $toyear){
				$query .= "  AND ";
			}else{
				$query .= " OR ";
			}
			$query .= "(m.`month` <='".(int) $tomonth."' AND m.`year`='".$toyear."')";
			
		$qrres = $dbase->executeQuery($query,"single");
			$period_revenue =$qrres['qr']/$months;
			$is_period =1;$reportname = "KPI ".$_POST['from_date']." to ".$_POST['to_date'];
		array_push($kpireportname_array," Revenue :".$_POST['from_date']." to ".$_POST['to_date']);
		$reportdata4['utilization'] = $period_revenue;
			$reportdata[] = $reportdata4;
			$month_nps = "NA";
			if($months <= 3 && $is_quarter_eq){
				$quarter_nps = $quarter_nps;
			}else{
			$quarter_nps = "NA";
			}
		}
		
		$cond = '';
		$procond='';
		$group = " GROUP BY t1.`employee_id`";
		if($deptid != ''){
			$subdeptcond = " AND t2.department_id = '".$deptid."'";
		}
		$workingdayscnt = $dbase->getWorkingDays($fromdate,$todate,$holidays);
		$dedicatedempcond = '';
		if($deptid != ''){
			$dedicatedempcond .= " AND department_id = '".$deptid."'";
		}
		if($subdepartment_ids != ''){
			$dedicatedempcond .= " AND subdepartment_id IN (".$subdepartment_ids.")";
		}
		$query="SELECT IFNULL(SUM(`noofresource`),0) as dedicatedresource FROM `pirlist` WHERE `isActive`='1' AND `contracttype`='2' ".$dedicatedempcond;
		$result = $dbase->executeQuery($query,'single');
		$totempcnt = $result['dedicatedresource'];			
		$datelist = $dbase->getDateLists($fromdate,$todate);
		$rptarr = array("Billed Hours / Available Hours","Actual Hours / Budgeted Hours","Actual hours used / Billed Hours","Revenue Target/Achieved");
		if(count($datelist)>0){
			$overalldata = array();
			$dedicateddata = array();
			$nondedicateddata = array();
			
			for($j=0;$j<count($datelist);$j++){
				$query1 = '';
				$dtcond1 = "";
				unset($report1);
				
				$cond = "  ORDER BY n.sname ASC ";
				$query1 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond1);
				$result1 = $dbase->executeQuery($query1,'multiple');
				if(count($result1) > 0){
					$overalldata[] = $result1;
				}				
				$query2 = '';$cond='';$cond = " AND n.ctype = '2' ORDER BY n.cname ASC ";
				$dtcond2 = " AND t1.is_dt='1'";
				unset($report2);	
				$query2 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond2);
				$result2 = $dbase->executeQuery($query2,'multiple');
				$dedicateddata[] = $result2;
									
				$query3 = '';
				$dtcond3 = " AND t1.is_dt='0'";$cond='';
				unset($report3);				
				$query3 = getRBUquery($datelist[$j],$cond,$group,$subdeptcond,$dtcond3);
				$result3 = $dbase->executeQuery($query3,'multiple');	
				if(count($result3) > 0){
					$nondedicateddata[] = $result3;
				}					
			}	
				$overallactualhourssarr = array();
				$overallbillablehoursarr = array();
				$cnt1=0;
				for($i=0;$i<count($overalldata);$i++){
					for($j=0;$j<count($overalldata[$i]);$j++){
						$subdepartment_name = $overalldata[$i][$j]['sname'];
						//$overallactualhourssarr[$subdepartment_name][] = $overalldata[$i][$j]['actualhours'];
						//$overallbillablehoursarr[$subdepartment_name][] = $overalldata[$i][$j]['billablehours'];
						
						$overallactualhourssarr[$subdepartment_name][] = $overalldata[$i][$j]['actualhours'];
						$overallbillablehoursarr[$subdepartment_name][] = $overalldata[$i][$j]['billablehours'];
						if($overalldata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins = $dbase->getminutes($overalldata[$i][$j]['presenthours']);
							$minimummins = $dbase->getminutes('06:00');
							if($presentmins <= $minimummins){
								$cnt1 += 0.5;
							}
							elseif($presentmins > $minimummins){
								$cnt1 += 1;
							}
							else{
								$cnt1 += 1;
							}
						}
						else{
							$cnt1 += 1;
						}
					}
				}	

			
				$nondedicatedactualhourssarr = array();
				$nondedicatedbillablehoursarr = array();
				$cnt2=0;
				for($i=0;$i<count($nondedicateddata);$i++){
					for($j=0;$j<count($nondedicateddata[$i]);$j++){
						$subdepartment_name = $nondedicateddata[$i][$j]['sname'];
						$nondedicatedactualhourssarr[$subdepartment_name][] = $nondedicateddata[$i][$j]['actualhours'];
						$nondedicatedbillablehoursarr[$subdepartment_name][] = $nondedicateddata[$i][$j]['billablehours'];
						if($nondedicateddata[$i][$j]['presenthours'] != '00:00:00'){
							$presentmins = $dbase->getminutes($nondedicateddata[$i][$j]['presenthours']);
							$minimummins = $dbase->getminutes('06:00');
							if($presentmins <= $minimummins){
								$cnt2 += 0.5;
							}
							elseif($presentmins > $minimummins){
								$cnt2 += 1;
							}
							else{
								$cnt2 += 1;
							}
						}
						else{
							$cnt2 += 1;
						}
					}
				}
			
			array_push($kpireportname_array,"Month NPS");
			if($months <=3 && $is_quarter_eq){
			array_push($kpireportname_array,"Quarter NPS");
			}
			if($is_period == 0){
				if($month_nps >= 1){
					$reportdata3['utilization'] = $month_nps;
				}else{$reportdata3['utilization'] =  0;}
				if($months <=3){
					if($quarter_nps >= 1){
						$reportdataqn['utilization'] = $quarter_nps;
					}else{$reportdataqn['utilization'] =  0;}
				}
				
			}else{
				if($is_quarter_eq){
					if($months <=3){
						$reportdata3['utilization'] = "NA";
						$reportdataqn['utilization'] = $quarter_nps;
					}else{
						$reportdata3['utilization'] = "NA";
						$reportdataqn['utilization'] = "NA";
					}
				}else{$reportdata3['utilization'] = "NA";}
			}
			$reportdata[] = $reportdata3;
			if($is_quarter_eq){
			$reportdata[] = $reportdataqn;
			}
			
			$dedicatedhrsarr = array();
			$clientnames = "";$dedicatedutilization ="";$color="#ffcc00";$dedicatedcolor = "";
			foreach($dedicatedactualhourssarr as $key2=>$val2){
				$dedicatedhrsarr[$key2]=(($dbase->addTime($val2,true)) / ($dbase->addTime($dedicatedbilledhrsarr,true)))*100;
			$clientnames.='"'.$key2.'",';
			$dedicatedcolor.='"'.$color.'",';
			$dedicatedutilization .=round((($dbase->addTime($val2,true)) / ($dbase->addTime($dedicatedbilledhrsarr,true)))*100).",";
															
			}
			$clientnames = substr($clientnames,0,-1);
			$dedicatedcolor = substr($dedicatedcolor,0,-1);
			$dedicatedutilization = substr($dedicatedutilization,0,-1);
			$dedicatedhrsarr['labels'] =$clientnames;
			$dedicatedhrsarr['dedicatedutil'] =$dedicatedutilization;
			$dedicatedhrsarr['colors'] =$dedicatedutilization;
			array_push($kpireportname_array,"Dedicated Team Utilization");
			$reportdata6['utilization'] = $dedicatedhrsarr;
			$reportdata[] = $reportdata6;
			
			$billablehrsarr = array();
			foreach($overallbillablehoursarr as $key1=>$val1){
				$billablehrsarr[$key1]=($dbase->addTime($val1,true) / ($cnt1 * $dbase->getminutes("08:30")))*100;
			}
			array_push($kpireportname_array,"Resource Billing Utilization");
			$reportdata5['utilization'] = $billablehrsarr;
			$reportdata[] = $reportdata5;
			$nondedicatedactualhrsarr = array();$projecteff ="";$nondedicatedval ="";$ndcolor ="";
			foreach($nondedicatedactualhourssarr as $key3=>$val3){
				$projecteff .='"'.$key3.'",';
				$nondedicatedval = round(($dbase->addTime($nondedicatedactualhourssarr[$key3],true) / ($dbase->addTime($nondedicatedbillablehoursarr[$key3],true)))*100);
				$nondedicatedarr .=$nondedicatedval.",";
				$ndcolor .='"'.getcolor($nondedicatedval).'",';
			}
		
			$ndcolor = substr($ndcolor,0,-1);
			$projecteff = substr($projecteff,0,-1);
			$nondedicatedarr = substr($nondedicatedarr,0,-1);
			array_push($kpireportname_array,"Non Dedicated Project Efficiency");
			$reportdata7['utilization'] = $nondedicatedactualhrsarr;
			$reportdata[] = $reportdata7;
						
		}		
		//print_r($reportdata);exit;
		$html = '';//print_r($kpireportname_array);
		if(count($reportdata) > 0){
			$o=1;
			$html .= '<div class="panel panel-default" id="kpitrptpanel"><div class="panel-heading text-center"><b>'.$reportname.'</b><button id="export" style="float:right;padding-top:-40px;display:none" class="btn btn-primary">Export</button></div>
			<div class="panel-body">';
			for($y=0;$y<count($reportdata);$y++){		
				$yy = $y+1;		
				if(round($reportdata[$y]['utilization'],0) > 100){
					$max = round($reportdata[$y]['utilization'],0);
				}
				else{
					$max = 100;
				}
				if(($is_period == 1 && $y==0) || ($is_period == 0 && $y<=1)){
					if($is_period == 0){$revenueformula = $rptname[3];}else{$revenueformula = 0;}
				$html .= '<div class="row">';
					if($is_period == 1){
				$html .= '<div class="col-sm-5">';
					}else{
				$html .= '<div class="col-sm-3">';}
					$html .='<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>
							<div class="panel-body">';
					if($is_period == 1){$html.="<div class='row'><div class='col-sm-2'></div><div class='col-sm-8'>";}
							$html .='	<canvas id="utilization_guage'.$yy.'" height="160"></canvas><div class="text-center" style="font-size:14px;">'.$kpireportname_array[$y].' - <b>'.round($reportdata[$y]['utilization'],0).' %</b></div>
									<script>
										var opts'.$yy.' = {
											lines: 12,
											angle: 0.05, 
											lineWidth: 0.19, 
											radiusScale: 1,
											pointer: {
												length: 0.5, 
												strokeWidth: 0.035, 
												color: "#000000"
											},
											  limitMax: false,
											  limitMin: false, 											
											generateGradient: true,
											highDpiSupport: true,
											staticLabels: {
												font: "10px sans-serif",
												labels: ['.round($reportdata[$y]['utilization'],0).'],
												color: "#000000", 
												fractionDigits: 0 
											},
										staticZones: [
											   {strokeStyle: "'.$utilization_legend_color['r'].'", min: 0, max: 80},
											   {strokeStyle: "'.$utilization_legend_color['a'].'", min: 80, max: 95},
											   {strokeStyle: "'.$utilization_legend_color['g'].'", min: 95, max: '.$max.'},
											],											
										};
										var target'.$yy.' = document.getElementById("utilization_guage'.$yy.'");
										var gauge'.$yy.' = new Gauge(target'.$yy.').setOptions(opts'.$yy.');
										gauge'.$yy.'.maxValue = 100;
										gauge'.$yy.'.setMinValue(0);
										gauge'.$yy.'.animationSpeed = 32;
										gauge'.$yy.'.set('.round($reportdata[$y]['utilization'],0).');
									</script>';
					if($is_period == 1){$html.="</div></div>";}
							$html.='</div>
					</div>
				</div>';
				}
				if(($is_period ==1 && $y == 1 ) || ($is_period ==0 && $y == 2 && $months<=3 && $is_quarter_eq) || ($is_period ==1 && $y == 2  && $months<=3 && $is_quarter_eq) || ($is_period ==0 && $y == 3  && $months<=3 && $is_quarter_eq) ){
					
						if($reportdata[$y]['utilization'] == "NA")
						{$npscolor="#d0d3d4";}else{
							$npscolor = getcolor($reportdata[$y]['utilization']);
						}
						if($reportdata[$y]['utilization'] <10){
							$y1=70;$z1=105;
						}else if($reportdata[$y]['utilization'] >10 && $reportdata[$y]['utilization'] <100){
							$y1=60;$z1=105;
						}else{
							$y1=60;$z1=105;
						}
					if($reportdata[$y]['utilization'] == "NA"){
						$y1=55;$z1=105;
					}
					if($reportdata[$y]['utilization'] == 0){
						$y1=70;$z1=105;
					}
					$html .='<div class="col-sm-2">
					<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>
							<div class="panel-body"><div class="row"><div class="col-sm-2">
								<canvas id="utilization_guage'.$yy.'" width=200 height="180"  ></canvas>
		
									<script>
										var canvas1 = document.getElementById("utilization_guage'.$yy.'");
										var context = canvas1.getContext("2d");
										context.beginPath();
										context.fillStyle = "'.$npscolor.'";
										context.strokeStyle = "black";
										context.font = "30px Times New Roman, Times, serif";
										context.lineWidth = 10;
										context.arc(80, 100, 80, 0, 2 * Math.PI, false);
										context.fill();
										context.beginPath();
										context.fillStyle = "black";
										context.fillText("'.$reportdata[$y]['utilization'].'", '.$y1.', '.$z1.');
										context.fill();
									</script></div></div>
							</div>
					</div>
				</div>';
					if($is_period == 1 && $y == 2){$html .= "</div>";}if(!$is_quarter_eq){ $html .= "</div>";}
					   if($is_period == 0 && $y == 3){ $html .= "</div></div>";}
				}
				
				if(($is_period ==1 && $y == 3 && $is_quarter_eq)|| ($is_period ==1 && $y == 2 && !$is_quarter_eq) || ($is_period ==0 && $y == 4)){
								include_once("kpidedicatedgraph.php");	
				$html .='<div class="row"><div class="col-sm-3"></div><div class="col-sm-6">
						<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>
						<div class="panel-body">
								<canvas id="utilization_guage'.$yy.'" width="500"></canvas>
								<div class="text-center" style="font-size:14px;">'.$rptarr[1].'</div>
								<script>
						var ctx = document.getElementById("utilization_guage'.$yy.'").getContext("2d");
						var myChart = new Chart(ctx, {
							type: "horizontalBar",
							width:600,
							data: {
							labels:['.$dedicatedlabel.'],
								datasets: [{
									data: ['.$dedicatedvalue.'],
									backgroundColor: ['.$dedicatedcolor.'],
									borderWidth: 1
								}]
							},
							options: {
								legend: {
								display: false
							},
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero: true
										}
									}]
								},
								tooltips: {
												enabled: true,
												  callbacks: {
														   label: function (tooltipItems, data) {
																return tooltipItems.xLabel + " %";
														   }	
														   }
											},
											hover: {
												animationDuration: 1
											},
											animation: {
											duration: 1,
											onComplete: function () {
												var chartInstance = this.chart,
													ctx = chartInstance.ctx;
													ctx.textAlign = "center";
													ctx.fillStyle = "rgba(0, 0, 0, 1)";
													ctx.textBaseline = "top";
													this.data.datasets.forEach(function (dataset, i) {
														var meta = chartInstance.controller.getDatasetMeta(i);
														meta.data.forEach(function (bar, index) {
															var data = dataset.data[index];
															ctx.fillText(data+"%", bar._model.x+14, bar._model.y-5);
														});
													});
												}
											}
							}
						});
						</script>
							</div>
							</div></div></div>';
				} 
				if(($is_period ==1 && $y == 4 && $is_quarter_eq) || ($is_period ==1 && $y == 3 && !$is_quarter_eq) || ($is_period ==0 && $y == 5)){
					include_once('kpiresourcebillinggraph.php');
					$html.='<div class="row" ><div class="col-sm-4"><div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>';
				$html .='<div class="panel-body">
							<div class="row"><div class="col-sm-12">';$colorarr=array("red","yellow","green");$cindex=0;
				
					foreach($resourcebillingsubdeptary as $keyr=>$valr){
								$selval =round($valr['utilization'],0);
						$color = getcolor($selval);
						if($selval > 0){
							$class= "over50";
						}else{
							$class= "";
						}
					
					$html.='<div class="col-sm-4"><div class="panel-heading text-center" style="padding-left:50px">'.$keyr.'</div>
										<div class="progress-circle '.$class.' p'.$selval.'">
										   <span>'.$selval.'%</span>
										   <div class="left-half-clipper">
											  <div class="first50-bar" style="border: 0.45em solid '.$color.'"></div>
											  <div class="value-bar" style="border: 0.45em solid '.$color.'"></div>
										   </div>
										</div>
									</div>';}
								$html .='</div>
							</div>
							<p style="margin-left:100px">'.$rptarr[0].'</p>
					</div></div></div>';
				
				}
				if(($is_period ==1 && $y == 5) || ($is_period ==1 && $y == 4 && !$is_quarter_eq) || ($is_period ==0 && $y == 6)){
					$html.='<div class="col-sm-5">
					<div class="panel panel-default">
						<div class="panel-heading text-center"><b>'.$kpireportname_array[$y].'</b></div>
							<div class="panel-body">
								<canvas id="utilization_guage'.$yy.'" ></canvas>
								<div class="text-center" style="font-size:14px;">'.$rptarr[2].'</div>
							<script>
							
						var ctx = document.getElementById("utilization_guage'.$yy.'").getContext("2d");
						var myChart = new Chart(ctx, {
							type: "horizontalBar",
		
							data: {
							
							labels: ['.$projecteff.'],
								datasets: [{
								backgroundColor: ['.$ndcolor.'],
								data: ['.$nondedicatedarr.'],
								borderWidth: 1, boxWidth: 10,
								}]
							},
							options: {
								legend: {
								display: false
							},
								scales: {
								 xAxes: [{
										ticks: {
											min: 0 // Edit the value according to what you need
										}
									}],
									yAxes: [{
										barPercentage: 0.3,
										display: true,
                            
									}]
								},
								tooltips: {
												enabled: true,
												callbacks: {
														   label: function (tooltipItems, data) {
																return tooltipItems.xLabel + " %";
														   }	
														   }
											},
											hover: {
												animationDuration: 1
											},
											animation: {
											duration: 1,
											onComplete: function () {
												var chartInstance = this.chart,
													ctx = chartInstance.ctx;
													ctx.textAlign = "center";
													ctx.fillStyle = "rgba(0, 0, 0, 1)";
													ctx.textBaseline = "bottom";
													this.data.datasets.forEach(function (dataset, i) {
														var meta = chartInstance.controller.getDatasetMeta(i);
														meta.data.forEach(function (bar, index) {
															var data = dataset.data[index];
																ctx.fillText(data+"%", bar._model.x+15, bar._model.y+8);
														});
													});
												}
											}
							}
						});
						</script>
							</div>
					</div>
				</div>';

					$html .= '</div>';
		
				}
		
			 }//for	
		
			
		}//rpt
		echo $html;exit;
	}
}
?>
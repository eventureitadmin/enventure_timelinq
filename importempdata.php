<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/xlsxwriter.class.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	
	$proj_header = array("employee_id","employee_name","designation","shift","dept","subdept","reporting_manager","reportid","role","projectadmin");
	if($_FILES){
		
		if(isset($_FILES['proj_file'])){		
		  $filepath = $docroot."/upload/";
		  $errors= array();
		  $file_name = $_FILES['proj_file']['name'];
		  $file_size =$_FILES['proj_file']['size'];
		  $file_tmp =$_FILES['proj_file']['tmp_name'];
		  $file_type=$_FILES['proj_file']['type'];
		  $file_ext=strtolower(end(explode('.',$_FILES['proj_file']['name'])));
		
		  $extensions= array("xls","xlsx");
		  
		  if(in_array($file_ext,$extensions)=== false){
			 $errors[]="extension not allowed, please choose a xls or xlsx file.";
		  }
		  
		  if(empty($errors)==true){		  
			 move_uploaded_file($file_tmp,$filepath.$file_name);
			try {
				$inputFileName = $filepath.$file_name;
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME). '": '.$e->getMessage());
			}
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$timestamps=time();
			$highestRow = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn(); 
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
			$i=0;
			$j=0;
			$fileheader = array();
			for ($col = 0; $col < $highestColumnIndex; ++$col) {
				$fileheader[] = strtolower(str_replace(' ', '_',trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue())));
			}
			
			if($proj_header !== $fileheader){
				header("Location: importempdata.php?msg=4");
				exit();				
		  	}	
			for ($row = 2; $row <= $highestRow; ++$row) {
				if($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()!=''){
					
				$col0=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
				$col1=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue())));
				$col2=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue())));
				$col3=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue())));
				$col4=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue())));
				$col5=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue())));
				$col6=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue())));
				$col7=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue())));
				$col8=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue())));
				$col9=mysql_escape_string(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue())));
					//
					$deptres = $dbase->executeQuery("SELECT COUNT(id) as cnt,id FROM department WHERE name='".trim($col4)."'","single");
					if($deptres['cnt'] == 1){
						$dept_id = $deptres['id'];
						$deptq = "UPDATE department SET `name` = '".trim($col4)."' WHERE `id` ='".$deptres['id']."'";
						//$deptres = $dbase->executeNonQuery("UPDATE department SET `name` = '".trim($col4)."' WHERE `id` ='".$deptres['id']."'");
					}else if($deptres['cnt'] == 0){
						$deptq = "INSERT INTO department(`name`) VALUES ( '".$col4."')";
						
						$deptres = $dbase->executeNonQuery("INSERT INTO department(`name`) VALUES ( '".$col4."')");
						$dept_id = mysql_insert_id();
					}
					//
				   $subdeptres = $dbase->executeQuery("SELECT COUNT(id) as cnt,id FROM subdepartment WHERE subname='".trim($col5)."'","single");
					if($subdeptres['cnt'] == 1){
						$subdept_id = $subdeptres['id'];
						$subdeptq = "UPDATE subdepartment SET `subname` = '".trim($col5)."' WHERE `id` ='".$subdeptres['id']."'";
						//$deptres = $dbase->executeNonQuery("UPDATE subdepartment SET `subname` = '".trim($col5)."' WHERE `id` ='".$subdeptres['id']."'");
					}else if($subdeptres['cnt'] == 0){
						$subdeptq = "INSERT INTO subdepartment(`subname`) VALUES ('".$col5."')";
						$deptres = $dbase->executeNonQuery($subdeptq);
						//$deptres = $dbase->executeNonQuery("INSERT INTO subdepartment(`subname`) VALUES ( '".$col5."')");
						//$subdept_id = mysql_insert_id();
					}
					$subdepartment_ids = $dbase->executeQuery("SELECT subdepartment_ids FROM employeelist WHERE id='".trim($col0)."'","single")[0];
					if($col3 == "GS"){
						$predate_logout = 0; 	
					}else{
						$predate_logout = 1; 	
					}
					 $rptqry = "SELECT id FROM employeelist WHERE emp_username =".trim($col7);
					//echo $rptqry."<br/>";
					 $report_id = $dbase->executeQuery($rptqry,"single")[0];
					if($dept_id >0 && $col0 > 0){
						if($subdept_id > 0){
							$subdepartarr = explode(',',$subdepartment_ids);
							if(!in_array($subdept_id,$subdepartarr) ){
								if(count($subdepartment_ids) > 0){
								$subdepartment_ids .= ",".$subdept_id;
								}else{
									$subdepartment_ids .= $subdept_id;
								}
							}
						}
					
						$emp_res = $dbase->executeQuery("SELECT COUNT(id) as cnt FROM employeelist WHERE emp_username='".trim($col0)."'","single")[0];
						$insert_flag = false;
						if($emp_res['cnt'] == 0){
							$date = date("Y-m-d H:i:s");
							$insert_flag = true;
							$userquery = "INSERT INTO employeelist(`emp_name`, `emp_username`, `emp_password`, 
										`department_ids`,`subdepartment_ids`,`isprojectadmin`,`predate_logout`,
										`designation`,`role_id`,`created_by`,`created_date`,`direct_rpt_id`) VALUES ( 
										'".$col1."','".$col0."','Env@1234','".$dept_id."','".
										$subdepartment_ids."','".$col9."','".$predate_logout.
										"','".$col2."','".$col8."','".$_SESSION['timesheet']['ID']."','".$date."','".$report_id."')";	
								$deptres = $dbase->executeNonQuery($userquery);
						}else{
							  $userquery = "UPDATE employeelist SET `designation` = '".trim($col2).
									"',`department_ids` = '".$dept_id."',`subdepartment_ids` = '".$subdepartment_ids.
									"',`direct_rpt_id` = '".$report_id."',`isprojectadmin` = '".$col9.
								 "',`role_id` = '".$col8."',`predate_logout` = '".$predate_logout."' WHERE `emp_username` ='".$col0."'";
								$userres = $dbase->executeNonQuery($userquery);
						}
					}
					
					//echo $deptq."<br/>";
					//echo $subdeptq."<br/>";
					//echo $userquery."<br/>";
					
				$i++;
						//insert Abbr
				  	 if(!$userres && $insert_flag){
							echo $i."-";
							echo $userquery;
							echo "<br/>";
							$j++;
							exit;
						}
				}
			}	
			header("Location: importempdata.php?msg=1&i=".$i."&j=".$j);
			exit();
		  }
		  else{
			 //print_r($errors);
				header("Location: importempdata.php?msg=2");
				exit();			 
		  }
	   }
	   else{
		header("Location: importempdata.php?msg=3");
		exit();	   
	   }   
}		
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
		<link href="css/jquery_confirm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/jquery_validate.js"></script>
		<script type="text/javascript" src="js/jquery_confirm.js"></script>
	   <?php include_once("includebootstrap.php"); ?>
    <body>
		<?php include("menu.php");?>
		
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">Import Employee Data</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
							
								<?php if($_GET['msg']==1){ echo 'Header file imported successfully'; } ?>
								<?php if($_GET['msg']==2){ echo 'File upload error'; } ?>
								<?php if($_GET['msg']==3){ echo 'No file is added for import. Please upload a file'; } ?>
								<?php if($_GET['msg']==4){ echo 'Import file header mismatching. Please change it and upload again.'; } ?>
								<?php if($_GET['msg']==5){ echo 'Please select Batch.'; } ?>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form role="form" id="frm_header" name="frm_header" action="" method="post" enctype="multipart/form-data">
                                         								
                                             <div class="form-group">
                                                <label>Upload Header File</label>
                                                <input type="file" id="proj_file" name="proj_file" class="required">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                    </div>
									<div class="col-lg-8">
								<div class="row">
									<p class="form-control-static" style="margin-left: 20px;">Note : Please check the header of the import file in the following format. If it is mismatch it wont allow to insert / update.</p>
								<?php
								$html = '';
								$html = '<style>
							table#projheadertable {
								border-collapse: collapse;
								margin-left: 20px;
							}

							table#projheadertable td, table#projheadertable th {
								border: 1px solid black;
								 padding: 10px; 
								 font-size:12px;
							}
							</style>';
								$html .= '<table id="projheadertable"><tr>';
								foreach($proj_header as $key =>$value){
									$html .='<td><b>'.ucfirst(str_replace('_', ' ',trim($value))).'</b></td>';
								}
								$html .= '</tr></table>';
								echo $html;
								?>				
								</div>										
									</div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
		<script type="text/javascript">
		$().ready(function() {
			$("#frm_header").validate();
			$("#proj_file").change(function () {
				var fileExtension = ["xls", "xlsx"];
				if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$.alert({
					title: 'Alert',
					content: 'Only formats are allowed : '+fileExtension.join(', '),
					//icon: 'fa fa-rocket',
					animation: 'scale',
					closeAnimation: 'scale',
					buttons: {
						okay: {
							text: 'Ok',
							btnClass: 'btn-blue'
						}
					}					
				});							
					$("#proj_file").val("");
				}
			});			
		});
		</script>			
	</body>

	<?php
		 include_once("footer.php");
	}	
?>

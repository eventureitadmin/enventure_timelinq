<?php
include_once("config.php");
if($_SESSION['timesheet']['ID']==''){
	header("Location:login.php");
}
else{
	if($_POST){
		$editid = $_POST['editid'];
		$department_id  = $_POST['department_id'];
		$subdepartment_id  = $_POST['subdepartment_id'];
		$pirno  = $_POST['pirno'];
		$clientname  = $_POST['clientname'];
		$projectname = $_POST['projectname'];
		$totrow = $_POST['totrow'];
		//check it in database
		if($editid !=''){
			$idcond = " AND ID != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM pirmaster WHERE isActive='1' AND pirno='".trim($pirno)."' AND projectname='".trim($projectname)."'".$idcond;
		$cnt_result = $dbase->executeQuery($check_query,"single");
		if($cnt_result['cnt'] > 0){
			header('Location: pirmaster.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
			$userquery = "UPDATE pirmaster SET `department_id` = '".$department_id."',`subdepartment_id` = '".$subdepartment_id."',`pirno` = '".$pirno."',`clientname` = '".$clientname."',`projectname` = '".$projectname."' WHERE `id` ='".$editid."'";
			$del_act_pir = "DELETE FROM pir_activity WHERE pirmaster_id='".$editid."'";
			$dbase->executeNonQuery($del_act_pir);
			$dbase->executeNonQuery($userquery);
			$pirid = $editid;
			}
			else{
			$userquery = "INSERT INTO pirmaster(`department_id`, `subdepartment_id`, `pirno`, `clientname`, `projectname`) VALUES ( '".$department_id."','".$subdepartment_id."','".$pirno."','".$clientname."','".$projectname."');";
			$dbase->executeNonQuery($userquery);
			$pirid = mysql_insert_id();
			}
			for($i=1;$i<=$totrow;$i++){
				if( $_POST['activity_id'.$i] !='' && $_POST['calculatehours'.$i] !=''){
					$pir_activity_insert = "INSERT INTO `timesheet`.`pir_activity` ( `pirmaster_id`, `activity_id`, `budgettime`,`calculatehours`) VALUES ( '".$pirid."', '".$_POST['activity_id'.$i]."', '".$_POST['budgettime'.$i]."', '".$_POST['calculatehours'.$i]."');";
					$dbase->executeNonQuery($pir_activity_insert);
				}
			}
			
			 
			 if($editid !=''){
				header('Location: pirmaster.php?m=2');
				exit();	
			 }
			 else{
				header('Location: pirmaster.php?m=1');
				exit();				 
			 }			
		}
	}
	if($_GET){
		$id = $_GET['id'];
		$del = $_GET['del'];
		if($del!=''){
			$userquery = "UPDATE pirmaster SET `isActive` = '0' WHERE `id` ='".$id."'";
			$dbase->executeNonQuery($userquery);
				header('Location: pirmaster.php?m=4');
				exit();				
		}		
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
		else{
			$label = "Add";
			$button = "Add";
		}
		$pir_query = "SELECT * FROM pirmaster WHERE id='".trim($id)."'";
		$pir_result = $dbase->executeQuery($pir_query,"single");
	}
	else{
		$label = "Add";
		$button = "Add";
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
   <head>
      <title>Enventure</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="js/jquery.js"></script>
      <script src="js/jqueryui.js"></script>
      <link type="text/css" href="css/jqueryui.css" rel="stylesheet" />
	   <link href="css/datatable.css" rel="stylesheet" type="text/css" />
      <link href="css/custom.css" rel="stylesheet">
	  <link rel="stylesheet" href="css/chosen.css">
      <script type="text/javascript" src="js/jquery_validate.js"></script>
	   <script type="text/javascript" src="js/datatable.js"></script>
	   <script src="js/easyconfirm.js"></script>
	   <script src="js/date.js"></script>
	    <script src="js/chosen.jquery.js" type="text/javascript"></script>
	  <style>
		table#usertable {
			empty-cells: show;
			border-collapse: collapse;
			font-size:12px;
			letter-spacing:0.5px;
			margin-left:2px;
		}
		table#usertable td, table#usertable th {
			border: 1px solid black;
		}	
	
	  </style>
   </head>
   <body>
<?php include("menu.php");?>
<?php if($_SESSION['timesheet']['ISADMIN']=='1'  || $_SESSION['timesheet']['ISPROJECTADMIN']=='1'){ ?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
<tr><td align="center" valign="top" width="15%" style="border-right:1px dotted" height="200px">
<?php include("adminmenu.php"); ?>
</td>
<td align="center" width="80%" valign="top">
<form id="frm_department" action="" method="POST" enctype="multipart/form-data">
<table id="usertable" border="0" cellpadding="0" cellspacing="0" align="left" width="100%">								 
<?php if($_GET['m']=='1'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Inserted Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='2'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "Updated Successfully"; ?></b></td></tr>
<?php }	?>	
<?php if($_GET['m']=='3'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "PIR Project Already Exists"; ?></b></td></tr>
<?php }	?>		
<?php if($_GET['m']=='4'){ ?>
<tr><td width="100%" height="40px" align="center" colspan="2" style="color:red;"><b><?php  echo "PIR Project Deleted Successfully"; ?></b></td></tr>
<?php }	?>
<tr><td width="100%" height="40px" align="left" colspan="2" style="padding-left:10px"><b><?php echo $label; ?> PIR</b></td></tr>		
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="department_id" name="department_id" class="required" onchange="getsubdepartment();loadtable();">
	<option value="">-Select-</option>
	<?php
		$deptQuery = "SELECT id,name FROM department WHERE isActive='1'";
		$deptResult = $dbase->executeQuery($deptQuery,"multiple");
		for($i=0;$i<count($deptResult);$i++){
			if($deptResult[$i]['id']==$pir_result['department_id']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$deptResult[$i]['id'].'" '.$select.'>'.$deptResult[$i]['name'].'</option>';
		}
	?>
	</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Select Sub Department</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="subdepartment_id" name="subdepartment_id"  class="required">
	<option value="">-Select-</option>
	</select>
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>PIR Number</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="pirno" name="pirno" class="required">
	<option value="">-Select-</option>
	<?php
		$pirQuery = "SELECT id,pirno FROM pirlist WHERE isActive='1'";
		$pirResult = $dbase->executeQuery($pirQuery,"multiple");
		for($i=0;$i<count($pirResult);$i++){
			if($pirResult[$i]['id']==$pir_result['pirno']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$pirResult[$i]['id'].'" '.$select.'>'.$pirResult[$i]['pirno'].'</option>';
		}
	?>
	</select>	
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Client Name</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="clientname" name="clientname" class="required">
	<option value="">-Select-</option>
	<?php
		$clientQuery = "SELECT id,clientname FROM clientlist WHERE isActive='1'";
		$clientResult = $dbase->executeQuery($clientQuery,"multiple");
		for($i=0;$i<count($clientResult);$i++){
			if($clientResult[$i]['id']==$pir_result['clientname']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$clientResult[$i]['id'].'" '.$select.'>'.$clientResult[$i]['clientname'].'</option>';
		}
	?>
	</select>	
	</td>
</tr>
<tr>
	<td height="40px" align="left" style="padding-left:10px" width="20%"><b>Project Name</b></td>
	<td height="40px" align="left" style="padding-left:10px">
	<select id="projectname" name="projectname" class="required">
	<option value="">-Select-</option>
	<?php
		$projectQuery = "SELECT id,projectname FROM projectlist WHERE isActive='1'";
		$projectResult = $dbase->executeQuery($projectQuery,"multiple");
		for($i=0;$i<count($projectResult);$i++){
			if($projectResult[$i]['id']==$pir_result['projectname']){
				$select = "selected";
			}
			else{
				$select = "";
			}
			echo '<option value="'.$projectResult[$i]['id'].'" '.$select.'>'.$projectResult[$i]['projectname'].'</option>';
		}
	?>
	</select>	
	</td>
</tr>
<tr>
	<td id="activitytr" height="40px" align="left"  colspan="2" width="20%">
	
	</td>
</tr>
<tr>
	<td colspan="2" height="40px" style="padding-left:10px" width="20%" align="center">
		<?php if($id!=''){ ?>
			<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
			<input type="hidden" name="subdept" id="subdept" value="<?php echo $pir_result['subdepartment_id']; ?>" />
		<?php } else{?>
		<input type="hidden" name="subdept" id="subdept" value="" />
		<?php } ?>
		<input type="hidden" id="totrow" name="totrow" />
		<input type="submit" id="submit" value="<?php echo $button; ?>" />
	</td>
</tr>
	</table>
	</form>
	</td>
	</tr>
<tr><td align="center" valign="top" width="15%">
</td>
<td align="center" width="80%" valign="top">
	<table id="userlisttable" class="display" style="width:100%">
		<thead>
		<tr>
			<td width="18%" align="center"><b>PIR No</b></td>
			<td width="10%" align="center"><b>Department</b></td>
			<td width="10%" align="center"><b>Sub Department</b></td>
			<td width="26%" align="center"><b>Client</b></td>
			<td width="26%" align="center"><b>Project</b></td>
			<td width="18%" align="center"><b>Action</b></td>
		</tr>
	</thead>
	<?php
		$pirQuery = "SELECT p.`id`, (SELECT p2.pirno FROM pirlist p2 WHERE p2.id=p.pirno) as pirno,(SELECT c.clientname FROM clientlist c WHERE c.id=p.clientname) as clientname,(SELECT p1.projectname FROM projectlist p1 WHERE p1.id=p.projectname) as projectname,(SELECT d.name FROM department d WHERE d.id=p.department_id) as deptname,(SELECT sd.subname FROM subdepartment sd WHERE sd.id=p.subdepartment_id) as subdeptname  FROM pirmaster p WHERE p.isActive='1'";
		$pirResult = $dbase->executeQuery($pirQuery,"multiple");
		for($i=0;$i<count($pirResult);$i++){
	?>
		<tr>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['pirno'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['deptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['subdeptname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['clientname'];?></td>
			<td align="left" style="padding-left:10px"><?php echo $pirResult[$i]['projectname'];?></td>
			<td align="left" style="padding-left:10px"><a href="pirmaster.php?id=<?php echo $pirResult[$i]['id'];?>">Edit</a> | <a href="pirmaster.php?del=1&id=<?php echo $pirResult[$i]['id'];?>">Delete</a> </td>
		</tr>
<?php }?>
 		<tfoot>
            <tr>
				<th>PIR No</th>
                <th>Department</th>
                <th>Sub Department</th>
                <th>Client</th>
                <th>Project</th>				
                <th>Action</th>
            </tr>
        </tfoot>		
	</table>	
</td>
</tr>	
	</table>
<?php } ?>
</body>
<script type="text/javascript">
var maximum_admin_entry = <?php echo MAXIMUM_ADMIN_ENTRY; ?>;
$(document).ready(function(){
jQuery.validator.addMethod("alphanumericpir", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\/]+$/i.test( value );
}, 'Please enter only alphabets numbers and /.');	
jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional( element ) || /^[a-zA-Z0-9\s]+$/i.test( value );
}, 'Please enter only alphabets numbers and space.');	
	  $("#frm_department").validate();	

<?php if($id!=''){ ?>
getsubdepartment();
<?php } ?>
    // Setup - add a text input to each footer cell
    $('#userlisttable tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#userlisttable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );	
	
		$(document).delegate('.add-record', 'click', function(e) {
			if($("#department_id").val()!=''){
				 e.preventDefault();    
				 var content = $('#activitydetailstable tbody').children('tr:first'),
				 size = $('#activitydetailstable >tbody >tr').length + 1,
					element = null,    
					element = content.clone();
					if(size<=<?php echo MAXIMUM_ADMIN_ENTRY;?>){
						element.attr('id', 'row'+size);
						element.find('.activity').attr('id', 'activity_id'+size);
						element.find('.activity').attr('name', 'activity_id'+size);
						element.find('.calculatehours').attr('id', 'calculatehours'+size);
						element.find('.calculatehours').attr('name', 'calculatehours'+size);
						element.find('.calculatehours').empty().append('<option value="">-Select-</option><option value="y">YES</option><option value="n">NO</option>');
						element.find('.budgettime').attr('id', 'budgettime'+size);
						element.find('.budgettime').attr('name', 'budgettime'+size);
						element.find('.budgettime').val('');
						element.find('.delete-record').attr('rowid', size);
						element.appendTo('#activitydetailstable');					
						$("#totrow").val(size);
						<?php if($id!=''){ ?>
						 dropdownchange(size);
						<?php } ?>							
					}
				  else{
					$('<div>Only '+maximum_admin_entry+' can enter</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
								return false;
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
				  }
			}
				else{
					$('<div>Please Select Department</div>').dialog({
						resizable: false,
						open: function(){
						 $(".ui-dialog-title").html("Alert");
						},				
							buttons: {
							"Ok": function() {
								$( "div" ).remove( ".ui-widget-overlay" );	
								$(this).dialog("close");
								return false;
							},
						}
					});	
					$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
				}				  
		   });
	
   
	$(document).delegate('.delete-record', 'click', function(e) {
		e.preventDefault();  
	  var id = $(this).attr('rowid');
	  if(id > 1){
	  var targetDiv = $(this).attr('targetDiv');
		$('<div>Are you sure wants to delete ?</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Yes": function() {
					  $('#row' + id).remove();
					//regnerate index number on table
					$('#activitydetailstable tbody tr').each(function(index) {
					  var rowid = index+1;
						$(this).attr('id', 'row'+rowid);
						$(this).find('.activity').attr('id', 'activity_id'+rowid);
						$(this).find('.activity').attr('name', 'activity_id'+rowid);
						$(this).find('.calculatehours').attr('id', 'calculatehours'+rowid);
						$(this).find('.calculatehours').attr('name', 'calculatehours'+rowid);
						$(this).find('.budgettime').attr('id', 'budgettime'+rowid);
						$(this).find('.budgettime').attr('name', 'budgettime'+rowid);
						$(this).find('.delete-record').attr('rowid', rowid);
						$("#totrow").val(rowid);
					});
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return true;
					

				},
				"No": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				}
			}
		});	
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );		
	  }
	  else{
		$('<div>You cant delete this row</div>').dialog({
			resizable: false,
			open: function(){
			 $(".ui-dialog-title").html("Alert");
			},				
				buttons: {
				"Ok": function() {
					$( "div" ).remove( ".ui-widget-overlay" );	
					$(this).dialog("close");
					return false;
				},
			}
		});
		$( '<div class="ui-widget-overlay"></div>' ).insertBefore( ".ui-dialog" );
	  }
	});
loadtable();
});	
function getsubdepartment(){
	var id=$("#department_id").val();
	var subdept = $("#subdept").val();
	$.get("getsubdepartmentpir.php?id="+id+"&sel="+subdept,function(data){
		$("#subdepartment_id").html(data);
	});
}
function loadtable(){
	var id=$("#editid").val();
	var deptid=$("#department_id").val();
	$.get("activitytable.php?id="+id+"&deptid="+deptid,function(data){
		var dataarr = data.split("~");
		$("#activitytr").html(dataarr[0]);
		$("#totrow").val(dataarr[1]);
	});	
}

function dropdownchange(size){
		$('#activity_id'+size).val('');
		$('#activity_id'+size).removeAttr('selected');
		$('#activity_id'+size+' option[value=""]').attr('selected', true);
}
function changebudgettextbox(id,value){
	var id = id.replace("calculatehours", "");
	if(value!=''){
		if(value=='y'){
			if(!$("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).addClass('required');
				$("#budgettime"+id).attr('readonly', false);
			}
		}
		if(value=='n'){
			if($("#budgettime"+id).hasClass("required")){
				$("#budgettime"+id).removeClass('required');
				$("#budgettime"+id).attr('readonly', true);
				if($("#budgettime"+id).val() > 0 && $("#budgettime"+id).val() != ''){
					$("#budgettime"+id).val('0');
				}
			}
		}
	}
}
</script>
</html>
<?php } ?>
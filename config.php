<?php
ob_start();

session_start();

date_default_timezone_set('Asia/Calcutta');

ini_set("display_errors",1);

ini_set('memory_limit', '-1');

error_reporting(1);

define("FOLDER_NAME","/");

global $docroot; 

$docroot=$_SERVER['DOCUMENT_ROOT'].FOLDER_NAME;//echo $docroot;

define("PAGEURL",$docroot);

define("DEBUG_MODE",false);

define("TEST_MODE",false);

define("SEND_MAIL",true);

$year = date('Y');

define("CURRENT_YEAR",$year);

define("TIME_TYPE","2");  // 1=> system time 2=> server time

define("OFFICE_START_TIME","10:00:00");

define("TOTAL_WORKING_HOURS","9");

define("MAXIMUM_ENTRY","16");

define("HR_ROLE","7");
define("EMPLOYEE_ROLE","6");
define("ADMIN_ROLE","2");

define("MAXIMUM_ADMIN_ENTRY","55");
define("ADMIN_ROLE_IDS","1,2");
define("SUPERADMIN_ROLE_ID","1");
define("ADMIN_ROLE_ID","2");
define("CURRENT_YEAR_FROMDATE_DB",CURRENT_YEAR."-01-01");

$chart_color = array('#5f4b8b', '#1E90FF', '#3ADF00', '#FFC0CB','#FF1493','#0090FF','#DC143C','#0000FF','#1E013C','#00B0CB','#FF0993');

//"4"=>"Dedicated Team Bench Utilization",
$reportname_array = array("1"=>"Overall Billing Utilization","2"=>"Dedicated Team Utilization","3"=>"Non Dedicated Project Efficiency","4"=>"Non Dedicated Team Utilization");

//"4"=>"Billed Hours on Projects / Billed Hours (Dedicated Resource Hours)",
$reportformula_array = array("1"=>"Billed Hours / Available Hours","2"=>"Actual Hours / Billed Hours","3"=>"Actual Hours / Budgeted Hours","4"=>"Billed Hours / Available Hours");

$report_legend = array("Actual Hours : Actual time spent on a project","Billed Hours : Billable time, paid for by client (168 hour per month)","Available Hours : No of resources * days in office * 9");

//utilization legend color
$utilization_legend_color = array('r'=>'#F03E3E','a'=>'#FFDD00','g'=>'#30B32D');
$userdashboard_legend_color = array('r'=>'#F03E3E','a'=>'#FFDD00','g'=>'#30B32D');
$kpi_legend_color = array('r'=>'#F03E3E','o'=>'#FFA500','g'=>'#30B32D','y'=>'#FFFF00');

//utilization legend color
$utilization_legend_label = array('r'=>'<b>0</b> to <b>80</b> ','a'=>'<b>80</b> to <b>95</b> ','g'=>'<b>95</b> and above');
$userdashboard_legend_label = array('r'=>'<b>0</b> to <b>80</b> ','a'=>'<b>81</b> to <b>95</b> ','g'=>'<b>96</b> and above');

$holidays=array("2019-11-01","2019-12-25");

define("DEDICATED_WORKING_HOURS","9");

define("NON_DEDICATED_PRESENT_HOURS_BEFORE","08:30:00");

define("NON_DEDICATED_PRESENT_HOURS_AFTER","09:00:00");

include_once("dbconn.php");

include_once($docroot."/class/data.php");

$dbase = new dbase();

include_once($docroot."/class/user_agent.php");

$ua = new UserAgent();

include_once($docroot."/class/master.php");

$master = new master();
?>
